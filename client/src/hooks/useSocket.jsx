/*
The useSocket.jsx hook is designed to manage real-time communication between a client and a server via WebSockets (using the
socket.io-client library).

It establishes and manages WebSocket communication, handling incoming events and exposing key data and methods related to the
robot controller and alert system for the rest of the application.

Its primary functions include:

- Socket Connection: It connects to a WebSocket server (whose URL is stored in the Redux state) and handles socket events from the server.
- Event Handling: It listens for specific events such as welcome, controller, alert, human_intervention_required, stopped, robotStatus, and
  execution, updating the internal state accordingly or triggering actions such as displaying alerts or status updates.
- Alert System: The hook manages alert dialogs by setting and clearing alert messages and titles when certain server events occur, such
  as human intervention requests.
- Robot Controller Status: It keeps track of the robot controller's status (controllerStatus) and protocol execution status (executionStatus).
- Socket Communication: The hook can emit responses back to the server when needed, such as after handling alerts or confirming/canceling human
  interventions.
- Context Provider: It provides the socket-related state and handlers through the SocketProvider and useSocketContext for components within
  the app to access and interact with WebSocket functionalities.

The SocketProvider and useSocketContext objects in the useSocket.jsx file are tools for managing and sharing the WebSocket connection and
related state across different components in a React application. Here's a breakdown of their roles:

- SocketProvider is responsible for creating and managing the socket connection and making it available through a context (SocketContext).
- useSocketContext is a hook that allows any component within the SocketProvider tree to access the socket state and methods.
*/

import { useEffect, useState, createContext, useContext } from "react";
import { useSelector } from 'react-redux';
import { io } from "socket.io-client";

const SocketContext = createContext();

const useSocket = (addSnack) => {

  const app_url = useSelector((state) => state.globalSlice.url);
  console.log("Current socket URL (from globalSlice):", app_url)

  const [controllerStatus, setControllerStatus] = useState("OFF");

  const [alert, setAlert] = useState({
    title: "",
    open: false,
    message: "",
    yesButton: "Ok",
    noButton: "Cancel",
    socketEmitOnConfirm: "",
    socketEmitOnCancel: "",
    socketEmitData: "",
  });

  const [executionStatus, setExecutionStatus] = useState(null);

  const handleConfirm = (e) => {
    console.log("Processing response from alert dialog.");
    e.preventDefault();
    setAlert({
      ...alert,
      title: "",
      open: false,
      message: "",
      socketEmitOnConfirm: "",
      socketEmitOnCancel: "",
      socketEmitData: "",
    });
    // If there is a response set to emit to the server:
    if (alert.socketEmitData) {
      console.log("Sending alert dialog response data to the server.");
      const socket = io(`${app_url}/frontend`);
      socket.emit(alert.socketEmitOnConfirm, alert.socketEmitData);
    }
  };

  const handleCancel = (e) => {
    e.preventDefault();
    setAlert({
      ...alert,
      title: "",
      open: false,
      message: "",
      socketEmitOnConfirm: "",
      socketEmitOnCancel: "",
      socketEmitData: "",
    });
    // If there is a response set to emit to the server:
    if (alert.socketEmitData) {
      const socket = io(`${app_url}/frontend`);
      socket.emit(alert.socketEmitOnCancel, alert.socketEmitData);
    }
  };

  const handleWelcome = (data) => {
    console.log(data);
  };

  const handleController = (data) => {
    console.log(data);
  };

  const handleAlert = (data) => {
    console.log(data)
    if (typeof data === "string") {
      // Handle string-type messages.
      setAlert({
        ...alert,
        title: "Alert from controller",
        open: true,
        message: data,
        noButton: null,
        yesButton: "Ok",
      });
    } else if (
      typeof data === "object" &&
      data.text &&
      typeof data.text === "string"
    ) {
      // Handle object-type messages.
      setAlert({
        ...alert,
        title: data.title || "Alert from controller",
        open: true,
        message: data.text,
        noButton: null,
        yesButton: "Ok",
      });
    }
  };

  const handleHumanIntervention = (data) => {
    console.log(data);

    let message_content = "-- Message data not found. --";

    if (typeof data === "string") {
      message_content = data;
    } else if (
      typeof data === "object" &&
      data.text &&
      typeof data.text === "string"
    ) {
      message_content = data.text;
    }

    // NOTE: These values are used in NavBar.jsx, and then
    // used to update the component defined in Alert.jsx,
    setAlert({
      ...alert,
      title: "Action required",
      open: true,
      yesButton: "Continue",
      noButton: "Cancel",
      message: message_content,
      socketEmitOnConfirm: "human_intervention_continue",
      socketEmitOnCancel: "human_intervention_cancelled",
      socketEmitData: "Human intervention responded.",
    });
  };

  const handleStopped = (data) => {
    console.log(data);
    if (addSnack) addSnack("The process was successfully stopped.", "warning");
  };

  const handleRobotStatus = (newStatus) => {
    // console.log(`newStatus: ${JSON.stringify(newStatus)}`);
    setControllerStatus(newStatus);
  };

  const handleProtocolExecution = (data) => {
    setExecutionStatus(data);
  };
  
  const [socket, setSocket] = useState(null);

  const cleanupSocket = (socketInstance) => {
    if (socketInstance) {
      // Remove all event listeners: https://socket.io/docs/v4/client-api/#socketoffeventname
      socketInstance.off();
      // Disconnect the socket
      socketInstance.disconnect();
      console.log("Socket disconnected.");
    }
  };

  useEffect(() => {
    const newSocket = io(`${app_url}/frontend`);
    setSocket(newSocket);
    console.log('Socket configured at', `${app_url}/frontend`)

    // Cleanup when app_url changes or on unmount
    return () => cleanupSocket(newSocket);
  }, [app_url]);

  useEffect(() => {
    if (socket) {
      socket.on("welcome", handleWelcome);
      socket.on("controller", handleController);
      socket.on("alert", handleAlert);
      socket.on("human_intervention_required", handleHumanIntervention);
      socket.on("stopped", handleStopped);
      socket.on("robotStatus", handleRobotStatus);
      socket.on("execution", handleProtocolExecution);

      console.log("Socket event listeners registered.");

      // Cleanup listeners on socket instance change
      // Reference: https://socket.io/docs/v4/client-api/#socketoffeventname
      return () => socket.off();
    }
  }, [socket]);

  return {
    alert,
    controllerStatus,
    handleConfirm,
    handleCancel,
    executionStatus,
    handleProtocolExecution,
  };
};


/* SocketProvider

This is a React component that wraps around other components to provide them
with access to the socket-related state and handlers from the useSocket hook.

How it works:

- The SocketProvider internally calls the useSocket hook to set up the WebSocket connection and handle all the socket-related events and state.
- It uses the React Context API (SocketContext) to store and make available the values returned by the useSocket hook.
- Components wrapped inside the SocketProvider will have access to the socket functionalities via the useSocketContext hook.

Example usage:
    <SocketProvider>
      <YourComponent />
    </SocketProvider>

This component is used once, and wrapps the entire application (see App.jsx).
*/

export const SocketProvider = ({ children, addSnack }) => {
  // Call the useSocket hook to initialize and manage the socket connection
  // and related functionality. The return value is an object with socket
  // state and functions that will be shared via context.
  const socket = useSocket(addSnack);  // Pass addSnack to useSocket.

  return (
    // Wrap children components with the SocketContext.Provider, passing the socket
    // object as the context value. This allows any child component to access the
    // socket state and functions by using the useSocketContext hook.
    <SocketContext.Provider value={socket}>
      {children} {/* Render all the children components inside the provider */}
    </SocketContext.Provider>
  );
};

/* useSocketContext

This is a custom hook used to access the socket context provided by the SocketProvider.

How it works:
 - It calls the useContext hook with the SocketContext, retrieving the values stored in it (which are provided by SocketProvider).
 - Any component that uses useSocketContext will gain access to the socket functionalities (like alerts, controller status, etc.)
   without needing to reinitialize the WebSocket connection.

 Example usage:
    const socket = useSocketContext();
    console.log(socket.controllerStatus);  // Access the controllerStatus from the socket context
*/
export const useSocketContext = () => {
  // The useContext hook is used to consume the "value" provided by SocketContext.Provider.
  // This value contains the WebSocket connection and relevant handlers from the useSocket hook.
  return useContext(SocketContext);
};

export default useSocket;
