import axios from "axios";
import { roundNumber } from "../utils/previewHelpers";


export const useFollowPlatform = () => {

    const followPlatform = async (movingPlatform, position, tool, roundUp) => {
        let positionx, positiony
        if (movingPlatform.element === "well") {
            const well = document.getElementById(`well-${movingPlatform.selectedPlatform}-${movingPlatform.selectedContent}`);
            positionx = parseFloat(well.getAttribute("cx")) + position.x;
            positiony = parseFloat(well.getAttribute("cy")) + position.y;
        } else {
            positionx = movingPlatform.colonyPosition.x + position.x;
            positiony = movingPlatform.colonyPosition.y + position.y;
        }
    
        const selections = {
            position: {
                x: roundNumber(positionx, roundUp),
                y: roundNumber(positiony, roundUp),
                z: position.z
            },
            tool: tool,
        };
    
        try {
            const response = await axios.post("/api/robot/goto", selections);
            console.log(response);
        } catch (error) {
            console.log(error);
        }

    }

    return followPlatform
}


