export const useExportData = () => {
  const exportData = (files, name) => {
    const jsonToExport = files.map((file) => {
      const modifiedFile = { ...file };
      delete modifiedFile._id;
      return modifiedFile;
    });
    const keyToRemove = "_id";
    const data = JSON.stringify(jsonToExport, (key, val) => key === keyToRemove ? undefined : val, 4);
    const fileName = `${name}.json`;
    const fileType = "text/json";

    const blob = new Blob([data], { type: fileType });
    const a = document.createElement("a");
    a.download = fileName;
    a.href = window.URL.createObjectURL(blob);
    const clickEvt = new MouseEvent("click", {
      view: window,
      bubbles: true,
      cancelable: true,
    });
    a.dispatchEvent(clickEvt);
    a.remove();
  };

  return exportData;
};
