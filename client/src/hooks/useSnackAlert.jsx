import { useState, useEffect, createContext, useContext } from "react";
import { Snackbar, Alert } from "@mui/material";

const SnackAlertContext = createContext();

export const SnackAlertProvider = ({ children }) => {
  const [snackInfo, setSnackInfo] = useState(null);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (snackInfo) {
      setOpen(true);
    }
  }, [snackInfo]);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const triggerSnackAlert = (message, severity = "info") => {
    if (!message) {
      setOpen(false);
    } else {
      const newSnackInfo = {
        message,
        severity,
        key: new Date().getTime(),
      };

      setSnackInfo(newSnackInfo);
    }
  };

  const SnackAlertComponent = () => (
    <Snackbar
      open={open}
      autoHideDuration={3000}
      onClose={handleClose}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
      disableWindowBlurListener
    >
      <Alert
        onClose={handleClose}
        severity={snackInfo ? snackInfo.severity : "info"}
        sx={{ width: "100%", marginTop: 6 }}
      >
        {snackInfo ? snackInfo.message : ""}
      </Alert>
    </Snackbar>
  );

  return (
    <SnackAlertContext.Provider value={triggerSnackAlert}>
      {children}
      <SnackAlertComponent />
    </SnackAlertContext.Provider>
  );
};

export const useSnackAlert = () => {
  return useContext(SnackAlertContext);
};
