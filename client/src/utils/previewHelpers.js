// Finds free spot for content
export const findFreeIndex = (array) => {
  const sortedArray = array.slice().sort(function (a, b) {
    return a.index - b.index;
  });
  let previousIndex = 0;
  for (let element of sortedArray) {
    if (element.index != previousIndex + 1) {
      // Found a gap.
      return previousIndex + 1;
    }
    previousIndex = element.index;
  }
  // Found no gaps.
  return previousIndex + 1;
};

export const roundNumber = (num, precision = 2) => {
  // return Number.parseFloat(num).toFixed(2);
  if (typeof num === "string") num = Number(num);
  const rounded = num.toFixed(precision);
  return Number(rounded);
};

export const formattedCoords = (array) => {
  let formattedArray = array.map((num) => {
    return roundNumber(num);
  });
  return formattedArray.join(" / ");
};

export const getUniqueName = (collection, name, index = 0) => {
  let checkName = name;

  if (index) {
    // Pattern
    checkName = `${checkName} (${index})`;
  }

  const nameExists = collection.filter((f) => f.name === checkName).length > 0;
  return nameExists ? getUniqueName(collection, name, index + 1) : checkName;
};

export const getThumbnail = () => {
  const svg = document.getElementById("workspaceSVG");
  if (svg) {
    let s = new XMLSerializer();
    let str = s.serializeToString(svg);
    return str;
  }
  return "";
};
