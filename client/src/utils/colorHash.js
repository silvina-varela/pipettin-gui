export function colorHash(inputString) {
    var sum = 0;
  
    for (var i in inputString) {
      sum += inputString.charCodeAt(i);
    }
  
    var r = ~~(
      ("0." +
        Math.sin(sum + 1)
          .toString()
          .substr(6)) *
      256
    );
    var g = ~~(
      ("0." +
        Math.sin(sum + 2)
          .toString()
          .substr(6)) *
      256
    );
    var b = ~~(
      ("0." +
        Math.sin(sum + 3)
          .toString()
          .substr(6)) *
      256
    );
  
    var rgb = "rgb(" + r + ", " + g + ", " + b + ")";
  
    var hex = "#";
  
    hex += ("00" + r.toString(16)).substr(-2, 2).toUpperCase();
    hex += ("00" + g.toString(16)).substr(-2, 2).toUpperCase();
    hex += ("00" + b.toString(16)).substr(-2, 2).toUpperCase();
  
    return { r: r, g: g, b: b, rgb: rgb, hex: hex };
  }
  
export function getLuma(c) {
    c = c.substring(1); // strip #
    var rgb = parseInt(c, 16); // convert rrggbb to decimal
    var r = (rgb >> 16) & 0xff; // extract red
    var g = (rgb >> 8) & 0xff; // extract green
    var b = (rgb >> 0) & 0xff; // extract blue
  
    var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // ITU-R BT.709
  
    return luma;
  }
