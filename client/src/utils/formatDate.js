export const formatDate = (dateString, withTime=false) => {
  const options = {
    year: "numeric",
    month: "numeric",
    day: "numeric",
    ...(withTime && {
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    }),
  };    const date = new Date(dateString);
    return date.toLocaleDateString('en-GB', options);
  };

