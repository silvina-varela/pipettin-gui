import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import App from "./App";
import { store } from "./redux/store";
import axios from "axios";
import "./main.css";
import config from "../../config.json";
import { SnackAlertProvider } from "./hooks/useSnackAlert";
import { setURL } from './redux/slices/globalSlice';

// Load the "config.json" file included in the "public" directory of the source.
// See: https://chat.openai.com/share/81b0e8e6-0413-4e97-aa75-8bc2d451f036
const fetchConfig = async () => {
  try {
    // Fetch the config.json file
    const response = await fetch('/config.json');
    if (!response.ok) {
      throw new Error('Failed to fetch config.json');
    }
    const configData = await response.json();
    
    return configData;
  } catch (error) {
    console.error('Error fetching config:', error);
    return null;
  }
};

// Note: "URL" is a globally accessible class defined by Node.
// Not a good name to override.

// Initialize socket.io connection after fetching config
fetchConfig().then(configData => {
  // Try getting the API URL from the browser's storage.
  const storedApiUrl = localStorage.getItem('apiUrl');
  console.log("Current stored API URL:", storedApiUrl);
  
  let app_url;
  if (configData) {
    console.log("Got configuration data:", configData);
    if(storedApiUrl && configData.USE_STORED_URL){
      // Override the config's URL with the stored one
      // if it is available and the config option is set.
      app_url = storedApiUrl;
      console.log("Using stored API URL:", app_url);
    } else {
      // Just use the value in config if available.
      app_url = `http://${configData.HOST}:${configData.PORT}`; 
      console.log("Loaded URL from configuration:", app_url);
    }
  } else if(storedApiUrl){
    // Try using the stored URL if available.
    app_url = storedApiUrl;
    console.log("Using stored API URL (no config data):", app_url);
  } else  {
    // Finally default to something.
    app_url = `http://${config.HOST}:${config.PORT}`;
    console.log("Loaded default URL (no config data):", app_url);
  }

  // Configure axios with the backend URL.
  console.log("Current URL:", app_url);
  axios.defaults.baseURL = app_url;
  // Dispatch action to set URL in Redux store (see globalSlice.js).
  // This is used by the socket connection (see useSocket.jsx), and
  // by the settings page (see Settings.jsx) there it is accessed 
  // through "state.globalSlice.url".
  store.dispatch(setURL(app_url));
  
  ReactDOM.createRoot(document.getElementById("root")).render(
    <Provider store={store}>
      <SnackAlertProvider>
        <App />
      </SnackAlertProvider>
    </Provider>
  );
});
