import {
  Grid,
  Box,
  Drawer,
  Toolbar,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  TextField,
  Button,
} from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import { WorkspaceSettingsForm } from "../components/GeneralSettings/WorkspaceSettingsForm";
import { DatabaseSettingsForm } from "../components/GeneralSettings/DatabaseSettingsForm";
import { ControllerSettingsForm } from "../components/GeneralSettings/ControllerSettingsForm";
import { ApiSettingsForm } from "../components/GeneralSettings/ApiSettingsForm";
import { useSelector, useDispatch } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { saveSettingsAsync } from "../redux/slices";
import { useEffect, useState } from "react";
import axios from 'axios';
import { setURL } from "../redux/slices/globalSlice";
import { clearSettingsError } from "../redux/slices/settingsSlice";
import { getSettingsAsync } from "../redux/slices";
// import { useSocketContext } from "../hooks/useSocket";

// List database names.
const fetchDatabases = async () => {
  try {
    const response = await axios.get('/api/settings/listDatabases');
    console.log("Got databases:", response.data);
    return response.data; // List of database names
  } catch (error) {
    console.error('Failed to fetch databases', error);
    return [];
  }
};

const drawerWidth = 240;

export const Settings = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const { settings, settingsError } = useSelector((state) => state.settingsSlice);
  const dispatch = useDispatch();
  const [existingDatabases, setExistingDatabases] = useState([]);

  // Get the current URL from the global slice.
  const currentURL = useSelector((state) => state.globalSlice.url);
  console.log("Current URL from the global slice:", currentURL);

  // Manual URL configuration variable.
  // Used when there is an error fetching the settings.
  const [manualURL, setManualURL] = useState(currentURL || '');
  
  const submitURL = (newURL) => {
    // Configure axios with the backend URL.
    axios.defaults.baseURL = newURL;

    // Save to local storage.
    localStorage.setItem('apiUrl', newURL);

    // Dispatch action to set URL in Redux store (see globalSlice.js).
    // This is used by the socket connection (see useSocket.jsx).
    dispatch(setURL(newURL));
    // TODO: Even though the socket seems to start using the new address,
    // several CORS errors still appear in the console using the previous one.
    
    // Optionally, reinitialize your socket connection if applicable
    // const socket = useSocketContext();
    // socket.disconnect();
    // socket.connect(newURL);

    // Clear the settings error after URL is set successfully
    dispatch(clearSettingsError());

    // Get settings.
    dispatch(getSettingsAsync());

    // Log.
    console.log("Set backend URL manually to:", newURL);
  }

  // Manual URL configuration handler.
  // Used when there is an error fetching the settings.
  const handleURLSubmit = () => {
    if (manualURL) {
      submitURL(manualURL);
    }
  };

  // Fetch databases when component mounts
  useEffect(() => {
    const getDatabases = async () => {
      const databases = await fetchDatabases();
      setExistingDatabases(databases);
    };
    
    getDatabases();
  }, []); // Empty dependency array to run only on mount

  const handleSave = (values, form, errors) => {
    if (!errors || !Object.values(errors).length) {
      dispatch(saveSettingsAsync({values, form}));
    }
  };

  const handleOptionClick = (event, option) => {
    event.preventDefault();
    navigate(`?option=${option}`);
  };

  const renderForm = () => {
    switch (location.search) {
      case `?option=workspace`:
        return (
          <WorkspaceSettingsForm handleSave={handleSave} settings={settings} />
        );
      case `?option=database`:
        return (
          <DatabaseSettingsForm
            handleSave={handleSave}
            settings={settings}
            existingDatabases={existingDatabases} // Pass existing databases here
          />
        );
      case `?option=controller`:
        return (
          <ControllerSettingsForm handleSave={handleSave} settings={settings} />
        );
      case `?option=api`:
        return (
          <ApiSettingsForm submitURL={submitURL} currentURL={currentURL} />
        );
      default:
        // Default to the API URL config tab.
        return (
          <ApiSettingsForm submitURL={submitURL} currentURL={currentURL} />
        );
    }
  };

  if (settingsError) {
    // Display an error page to fix the API URL when settings fail to load.
    // TODO: It would be nice to reuse the ApiSettingsForm component, instead of the one below.
    return (
      <Box p={3}>
        <p>Unable to obtain data from the URL: <b>{axios.defaults.baseURL}</b></p>
        <p>This can happen if the Pipettin Writer's API has changed address, or if it not running at that address.</p>
        <p>Provisionally, you may set a new URL for the API below. It will be used for the current session.</p>
        <p>Provide a new backend URL manually:</p>
        <TextField
          label="Backend URL"
          variant="outlined"
          value={manualURL}
          onChange={(e) => setManualURL(e.target.value)}
          fullWidth
        />
        <Button variant="contained" onClick={handleURLSubmit} sx={{ mt: 2 }}>
          Set URL
        </Button>

        <p><b>Note</b>: to permanently fix this issue, update the IP addresses in the applications 'config.json' file, and restart the application's server.</p>
      </Box>
    );
  } else if (!Object.keys(settings).length) {
    return <>Unable to load settings: none retrieved.</>;
  } else {
    return (
      <>
        <Box sx={{ display: "flex" }}>
          <CssBaseline />
          <Drawer
            variant="permanent"
            sx={{
              width: drawerWidth,
              flexShrink: 0,
              [`& .MuiDrawer-paper`]: {
                width: drawerWidth,
                boxSizing: "border-box",
              },
            }}
          >
            <Toolbar />
            <Box
              sx={{
                position: "relative",
                zIndex: 1200,
              }}
            >
              {/* Settings tabs */}
              <List>

                {/* API tab */}
                <ListItem disablePadding>
                  <ListItemButton
                    onClick={(e) => handleOptionClick(e, "api")}
                    sx={{
                      backgroundColor: !location.search
                        ? "rgba(0, 0, 0, 0.08)"
                        : location.search === `?option=api`
                        ? "rgba(0, 0, 0, 0.08)"
                        : "transparent",
                    }}
                  >
                    <ListItemText primary="API" />
                  </ListItemButton>
                </ListItem>

                {/* Workspace tab */}
                <ListItem disablePadding>
                  <ListItemButton
                    onClick={(e) => handleOptionClick(e, "workspace")}
                    sx={{
                      backgroundColor: !location.search
                        ? "rgba(0, 0, 0, 0.08)"
                        : location.search === `?option=workspace`
                        ? "rgba(0, 0, 0, 0.08)"
                        : "transparent",
                    }}
                  >
                    <ListItemText primary="Workspace" />
                  </ListItemButton>
                </ListItem>

                {/* Database tab */}
                <ListItem disablePadding>
                  <ListItemButton
                    onClick={(e) => handleOptionClick(e, "database")}
                    sx={{
                      backgroundColor:
                        location.search === `?option=database`
                          ? "rgba(0, 0, 0, 0.08)"
                          : "transparent",
                    }}
                  >
                    <ListItemText primary="Database" />
                  </ListItemButton>
                </ListItem>

                {/* Controller tab */}
                <ListItem disablePadding>
                  <ListItemButton
                    onClick={(e) => handleOptionClick(e, "controller")}
                    sx={{
                      backgroundColor:
                        location.search === `?option=controller`
                          ? "rgba(0, 0, 0, 0.08)"
                          : "transparent",
                    }}
                  >
                    <ListItemText primary="Controller" />
                  </ListItemButton>
                </ListItem>
                {/* No more settings tabs */}
              </List>
            </Box>
          </Drawer>
          <Grid container>
            <Grid item xs={12} sm={10}>
              {renderForm()}
            </Grid>
          </Grid>
        </Box>
      </>
    );
  }
};
