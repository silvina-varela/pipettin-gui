/* eslint-disable react/no-unescaped-entities */
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getWorkspacesAsync } from "../redux/slices";
import {
  Container,
  Grid,
  AppBar,
  Toolbar,
  Button,
  Typography,
  Box,
} from "@mui/material";
import { WorkspaceCard } from "../components/Workspaces/WorkspaceCard";
import { Popup } from "../components/Dialogs/Popup";
import { WorkspaceForm } from "../components/Workspaces/WorkspaceForm";
import { useNavigate } from "react-router-dom";
import { SearchBar } from "../components/SearchBar";
import { Alert } from "../components/Dialogs/Alert";

export const Dashboard = () => {
  const dispatch = useDispatch();
  const { workspaces } = useSelector((state) => state.workspaceSlice);
  const [searchTerm, setSearchTerm] = useState("");
  const [openPopUp, setOpenPopUp] = useState(false);
  const [workspaceNotFound, setWorkspaceNotFound] = useState(false);
  const navigate = useNavigate();

  const [openAlert, setOpenAlert] = useState(false);
  const [alert, setAlert] = useState({
    yesMessage: "",
    noMessage: "",
    type: "",
    message: "",
    dialogWidth: undefined,
    onConfirm: () => {},
  });

  useEffect(() => {
    const handleNavigateToSettings = (e) => {
      e.preventDefault();
      navigate("/settings?option=database");
    };

    dispatch(getWorkspacesAsync())
      .unwrap()
      .then((response) => {
        if (!response.length) {
          setOpenAlert(true);
          setAlert({
            title: "Welcome to pipettin!",
            yesMessage: "Take me to settings",
            noMessage: "Cancel",
            message: (
              <>
                You are seeing this message because the database is currently empty.
                <br />
                <br />
                If you want to populate it with the default data,
                visit the settings page to seed the database.
                <br />
                <br />
                We recommend seeding the defaults, but you may continue to use the
                application without it, and import your data using the menus.
              </>
            ),
            dialogWidth: "xs",
            onConfirm: (e) => handleNavigateToSettings(e),
            onCancel: () => setOpenAlert(false),
          });
        }
      });
  }, [dispatch, navigate]);

  const filteredWorkspaces = [...workspaces]
    ?.sort((a, b) => {
      const dateA = a?.updatedAt ? new Date(a.updatedAt) : null;
      const dateB = b?.updatedAt ? new Date(b.updatedAt) : null;

      if (dateA && dateB) {
        return dateB - dateA;
      } else if (dateA) {
        return -1;
      } else if (dateB) {
        return 1;
      } else {
        return 0;
      }
    })
    .filter((workspace) =>
      workspace.name.toLowerCase().includes(searchTerm.toLowerCase())
    );

  const [visibleItems, setVisibleItems] = useState(11);

  //Receives the searched workspace name
  const handleSearchInput = (event) => {
    setSearchTerm(event);
    // Checks if the searched workspace is not found
    const isWorkspaceFound = filteredWorkspaces.some((workspace) =>
      workspace.name.toLowerCase().includes(event.toLowerCase())
    );
    setWorkspaceNotFound(!isWorkspaceFound);
  };
  //Checks to show new workspace card
  const isNewWorkspaceVisible = searchTerm.length === 0;

  const handleViewMore = () => {
    setVisibleItems((prevVisibleItems) => prevVisibleItems + 11);
  };

  const handleViewLess = () => {
    setVisibleItems(11);
  };

  const renderedItems =
    workspaceNotFound && searchTerm.length > 0 ? (
      <Container sx={{ display: "flex", justifyContent: "center", mt: "50px" }}>
        <Typography variant="subtitle">
          There is no workspace that matches "{searchTerm}"
        </Typography>
      </Container>
    ) : (
      filteredWorkspaces.slice(0, visibleItems).map((item, index) => (
        <Grid item xs={3} key={index}>
          <WorkspaceCard workspace={item} />
        </Grid>
      ))
    );

  return (
    <Container
      sx={{
        paddingTop: 5,
        paddingBottom: 10,
        display: "flex",
        flexDirection: "column",
        height: "100vh",
      }}
    >
      <Typography variant="h5">Welcome to Pipettin</Typography>
      <AppBar
        position="static"
        color="transparent"
        elevation={0}
        sx={{ marginBottom: 2 }}
      >
        <Toolbar
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignContent: "center",
            pl: "0 !important",
          }}
        >
          <Typography variant="h7">
            {searchTerm.length || !filteredWorkspaces.length
              ? ""
              : "Recently used workspaces"}
          </Typography>
          <Box>
            <SearchBar searchInput={handleSearchInput} />
          </Box>
        </Toolbar>
      </AppBar>
      <Box sx={{ overflowY: "auto" }}>
        <Grid container spacing={3}>
          {isNewWorkspaceVisible ? (
            <Grid item xs={3}>
              <WorkspaceCard handleCardClick={() => setOpenPopUp(true)} />
            </Grid>
          ) : null}
          {renderedItems}
        </Grid>

        {searchTerm.length ? null : !filteredWorkspaces.length ? null : visibleItems <
          filteredWorkspaces.length ? (
          <Box
            sx={{ display: "flex", justifyContent: "flex-end", marginTop: 4 }}
          >
            <Button color="inherit" onClick={handleViewMore}>
              View More
            </Button>
          </Box>
        ) : visibleItems > 11 ? (
          <Box
            sx={{ display: "flex", justifyContent: "flex-end", marginTop: 4 }}
          >
            <Button color="inherit" onClick={handleViewLess}>
              View Less
            </Button>
          </Box>
        ) : null}
      </Box>
      <Popup
        title="New Workspace"
        openPopup={openPopUp}
        component={
          <WorkspaceForm handleClosePopup={() => setOpenPopUp(false)} />
        }
        setOpenPopup={setOpenPopUp}
      />
      <Alert
        open={openAlert}
        title={alert.title}
        message={alert.message}
        onConfirm={alert.onConfirm}
        onCancel={() => setOpenAlert(false)}
        onClose={() => setOpenAlert(false)}
        yesMessage={alert.yesMessage}
        noMessage={alert.noMessage}
        type={alert.type}
        dialogWidth={alert.dialogWidth}
      />
    </Container>
  );
};
