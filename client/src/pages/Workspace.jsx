import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { PlatformPanel } from "../components/PlatformsSidePanel/PlatformPanel";
import { ProtocolPanel } from "../components/ProtocolsSidePanel/ProtocolPanel";
import { WorkspaceArea } from "../components/Workspaces/WorkspaceArea";
import { styled } from "@mui/material/styles";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import MuiDrawer from "@mui/material/Drawer";
import { Box, Tooltip, Typography, Toolbar, IconButton } from "@mui/material";
import {
  resetWorkspace,
  selectPlatform,
  fetchAllWorkspaceData,
  saveWorkspaceAsync,
} from "../redux/slices";
import { useSnackAlert } from "../hooks/useSnackAlert";

const openedMixin = (theme, drawerWidth) => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflow: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(6)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(7)} + 1px)`,
  },
});
const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open" && prop !== "drawerWidth",
})(({ theme, open, drawerWidth }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme, drawerWidth),
    "& .MuiDrawer-paper": openedMixin(theme, drawerWidth),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

const Main = styled("main", { shouldForwardProp: (prop) => prop !== "open" })(
  () => ({
    flexGrow: 1,
    height: "97vh",
  })
);

export const Workspace = () => {
  const dispatch = useDispatch();
  const { settings } = useSelector((state) => state.settingsSlice);

  const { workspaceId } = useParams();

  const drawerWidth = settings?.workspace?.panelWidth || 240;

  /** Loads data */
  useEffect(() => {
    dispatch(fetchAllWorkspaceData(workspaceId));

    return () => {
      dispatch(resetWorkspace());
      dispatch(selectPlatform({ platform: "", content: [] }));
    };
  }, [dispatch, workspaceId]);

  const [openLeftDrawer, setOpenLeftDrawer] = useState(true);
  const [openRightDrawer, setOpenRightDrawer] = useState(true);

  const handleDrawerOpen = (event, side) => {
    event.preventDefault();
    if (side === "left") setOpenLeftDrawer(true);
    if (side === "right") setOpenRightDrawer(true);
  };

  const handleDrawerClose = (event, side) => {
    event.preventDefault();
    if (side === "left") setOpenLeftDrawer(false);
    if (side === "right") setOpenRightDrawer(false);
  };

  const addSnack = useSnackAlert();

  useEffect(() => {
    const handleKeyDown = (event) => {
      const isControlKeyPressed = event.ctrlKey || event.metaKey;
      const isSKeyPressed = event.key === "s" || event.key === "S";

      if (isControlKeyPressed && isSKeyPressed) {
        event.preventDefault();
        dispatch(saveWorkspaceAsync())
          .unwrap()
          .then(() => {
            addSnack("Workspace saved", "success");
          })
          .catch((error) => {
            console.log(error);
          });
      }
    };
    document.addEventListener("keydown", handleKeyDown);

    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    };
  });

  return (
    <Box sx={{ display: "flex" }}>
      <Drawer
        open={openLeftDrawer}
        drawerWidth={drawerWidth}
        variant="permanent"
      >
        <Toolbar />
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
            zIndex: 1,
            backgroundColor: openLeftDrawer && "lightgrey",
            padding: 1,
          }}
        >
          <Box position={"absolute"}>
            <Box>
              <Tooltip
                title={
                  openLeftDrawer
                    ? "Collapse platform panel"
                    : "Expand platform panel"
                }
              >
                <IconButton
                  onClick={
                    openLeftDrawer
                      ? (e) => handleDrawerClose(e, "left")
                      : (e) => handleDrawerOpen(e, "left")
                  }
                >
                  {openLeftDrawer ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                </IconButton>
              </Tooltip>
            </Box>
          </Box>

          <Typography
            variant="h6"
            sx={{
              flexGrow: 1,
              textAlign: "center",
              color: "#252525",
              userSelect: "none",
              visibility: !openLeftDrawer && "hidden",
            }}
          >
            Platforms
          </Typography>
        </Box>
        <Box sx={{ overflow: "auto", display: !openLeftDrawer && "none" }}>
          <PlatformPanel />
        </Box>
      </Drawer>
      <Main open={openLeftDrawer}>
        <WorkspaceArea />
      </Main>
      <Drawer
        variant="permanent"
        anchor="right"
        open={openRightDrawer}
        drawerWidth={drawerWidth}
      >
        <Toolbar />
        <ProtocolPanel
          handleDrawerClose={handleDrawerClose}
          handleDrawerOpen={handleDrawerOpen}
          openDrawer={openRightDrawer}
        />
      </Drawer>
    </Box>
  );
};
