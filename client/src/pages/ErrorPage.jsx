import { Box, Button, Container, Typography } from "@mui/material";

export const ErrorPage = ({resetErrorBoundary}) => {

  return (
    <Container sx={{ height: "100vh" }}>
      <Box
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-evenly",
        }}
      >
        <Box style={{ flex: 1, maxWidth: "50%", marginLeft: "10px", marginTop: "100px" }}>
          <Typography variant="h4" style={{ marginBottom: "30px" }}>
            Sorry, something went wrong.
          </Typography>
          <Typography style={{ marginBottom: "50px" }}>
            Please follow the steps explained at the following link to report the error:
            <br />
            <a href="https://gitlab.com/pipettin-bot/pipettin-gui/-/blob/develop/doc/Troubleshooting.md" target="_blank" style={{ textDecoration: "underline" }}>
              Troubleshooting instructions
            </a>

          </Typography>
          <Button variant="contained" onClick={resetErrorBoundary}>
            Refresh page
          </Button>
        </Box>
        <Box>
          <img
            src="/images/error.png"
            style={{ width: "300px", height: "300px", marginTop: "100px" }}
          />
        </Box>
      </Box>
      <Box
        style={{
          position: "fixed",
          bottom: "0",
          right: "0",
          marginRight: "3px",
          fontSize: "8px",
        }}
      >
        <a href="https://www.freepik.com/free-vector/error-404-concept-landing-page_4660853.htm#query=cat%20error%20page&position=2&from_view=search&track=ais&uuid=19382655-130a-4543-8de1-f782dde0bf7e">
          Image by pikisuperstar
        </a>{" "}
        on Freepik
      </Box>
    </Container>
  );
};
