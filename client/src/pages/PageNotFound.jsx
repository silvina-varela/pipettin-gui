import { Box, Button, Container, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";

export const PageNotFound = () => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate("/");
  };

  return (
    <Container style={{ height: "100%" }}>
      <Box
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-evenly",
        }}
      >
        <Box style={{ flex: 1, maxWidth: "50%", marginLeft: "10px" }}>
          <Typography variant="h2" style={{ marginBottom: "40px" }}>
            Something is not right...
          </Typography>
          <Typography style={{ marginBottom: "50px" }}>
            The page you are trying to open does not exist. You may have
            mistyped the address, or the page has been moved to another URL. If
            you think this is an error you can send an e-mail to{" "}
            <span style={{ textDecoration: "underline" }}>
              contact-project+pipettin-bot-pipettin-gui-support@incoming.gitlab.com
            </span>
            .
          </Typography>
          <Button variant="contained" onClick={handleClick}>
            Get back to Dashboard
          </Button>
        </Box>
        <Box>
          <img
            src="/images/PageNotFound.svg"
            style={{ height: "30rem", marginRight: "10px" }}
          />
        </Box>
      </Box>
      <Box
        style={{
          position: "fixed",
          bottom: "0",
          right: "0",
          marginRight: "3px",
          fontSize: "8px",
        }}
      >
        <a href="https://www.freepik.es/vector-gratis/ilustracion-concepto-error-404-animal-lindo_8030429.htm#query=404%20cat%20error&position=5&from_view=search&track=ais&uuid=86591083-193e-4a65-8e67-9333f5b6e3bb">
          Image by storyset
        </a>{" "}
        on Freepik
      </Box>
    </Container>
  );
};
