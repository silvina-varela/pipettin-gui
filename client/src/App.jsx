import { useEffect } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { NavBar } from "./components/NavBar/NavBar";
import { Dashboard } from "./pages/Dashboard";
import { Workspace } from "./pages/Workspace";
import { lilac, violet, blue, gray } from "./colors";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { CssBaseline } from "@mui/material";
import { Settings } from "./pages/Settings";
import { useDispatch, useSelector } from "react-redux";
import { useSnackAlert } from "./hooks/useSnackAlert";
import { SocketProvider } from "./hooks/useSocket";
import { PageNotFound } from "./pages/PageNotFound";
import { ErrorBoundary } from "react-error-boundary";
import { ErrorPage } from "./pages/ErrorPage";
import { getSettingsAsync } from "./redux/slices";

const theme = createTheme({
  palette: {
    primary: {
      main: violet[600],
    },
    secondary: {
      main: blue[700],
    },
    tertiary: {
      main: lilac[200],
    },
    lightgray: {
      main: gray[200],
      contrastText: gray[500],
    },
    neutral: {
      main: gray[500],
      contrastText: gray[800],
    },
  },
});

const App = () => {
  const dispatch = useDispatch();
  const { error, errorMessage } = useSelector((state) => state.globalSlice);

  const addSnack = useSnackAlert();

  useEffect(() => {
    dispatch(getSettingsAsync());
  }, [dispatch]);

  useEffect(() => {
    if (error) {
      let formattedMessage = errorMessage;
      if (typeof errorMessage === 'string') {
        // Replace newline characters with <br> tags.
        formattedMessage = errorMessage.split('\n').map((line, index) => (
          <span key={index}>{line}<br/></span>
        ));
      }
      addSnack(formattedMessage, "error");
    }
  }, [error, errorMessage]);

  return (
    <SocketProvider addSnack={addSnack}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <BrowserRouter>
          <NavBar />
          <Routes>
            <Route
              path="/"
              element={
                <ErrorBoundary FallbackComponent={ErrorPage}>
                  <Dashboard />
                </ErrorBoundary>
              }
            />
            <Route
              path="/workspace/:workspaceId"
              element={
                <ErrorBoundary FallbackComponent={ErrorPage}>
                  <Workspace />
                </ErrorBoundary>
              }
            />
            <Route
              path="/settings"
              element={
                <ErrorBoundary FallbackComponent={ErrorPage}>
                  <Settings />
                </ErrorBoundary>
              }
            />
            <Route path="*" element={<PageNotFound />} />
          </Routes>
        </BrowserRouter>
      </ThemeProvider>
    </SocketProvider>
  );
};

export default App;
