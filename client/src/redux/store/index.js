import { configureStore } from "@reduxjs/toolkit";
import workspaceSlice from "../slices/workspaceSlice";
import protocolSlice from "../slices/protocolSlice";
import platformSlice from "../slices/platformSlice";
import toolSlice from "../slices/toolSlice";
import containerSlice from "../slices/containerSlice";
import settingsSlice from "../slices/settingsSlice";
import globalSlice from "../slices/globalSlice";

const rootReducer = {
  workspaceSlice,
  protocolSlice,
  platformSlice,
  toolSlice,
  containerSlice,
  settingsSlice,
  globalSlice,
};

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    // serializableCheck: false,
  }),
  devTools: true,
});
