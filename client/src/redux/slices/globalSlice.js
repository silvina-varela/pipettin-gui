import { createSlice, unwrapResult } from "@reduxjs/toolkit";
import { getWorkspaceByIdAsync } from "./workspaceSlice";
import { getWorkspaceProtocolsAsync } from "./protocolSlice";
import { getPlatformsAsync } from "./platformSlice";
import { getToolsAsync } from "./toolSlice";
import { getContainersAsync } from "./containerSlice";

const initialState = {
  error: null,
  errorMessage: "",
  loading: null,
  url: null,
};

export const globalSlice = createSlice({
  name: "globalSlice",
  initialState,
  reducers: {
    setError(state, action) {
      state.error = true;
      state.errorMessage = action.payload;
    },
    clearError(state) {
      state.error = false;
      state.errorMessage = "";
    },
    setLoading(state, action) {
      state.loading = action.payload;
    },
    setURL(state, action) {
      state.url = action.payload;
      console.log("New API URL set in the global slice:", action.payload);
    },
  },
});

export const { setError, setLoading, clearError, setURL } = globalSlice.actions;

export const fetchAllWorkspaceData = (workspaceID) => async (dispatch) => {
  try {
    dispatch(setLoading(true));

    const workspaceResult = await dispatch(getWorkspaceByIdAsync(workspaceID));
    const unwrappedResult = unwrapResult(workspaceResult);
    if (unwrappedResult) {
      await Promise.all([
        dispatch(getWorkspaceProtocolsAsync(workspaceID)),
        dispatch(getPlatformsAsync()),
        dispatch(getToolsAsync()),
        dispatch(getContainersAsync()),
      ]);
    }

    dispatch(setLoading(false));
  } catch (error) {
    dispatch(setError(error.message));
    dispatch(setLoading(false));
  }
};

export default globalSlice.reducer;
