import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { clearError, setError } from "./globalSlice";
import { saveWorkspaceAsync } from "./workspaceSlice";

const initialState = {
  platforms: [],
  platform: {},
  loadingPlatforms: null,
  platformError: null,
};

export const platformSlice = createSlice({
  name: "platformSlice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getPlatformsAsync.pending, (state) => {
        state.loadingPlatforms = true;
        state.platformError = null;
      })
      .addCase(getPlatformsAsync.fulfilled, (state, action) => {
        state.platforms = action.payload;
        state.loadingPlatforms = false;
      })
      .addCase(getPlatformsAsync.rejected, (state, action) => {
        state.platformError = action.payload;
        state.loadingPlatforms = false;
      })
      .addCase(deletePlatformAsync.pending, (state) => {
        state.platformError = null;
      })
      .addCase(deletePlatformAsync.fulfilled, (state, action) => {
        state.platforms = action.payload;
      })
      .addCase(deletePlatformAsync.rejected, (state, action) => {
        state.platformError = action.payload;
      })
      .addCase(createPlatformAsync.pending, (state) => {
        state.platformError = null;
      })
      .addCase(createPlatformAsync.fulfilled, (state, action) => {
        state.platforms = action.payload;
      })
      .addCase(createPlatformAsync.rejected, (state, action) => {
        state.platformError = action.payload;
      })
      .addCase(updatePlatformAsync.pending, (state) => {
        state.platformError = null;
      })
      .addCase(updatePlatformAsync.fulfilled, (state, action) => {
        state.platforms = action.payload;
      })
      .addCase(updatePlatformAsync.rejected, (state, action) => {
        state.platformError = action.payload;
      });
  },
});

export const getPlatformsAsync = createAsyncThunk(
  "platformSlice/getPlatforms",
  async (_, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const response = await axios.get("/api/platforms");
      return response.data;
    } catch (error) {
      console.log(error)
      dispatch(
        setError(`Server error: there was an error loading the platforms`)
      );
      return rejectWithValue(error.message);
    }
  }
);

export const createPlatformAsync = createAsyncThunk(
  "platformSlice/createPlatform",
  async (values, { rejectWithValue, dispatch, getState }) => {
    try {
      dispatch(clearError());

      // Call "createPlatform" on the backend side, and receive its output here.
      const response = await axios.post("/api/platforms", values);

      const { workspace } = getState().workspaceSlice;
      if (workspace._id) dispatch(saveWorkspaceAsync());
      
      // return response.data;
      // TODO: Disabled this return because "createPlatform" only returns a platform,
      // but the "fulfilled" reducer above expects all platforms instead. Review needed.
      // Instead, get the "updated" platforms through a separate call, and return them.
      const get_platforms_response = await axios.get("/api/platforms");
      return get_platforms_response.data;

    } catch (error) {
      console.log(error)
      dispatch(
        setError(`Server error: there was an error creating the platform`)
      );
      return rejectWithValue(error.message);
    }
  }
);

export const updatePlatformAsync = createAsyncThunk(
  "platformSlice/updatePlatform",
  async (values, { rejectWithValue, dispatch, getState }) => {
    try {
      dispatch(clearError());
      const response = await axios.put(
        `/api/platforms?id=${values._id}`,
        values
      );
      
      // Update the workspace too, if any is open.
      // NOTE: Not sure why though...
      const { workspace } = getState().workspaceSlice;
      if (workspace._id) dispatch(saveWorkspaceAsync());

      return response.data;
    } catch (error) {
      console.log(error)
      dispatch(
        setError(`Server error: there was an error updating the platform`)
      );
      return rejectWithValue(error.message);
    }
  }
);

export const deletePlatformAsync = createAsyncThunk(
  "platformSlice/deletePlatform",
  async (id, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const response = await axios.delete(`/api/platforms?id=${id}`);
      return response.data;
    } catch (error) {
      console.log(error)
      dispatch(
        setError(`Server error: there was an error deleting the platform`)
      );
      return rejectWithValue(error.message);
    }
  }
);

export default platformSlice.reducer;
