import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

const initialState = {
  settings: {},
  settingsError: null,
  loadingSettings: null,
};

export const settingsSlice = createSlice({
  name: 'settingsSlice',
  initialState,
  reducers: {
    // Action to clear the settings error
    clearSettingsError: (state) => {
      // console.log("Settings error state cleared.");
      state.settingsError = null;
    }
  },
  extraReducers: (builder) => {
    builder
    .addCase(getSettingsAsync.pending, (state) => {
      state.loadingSettings = true;
      state.settingsError = null; // Clear any previous errors
    })
    .addCase(getSettingsAsync.fulfilled, (state, action) => {
      state.settings = action.payload;
      state.loadingSettings = false;
    })
    .addCase(getSettingsAsync.rejected, (state, action) => {
      state.settingsError = action.error.message;
      state.loadingSettings = false;
    })
    .addCase(saveSettingsAsync.pending, (state) => {
      state.loadingSettings = true;
      state.settingsError = null; 
    })
    .addCase(saveSettingsAsync.fulfilled, (state, action) => {
      state.settings = action.payload;
      state.loadingSettings = false;
    })
    .addCase(saveSettingsAsync.rejected, (state, action) => {
      state.settingsError = action.error.message;
      state.loadingSettings = false;
    })
    .addCase(changeDatabaseAsync.pending, (state) => {
      state.loadingSettings = true;
      state.settingsError = null;
    })
    .addCase(changeDatabaseAsync.fulfilled, (state, action) => {
      state.settings = action.payload;
      state.loadingSettings = false;
    })
    .addCase(changeDatabaseAsync.rejected, (state, action) => {
      state.settingsError = action.error.message;
      state.loadingSettings = false;
    })
  }
});

// Export the action clearing settings error
export const { clearSettingsError } = settingsSlice.actions;

export const getSettingsAsync = createAsyncThunk(
  'settingsSlice/getSettings',
  async (_, { rejectWithValue }) => {
    try {
      const response = await axios.get('/api/settings', {
        timeout: 3000, // Set timeout in milliseconds.
      });
      return response.data;
    } catch (error) {
      console.log(error.message);
      return rejectWithValue(error.message);
    }
  }
);

export const saveSettingsAsync = createAsyncThunk(
  'settingsSlice/saveSettings',
  async ({values, form}, { rejectWithValue }) => {
    try {
      const response = await axios.put("/api/settings", { [form]: values });
      return response.data;
    } catch (error) {
      console.log(error.message);
      return rejectWithValue(error.message);
    }
  }
);

export const changeDatabaseAsync = createAsyncThunk(
  'settingsSlice/changeDatabase',
  async (values, { rejectWithValue }) => {
    try {
      const response = await axios.post("/api/settings", values);
      return response.data;
    } catch (error) {
      console.log(error.message);
      return rejectWithValue(error.message);
    }
  }
);

export default settingsSlice.reducer;
