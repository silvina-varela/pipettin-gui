import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { clearError, setError } from "./globalSlice";

const initialState = {
  protocols: [],
  protocolError: null,
  loadingProtocols: null,
  loadingProtocol: null,
  protocol: {},
  steps: [],
  allProtocols: [],
  protocolExecutionList: [],
  protocolTemplates: [],
  protocolUpdated: false,
};

export const protocolSlice = createSlice({
  name: "protocolSlice",
  initialState,
  reducers: {
    deselectProtocol(state) {
      state.protocol = {};
      state.steps = [];
      state.protocolUpdated = false;
    },
    setSteps: (state, action) => {
      state.protocolUpdated = true;
      state.steps = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getWorkspaceProtocolsAsync.pending, (state) => {
        state.loadingProtocols = true;
        state.protocolError = null;
      })
      .addCase(getWorkspaceProtocolsAsync.fulfilled, (state, action) => {
        state.protocols = action.payload;
        state.loadingProtocols = false;
      })
      .addCase(getWorkspaceProtocolsAsync.rejected, (state, action) => {
        state.protocolError = action.payload;
        state.loadingProtocols = false;
      })
      .addCase(getAllProtocolsAsync.pending, (state) => {
        state.loadingProtocols = true;
        state.protocolError = null;
      })
      .addCase(getAllProtocolsAsync.fulfilled, (state, action) => {
        state.allProtocols = action.payload;
        state.loadingProtocols = false;
      })
      .addCase(getAllProtocolsAsync.rejected, (state, action) => {
        state.protocolError = action.payload;
        state.loadingProtocols = false;
      })
      .addCase(getProtocolExecutionListAsync.pending, (state) => {
        state.loadingProtocols = true;
        state.protocolError = null;
      })
      .addCase(getProtocolExecutionListAsync.fulfilled, (state, action) => {
        state.protocolExecutionList = action.payload;
        state.loadingProtocols = false;
      })
      .addCase(getProtocolExecutionListAsync.rejected, (state, action) => {
        state.protocolError = action.payload;
        state.loadingProtocols = false;
      })
      .addCase(saveProtocolAsync.pending, (state) => {
        state.protocolError = null;
      })
      .addCase(saveProtocolAsync.fulfilled, (state, action) => {
        state.protocol = action.payload;
        state.steps = [...action.payload.steps];
        state.protocolUpdated = false;
      })
      .addCase(saveProtocolAsync.rejected, (state, action) => {
        state.protocolError = action.payload;
      })
      .addCase(createProtocolAsync.pending, (state) => {
        state.protocolError = null;
      })
      .addCase(createProtocolAsync.fulfilled, (state, action) => {
        state.protocol = action.payload;
        state.steps = action.payload.steps;
        state.protocolUpdated = false;
      })
      .addCase(createProtocolAsync.rejected, (state, action) => {
        state.protocolError = action.payload;
      })
      .addCase(deleteProtocolAsync.pending, (state) => {
        state.protocolError = null;
      })
      .addCase(deleteProtocolAsync.fulfilled, (state, action) => {
        state.protocols = action.payload;
      })
      .addCase(deleteProtocolAsync.rejected, (state, action) => {
        state.protocolError = action.payload;
      })
      .addCase(getProtocolTemplatesAsync.pending, (state) => {
        state.protocolError = null;
      })
      .addCase(getProtocolTemplatesAsync.fulfilled, (state, action) => {
        state.protocolTemplates = action.payload;
      })
      .addCase(getProtocolTemplatesAsync.rejected, (state, action) => {
        state.protocolError = action.payload;
      })
      .addCase(deleteProtocolTemplatesAsync.pending, (state) => {
        state.protocolError = null;
      })
      .addCase(deleteProtocolTemplatesAsync.fulfilled, (state, action) => {
        state.protocolTemplates = action.payload;
      })
      .addCase(deleteProtocolTemplatesAsync.rejected, (state, action) => {
        state.protocolError = action.payload;
      })
      .addCase(getProtocolByIdAsync.pending, (state) => {
        state.protocolError = null;
        state.loadingProtocol = true;
      })
      .addCase(getProtocolByIdAsync.fulfilled, (state, action) => {
        state.protocol = action.payload;
        state.steps = action.payload.steps;
        state.loadingProtocol = false;
      })
      .addCase(getProtocolByIdAsync.rejected, (state, action) => {
        state.protocolError = action.payload;
        state.loadingProtocol = false;
      });
  },
});

export const { deselectProtocol, setSteps } = protocolSlice.actions;

export const getWorkspaceProtocolsAsync = createAsyncThunk(
  "protocolSlice/getProtocols",
  async (workspaceID, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const response = await axios.get(
        `/api/protocols?workspaceID=${workspaceID}`
      );
      return response.data;
    } catch (error) {
      dispatch(
        setError(`Server error: there was an error loading the protocols`)
      );
      return rejectWithValue(error.message);
    }
  }
);

export const getAllProtocolsAsync = createAsyncThunk(
  "protocolSlice/getAllProtocols",
  async (_, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const response = await axios.get(`/api/protocols`);
      return response.data;
    } catch (error) {
      console.log(error.message);
      dispatch(
        setError(`Server error: there was an error loading the protocols`)
      );
      return rejectWithValue(error.message);
    }
  }
);

export const getProtocolExecutionListAsync = createAsyncThunk(
  "protocolSlice/getProtocolExecutions",
  async ({ hLprotocol, workspaceName }, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const response = await axios.get(
        `/api/protocols/midLevel?hLprotocol=${hLprotocol}&workspace=${workspaceName}`
      );
      return response.data;
    } catch (error) {
      console.log(error.message);
      dispatch(
        setError(`Server error: there was an error loading the protocols`)
      );
      return rejectWithValue(error.message);
    }
  }
);

export const saveProtocolAsync = createAsyncThunk(
  "protocolSlice/saveProtocol",
  async (data, { getState, rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const { steps } = getState().protocolSlice;
      const protocol = data.values;

      // Iterate over templateFields to find any file upload fields
      const updatedTemplateDefinition = { ...protocol.templateDefinition };
      if (protocol.templateFields?.fields) {
        // Find and replace file upload fields with base64-encoded data.
        for (const field of protocol.templateFields.fields) {
          if (field.field_type === "fileUpload") {
            const fileField = protocol.templateDefinition[field.field_id];

            if (fileField instanceof File) {
              const fileData = await new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.onloadend = () => resolve(reader.result.split(",")[1]);
                reader.onerror = reject;
                reader.readAsDataURL(fileField);
              });

              // Replace the file object with its metadata and Base64 string in templateDefinition
              updatedTemplateDefinition[field.field_id] = {
                data: fileData,
                name: fileField.name,
                type: fileField.type,
                size: fileField.size,
              };
            }
          }
        }
      }
      
      // Prepare the protocol object with the updated templateDefinition
      const protocolToSave = {
        ...protocol,
        steps: [...steps],
        templateDefinition: updatedTemplateDefinition,
      };

      const response = await axios.put(`/api/protocols?id=${protocol._id}`, {
        protocol: protocolToSave,
        workspaceValues: data.workspaceValues,
      });
      return response.data;
    } catch (error) {
      console.log(error.message);
      if (data.workspaceValues) {
        dispatch(
          setError(`Server error: there was an error with the controller`)
        );
      } else {
        dispatch(
          setError(`Server error: there was an error saving the protocol`)
        );
      }
      return rejectWithValue(error.message);
    }
  }
);

export const createProtocolAsync = createAsyncThunk(
  "protocolSlice/createProtocol",
  async (data, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());

      // Iterate over templateFields to find any file upload fields
      const protocol = data.values;
      const updatedTemplateDefinition = { ...protocol.templateDefinition };
      if (protocol.templateFields?.fields) {
        // Find and replace file upload fields with base64-encoded data.
        for (const field of protocol.templateFields.fields) {
          if (field.field_type === "fileUpload") {
            const fileField = protocol.templateDefinition[field.field_id];

            if (fileField instanceof File) {
              const fileData = await new Promise((resolve, reject) => {
                const reader = new FileReader();
                reader.onloadend = () => resolve(reader.result.split(",")[1]);
                reader.onerror = reject;
                reader.readAsDataURL(fileField);
              });

              // Replace the file object with its metadata and Base64 string in templateDefinition
              updatedTemplateDefinition[field.field_id] = {
                data: fileData,
                name: fileField.name,
                type: fileField.type,
                size: fileField.size,
              };
            }
          }
        }
      }
      
      // Prepare the protocol object with the updated templateDefinition
      const protocolToSave = {
        ...protocol,
        templateDefinition: updatedTemplateDefinition,
      };

      const response = await axios.post(`/api/protocols`, {
        protocol: protocolToSave,
        workspaceValues: data.workspaceValues,
      });
      return response.data;
    } catch (error) {
      console.log(error.message);
      if (data.workspaceValues) {
        dispatch(
          setError(`Server error: there was an error with the controller`)
        );
      } else {
        dispatch(
          setError(`Server error: there was an error creating the protocol`)
        );
      }
      return rejectWithValue(error.message);
    }
  }
);

export const deleteProtocolAsync = createAsyncThunk(
  "protocolSlice/deleteProtocol",
  async (protocolID, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const response = await axios.delete(`/api/protocols?id=${protocolID}`);
      return response.data;
    } catch (error) {
      console.log(error.message);
      dispatch(
        setError(`Server error: there was an error deleting the protocol`)
      );
      return rejectWithValue(error.message);
    }
  }
);

export const getProtocolByIdAsync = createAsyncThunk(
  "protocolSlice/getProtocolById",
  async (protocolID, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const response = await axios.get(`/api/protocols?id=${protocolID}`);
      return response.data;
    } catch (error) {
      dispatch(
        setError("Server error: there was an error loading the protocol")
      );
      return rejectWithValue(error.message);
    }
  }
);

export const addNewStepToProtocol = (newStep) => {
  return async (dispatch, getState) => {
    const { steps } = getState().protocolSlice;

    const updatedSteps = steps ? [...steps] : [];

    updatedSteps.push(newStep);

    dispatch(setSteps(updatedSteps));
  };
};

export const updateProtocolStep = (index, updatedStep) => {
  return async (dispatch, getState) => {
    const { steps } = getState().protocolSlice;

    const updatedSteps = [...steps];
    updatedSteps[index] = updatedStep;

    dispatch(setSteps(updatedSteps));
  };
};

export const deleteProtocolSteps = (selectedSteps) => {
  return async (dispatch, getState) => {
    const { steps } = getState().protocolSlice;

    const selected = steps.filter(
      (step) =>
        !selectedSteps.some((selectedStep) => selectedStep.name === step.name)
    );

    const updatedSteps = selected.map((step, index) => {
      return {
        ...step,
        order: index + 1,
      };
    });

    dispatch(setSteps(updatedSteps));
  };
};

export const getProtocolTemplatesAsync = createAsyncThunk(
  "protocolSlice/getProtocolTemplates",
  async (_, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const response = await axios.get(`/api/templates`);
      return response.data;
    } catch (error) {
      console.log(error.message);
      dispatch(
        setError(`Server error: there was an error loading the templates`)
      );
      return rejectWithValue(error.message);
    }
  }
);

export const deleteProtocolTemplatesAsync = createAsyncThunk(
  "protocolSlice/deleteProtocolTemplates",
  async (id, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const response = await axios.delete(`/api/templates?id=${id}`);
      return response.data;
    } catch (error) {
      console.log(error.message);
      dispatch(
        setError(`Server error: there was an error deleting the template`)
      );
      return rejectWithValue(error.message);
    }
  }
);

export default protocolSlice.reducer;
