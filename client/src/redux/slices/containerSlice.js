import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { clearError, setError } from "./globalSlice";
import { saveWorkspaceAsync } from "./workspaceSlice";

const initialState = {
  containers: [],
  containersError: null,
  loadingContainers: null,
  selectedContainer: {},
};

export const containerSlice = createSlice({
  name: "containerSlice",
  initialState,
  reducers: {
    selectContainer(state, action) {
      state.selectedContainer = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getContainersAsync.pending, (state) => {
        state.loadingContainers = true;
        state.containersError = null;
      })
      .addCase(getContainersAsync.fulfilled, (state, action) => {
        state.containers = action.payload;
        state.loadingContainers = false;
      })
      .addCase(getContainersAsync.rejected, (state, action) => {
        state.containersError = action.payload;
        state.loadingContainers = false;
      })
      .addCase(createContainerAsync.pending, (state) => {
        state.containersError = null;
      })
      .addCase(createContainerAsync.fulfilled, (state, action) => {
        state.containers = action.payload;
      })
      .addCase(createContainerAsync.rejected, (state, action) => {
        state.containersError = action.payload;
      })
      .addCase(updateContainerAsync.pending, (state) => {
        state.containersError = null;
      })
      .addCase(updateContainerAsync.fulfilled, (state, action) => {
        state.containers = action.payload;
      })
      .addCase(updateContainerAsync.rejected, (state, action) => {
        state.containersError = action.payload;
      })
      .addCase(deleteContainerAsync.pending, (state) => {
        state.containersError = null;
      })
      .addCase(deleteContainerAsync.fulfilled, (state, action) => {
        state.containers = action.payload;
      })
      .addCase(deleteContainerAsync.rejected, (state, action) => {
        state.containersError = action.payload;
      });
  },
});

export const { selectContainer } = containerSlice.actions;

export const getContainersAsync = createAsyncThunk(
  "containerSlice/getContainers",
  async (_, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError())
      const response = await axios.get("/api/containers");
      return response.data;
    } catch (error) {
      dispatch(setError(`Server error: there was an error loading the containers`))
      return rejectWithValue(error.message);

    }
  }
);

export const createContainerAsync = createAsyncThunk(
  "containerSlice/createContainer",
  async (newContainer, { rejectWithValue, dispatch, getState }) => {
    try {
      dispatch(clearError())
      const response = await axios.post("/api/containers", newContainer);
      const { workspace } = getState().workspaceSlice;
      if (workspace) dispatch(saveWorkspaceAsync());
      return response.data;
    } catch (error) {
      console.log(error);
      dispatch(setError(`Server error: there was an error creating the container`))
      return rejectWithValue(error.message);
    }
  }
);

export const updateContainerAsync = createAsyncThunk(
  "containerSlice/updateContainer",
  async (container, { rejectWithValue, getState, dispatch }) => {
    try {
      dispatch(clearError())
      const response = await axios.put(
        `/api/containers?id=${container._id}`,
        container
      );
      const { workspace } = getState().workspaceSlice;
      if (workspace) dispatch(saveWorkspaceAsync());
      return response.data;
    } catch (error) {
      console.log(error);
      dispatch(setError(`Server error: there was an error updating the container`))
      return rejectWithValue(error.message);
    }
  }
);

export const deleteContainerAsync = createAsyncThunk(
  "containerSlice/deleteContainer",
  async (containerId, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError())
      const response = await axios.delete(`/api/containers?id=${containerId}`);
      return response.data;
    } catch (error) {
      console.log(error);
      dispatch(setError(`Server error: there was an error deleting the container`))
      return rejectWithValue(error.message);
    }
  }
);

export default containerSlice.reducer;
