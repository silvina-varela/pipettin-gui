import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { clearError, setError } from "./globalSlice";

const initialState = {
  tools: [],
  loadingTools: null,
  toolsError: null,
  selectedTool: null,
};

export const toolSlice = createSlice({
  name: "toolSlice",
  initialState,
  reducers: {
    selectTool(state, action) {
      state.selectedTool = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getToolsAsync.pending, (state) => {
        state.loadingTools = true;
        state.toolsError = null;
      })
      .addCase(getToolsAsync.fulfilled, (state, action) => {
        state.tools = action.payload;
        state.loadingTools = false;
      })
      .addCase(getToolsAsync.rejected, (state, action) => {
        state.toolsError = action.payload;
        state.loadingTools = false;
      })
      .addCase(createToolAsync.pending, (state) => {
        state.toolsError = null;
      })
      .addCase(createToolAsync.fulfilled, (state, action) => {
        state.tools = action.payload;
      })
      .addCase(createToolAsync.rejected, (state, action) => {
        state.toolsError = action.payload;
      })
      .addCase(updateToolAsync.pending, (state) => {
        state.toolsError = null;
      })
      .addCase(updateToolAsync.fulfilled, (state, action) => {
        state.tools = action.payload;
      })
      .addCase(updateToolAsync.rejected, (state, action) => {
        state.toolsError = action.payload;
      })
      .addCase(deleteToolAsync.pending, (state) => {
        state.toolsError = null;
      })
      .addCase(deleteToolAsync.fulfilled, (state, action) => {
        state.tools = action.payload;
      })
      .addCase(deleteToolAsync.rejected, (state, action) => {
        state.toolsError = action.payload;
      });
  },
});

export const { selectTool } = toolSlice.actions;

export const getToolsAsync = createAsyncThunk(
  "toolSlice/getTools",
  async (data, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const response = await axios.get("/api/tools");
      return response.data;
    } catch (error) {
      dispatch(setError(`Server error: there was an error loading the tools`));
      return rejectWithValue(error.message);
    }
  }
);

export const createToolAsync = createAsyncThunk(
  "toolSlice/createTool",
  async (newTool, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const response = await axios.post("/api/tools", newTool);
      return response.data;
    } catch (error) {
      console.log(error);
      dispatch(setError(`Server error: there was an error creating the tool`));
      return rejectWithValue(error.message);
    }
  }
);

export const updateToolAsync = createAsyncThunk(
  "toolSlice/updateTool",
  async (tool, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const response = await axios.put(`/api/tools?id=${tool._id}`, tool);
      return response.data;
    } catch (error) {
      console.log(error);
      dispatch(setError(`Server error: there was an error updating the tool`));
      return rejectWithValue(error.message);
    }
  }
);

export const deleteToolAsync = createAsyncThunk(
  "toolSlice/deleteTool",
  async (toolId, { rejectWithValue, dispatch }) => {
    try {
      dispatch(clearError());
      const response = await axios.delete(`/api/tools?id=${toolId}`);
      return response.data;
    } catch (error) {
      console.log(error);
      dispatch(setError(`Server error: there was an error deleting the tool`));
      return rejectWithValue(error.message);
    }
  }
);

export default toolSlice.reducer;
