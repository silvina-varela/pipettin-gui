/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState, useMemo, lazy, Suspense } from "react";
import { useSelector, useDispatch } from "react-redux";
import Typography from "@mui/material/Typography";
import { Container, Box, Skeleton } from "@mui/material";
import { styled } from "@mui/material/styles";
import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp";
import MuiAccordion from "@mui/material/Accordion";
import MuiAccordionSummary from "@mui/material/AccordionSummary";
import MuiAccordionDetails from "@mui/material/AccordionDetails";
import Square from "@mui/icons-material/SquareRounded";
import { ContextMenu } from "./ContextMenu";
import LockIcon from "@mui/icons-material/Lock";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import { getUniqueName } from "../../utils/previewHelpers";
import {
  deleteWorkspaceItem,
  duplicateItem,
  selectPlatform,
  deleteWorkspaceContent,
  updateWorkspaceContent,
  updateWorkspaceItem,
} from "../../redux/slices";

const LazyPlatformContentAccordion = lazy(() => import("./PlatformContentAccordion"));

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  "&:not(:last-child)": {
    borderBottom: 0,
  },
  "&:before": {
    display: "none",
  },
}));

const AccordionSummary = styled((props) => <MuiAccordionSummary {...props} />)(
  ({ theme }) => ({
    flexDirection: "row-reverse",
    backgroundColor: "transparent !important",
    "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
      transform: "rotate(90deg)",
    },
    "& .MuiAccordionSummary-content": {
      marginLeft: theme.spacing(1),
      maxWidth: "90%",
      left: 0,
    },
  })
);

const AccordionDetails = styled(MuiAccordionDetails)(() => ({
  padding: 0,
}));

export const PlatformAccordion = ({ platform, platformIndex, searchTerm }) => {
  const dispatch = useDispatch();

  const { workspace, selectedPlatform, selectedContent } = useSelector(
    (state) => state.workspaceSlice
  );
  const { platformData } = platform;

  // /** Expand accordion */
  const [isExpanded, setIsExpanded] = useState(false);

  const handleExpandIconClick = (event) => {
    event.stopPropagation();
    setIsExpanded(!isExpanded);
  };

  const selectAccordion = (e) => {
    e.preventDefault();
    e.stopPropagation();

    if (platformData) {
      if (platformIndex === selectedPlatform)
        dispatch(selectPlatform({ platform: "", content: [] }));
      else dispatch(selectPlatform({ platform: platformIndex, content: [] }));
    }
  };

  // /** Platform name edit */
  const [isPlatformNameEditing, setIsPlatformNameEditing] = useState(false);

  const [platformName, setPlatformName] = useState(platform.name);

  useEffect(() => {
    setPlatformName(platform.name);
  }, [platform.name]);

  const handlePlatformNameDoubleClick = (event) => {
    event.stopPropagation();
    setIsPlatformNameEditing(true);
  };

  const handlePlatformNameBlur = () => {
    handleSavePlatformName();
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      handlePlatformNameBlur();
    }
  };

  const handlePlatformNameChange = (event) => {
    setPlatformName(event.target.value);
  };

  const handleSavePlatformName = () => {
    if (platform.name !== platformName && platformName.length) {
      const uniqueName = getUniqueName(workspace.items, platformName);
      const updatedPlatform = { ...platform, name: uniqueName };
      setPlatformName(uniqueName);
      dispatch(updateWorkspaceItem(platformIndex, updatedPlatform)).then(() =>
        setIsPlatformNameEditing(false)
      );
    } else {
      setIsPlatformNameEditing(false);
      setPlatformName(platform.name);
    }
  };

  const handlePlatformContentChanges = (contentIndex, changedContent) => {
    console.log("Saving changes to platform with index " + platformIndex + " and content index " + contentIndex)
    dispatch(
      updateWorkspaceContent(platformIndex, contentIndex, changedContent)
    );
  };

  /** Platform delete */

  const handlePlatformDuplicate = (event) => {
    event.preventDefault();
    event.stopPropagation();

    if (selectedPlatform === platformIndex) {
      dispatch(selectPlatform({ platform: "", content: [] }));
    }

    dispatch(duplicateItem(platform));
  };

  const handlePlatformDelete = (event) => {
    event.preventDefault();
    event.stopPropagation();

    if (selectedPlatform === platformIndex) {
      dispatch(selectPlatform({ platform: "", content: [] }));
    }

    dispatch(deleteWorkspaceItem(platformIndex));
  };

  const handleDeleteKeyDown = (event) => {
    // TODO: Check if this needs unification with the "handleKeyDown" hook in "Platform.jsx".
    if (
      event.key === "Delete" && 
      platformIndex === selectedPlatform &&
      !selectedContent.length
    ) {
      console.log(
        "Delete key pressed (platform accordion). Delete platform with index:",
        platformIndex
      );
      handlePlatformDelete(event);
    } else if (
      event.key === "Delete" &&
      platformIndex === selectedPlatform &&
      selectedContent.length
    ) {
      console.log("Delete key pressed (platform accordion). Delete selected content");

      dispatch(deleteWorkspaceContent(selectedPlatform, selectedContent));
      dispatch(selectPlatform({ platform: selectedPlatform, content: [] }));
    }
  };

  const type = {
    TIP_RACK: "TIP RACK",
    TUBE_RACK: "TUBE RACK",
    BUCKET: "BUCKET",
    PETRI_DISH: "PETRI DISH",
    CUSTOM: "CUSTOM",
    ANCHOR: "ANCHOR",
  };

  const [expandedItems, setExpandedItems] = useState({});
  const handleChildAccordionExpand = (itemIndex, isExpanded) => {
    setExpandedItems((prevExpanded) => ({
      ...prevExpanded,
      [itemIndex]: isExpanded,
    }));
  };

  const content = useMemo(
    () => (!platform.content.length ? [] : platform.content),
    [JSON.stringify(platform.content)]
  );

  const matchContent = (a_content) => {
    // Checks if content name matches the search term
    const contentNameMatch = a_content?.name
      .toLowerCase()
      .includes(searchTerm.toLowerCase());
    // Checks if any tag in the content matches the search term
    const tagMatch = a_content?.tags.some((tag) =>
      tag.toLowerCase().includes(searchTerm.toLowerCase())
    );

    // Includes the content if there is a match in content name or any of the tags
    return contentNameMatch || tagMatch;
  }

  const filterContent = () =>
    content
      ? content
          .filter((a_content) => matchContent(a_content))
          // Sort the content list by their "grid index" (not the index in the item's content list).
          .sort((a, b) => a.index - b.index)
      : [];

  const filteredContent = useMemo(
    () => {
      // Retrieve and sort the "content" object of the 
      // selected contents in the current platform.
      return filterContent();
    },
    [searchTerm, JSON.stringify(content)]
  );

  const contextMenuItems = [
    {
      title: "Delete",
      onClick: (event) => handlePlatformDelete(event),
    },
    {
      title: "Duplicate",
      onClick: (event) => handlePlatformDuplicate(event),
    },
  ];

  const [contextMenuClick, setContextMenuClick] = React.useState(null);

  const handleContextMenu = (event) => {
    event.preventDefault();

    if (platformData) {
      dispatch(
        selectPlatform({
          platform: platformIndex,
          content: [],
        })
      );
    }

    setContextMenuClick(
      contextMenuClick === null
        ? {
            mouseX: event.clientX + 2,
            mouseY: event.clientY - 6,
          }
        : null
    );
  };

  const handleClose = () => {
    setContextMenuClick(null);
  };

  const handleLockPlatform = (e, lockState) => {
    e.preventDefault();
    e.stopPropagation();

    dispatch(
      updateWorkspaceItem(platformIndex, { ...platform, locked: lockState })
    );
  };

  if (platform && platformData) {
    return (
      <>
        <ContextMenu
          contextMenuItems={contextMenuItems}
          contextMenuClick={contextMenuClick}
          handleClose={handleClose}
        />
        <Accordion
          expanded={isExpanded}
          className={
            platformIndex === selectedPlatform ? "selectedPlatform" : ""
          }
          onKeyDown={handleDeleteKeyDown}
          onContextMenu={handleContextMenu}
        >
          <AccordionSummary
            aria-controls="platformAccordion"
            id="platformAccordion-header"
            expandIcon={
              <ArrowForwardIosSharpIcon
                sx={{ fontSize: "0.9rem" }}
                onClick={handleExpandIconClick}
              />
            }
            onClick={selectAccordion}
          >
            <Box
              sx={{ marginRight: 0.5, display: "flex", alignItems: "center" }}
            >
              <Square
                sx={{ color: platformData.color || "grey" }}
                fontSize="0.3rem"
              />
            </Box>

            {isPlatformNameEditing ? (
              <input
                type="text"
                value={platformName}
                onChange={handlePlatformNameChange}
                onBlur={handlePlatformNameBlur}
                onKeyDown={handleKeyDown}
                onClick={(e) => e.stopPropagation()}
                style={{ maxWidth: "85%" }}
              />
            ) : (
              <Typography
                sx={{
                  fontSize: "small",
                  fontWeight: "bold",
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                }}
                onDoubleClick={handlePlatformNameDoubleClick}
              >
                {platform.name}
              </Typography>
            )}
            <Box sx={{ position: "absolute", right: 0, marginRight: 1 }}>
              {!platform.locked ? (
                <LockOpenIcon
                  sx={{
                    fontSize: "0.8rem",
                    transition: "color 0.3s ease",
                  }}
                  onClick={(e) => handleLockPlatform(e, true)}
                />
              ) : (
                <LockIcon
                  sx={{
                    fontSize: "0.8rem",
                    transition: "color 0.3s ease",
                  }}
                  onClick={(e) => handleLockPlatform(e, false)}
                />
              )}
            </Box>
          </AccordionSummary>
          <AccordionDetails>
            {isExpanded && (
              <>
                <Typography fontSize="0.6rem" ml={4.5} top={0} mb={2}>
                  {type[platformData.type]}: {platformData.name}
                </Typography>
                <Container disableGutters>
                  <Suspense
                    fallback={
                      <Skeleton
                        variant="rectangular"
                        height={50}
                        sx={{ marginTop: 0.2 }}
                      />
                    }
                  >
                    {
                      content?.map((item, content_index) => ({ item, content_index }))
                        .filter(({ item }) => matchContent(item))
                        // .sort((a, b) => a.content_index - b.content_index)
                        .sort((a, b) => a.item.index - b.item.index)
                        .map(({item, content_index}, filter_index) => {
                          // Iterate over the "filteredContent" array, drawing content accordions.
                          const isExpanded = expandedItems[filter_index] || false;
                          return (
                            <LazyPlatformContentAccordion
                              key={filter_index}
                              contentIndex={content_index}  // Number used to display the "Slot".
                              platformIndex={platformIndex}
                              content={item}
                              isExpanded={isExpanded}
                              setIsExpanded={(isExpanded) =>
                                handleChildAccordionExpand(filter_index, isExpanded)
                              }
                              saveChanges={handlePlatformContentChanges}
                              platformType={platformData.type}
                            />
                          );
                        })
                    }
                  </Suspense>
                </Container>
              </>
            )}
          </AccordionDetails>
        </Accordion>
      </>
    );
  } else {
    return (
      <>
        <ContextMenu
          contextMenuItems={contextMenuItems}
          contextMenuClick={contextMenuClick}
          handleClose={handleClose}
        />
        <Accordion expanded={isExpanded} onContextMenu={handleContextMenu}>
          <AccordionSummary
            aria-controls="platformAccordion"
            id="platformAccordion-header"
            expandIcon={
              <ArrowForwardIosSharpIcon
                sx={{ fontSize: "0.9rem" }}
                onClick={handleExpandIconClick}
              />
            }
            onClick={selectAccordion}
          >
            <Box
              sx={{ marginRight: 0.5, display: "flex", alignItems: "center" }}
            >
              <Square sx={{ color: "gray" }} fontSize="0.3rem" />
            </Box>
            <Typography
              color="error"
              sx={{
                fontSize: "small",
                fontWeight: "bold",
                whiteSpace: "nowrap",
                overflow: "hidden",
                textOverflow: "ellipsis",
              }}
            >
              Unable to load platform
            </Typography>        
          </AccordionSummary>
          <AccordionDetails>
            {isExpanded && (
              <Box display={"flex"} flexWrap={"wrap"}>
                <Typography
                  fontSize="0.6rem"
                  ml={4.5}
                  top={0}
                  mb={2}
                  sx={{
                    display: "inline-block",
                    whiteSpace: "pre-line",
                  }}
                >
                  Item "{platform.name}" with platform ID "{platform.platform}" 
                  was not found in the database
                </Typography>
                <Typography
                  fontSize="0.6rem"
                  ml={4.5}
                  top={0}
                  mb={2}
                  sx={{
                    display: "inline-block",
                    whiteSpace: "pre-line",
                  }}
                >
                  Please delete this platform from the workspace or create a
                  matching platform
                </Typography>
              </Box>
            )}
          </AccordionDetails>
        </Accordion>
      </>
    );
  }
};
