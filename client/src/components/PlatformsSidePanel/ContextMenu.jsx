import DeleteIcon from "@mui/icons-material/Delete";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import { Stack, Typography, Menu, MenuItem } from "@mui/material";

export const ContextMenu = ({
  contextMenuClick,
  handleClose,
  contextMenuItems,
}) => {
  const handleClick = (event, onClick) => {
    event.preventDefault();
    onClick(event);
    handleClose();
  };

  const getIcon = (title) => {
    switch (title.toLowerCase()) {
      case "delete":
        return <DeleteIcon fontSize="small" />;
      case "duplicate":
        return <ContentCopyIcon fontSize="small" />;
      default:
        return null;
    }
  };

  return (
    <div
      onContextMenu={(e) => e.preventDefault()}
      onMouseDownCapture={(e) => {
        if (e.button === 2) handleClose();
      }}
    >
      <Menu
        open={contextMenuClick !== null}
        onClose={handleClose}
        anchorReference="anchorPosition"
        anchorPosition={
          contextMenuClick !== null
            ? { top: contextMenuClick.mouseY, left: contextMenuClick.mouseX }
            : undefined
        }
        transitionDuration={0}
      >
        {contextMenuItems.map((item, index) => (
          <MenuItem
            key={index}
            onClick={(event) => handleClick(event, item.onClick)}
            sx={{ minWidth: 100 }}
          >
            <Stack
              direction={"row"}
              alignItems={"center"}
              spacing={1}
              color="text.secondary"
            >
              {getIcon(item.title)}
              <Typography variant="body2">{item.title}</Typography>
            </Stack>
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
};
