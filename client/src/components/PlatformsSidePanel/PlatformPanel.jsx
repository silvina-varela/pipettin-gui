import { useState, useMemo } from "react";
import { useSelector } from "react-redux";
import { Skeleton, Box } from "@mui/material";
import { PlatformAccordion } from "./PlatformAccordion";
import { SearchBar } from "../SearchBar";

export const PlatformPanel = () => {
  const { workspace } = useSelector(
    (state) => state.workspaceSlice
  );

  const { loading } = useSelector(
    (state) => state.globalSlice
  );

  const [searchTerm, setSearchTerm] = useState("");

  const filterItems = () =>
    workspace.items
      ? workspace.items.filter((platform) => {
          // Checks if platform name matches the search term
          const platformMatch = platform.name
            ?.toLowerCase()
            .includes(searchTerm?.toLowerCase());

          // Checks if any content in the platform matches the search term
          const contentMatch = platform.content?.some((content) => {
            // Check if content name matches the search term
            const contentNameMatch = content.name
              ?.toLowerCase()
              .includes(searchTerm?.toLowerCase());

            // Check if any tag in the content matches the search term
            const tagMatch = content.tags?.some((tag) =>
              tag?.toLowerCase().includes(searchTerm?.toLowerCase())
            );

            // Include the content in the filtered items if there is a match in content name or any of the tags
            return contentNameMatch || tagMatch;
          });

          // Includes the platform in the filtered items if there is a match in platform name or content name
          return platformMatch || contentMatch;
        })
      : [];

  //Receives search input
  const handleSearchInput = (event) => {
    setSearchTerm(event);
  };

  const filteredItems = useMemo(
    () => filterItems(),
    [JSON.stringify(workspace.items), searchTerm]
  );

  if (loading)
    // Load skeleton
    return (
      <>
        <Skeleton variant="rectangular" height={50} sx={{ marginTop: 0.2 }} />
        <Skeleton variant="rectangular" height={50} sx={{ marginTop: 0.2 }} />
        <Skeleton variant="rectangular" height={50} sx={{ marginTop: 0.2 }} />
        <Skeleton variant="rectangular" height={50} sx={{ marginTop: 0.2 }} />
        <Skeleton variant="rectangular" height={50} sx={{ marginTop: 0.2 }} />
      </>
    );
  // Load component
  else
    return (
      <>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            m: 1,
          }}
        >
          <SearchBar searchInput={handleSearchInput} />
        </Box>
        {filteredItems
          ? [
              ...(filteredItems.filter(
                (platform) => platform?.platformData?.type !== "ANCHOR"
              ) || []),
              ...(filteredItems.filter(
                (platform) => platform?.platformData?.type === "ANCHOR"
              ) || []),
            ].map((platform, index) => {
              const stableIndex = workspace.items.findIndex(
                (item) => item.name === platform.name
              );
              return (
                <PlatformAccordion
                  key={index}
                  platform={platform}
                  platformIndex={stableIndex}
                  searchTerm={searchTerm}
                />
              );
            })
          : null}
      </>
    );
};
