/* eslint-disable react/no-unescaped-entities */
import React, { useState } from "react";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Tags from "./Tags";
import { styled } from "@mui/material/styles";
import MuiAccordionDetails from "@mui/material/AccordionDetails";

const InputField = styled(TextField)(() => ({
  "& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button": {
    display: "none",
  },
  "& input[type=number]": {
    MozAppearance: "textfield",
  },
}));

const AccordionDetails = styled(MuiAccordionDetails)(() => ({
  paddingTop: 0,
}));

const VolumeField = React.memo(
  ({ id, volume, maxVolume, handleSaveVolume }) => {
    const [contentVolume, setContentVolume] = useState(volume);
    const [editVolume, setEditVolume] = useState(false);

    const volumeError =
      (!maxVolume && contentVolume !== 0) ||
      (contentVolume !== "" &&
        (Number(contentVolume) < 0 || Number(contentVolume) > maxVolume));

    const handleSave = (e) => {
      e.preventDefault();
      if (!volumeError) handleSaveVolume(contentVolume);
      setEditVolume(false);
    };
    const handleKeyDown = (event) => {
      if (event.key === "Enter") {
        event.target.blur();
      }
    };

    return (
      <>
        {editVolume ? (
          <InputField
            id={id}
            type="number"
            variant="standard"
            sx={{ maxWidth: "35px", marginTop: 0.5 }}
            size="small"
            value={Number(contentVolume).toString()}
            // value={edited ? contentVolume : ""}
            inputProps={{
              style: { fontSize: "0.7rem" },
              max: maxVolume,
            }}
            error={volumeError}
            onChange={(e) => setContentVolume(Number(e.target.value))}
            // onChange={handleChange}
            onKeyDown={handleKeyDown}
            onBlur={handleSave}
          />
        ) : (
          <Typography
            fontSize="0.7rem"
            sx={{
              cursor: "pointer",
              display: "inline-block",
              mr: 0.5,
              "&:hover": { color: "primary" },
            }}
            onClick={() => setEditVolume(true)}
          >
            {contentVolume}
          </Typography>
        )}
        <Typography fontSize="0.7rem" sx={{ display: "inline-block" }}>
          / {maxVolume}{" "}
        </Typography>
        <br />
        <Typography fontSize="0.7rem" ml={3} mt={0.5} color="error">
          {volumeError ? "The value is out of range" : ""}
        </Typography>
      </>
    );
  }
);
const ContentAccordionDetail = React.memo(
  ({
    contentIndex,
    saveChanges,
    platformIndex,
    content,
    platformType
  }) => {
    const handleSaveVolume = (newVolume) => {
      saveChanges(contentIndex, { ...content, volume: newVolume });
    };

    const handleSaveTags = (newTags) => {
      console.log("Saving new tags for index " + contentIndex + " and tags:" + newTags);
      console.log(content)
      // Handled by "handlePlatformContentChanges" in "PlatformAccordion.jsx".
      saveChanges(contentIndex, { ...content, tags: newTags });
    };

    return (
      <AccordionDetails
        id={`contentAccordion-${platformIndex}-${contentIndex}`}
      >
        {platformType === "TUBE_RACK" || platformType === "TIP_RACK" ? (
          <>
            {content.containerData ? null : (
              <Typography
                ml={3}
                mb={2}
                color={"error"}
                fontSize="0.6rem"
                sx={{
                  display: "inline-block",
                  whiteSpace: "pre-line",
                }}
              >
                The container '{content.container}' was not found in the
                database. Please delete this container from the workspace or
                create a matching container
              </Typography>
            )}
            <Typography fontSize="0.7rem" ml={3}>
              Order: {contentIndex + 1}
            </Typography>
            <Typography fontSize="0.7rem" ml={3} mt={0.5}>
              Slot: {content.index}
            </Typography>
            <Typography fontSize="0.7rem" ml={3} mt={0.5}>
              {content.position ? "Column " + content.position.col : ""},{" "}
              {content.position ? "row " + content.position.row : ""}
            </Typography>
            <Typography
              fontSize="0.7rem"
              ml={3}
              mr={1}
              sx={{ display: "inline-block" }}
            >
              Volume (µl):
            </Typography>
            <VolumeField
              volume={content.volume}
              id={"volume" + platformIndex + contentIndex}
              maxVolume={content.containerData?.maxVolume || 0}
              handleSaveVolume={handleSaveVolume}
            />
            <Typography fontSize="0.7rem" ml={3} mt={0.5}>
              Container: {content.containerData?.name || content.container}
            </Typography>
          </>
        ) : platformType === "PETRI_DISH" ? (
          <>
            <Typography fontSize="0.7rem" ml={3}>
              Order: {contentIndex + 1}
            </Typography>
            <Typography fontSize="0.7rem" ml={3}>
              Position (x, y): {content.position ? content.position.x : ""},{" "}
              {content.position ? content.position.y : ""}
            </Typography>
          </>
        ) : (
          <>
            <Typography fontSize="0.7rem" ml={3} mt={0.5}>
              Index: {content.index}
            </Typography>
            <Typography fontSize="0.7rem" ml={3} mt={0.5}>
              Position (x, y): {content.position ? content.position.x : ""},{" "}
              {content.position ? content.position.y : ""}
            </Typography>
            <Typography
              fontSize="0.7rem"
              ml={3}
              mr={1}
              sx={{ display: "inline-block" }}
            >
              Volume (µl):
            </Typography>
            <VolumeField
              volume={content.volume}
              id={"volume" + platformIndex + contentIndex}
              maxVolume={content.containerData?.maxVolume}
              handleSaveVolume={handleSaveVolume}
            />
            <Typography fontSize="0.7rem" ml={3} mt={0.5}>
              Container: {content.containerData?.name}
            </Typography>
          </>
        )}
        <Tags currentTags={content.tags} saveChanges={handleSaveTags} />
      </AccordionDetails>
    );
  }
);

export default ContentAccordionDetail;

VolumeField.displayName = "VolumeField";
ContentAccordionDetail.displayName = "ContentAccordionDetail";
