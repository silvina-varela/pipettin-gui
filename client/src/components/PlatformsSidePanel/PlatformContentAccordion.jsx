import React, { useEffect, useState, lazy, Suspense } from "react";
import { useSelector, useDispatch } from "react-redux";
import Typography from "@mui/material/Typography";
import { styled } from "@mui/material/styles";
import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp";
import MuiAccordion from "@mui/material/Accordion";
import MuiAccordionSummary from "@mui/material/AccordionSummary";
import { ContextMenu } from "./ContextMenu";
import { getUniqueName } from "../../utils/previewHelpers";
import { deleteWorkspaceContent, selectPlatform } from "../../redux/slices";

const LazyContentAccordionDetail = lazy(() => import("./ContentAccordionDetail"));

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(() => ({
  backgroundColor: "transparent",
  "&:not(:last-child)": {
    borderBottom: 0,
  },
  "&:before": {
    display: "none",
  },
}));

const AccordionSummary = styled((props) => <MuiAccordionSummary {...props} />)(
  ({ theme }) => ({
    flexDirection: "row-reverse",
    backgroundColor: "transparent !important",
    "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
      transform: "rotate(90deg)",
    },
    "& .MuiAccordionSummary-content": {
      marginLeft: theme.spacing(1.5),
      maxWidth: "90%",
      left: 0,
    },
  })
);

const PlatformContentAccordion = React.memo(
  ({
    contentIndex,
    saveChanges,
    platformIndex,
    content,
    isExpanded,
    setIsExpanded,
    platformType,
  }) => {
    const dispatch = useDispatch();

    const { selectedPlatform, selectedContent, workspace } = useSelector(
      (state) => state.workspaceSlice
    );

    /** Expand accordion */
    const handleExpandIconClick = (event) => {
      event.stopPropagation();
      setIsExpanded(!isExpanded);
    };

    /** Edit name */
    const [isEditingContentName, setIsEditingContentName] = useState(false);
    const [contentName, setContentName] = useState(content.name);

    useEffect(() => {
      setContentName(content.name);
    }, [content.name]);

    const handleContentNameClick = (event) => {
      event.stopPropagation();
      setIsEditingContentName(true);
    };

    const handleContentNameBlur = () => {
      handleSaveContentName();
      setIsEditingContentName(false);
    };

    const handleKeyDown = (event) => {
      if (event.key === "Enter") {
        handleContentNameBlur();
      }
    };

    const handleContentNameChange = (event) => {
      setContentName(event.target.value);
    };

    const handleSaveContentName = () => {
      const updatedItems = [...workspace.items];
      const allContents = updatedItems.flatMap((item) => item.content);
      const newName = getUniqueName(allContents, contentName);

      saveChanges(contentIndex, { ...content, name: newName });
    };

    /** Select content */
    const isSelected =
      platformIndex === selectedPlatform &&
      selectedContent?.length &&
      selectedContent.includes(content.index - 1)
    /** Select content style */
    const selectedStyle = isSelected ? "selectedContent" : "";

    const selectContent = (e) => {
      e.preventDefault();
      e.stopPropagation();

      if (e.type === "contextmenu") {
        if (
          platformIndex !== selectedPlatform ||
          !selectedContent.includes(content.index - 1)
        ) {
          dispatch(
            selectPlatform({
              platform: platformIndex,
              content: [content.index - 1],
            })
          );
        }
        setContextMenuClick({
          mouseX: e.clientX - 2,
          mouseY: e.clientY - 4,
        });
      } else {
        if (selectedContent.includes(content.index - 1)) {
          let newContentSelected = selectedContent.filter(
            (el) => el !== content.index - 1
          );
          dispatch(
            selectPlatform({
              platform: platformIndex,
              content: newContentSelected,
            })
          );
        } else {
          let newContentSelected = [...selectedContent, content.index - 1];
          dispatch(
            selectPlatform({
              platform: platformIndex,
              content: newContentSelected,
            })
          );
        }
      }
    };

    /** Handle context menu */
    const [contextMenuClick, setContextMenuClick] = React.useState(null);

    const handleContextMenu = (event) => {
      event.preventDefault();
      event.stopPropagation();

      setContextMenuClick({
        mouseX: event.clientX - 2,
        mouseY: event.clientY - 4,
      });
    };

    const handleCloseContextMenu = () => {
      setContextMenuClick(null);
    };

    const handleDeleteContent = () => {
      dispatch(deleteWorkspaceContent(selectedPlatform, selectedContent));
      dispatch(selectPlatform({ platform: selectedPlatform, content: [] }));

      handleCloseContextMenu();
    };

    const contextMenuItems = [
      {
        title: "Delete",
        onClick: handleDeleteContent,
      },
    ];

    return (
      <div onContextMenu={handleContextMenu}>
        <Accordion
          expanded={isExpanded}
          // NOTE: 
          className={selectedStyle}
          onClick={selectContent}
          onContextMenu={selectContent}
          sx={{
            borderLeft: isSelected ? '4px solid #673ab7' : '1px solid #dddddd', // Thicker left border when expanded
            borderColor: isSelected ? '#673ab7' : '#dddddd', // Change color when expanded
            '&.Mui-expanded': {
              borderLeftWidth: '4px',
            },
          }}
        >
          <AccordionSummary
            aria-controls="contentAccordion"
            id="contentAccordion-header"
            expandIcon={
              <ArrowForwardIosSharpIcon
                sx={{ fontSize: "0.9rem" }}
                onClick={handleExpandIconClick}
              />
            }
          >
            {isEditingContentName ? (
              <input
                type="text"
                value={contentName}
                onChange={handleContentNameChange}
                onBlur={handleContentNameBlur}
                onKeyDown={handleKeyDown}
                onClick={(e) => e.stopPropagation()}
              />
            ) : (
              <Typography
                sx={{
                  fontSize: "small",
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                }}
                onDoubleClick={handleContentNameClick}
              >
                {contentName}
              </Typography>
            )}
          </AccordionSummary>
          <Suspense fallback={<div>Loading</div>}>
            <LazyContentAccordionDetail
              contentIndex={contentIndex}
              platformIndex={platformIndex}
              saveChanges={saveChanges}
              content={content}
              platformType={platformType}
            />
          </Suspense>
        </Accordion>
        <ContextMenu
          contextMenuItems={contextMenuItems}
          contextMenuClick={contextMenuClick}
          handleClose={handleCloseContextMenu}
        />
      </div>
    );
  }
);

export default PlatformContentAccordion;

PlatformContentAccordion.displayName = "PlatformContentAccordion";
