import React, { useState, useEffect, useRef } from "react";
import {
  TextField,
  Box,
  Chip,
  Typography,
  IconButton,
  Button,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import { colorHash, getLuma } from "../../utils/colorHash";
import CloseIcon from "@mui/icons-material/Close";
import AddIcon from "@mui/icons-material/Add";

const ListItem = styled("li")(({ theme }) => ({
  margin: theme.spacing(0.3),
}));

const InputField = styled(TextField)(() => ({
  "& input[type=text]": {
    MozAppearance: "textfield",
    fontSize: "0.9rem",
  },
}));

const TagChip = React.memo(({ tag, onDelete }) => {
  const tagColor = colorHash(tag).hex;
  const luma = parseInt(getLuma(tagColor), 10);

  return (
    <ListItem>
      <Chip
        label={tag}
        onDelete={onDelete}
        sx={{
          backgroundColor: tagColor,
          color: luma < 60 ? "white" : "black",
        }}
        size="small"
      />
    </ListItem>
  );
});

const Tags = React.memo(({ currentTags, saveChanges }) => {
  const [newTag, setNewTag] = useState("");
  const [error, setError] = useState(null);
  const [addTags, setAddTags] = useState(false);

  const inputRef = useRef(null);
  useEffect(() => {
    if (addTags && inputRef.current) {
      inputRef.current.focus();
    }
  }, [addTags]);

  const handleDelete = (event, deleteTag) => {
    event.preventDefault();
    event.stopPropagation();

    let newTags = currentTags.filter((tag) => tag !== deleteTag);

    // NOTE: "saveChanges" is "updateWorkspaceContent" from "workspaceSlice.js".
    saveChanges(newTags);
  };

  const handleTagChange = (e) => {
    setNewTag(e.target.value);
    setError(null);
  };

  const handleSave = (e) => {
    e.preventDefault();
    if (newTag && !currentTags.includes(newTag)) {
      const updatedTags = [...currentTags, newTag];
      // NOTE: "saveChanges" is "updateWorkspaceContent" from "workspaceSlice.js".
      saveChanges(updatedTags);
      setNewTag("");
    } else {
      setError("This tag already exists");
    }
  };

  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      handleSave(e);
    }
  };

  return (
    <Box ml={2}>
      <Box
        sx={{
          display: "flex",
          justifyContent: "start",
          flexWrap: "wrap",
          listStyle: "none",
          p: 0.5,
          m: 0,
        }}
        component="ul"
      >
        {currentTags?.map((tag, index) => (
          <TagChip
            key={index}
            tag={tag}
            onDelete={(e) => handleDelete(e, tag)}
          />
        ))}
      </Box>
      {addTags ? (
        <>
          <Box display="flex">
            <InputField
              size="small"
              fullWidth
              placeholder="Add tags..."
              onChange={handleTagChange}
              value={newTag}
              onKeyDown={handleKeyDown}
              inputRef={inputRef} // Attach the ref here
            />
            <IconButton color="neutral" onClick={() => setAddTags(false)}>
              <CloseIcon fontSize="small" />
            </IconButton>
          </Box>
        </>
      ) : (
        <Button
          onClick={() => setAddTags(true)}
          color="inherit"
          size="small"
          startIcon={<AddIcon sx={{ fontSize: "small" }} />}
        >
          Add tags
        </Button>
      )}
      <Typography fontSize="0.7rem" ml={3} mt={0.5} color="error">
        {error && error}
      </Typography>
    </Box>
  );
});

export default Tags;

TagChip.displayName = "TagChip";
Tags.displayName = "Tags";
