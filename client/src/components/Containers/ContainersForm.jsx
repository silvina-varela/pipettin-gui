import { useDispatch } from "react-redux";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import {
  Button,
  TextField,
  Grid,
  MenuItem,
  FormControl,
  InputLabel,
  FormHelperText,
} from "@mui/material";
import { InputNumber } from "../Platforms/FormFields/InputNumber";
import { Select } from "@mui/material";
import {
  createContainerAsync,
  updateContainerAsync,
} from "../../redux/slices";

const containerTypes = [
  {
    value: "tube",
    label: "Tube",
  },
  {
    value: "tip",
    label: "Tip",
  },
  {
    value: "well",
    label: "Well",
  },
];

const createContainersSchema = Yup.object().shape({
  name: Yup.string()
    .min(3, "Must have at least 3 characters")
    .max(100, "Container name can't have more than 100 characters")
    .required("Please enter a name"),
  type: Yup.string().required("Please enter the container type"),
  description: Yup.string().max(
    1000,
    "Description can't have more than 1000 characters"
  ),
  length: Yup.number().required("This field is required"),
  maxVolume: Yup.number().required("This field is required"),
  activeHeight: Yup.number().required("This field is required"),
});

export const ContainersForm = ({
  isEditMode = false,
  container = {},
  closePopup,
}) => {
  const dispatch = useDispatch();

  const createFormInitialValues = {
    name: "",
    type: "",
    description: "",
    length: 50,
    maxVolume: 200,
    activeHeight: 0,
  };

  const initialValues = isEditMode ? container : createFormInitialValues;

  const handleSubmit = async (values) => {
    try {
      if (isEditMode) {
        dispatch(updateContainerAsync(values));
        closePopup();
      } else {
        dispatch(createContainerAsync(values));
        closePopup();
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={createContainersSchema}
      onSubmit={handleSubmit}
    >
      {({ values, touched, errors, handleChange }) => (
        <Form id="containersForm">
          <Grid container spacing={3}>
            <Grid item xs={6}>
              {/* NAME */}
              <TextField
                fullWidth
                variant="standard"
                id="name"
                name="name"
                label="Name*"
                value={values.name}
                onChange={handleChange}
                onKeyDown={(e) => {
                  e.key === "Enter" && e.preventDefault();
                }}
                error={touched.name && Boolean(errors.name)}
                helperText={touched.name && errors.name}
              />
            </Grid>
            {/* TYPE */}
            <Grid item xs={6}>
              <FormControl
                fullWidth
                error={touched.type && Boolean(errors.type)}
              >
                <InputLabel variant="standard" id="type">
                  Type*
                </InputLabel>
                <Select
                  id="type"
                  name="type"
                  value={values.type}
                  onChange={handleChange}
                  variant="standard"
                >
                  {containerTypes.map((container, index) => (
                    <MenuItem key={index} value={container.value}>
                      {container.label}
                    </MenuItem>
                  ))}
                </Select>
                <FormHelperText sx={{ ml: -0.1 }}>
                  {touched.type && errors.type ? errors.type : null}
                </FormHelperText>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              {/* DESCRIPTION */}
              <TextField
                variant="standard"
                multiline
                fullWidth
                rows={8}
                id="description"
                name="description"
                label="Description"
                value={values.description}
                onChange={handleChange}
                onKeyDown={(e) => {
                  e.key === "Enter" && e.preventDefault();
                }}
                error={touched.description && Boolean(errors.description)}
                helperText={touched.description && errors.description}
              />
            </Grid>
            <Grid item xs={12}>
              {/* LENGTH, MAX VOLUME, ACTIVE HEIGHT */}
              <Grid container spacing={5}>
                <Grid item>
                  <InputNumber
                    inputWidth={100}
                    id="length"
                    name="length"
                    label="Length*"
                    value={values.length}
                    onChange={handleChange}
                    error={touched.length && Boolean(errors.length)}
                    helperText={touched.length && errors.length}
                  />
                </Grid>
                <Grid item>
                  <InputNumber
                    inputWidth={100}
                    id="maxVolume"
                    name="maxVolume"
                    label="Max Volume*"
                    value={values.maxVolume}
                    onChange={handleChange}
                    error={touched.maxVolume && Boolean(errors.maxVolume)}
                    helperText={touched.maxVolume && errors.maxVolume}
                  />
                </Grid>
                <Grid item>
                  <InputNumber
                    inputWidth={100}
                    id="activeHeight"
                    name="activeHeight"
                    label="Active Height*"
                    value={values.activeHeight}
                    onChange={handleChange}
                    error={touched.activeHeight && Boolean(errors.activeHeight)}
                    helperText={touched.activeHeight && errors.activeHeight}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sx={{ textAlign: "right" }}>
              {/* SUBMIT */}
              <Button variant="contained" type="submit">
                SUBMIT
              </Button>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};
