import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ListView } from "../List/ListView";
import { useExportData } from "../../hooks/useExportData";
import { ContainersForm } from "./ContainersForm";
import { Popup } from "../Dialogs/Popup";
import {
  deleteContainerAsync,
  getContainersAsync,
  saveWorkspaceAsync
} from "../../redux/slices";
import { GenericImport } from "../NavBar/GenericImport";

export const ContainersList = () => {
  const dispatch = useDispatch();
  const exportData = useExportData();

  const { containers } = useSelector((state) => state.containerSlice);
  const { workspace } = useSelector((state) => state.workspaceSlice);

  /**Popup settings */
  const [openPopup, setOpenPopup] = useState(false);
  const [popupTitle, setPopupTitle] = useState("");
  const [popupComponent, setPopupComponent] = useState(null);
  const [popupWidth, setPopupWidth] = useState("");

  useEffect(() => {
    dispatch(getContainersAsync());
  }, [dispatch]);

  /** Delete containers */
  const deleteAlert = {
    title: "Delete containers",
    message:
      "Are you sure you want to delete the selected container(s)? This process cannot be undone",
    type: "delete",
    yesMessage: "Delete",
  };

  const handleDeleteContainers = async (containers) => {
    try {
      if (containers.length) {
        await Promise.all(
          containers.map((element) =>
            dispatch(deleteContainerAsync(element._id))
          )
        );
      }
      if (workspace?._id) dispatch(saveWorkspaceAsync());
    } catch (error) {
      console.log(error);
    }
  };

  /** Export containers */
  const handleExport = (containers) => {
    exportData(containers, "Containers")
  };

  const handleClosePopup = () => {
    setOpenPopup(false);
    setPopupTitle("");
    setPopupComponent("");
  };

  /** Edit container */
  const handleEdit = (container) => {
    setPopupTitle("Edit container");
    setPopupComponent(<ContainersForm isEditMode={true} container={container} closePopup={handleClosePopup} />);
    setPopupWidth("xs");
    setOpenPopup(true);
  };

  /**Create new container*/
  const handleNewContainer = () => {
    setPopupTitle("Create new container");
    setPopupComponent(<ContainersForm closePopup={handleClosePopup} />);
    setPopupWidth("xs");
    setOpenPopup(true);
  };

  /** Import containers */
  const handleImportContainers = () => {
    setPopupTitle("Import containers");
    setPopupComponent(<GenericImport type="containers" />);
    setPopupWidth("xs");
    setOpenPopup(true);
  };

  const columns = [
    {
      id: "name",
      numeric: false,
      disablePadding: false,
      label: "Name",
      width: 300,
    },
    {
      id: "type",
      numeric: false,
      disablePadding: false,
      label: "Type",
      width: 150,
    },
    {
      id: "description",
      numeric: false,
      disablePadding: false,
      label: "Description",
      width: 350,
    },
  ];

  return (
    <>
      <ListView
        data={containers}
        handleDelete={handleDeleteContainers}
        deleteAlert={deleteAlert}
        handleExport={handleExport}
        handleEdit={handleEdit}
        handleNew={handleNewContainer}
        handleImport={handleImportContainers}
        columns={columns}
        word="containers"
      />
      <Popup
        title={popupTitle}
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        component={popupComponent}
        width={popupWidth}
      />
    </>
  );
};
