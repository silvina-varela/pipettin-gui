import { useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Paper,
  Checkbox,
  IconButton,
  Tooltip,
  Typography,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import EditIcon from "@mui/icons-material/Edit";
import { Alert } from "../Dialogs/Alert";
import { ListToolBar } from "./ListToolBar";
import { ListHeader } from "./ListHeader";
import { formatDate } from "../../utils/formatDate";
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CancelIcon from '@mui/icons-material/Cancel';

/** Table Sorting */
const descendingComparator = (a, b, orderBy) => {
  const valueA = a[orderBy]?.toLowerCase();
  const valueB = b[orderBy]?.toLowerCase();

  if (valueB < valueA) {
    return -1;
  }
  if (valueB > valueA) {
    return 1;
  }
  return 0;
};

const getComparator = (order, orderBy) => {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
};

const stableSort = (array, comparator) => {
  const stabilizedThis = array?.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
};

export const ListView = ({
  data,
  handleDelete,
  deleteAlert,
  handleExport,
  handleClickRow,
  handleEdit,
  columns,
  orderTableBy = 'name',
  orderTableDirection = "asc",
  word,
  showSearchBar,
  handleNew,
  handleImport
}) => {
  /** Table sorting */
  const [order, setOrder] = useState(orderTableDirection);
  const [orderBy, setOrderBy] = useState(orderTableBy);

  const handleRequestSort = (event, property) => {
    event.preventDefault();
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  /** Searchbox */
  const [searchTerm, setSearchTerm] = useState("");

  const rows = data
    ? data.filter((item) =>
      item.name.toLowerCase().includes(searchTerm.toLowerCase())
    )
    : [];

  const handleSearchTerm = (event) => {
    setSearchTerm(event);
  };

  /** Checkbox */
  const [checked, setChecked] = useState([]);

  const handleSelectAllClick = (event) => {
    event.preventDefault();
    if (event.target.checked) {
      const newSelected = rows.map((n) => n);
      setChecked(newSelected);
      return;
    }
    setChecked([]);
  };

  const handleClickCheckBox = (event, row) => {
    event.preventDefault();
    event.stopPropagation();

    let selectedIndex = checked.findIndex((i) => i._id === row._id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(checked, row);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(checked.slice(1));
    } else if (selectedIndex === checked.length - 1) {
      newSelected = newSelected.concat(checked.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        checked.slice(0, selectedIndex),
        checked.slice(selectedIndex + 1)
      );
    }

    setChecked(newSelected);
  };

  const isChecked = (row) => checked.findIndex((i) => i._id === row._id) !== -1;

  /** Hover state */
  const [hoveredRow, setHoveredRow] = useState(null);
  const isHovered = (index) => hoveredRow === index;

  // Elements selected for button actions
  const [selected, setSelected] = useState([]);

  /** Delete elements */
  const [openAlert, setOpenAlert] = useState(false);

  const handleClickDelete = (event, elements) => {
    event.preventDefault();
    event.stopPropagation();

    setSelected(elements);
    setOpenAlert(true);
  };

  const handleConfirmDelete = async (event) => {
    event.preventDefault();

    await handleDelete(selected);
    setSelected([]);
    setChecked([]);
    setOpenAlert(false);
  };

  /** Export elements */
  const handleClickExport = (event, elements) => {
    event.preventDefault();
    event.stopPropagation();

    handleExport(elements);
    setChecked([]);
  };

  /** Edit elements */
  const handleClickEdit = (event, element) => {
    event.preventDefault();
    handleEdit(element);
    setChecked([]);
  };

  /** Add new elements */
  const handleClickNew = (event) => {
    event.preventDefault();
    handleNew();
  };

  /** Import elements */
  const handleClickImport = (event) => {
    event.preventDefault();
    event.stopPropagation();
    handleImport();
  };
  return (
    <>
      <Paper sx={{ minHeight: "70vh" }}>
        <ListToolBar
          numSelected={checked.length}
          onExportClick={handleExport ? (event) => handleClickExport(event, checked) : undefined}
          onDeleteClick={handleDelete ? (event) => handleClickDelete(event, checked) : undefined}
          onNewClick={handleNew ? (event) => handleClickNew(event) : undefined}
          onImportClick={handleImport ? (event) => handleClickImport(event) : undefined}
          searchInput={handleSearchTerm}
          showSearchBar={showSearchBar}
          listType={word}
        />
        <TableContainer sx={{ height: "64vh" }}>
          <Table stickyHeader>
            <ListHeader
              numSelected={checked.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows?.length}
              columns={columns}
            />
            {Object.keys(data).length > 0 ?
              <TableBody>
                {stableSort(rows, getComparator(order, orderBy)).map(
                  (row, index) => {
                    const isItemSelected = isChecked(row);
                    const isRowHovered = isHovered(index);
                    const labelId = `row-${index}`;

                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        aria-checked={isItemSelected}
                        tabIndex={-1}
                        key={index}
                        id={labelId}
                        selected={isItemSelected}
                        sx={{ cursor: handleClickRow && "pointer" }}
                        onClick={handleClickRow ? (e) => handleClickRow(e, row) : undefined}
                        onMouseEnter={() => setHoveredRow(index)} // Show buttons on hover
                        onMouseLeave={() => setHoveredRow(null)}
                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            id={`${labelId}-checkbox`}
                            checked={isItemSelected}
                            onClick={(event) => handleClickCheckBox(event, row)}
                            inputProps={{
                              "aria-labelledby": labelId,
                            }}
                          />
                        </TableCell>
                        {columns?.map((column, index) => {
                          if (
                            column.id === "updatedAt" ||
                            column.id === "createdAt"
                          ) {
                            return (
                              <TableCell align="left" key={index}>
                                <Typography> {formatDate(row[column.id], false)} </Typography>
                              </TableCell>
                            );

                          }
                          else if (
                            column.id === "date"
                          ) {
                            return (
                              <TableCell align="left" key={index}>
                                <Typography> {row[column.id]} </Typography>
                              </TableCell>
                            )
                          }
                          else {
                            const value = row[column.id];
                            const value_rep = typeof value === "boolean" 
                              ? value 
                                ? <Tooltip title="True"><CheckCircleIcon color="success" /></Tooltip> 
                                : <Tooltip title="False"><CancelIcon color="error" /></Tooltip>
                              : value;
                            return (
                              <TableCell
                                key={index}
                                align="left"
                                sx={{
                                  whiteSpace: "nowrap",
                                  overflow: "hidden",
                                  textOverflow: "ellipsis",
                                  maxWidth: column.width,
                                }}
                              >
                                {value_rep}
                              </TableCell>
                            );
                          }
                        })}
                        {/* BUTTONS */}
                        <TableCell align="right" sx={{ minWidth: "15%" }}>
                          {handleEdit && (
                            <Tooltip
                              title="Edit"
                              sx={{ visibility: !isRowHovered && "hidden" }}
                            >
                              <IconButton
                                aria-label="Edit"
                                onClick={(event) =>
                                  handleClickEdit(event, row)
                                }
                              >
                                <EditIcon />
                              </IconButton>
                            </Tooltip>
                          )}
                          {handleExport && (
                            <Tooltip
                              title="Export"
                              sx={{ visibility: !isRowHovered && "hidden" }}
                            >
                              <IconButton
                                aria-label="Export"
                                onClick={(event) =>
                                  handleClickExport(event, [row])
                                }
                              >
                                <FileDownloadIcon />
                              </IconButton>
                            </Tooltip>
                          )}
                          {handleDelete && (
                            <Tooltip
                              title="Delete"
                              sx={{ visibility: !isRowHovered && "hidden" }}
                            >
                              <IconButton
                                onClick={(event) => handleClickDelete(event, [row])}
                                key={row._id}
                                aria-label="delete"
                              >
                                <DeleteIcon />
                              </IconButton>
                            </Tooltip>
                          )}
                        </TableCell>
                      </TableRow>
                    );
                  }
                )}
              </TableBody> : (
                <TableBody>
                  <TableRow>
                    <TableCell colSpan={columns.length + 2} align="center">
                      {`There are no ${word} to show`}
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
          </Table>
        </TableContainer>
        {deleteAlert && <Alert
          title={deleteAlert.title}
          open={openAlert}
          message={deleteAlert.message}
          yesMessage={deleteAlert.yesMessage}
          type={deleteAlert.type}
          onConfirm={handleConfirmDelete}
          onCancel={() => setOpenAlert(false)}
          onClose={() => setOpenAlert(false)}
        />}

      </Paper>
    </>
  );
};

