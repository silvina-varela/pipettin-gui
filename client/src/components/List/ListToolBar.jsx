import { alpha } from "@mui/material/styles";
import {
  Box,
  Toolbar,
  Typography,
  IconButton,
  Tooltip,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { SearchBar } from "../SearchBar";
import AddIcon from "@mui/icons-material/Add";
import FileUploadIcon from "@mui/icons-material/FileUpload";

export const ListToolBar = (props) => {
  const {
    numSelected,
    searchInput,
    onExportClick,
    onDeleteClick,
    showSearchBar = true,
    onNewClick,
    onImportClick,
  } = props;

  return (
    <>
      <Toolbar
        sx={{
          backgroundColor: "background.paper",
          width: "-webkit-fill-available",
          pl: { sm: 2 },
          pr: { xs: 1, sm: 1 },
          ...(numSelected > 0 && {
            bgcolor: (theme) =>
              alpha(
                theme.palette.primary.main,
                theme.palette.action.activatedOpacity
              ),
          }),
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <Box>
          {numSelected > 0 ? (
            <Typography color="inherit" variant="body2" component="div">
              {numSelected} selected
            </Typography>
          ) : showSearchBar ? (
            <SearchBar searchInput={searchInput} />
          ) : (
            <Typography>Choose files to export</Typography>
          )}
        </Box>
        <Box>
          {onNewClick && (
            <Tooltip title="New">
              <span>
                <IconButton onClick={onNewClick}>
                  <AddIcon />
                </IconButton>
              </span>
            </Tooltip>
          )}
          {onImportClick && (
            <Tooltip title="Import">
              <span>
                <IconButton onClick={onImportClick}>
                  <FileUploadIcon />
                </IconButton>
              </span>
            </Tooltip>
          )}
          {onExportClick && (
            <Tooltip title={numSelected > 0 ? "Export" : "Select items"}>
              <span>
                <IconButton
                  onClick={onExportClick}
                  disabled={numSelected === 0}
                >
                  <FileDownloadIcon />
                </IconButton>
              </span>
            </Tooltip>
          )}
          {onDeleteClick && (
            <Tooltip title={numSelected > 0 ? "Delete" : "Select items"}>
              <span>
                <IconButton
                  onClick={onDeleteClick}
                  disabled={numSelected === 0}
                >
                  <DeleteIcon />
                </IconButton>
              </span>
            </Tooltip>
          )}
        </Box>
      </Toolbar>
    </>
  );
};
