import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ListView } from "../List/ListView";
import { ToolForm } from "./ToolForm";
import { useExportData } from "../../hooks/useExportData";
import { Popup } from "../Dialogs/Popup";
import { deleteToolAsync, getToolsAsync } from "../../redux/slices";
import { GenericImport } from "../NavBar/GenericImport";

export const ToolsList = () => {
  const dispatch = useDispatch();
  const { tools } = useSelector((state) => state.toolSlice);
  const exportData = useExportData();

  /**Popup settings */
  const [openPopup, setOpenPopup] = useState(false);
  const [popupTitle, setPopupTitle] = useState("");
  const [popupComponent, setPopupComponent] = useState(null);
  const [popupWidth, setPopupWidth] = useState("");

  useEffect(() => {
    dispatch(getToolsAsync());
  }, [dispatch]);

  /** Delete tools */
  const deleteAlert = {
    title: "Delete tools",
    message:
      "Are you sure you want to delete the selected tool(s)? This process cannot be undone, and a controller restart will be needed",
    type: "delete",
    yesMessage: "Delete",
  };

  const handleDeleteTools = async (tools) => {
    try {
      if (tools.length) {
        await Promise.all(
          tools.map((element) => dispatch(deleteToolAsync(element._id)))
        );
      }
    } catch (error) {
      console.log(error);
    }
  };

  /** Export tools */
  const handleExport = (tools) => {
    exportData(tools, "Tools")
  };

  const handleClosePopup = () => {
    setOpenPopup(false);
    setPopupTitle("");
    setPopupComponent("");
  };

  /** Edit tool */
  const handleEdit = (tool) => {
    setPopupTitle("Edit tool");
    setPopupComponent(<ToolForm isEditMode={true} tool={tool} closePopup={handleClosePopup} />);
    setPopupWidth("md");
    setOpenPopup(true);
  };

  /**Create new tool */
  const handleNewTool = () => {
    setPopupTitle("Create new tool");
    setPopupComponent(<ToolForm closePopup={handleClosePopup} />);
    setPopupWidth("md");
    setOpenPopup(true);
  };

  /** Import tools */
  const handleImportTools = () => {
    setPopupTitle("Import tools");
    setPopupComponent(<GenericImport type="tools" />);
    setPopupWidth("xs");
    setOpenPopup(true);
  };

  const columns = [
    {
      id: "name",
      numeric: false,
      disablePadding: false,
      label: "Name",
      width: 300,
    },
    {
      id: "type",
      numeric: false,
      disablePadding: false,
      label: "Type",
      width: 150,
    },
    {
      id: "enabled",
      numeric: false,
      disablePadding: false,
      label: "Enabled",
      width: 50,
    },
    {
      id: "description",
      numeric: false,
      disablePadding: false,
      label: "Description",
      width: 300,
    },
  ];


  return (
    <>
      <ListView
        data={tools}
        handleDelete={handleDeleteTools}
        deleteAlert={deleteAlert}
        handleExport={handleExport}
        handleEdit={handleEdit}
        handleNew={handleNewTool}
        handleImport={handleImportTools}
        columns={columns}
        word="tools"
      />
      <Popup
        title={popupTitle}
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        component={popupComponent}
        width={popupWidth}
      />
    </>
  );
};
