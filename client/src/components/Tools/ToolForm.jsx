import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  TextField,
  Grid,
  Typography,
  CircularProgress,
  Box,
  Switch,
  FormControlLabel
} from "@mui/material";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import Autocomplete, { createFilterOptions } from "@mui/material/Autocomplete";
import { MultipleSelect } from "./Inputs/MultipleSelect";
import Editor from "@monaco-editor/react";
import {
  createToolAsync,
  updateToolAsync,
  getPlatformsAsync,
} from "../../redux/slices";

import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-json";
import "ace-builds/src-noconflict/ext-searchbox";
import "ace-builds/src-noconflict/worker-json";
//import "ace-builds/src-noconflict/theme-monokai";

const createToolSchema = Yup.object().shape({
  name: Yup.string()
    .min(3, "Tool name must have at least 3 characters")
    .max(100, "Tool name can't have more than 100 characters")
    .required("Please enter a name"),
  description: Yup.string().max(
    1000,
    "Description can't have more than 300 characters"
  ),
  type: Yup.string()
    .min(3, "Tool type must have at least 3 character")
    .required("Please enter the tool type"),
  enabled: Yup.bool()
    .required()
    .default(true)
});

const toolTypes = [
  { label: "Micropipette" },
  { label: "Syringe" },
  { label: "Picker" },
  { label: "Pinner" },
  { label: "Camera" },
  { label: "Probe" },
  { label: "Laser" },
];

const platformTypes = {
  TUBE_RACK: "Tube rack",
  TIP_RACK: "Tip rack",
  PETRI_DISH: "Petri dish",
  BUCKET: "Bucket",
  CUSTOM: "Custom",
};

const stepTypes = {
  TRANSFER: "Transfer",
  WAIT: "Wait",
  HUMAN: "Human",
  COMMENT: "Comment",
  MIX: "Mix",
  JSON: "Json",
};

const filter = createFilterOptions();

export const ToolForm = ({ isEditMode = false, tool = {}, closePopup }) => {
  const dispatch = useDispatch();
  const { platforms } = useSelector((state) => state.platformSlice);
  const [platformList, setPlatformList] = useState({});
  const [selectedPlatformNames, setPlatformNames] = useState([]);
  const [selectedPlatformTypes, setPlatformTypes] = useState([]);
  const [selectedStepTypes, setStepTypes] = useState([]);
  const [parameters, setParameters] = useState(
    isEditMode && Object.prototype.hasOwnProperty.call(tool, "parameters")
      ? JSON.stringify(tool.parameters, null, 2)
      : JSON.stringify({}, null, 2)
  );
  const [validation, setValidation] = useState([]);

  useEffect(() => {
    dispatch(getPlatformsAsync());
  }, [dispatch]);

  useEffect(() => {
    if (platforms.length) {
      let platformValues = {};
      platforms?.forEach(
        (platform) => (platformValues[platform.name] = platform.name)
      );
      setPlatformList(platformValues);
    }
  }, [platforms]);

  const createFormInitialValues = {
    name: "",
    type: "",
    description: "",
    enabled: true,
    parameters: {},
  };

  const initialValues = isEditMode ? tool : createFormInitialValues;

  const handleUpdateForm = (values) => {
    try {
      setParameters(values);
    } catch (error) {
      setValidation(error);
    }
  };

  const validationText = () => {
    if (validation[0].message?.length) {
      if (
        Object.prototype.hasOwnProperty.call(validation[0], "endLineNumber")
      ) {
        return `There is an error on line ${validation[0].endLineNumber}: ${validation[0].message}`;
      } else if (
        Object.prototype.hasOwnProperty.call(validation[0], "element")
      ) {
        if (validation[0].element === "all") return validation[0].message;
        else if (validation[0].element === "empty")
          return validation[0].message;
        else
          return `There is an error on field ${validation[0].element + 1}: ${
            validation[0].message
          }`;
      }
    } else return "";
  };

  const handleValues = (values) => {
    values.parameters = !parameters?.length ? {} : JSON.parse(parameters);
    values.platforms = selectedPlatformNames;
    values.platformTypes = selectedPlatformTypes;
    values.stepTypes = selectedStepTypes;
  };

  const handleSubmit = async (values) => {
    // Clone the values object to avoid direct mutation in handleValues,
    // which is not allowed. Values are read-only.
    const values_copy = { ...values };
    handleValues(values_copy);

    if (Object.keys(parameters).length === 0) {
      setValidation([
        {
          element: "empty",
          message: "You can't create a tool without parameters",
        },
      ]);
    } else {
      setValidation([]);

      if (isEditMode) {
        dispatch(updateToolAsync(values_copy))
          .unwrap()
          .then(() => {
            closePopup();
          })
          .catch((error) => {
            console.log(error);
          });
      } else {
        dispatch(createToolAsync(values_copy))
          .unwrap()
          .then(() => {
            closePopup();
          })
          .catch((error) => {
            console.log(error);
          });
      }
    }
  };

  return (
    <div>
      <Formik
        initialValues={initialValues}
        validationSchema={createToolSchema}
        onSubmit={handleSubmit}
      >
        {({ values, touched, errors, handleChange, setFieldValue }) => (
          <Form id="toolForm">
            <Grid container spacing={3}>

                {/* NAME */}
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  variant="standard"
                  id="name"
                  name="name"
                  label="Name*"
                  value={values.name}
                  onChange={handleChange}
                  onKeyDown={(e) => {
                    e.key === "Enter" && e.preventDefault();
                  }}
                  error={touched.name && Boolean(errors.name)}
                  helperText={touched.name && errors.name}
                />
              </Grid>

              {/* TYPE */}
              <Grid item xs={12} sm={4}>
                <Autocomplete
                  value={values.type}
                  onChange={(event, newValue) => {
                    if (typeof newValue === "string") {
                      setFieldValue("type", { label: newValue });
                    } else if (newValue && newValue.inputValue) {
                      setFieldValue("type", newValue.inputValue);
                    } else {
                      setFieldValue("type", newValue ? newValue.label : "");
                    }
                  }}
                  onKeyDown={(e) => {
                    e.key === "Enter" && e.preventDefault();
                  }}
                  filterOptions={(options, params) => {
                    const filtered = filter(options, params);

                    const { inputValue } = params;
                    // Suggest the creation of a new value
                    const isExisting = options.some(
                      (option) => inputValue === option.label
                    );
                    if (inputValue !== "" && !isExisting) {
                      filtered.push({
                        inputValue,
                        label: `Add "${inputValue}"`,
                      });
                    }

                    return filtered;
                  }}
                  selectOnFocus
                  clearOnBlur
                  handleHomeEndKeys
                  id="type"
                  name="type"
                  options={toolTypes}
                  getOptionLabel={(option) => {
                    // Value selected with enter, right from the input
                    if (typeof option === "string") {
                      return option;
                    }
                    // Add "xxx" option created dynamically
                    if (option.inputValue) {
                      return option.inputValue;
                    }
                    // Regular option
                    return option.label;
                  }}
                  renderOption={(props, option) => (
                    <li {...props}>{option.label}</li>
                  )}
                  freeSolo
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="standard"
                      label="Type*"
                      error={touched.type && Boolean(errors.type)}
                      helperText={touched.type && errors.type}
                    />
                  )}
                />
              </Grid>

              {/* ENABLED */}
              <Grid item xs={12} sm={2}>
                <FormControlLabel
                  control={
                    <Switch
                      id="enabled"
                      name="enabled"
                      checked={!!values.enabled} // Ensure boolean
                      onChange={(e) => setFieldValue('enabled', e.target.checked)}
                      color="primary"
                    />
                  }
                  label="Enabled"
                />
              </Grid>

              {/* DESCRIPTION */}
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  variant="standard"
                  multiline
                  rows={1}
                  id="description"
                  name="description"
                  label="Description"
                  value={values.description}
                  onChange={handleChange}
                  onKeyDown={(e) => {
                    e.key === "Enter" && e.preventDefault();
                  }}
                  error={touched.description && Boolean(errors.description)}
                  helperText={touched.description && errors.description}
                />
              </Grid>

              {/* ASSOCIATED PLATFORMS AND STEPS */}
              <Grid container sx={{ ml: 0.1, mt: 1 }} spacing={3}>
                <Grid item xs={12} sm={6} md={4}>
                  <MultipleSelect
                    label={"Platform Names"}
                    names={platformList}
                    formValues={values.platforms}
                    setFormValues={setPlatformNames}
                    isEditMode={isEditMode}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={4}>
                  <MultipleSelect
                    label={"Platform Types"}
                    names={platformTypes}
                    formValues={values.platformTypes}
                    setFormValues={setPlatformTypes}
                    isEditMode={isEditMode}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={4}>
                  <MultipleSelect
                    label={"Step Types"}
                    names={stepTypes}
                    formValues={values.stepTypes}
                    setFormValues={setStepTypes}
                    isEditMode={isEditMode}
                  />
                </Grid>
              </Grid>

              {/* PARAMETERS */}
              <Grid container sx={{ ml: 3, mt: 3, mb: 1 }}>
                {/* Error messages for parameters */}
                <Grid item xs={12}>
                  <Typography>Tool Parameters:</Typography>
                  <Typography color="error" variant="caption">
                    {errors.parameters}
                  </Typography>
                </Grid>
                <Grid item>
                  <Box height={validation.length ? "auto" : 48}>
                    <Typography color="error" variant="caption">
                      {!validation.length ? " " : validationText()}
                    </Typography>
                  </Box>
                </Grid>
              </Grid>
              {/* Parameter editor component */}
              <Grid container sx={{ display: "flex", flexDirection: "column" }}>
                <AceEditor
                  // Available properties: https://github.com/securingsincity/react-ace/blob/master/docs/Ace.md#available-props
                  name="tool-form-editor"
                  mode="json"
                  // theme="monokai"
                  value={parameters}
                  onChange={(values) => handleUpdateForm(values)} // occurs on document change it has 2 arguments the value and the event.
                  onValidate={(values) => setValidation(values)} // triggered, when annotations are changed
                  width="100%"
                  height="45vh"
                  setOptions={{
                    showLineNumbers: true,
                    tabSize: 2,
                    // Disable the worker to prevent errors loading "worker-json.js".
                    useWorker: false,
                  }}
                />
              </Grid>
              <Grid
                item
                xs={12}
                sx={{ display: "flex", justifyContent: "flex-end" }}
              >
                {/* SUBMIT */}
                <Button variant="contained" type="submit">
                  {isEditMode ? "Save" : "Create"}
                </Button>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );
};
