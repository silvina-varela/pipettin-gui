import { useState, useEffect } from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import ListItemText from '@mui/material/ListItemText';
import Select from '@mui/material/Select';
import Checkbox from '@mui/material/Checkbox';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

export const MultipleSelect = ({ label, names, formValues, setFormValues, isEditMode = false }) => {
  const [selection, setSelection] = useState([]);

  useEffect(()=>{
   setFormValues(selection);
  },[selection])
  
  useEffect(()=>{
    if (isEditMode) setSelection(formValues)
  },[])

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setSelection(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
    
  };

  return (
    <div>
      <FormControl sx={{ width: 260 }}>
        <InputLabel id="multiple-select-label" variant="standard">{label}</InputLabel>
        <Select
          labelId="multiple-select"
          id="multiple-select"
          label={label}
          multiple
          value={ selection}
          onChange={handleChange}
          renderValue={(selected) => {
            const valuesList = selected.map((item) => names[item])
            return valuesList.join(', ')
          }}
          MenuProps={MenuProps}
          variant="standard"
        >
          {Object.keys(names).map((key) => (
            <MenuItem key={key} value={key}>
              <Checkbox checked={selection.indexOf(key) > -1} />
              <ListItemText primary={names[key]}
              />
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}