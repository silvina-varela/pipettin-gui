import { useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { Button, Chip, Grid, Typography } from "@mui/material";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import axios from "axios";
import { useParams } from "react-router-dom";
import { getWorkspacesAsync, getWorkspaceProtocolsAsync, getPlatformsAsync, getToolsAsync, getContainersAsync } from "../../redux/slices";

export const GenericImport = ({ type }) => {
    const { workspaceId } = useParams();
    const dispatch = useDispatch();
    const inputRef = useRef(null);
    const [importList, setImportList] = useState([]);
    const [importMessage, setImportMessage] = useState(null);

    const hasFilesToUpload = importList.length > 0;

    const handleClick = () => {
        inputRef.current.click();
    };

    const handleSubmit = () => {
        switch (type) {
            case "workspaces":
                workspaceSubmit();
                break;
            case "platforms":
                platformSubmit();
                break;
            case "protocols":
                protocolSubmit();
                break;
            case "containers":
                containerSubmit();
                break;
            case "tools":
                toolSubmit();
                break;
            default:
                break;
        }
    };

    const handleResponse = (response, type, action) => {
        console.log(response);

        if (type === "protocols") {
            dispatch(action(workspaceId));

            setImportMessage(
                <>
                    The following protocols were added successfully:
                    <br />
                    <ul>
                        {response.data.map((protocol, index) => (
                            <li key={index}>{protocol}</li>
                        ))}
                    </ul>
                </>
            );
        } else {
            dispatch(action());
            if (type === "workspaces") {

                const importedNames = response.data.map((workspace) => workspace.name);

                const importMessage = (
                    <div>
                        <p>The following workspaces were imported successfully:</p>
                        <ul>
                            {importedNames.map((name, index) => (
                                <li key={index}>{name}</li>
                            ))}
                        </ul>
                    </div>
                );

                setImportMessage(importMessage);

            } else {
                const successMessage = !response.data?.success?.length ? null : (
                    <>
                        The following {type} were created successfully:
                        <ul>
                            {response.data.success.map((item, index) => (
                                <li key={index}>{item}</li>
                            ))}
                        </ul>
                    </>
                );

                const errorMessage = !response.data?.errors?.length ? null : (
                    <>
                        The following {type} were not created:
                        <br />
                        <ul>
                            {response.data.errors.map((item, index) => (
                                <li key={index}>{item}</li>
                            ))}
                        </ul>
                    </>
                );

                setImportMessage(<>
                    {successMessage && successMessage}
                    {errorMessage && errorMessage}
                </>);
            }
        }
    };

    const containerSubmit = async () => {
        try {
            const response = await axios.post("/api/containers/import", importList);
            handleResponse(response, "containers", getContainersAsync);
        } catch (error) {
            console.log(error);
            setImportMessage(error.response?.data);
        }
    };

    const toolSubmit = async () => {
        try {
            const response = await axios.post("/api/tools/import", importList);
            handleResponse(response, "tools", getToolsAsync);
        } catch (error) {
            console.log(error);
            setImportMessage(error.response?.data);
        }
    };

    const protocolSubmit = async () => {
        try {
            const response = await axios.post("/api/protocols/import", {
                protocols: importList,
                workspaceID: workspaceId,
            });
            handleResponse(response, "protocols", getWorkspaceProtocolsAsync);
        } catch (error) {
            console.log(error);
            setImportMessage(error.response?.data);
        }
    };

    const platformSubmit = async () => {
        try {
            const response = await axios.post("/api/platforms/createPlatformsInBulk", importList);
            handleResponse(response, "platforms", getPlatformsAsync);
        } catch (error) {
            console.log(error);
            setImportMessage(error.response?.data);
        }
    };

    const workspaceSubmit = async () => {
        try {
            const response = await axios.post("/api/workspaces/import", importList);
            handleResponse(response, "workspaces", getWorkspacesAsync);
        } catch (error) {
            console.log(error);
            setImportMessage(error.response?.data);
        }
    };

    const handleChooseFiles = (fileInputList) => {
        if (fileInputList.length <= 0) {
            return;
        }

        const newList = [];

        const readNextFile = (index) => {
            if (index >= fileInputList.length) {
                // All files have been processed
                setImportList((prevList) => [...prevList, ...newList]);
                return;
            }

            const fileInput = fileInputList[index];
            const fr = new FileReader();

            fr.onload = function (e) {
                const result = JSON.parse(e.target.result);
                console.log(result);
                const item = { fileName: fileInput.name, content: result };
                newList.push(item);

                // Read the next file
                readNextFile(index + 1);
            };

            fr.readAsText(fileInput);
        };

        // Start processing the files
        readNextFile(0);
    };

    const handleFileInputChange = (e) => {
        const files = e.target.files;
        handleChooseFiles(Array.from(files));
    };

    const handleDelete = (event, deleteItem) => {
        event.preventDefault();
        event.stopPropagation();

        let newList = importList.filter(
            (item) => item !== deleteItem
        );
        setImportList(newList);
    };

    return (
        <>
            {importMessage ? (
                importMessage
            ) : (
                <Grid container direction={"column"} mb={2}>
                    <Grid item>
                        {importList?.map((item, index) => {
                            return (
                                <Chip
                                    sx={{ m: 0.5 }}
                                    key={index}
                                    label={item.fileName}
                                    size="small"
                                    variant="outlined"
                                    onDelete={(event) => handleDelete(event, item)}
                                />
                            );
                        })}
                    </Grid>
                    <Grid item>
                        <Button onClick={handleClick} startIcon={<AddCircleOutlineIcon />}>
                            Add file
                        </Button>
                    </Grid>
                    <Grid item sx={{ ml: 1 }}>
                        {type !== "protocols" ?
                            <Typography>Accepted file format: .json</Typography> :
                            <Typography>
                                Accepted file format: .json
                                <br />
                                Please import only one file at a time
                            </Typography>
                        }

                    </Grid>
                    <Grid item mt={2} sx={{ textAlign: "right" }}>
                        <input
                            style={{ display: "none" }}
                            ref={inputRef}
                            type="file"
                            accept=".json"
                            value=""
                            name="files"
                            multiple
                            onChange={handleFileInputChange}
                        />
                        <Button
                            type="submit"
                            variant="contained"
                            onClick={handleSubmit}
                            style={{
                                pointerEvents: hasFilesToUpload ? "auto" : "none", // Enable pointer events if there are files
                                opacity: hasFilesToUpload ? 1 : 0.5, // Adjust opacity to make it look disabled
                            }}
                        >
                            UPLOAD
                        </Button>
                    </Grid>
                </Grid>
            )}
        </>
    );
};
