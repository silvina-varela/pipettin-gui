import { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { AppBar, Box, Toolbar, Container } from "@mui/material";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";
import RestartAltIcon from '@mui/icons-material/RestartAlt';
import { NavMenus } from "./NavMenus";
import { ActionButton } from "../Buttons/ActionButton";
import { Button, Tooltip } from "@mui/material";
import { HelpButton } from "../Buttons/HelpButton";
import { SettingsButton } from "../Buttons/SettingsButton";
import { Joystick } from "../Buttons/Joystick";
import axios from "axios";
import { Alert } from "../Dialogs/Alert";
import { StatusController } from "../Buttons/StatusController";
import { useSocketContext } from "../../hooks/useSocket";
import { useSnackAlert } from "../../hooks/useSnackAlert";
import { saveWorkspaceAsync } from "../../redux/slices";

export const NavBar = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [saveWorkspaceAlert, setSaveWorkspaceAlert] = useState(false);

  const { workspaceUpdated } = useSelector((state) => state.workspaceSlice);

  const addSnack = useSnackAlert();
  const { controllerStatus, handleConfirm, handleCancel, alert } =
    useSocketContext();

  // Check if the current route is "/workspace"
  const location = useLocation();
  const isWorkspaceRoute = location.pathname.includes("/workspace");

  const handleRestart = async (e) => {
    e.preventDefault();

    return await axios
      .post("/api/robot/restart-controller")
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error.response.data);
        addSnack(`Failed to restart the controller: ${JSON.stringify(error.response.data)}`, "error");
      });
  };

  const handleStop = async (e) => {
    e.preventDefault();

    return await axios
      .post("/api/robot/stop")
      .then((response) => {
        console.log(response.data);
      })
      .catch((error) => {
        console.log(error.response.data);
        addSnack(`Failed to stop the controller: ${JSON.stringify(error.response.data)}`, "error");
      });
  };

  const handleClickLogo = (e) => {
    e.preventDefault();

    if (isWorkspaceRoute && workspaceUpdated) {
      setSaveWorkspaceAlert(true);
    } else navigate("/");
  };

  const saveWorkspaceAndNavigate = () => {
    dispatch(saveWorkspaceAsync())
      .unwrap()
      .then(() => {
        navigate("/");
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => setSaveWorkspaceAlert(false));
  };

  return (
    <>
      <AppBar position="fixed" color="default" sx={{ zIndex: 1201 }}>
        <Container maxWidth="xl">
          <Toolbar disableGutters sx={{ height: "3vh" }}>
            <img
              src="/images/Pipettin.svg"
              style={{ height: "3rem", marginRight: "10px", cursor: "pointer" }}
              alt="logo"
              onClick={handleClickLogo}
            />
            <NavMenus />

            <Box sx={{ flexGrow: 0 }} mr={1}>
              <Tooltip title="Restart the Controller">
                <Button
                  color="grey"
                  variant="contained"
                  onClick={handleRestart}
                  sx={{
                    minHeight: "36px", // Set a minimum height for the button
                  }}
                  ><RestartAltIcon/></Button>
              </Tooltip>
            </Box>

            <Box sx={{ flexGrow: 0 }}>
              {isWorkspaceRoute && (
                <ActionButton
                  //TODO: Disable when protocol is not running
                  name="STOP"
                  tooltip="Emergency stop!"
                  startIcon={<ErrorOutlineIcon />}
                  color="error"
                  handleClick={handleStop}
                />
              )}
            </Box>
            <Box ml={3}>
              <StatusController controllerStatus={controllerStatus} />
            </Box>
            <Box ml={3}>
              <SettingsButton />
            </Box>
            <Box ml={3}>
              <HelpButton />
            </Box>
            <Box ml={3}>
              <Joystick />
            </Box>
          </Toolbar>
        </Container>
        <Alert
          title="Leave workspace"
          open={saveWorkspaceAlert}
          message="Do you want to save this workspace before closing it?"
          onConfirm={() => saveWorkspaceAndNavigate()}
          onCancel={() => {
            setSaveWorkspaceAlert(false);
            navigate("/");
          }}
          onClose={() => setSaveWorkspaceAlert(false)}
        />

        <Alert
          open={alert.open}
          title={alert.title}
          message={alert.message}
          onConfirm={handleConfirm}
          onCancel={handleCancel}
          onClose={handleCancel}
          yesMessage={alert.yesButton}
          noMessage={alert.noButton}
        />
      </AppBar>
      <Toolbar />
    </>
  );
};
