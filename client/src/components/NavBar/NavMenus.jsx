import { useState } from "react";
import { Box } from "@mui/material";
import { Popup } from "../Dialogs/Popup";
import { PlatformsList } from "../Platforms/PlatformsList";
import { WorkspacesList } from "../Workspaces/WorkspacesList";
import { NavBarMenu } from "./NavBarMenu";
import { ProtocolList } from "../Protocols/ProtocolList";
import { ToolsList } from "../Tools/ToolsList";
import { ContainersList } from "../Containers/ContainersList";
import { TemplateList } from "../Protocols/TemplateList";

export const NavMenus = () => {
  // Handles popup
  const [openPopup, setOpenPopup] = useState(false);
  const [popupTitle, setPopupTitle] = useState("");
  const [openComponent, setOpenComponent] = useState("");
  const [popupWidth, setPopupWidth] = useState("");

  const handleOpenPopup = (event, title, popupSize, component) => {
    event.preventDefault();
    setPopupTitle(title);
    setOpenComponent(component);
    setOpenPopup(true);
    setPopupWidth(popupSize);
  };

  const handleClosePopup = () => {
    setOpenPopup(false);
    setPopupTitle("");
    setOpenComponent("");
  };

  const handleMenuClick = (event, component) => {
    handleOpenPopup(event, component.title, component.popupSize, component.component);
  };

  const handleChange = (newTitle, newComponent, popupSize) => {
    setPopupTitle(newTitle);
    setOpenComponent(newComponent);
    setOpenPopup(true);
    if (popupSize) setPopupWidth(popupSize);
  };


  const menuItems = [
    {
      name: "Workspaces",
      component: {
        title: "Workspaces",
        popupSize: "lg",
        component: <WorkspacesList closePopup={handleClosePopup} changeTo={handleChange}/>,
      },
    },
    {
      name: "Platforms",
      component: {
        title: "Platforms",
        popupSize: "lg",
        component: <PlatformsList closePopup={handleClosePopup}/>,
      },
    },
    {
      name: "Containers",
      component: {
        title: "Containers",
        popupSize: "lg",
        component: <ContainersList closePopup={handleClosePopup}/>,
      },
    },
    {
      name: "Protocols",
      component: {
        title: "Protocols",
        popupSize: "lg",
        component: <ProtocolList closePopup={handleClosePopup} />,
      },
    },
    {
      name: "Templates",
      component: {
        title: "Protocol templates",
        popupSize: "lg",
        component: <TemplateList closePopup={handleClosePopup}/>,
      },
    },
    {
      name: "Tools",
      component: {
        title: "Tools",
        popupSize: "lg",
        component: <ToolsList closePopup={handleClosePopup}/>,
      },
    },
  ];

  return (
    <Box sx={{ flexGrow: 1, display: "flex" }}>
      <NavBarMenu name="Menu" menuItems={menuItems} onItemClick={handleMenuClick} />
      <Popup
        title={popupTitle}
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        component={openComponent}
        width={popupWidth}
      />
    </Box>
  );
};
