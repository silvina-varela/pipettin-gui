import { useState } from "react";
import { Formik, Field, Form } from "formik";
import * as yup from "yup";
import {
  Typography,
  Box,
  TextField,
  Popover,
  IconButton,
  Grid,
} from "@mui/material";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import ControllerEditor from "./ControllerEditor";

const validationSchema = yup.object({
  joystickUrl: yup
    .string("Enter a joystick URL")
    .test("no-space", "URL cannot contain spaces", (value) => {
      if (value) {
        return !/\s/.test(value); // Check if the value contains any space character
      }
      return true; // If the value is empty, consider it valid
    })
    .required("Joystick URL is required"),
  helpLink: yup
    .string("Enter a help link")
    .test("no-space", "Help link cannot contain spaces", (value) => {
      if (value) {
        return !/\s/.test(value); // Check if the value contains any space character
      }
      return true; // If the value is empty, consider it valid
    }),
});

export const ControllerSettingsForm = ({ handleSave, settings }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [popoverMessage, setPopoverMessage] = useState("");
  const [validation, setValidation] = useState([]);

  const handleInfo = (message) => {
    setPopoverMessage(message);
  };

  const initialValues = settings.controller ? settings.controller : {};

  const joystickHelpMessage =
    "This is the link for the joystick at the top of the page";

  const helpLinkHelpMessage =
    "This is the link for the help button at the top of the page";

  const configurationHelpMessage =
    "Provide configuration parameters for the controller";

  const handleKeyPress = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      event.target.blur();
    }
  };

  const validationText = () => {
    if (validation[0].message?.length) {
      if (Object.prototype.hasOwnProperty.call(validation[0], "endLineNumber")) {
        return `There is an error on line ${validation[0].endLineNumber}: ${validation[0].message}`;
      } else if (Object.prototype.hasOwnProperty.call(validation[0], "element")) {
        if (validation[0].element === "all") return validation[0].message;
        else if (validation[0].element === "empty")
          return validation[0].message;
        else
          return `There is an error on field ${validation[0].element + 1}: ${
            validation[0].message
          }`;
      }
    } else return "";
  };

  return (
    <Box sx={{ p: 3 }}>
      <Typography
        variant="h6"
        sx={{ marginBottom: "2rem", borderBottom: "1px solid #ccc" }}
      >
        Controller Settings
      </Typography>
      <Box sx={{ marginTop: "2rem" }}>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
        >
          {({ values, errors, handleChange }) => (
            <Form>
              <Grid container spacing={4} direction={"column"}>
                <Grid item>
                  <Grid
                    container
                    direction={"row"}
                    alignItems={"center"}
                    spacing={1}
                  >
                    <Grid item>
                      <Typography
                        variant="h8"
                        color="initial"
                        fontWeight="fontWeightBold"
                      >
                        Joystick URL
                      </Typography>
                      <IconButton
                        onClick={(event) => {
                          setAnchorEl(event.currentTarget);
                          handleInfo(joystickHelpMessage);
                        }}
                        size="small"
                      >
                        <InfoOutlinedIcon sx={{ fontSize: 15 }} />
                      </IconButton>
                    </Grid>
                    <Grid item>
                      <Field
                        as={TextField}
                        id="joystickUrl"
                        label=""
                        type="text"
                        InputLabelProps={{
                          shrink: false,
                        }}
                        value={values.joystickUrl}
                        variant="outlined"
                        size="small"
                        sx={{
                          minWidth: 300,
                          "& .MuiFormHelperText-root.Mui-error": {
                            position: "absolute",
                            top: "100%",
                          },
                        }}
                        name="joystickUrl"
                        onChange={handleChange}
                        onBlur={() => handleSave(values, "controller", errors)}
                        onKeyDown={handleKeyPress}
                        placeholder="Eg: http://localhost:8000/"
                        error={Boolean(errors.joystickUrl)}
                        helperText={errors.joystickUrl}
                      />
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Grid
                    container
                    direction={"row"}
                    alignItems={"center"}
                    spacing={1}
                  >
                    <Grid item>
                      <Typography
                        variant="h8"
                        color="initial"
                        fontWeight="fontWeightBold"
                      >
                        Help link
                      </Typography>
                      <IconButton
                        onClick={(event) => {
                          setAnchorEl(event.currentTarget);
                          handleInfo(helpLinkHelpMessage);
                        }}
                        size="small"
                      >
                        <InfoOutlinedIcon sx={{ fontSize: 15 }} />
                      </IconButton>
                    </Grid>
                    <Grid item>
                      <Field
                        as={TextField}
                        id="helpLink"
                        label=""
                        type="text"
                        InputLabelProps={{
                          shrink: false,
                        }}
                        value={values.helpLink}
                        variant="outlined"
                        size="small"
                        sx={{
                          minWidth: 300,
                          "& .MuiFormHelperText-root.Mui-error": {
                            position: "absolute",
                            top: "100%",
                          },
                        }}
                        name="helpLink"
                        onChange={handleChange}
                        onBlur={() => handleSave(values, "controller", errors)}
                        onKeyDown={handleKeyPress}
                        placeholder="Eg: https://gitlab.com/pipettin-bot/pipettin-gui"
                        error={Boolean(errors.helpLink)}
                        helperText={errors.helpLink}
                      />
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Grid container direction={"column"} spacing={1}>
                    <Grid item>
                      <Typography
                        variant="h8"
                        color="initial"
                        fontWeight="fontWeightBold"
                      >
                        Configuration
                      </Typography>
                      <IconButton
                        onClick={(event) => {
                          setAnchorEl(event.currentTarget);
                          handleInfo(configurationHelpMessage);
                        }}
                        size="small"
                      >
                        <InfoOutlinedIcon sx={{ fontSize: 15 }} />
                      </IconButton>
                    </Grid>
                    <Grid item height={validation.length ? "auto" : 32}>
                      <Typography color="error" variant="caption">
                        {!validation.length ? " " : validationText()}
                      </Typography>
                    </Grid>
                    <Grid item>
                      <ControllerEditor
                        setValidation={setValidation}
                        handleSave={handleSave}
                        validation={validation}
                        formValues={values}
                        settings={settings}
                      />
                    </Grid>
                  </Grid>
                </Grid>

                <Popover
                  id="popoverSettings"
                  open={Boolean(anchorEl)}
                  anchorEl={anchorEl}
                  onClose={() => setAnchorEl(null)}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "right",
                  }}
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "left",
                  }}
                  sx={{ maxWidth: "60vw", maxHeight: "50vh" }}
                >
                  <Box p={1}>
                    <Typography variant="caption">{popoverMessage}</Typography>
                  </Box>
                </Popover>
              </Grid>
            </Form>
          )}
        </Formik>
      </Box>
    </Box>
  );
};
