import React, { useState, useEffect } from 'react';
import { Typography, Box, TextField, Button } from '@mui/material'; 
import axios from 'axios';

export const ApiSettingsForm = ({ submitURL, currentURL }) => {
  
  // Get an initial display value for the API URL form.
  const [apiUrl, setApiUrl] = useState(currentURL || axios.defaults.baseURL);
  console.log("Displaying API URL:", apiUrl, `(current URL: ${currentURL} / axios URL: ${axios.defaults.baseURL})`);

  const handleChange = (event) => {
    setApiUrl(event.target.value);
  };

  const handleSubmit = () => {
    // Basic validation: Check if the input is a valid URL
    const urlPattern = /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/i; 
    if (!urlPattern.test(apiUrl)) {
      alert('Please enter a valid URL.');
      return;
    }
    // Call handleSave through submitURL in Settings.jsx to update the URL.
    submitURL(apiUrl);
  };

  return (
    <Box sx={{ p: 3 }}>
      <Typography variant="h6" sx={{ marginBottom: '2rem', borderBottom: '1px solid #ccc' }}>
        API Settings
      </Typography>
      <Box sx={{ marginTop: '2rem' }}>
        <TextField
          label="API Address"
          variant="outlined"
          value={apiUrl}
          onChange={handleChange}
          fullWidth
        />
        <Button variant="contained" onClick={handleSubmit} sx={{ mt: 2 }}>
          Save
        </Button>
      </Box>
    </Box>
  );
}; 
