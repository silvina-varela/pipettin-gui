/* eslint-disable react/no-unescaped-entities */
import { useState } from "react";
import { Formik, Field, Form } from "formik";
import * as yup from "yup";
import {
  Typography,
  Box,
  TextField,
  Popover,
  IconButton,
  Grid,
  Button,
  Autocomplete
} from "@mui/material";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import { Alert } from "../Dialogs/Alert";
import axios from "axios";
import { useDispatch } from "react-redux";
import { changeDatabaseAsync } from "../../redux/slices";

// Validation schema for form
const validationSchema = yup.object({
  uri: yup
    .string("Enter database URI")
    .matches(/^[0-9].*:[0-9].*$/, "Invalid URI format")
    .test("no-space", "URI cannot contain spaces", (value) => {
      if (value) {
        return !/\s/.test(value); // Check if the value contains any space character
      }
      return true; // If the value is empty, consider it valid
    })
    .required("URI is required"),
  name: yup
    .string("Enter a database name")
    .test("no-space", "Database name cannot contain spaces", (value) => {
      if (value) {
        return !/\s/.test(value); // Check if the value contains any space character
      }
      return true; // If the value is empty, consider it valid
    })
    .required("Database name is required"),
});

export const DatabaseSettingsForm = ({ settings, existingDatabases }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [popoverMessage, setPopoverMessage] = useState("");
  const dispatch = useDispatch();

  const handleSaveDatabaseValues = (values, errors) => {
    if (!errors || !Object.values(errors).length) {
      dispatch(changeDatabaseAsync(values));
    }
  };

  const handleInfo = (message) => {
    setPopoverMessage(message);
  };

  const uriHelpMessage =
    "When changing the MongoDB URI, you should make sure to first find an available port and then change the MongoDB connection with the command mongod --port [PORT]. If it fails you might be missing the data folder. Set your data folder with mongod --dbpath [folder path].";

  const databaseNameMessage =
    "When changing the database name, all the data will remain in the previous database and will not be transferred to the new database";

  const handleKeyPress = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      event.target.blur();
    }
  };

  /** Reset database */
  const [openAlert, setOpenAlert] = useState(false);
  const [alert, setAlert] = useState({
    yesMessage: "",
    noMessage: "",
    type: "",
    message: "",
    onConfirm: () => {},
  });

  const handleConfirmReset = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post("/api/settings/resetDatabase");

      setAlert({
        yesMessage: "Ok",
        noMessage: "",
        title: "Reset database",
        message: response.data,
        onConfirm: () => setOpenAlert(false),
        type: "",
      });
    } catch (error) {
      console.log(error);
      setAlert({
        yesMessage: "Ok",
        noMessage: "",
        title: "Reset database",
        message: error.response.data,
        onConfirm: () => setOpenAlert(false),
      });
    }
  };

  const handleResetDatabase = (event) => {
    event.preventDefault();
    setOpenAlert(true);

    setAlert({
      yesMessage: "Reset database",
      noMessage: "Cancel",
      type: "delete",
      message: (
        <>
          Are you sure you want to reset the database{" "}
          <span style={{ fontWeight: "bold" }}>"{settings.database.name}"</span>?
          <br />
          This will delete all documents from the database and you won't be able
          to recover them.
          <br />
          <span style={{ fontWeight: "bold" }}>The process is irreversible.</span>
        </>
      ),
      title: "Reset database",
      onConfirm: (e) => handleConfirmReset(e),
    });
  };

  const handleConfirmSeed = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post("/api/settings/seedDatabase");

      setAlert({
        yesMessage: "Ok",
        noMessage: "",
        title: "Seed database",
        message: response.data,
        onConfirm: () => setOpenAlert(false),
      });
    } catch (error) {
      console.log(error);
      setAlert({
        yesMessage: "Ok",
        noMessage: "",
        title: "Seed database",
        message: error.response.data,
        onConfirm: () => setOpenAlert(false),
      });
    }
  };

  const handleSeed = (event) => {
    event.preventDefault();
    setOpenAlert(true);

    setAlert({
      yesMessage: "Seed database",
      noMessage: "Cancel",
      message: <>Do you want to fill the database with default data?</>,
      title: "Seed database",
      onConfirm: (e) => handleConfirmSeed(e),
    });
  };

  return (
    <Box sx={{ p: 3 }}>
      <Typography
        variant="h6"
        sx={{ marginBottom: "2rem", borderBottom: "1px solid #ccc" }}
      >
        Database Settings
      </Typography>
      <Box sx={{ marginTop: "2rem" }}>
        <Formik
          initialValues={settings.database}
          validationSchema={validationSchema}
        >
          {({ values, errors, handleChange, setFieldValue }) => (
            <Form>
              <Grid container spacing={4} direction={"column"}>
                <Grid item>
                  <Grid
                    container
                    direction={"row"}
                    alignItems={"center"}
                    spacing={1}
                  >
                    <Grid item>
                      <Typography variant="h8" fontWeight="fontWeightBold">
                        Database URI{" "}
                      </Typography>
                      <IconButton
                        onClick={(event) => {
                          setAnchorEl(event.currentTarget);
                          handleInfo(uriHelpMessage);
                        }}
                        size="small"
                      >
                        <InfoOutlinedIcon sx={{ fontSize: 15 }} />
                      </IconButton>
                    </Grid>
                    <Grid item>
                      <Field
                        as={TextField}
                        id="uri"
                        label=""
                        type="text"
                        InputLabelProps={{
                          shrink: false,
                        }}
                        value={values.uri}
                        variant="outlined"
                        size="small"
                        sx={{
                          minWidth: 300,
                          "& .MuiFormHelperText-root.Mui-error": {
                            position: "absolute",
                            top: "100%",
                          },
                        }}
                        name="uri"
                        onChange={handleChange}
                        onBlur={() => handleSaveDatabaseValues(values, errors)}
                        onKeyDown={handleKeyPress}
                        placeholder="Eg: 127.0.0.1:27017"
                        error={Boolean(errors.uri)}
                        helperText={errors.uri}
                      />
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item>
                  <Grid container alignItems="center" spacing={1}>
                    <Grid item>
                      <Typography
                        variant="h8"
                        color="initial"
                        fontWeight="fontWeightBold"
                      >
                        Database name
                      </Typography>
                      <IconButton
                        onClick={(event) => {
                          setAnchorEl(event.currentTarget);
                          handleInfo(databaseNameMessage);
                        }}
                        size="small"
                      >
                        <InfoOutlinedIcon sx={{ fontSize: 15 }} />
                      </IconButton>
                    </Grid>
                    <Grid item>
                      <Autocomplete
                        id="name"
                        freeSolo
                        options={existingDatabases}
                        value={values.name}
                        onInputChange={(event, newValue) => {
                          setFieldValue("name", newValue);
                        }}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            label=""
                            placeholder="Select or type database name"
                            variant="outlined"
                            size="small"
                            sx={{
                              minWidth: 300,
                              "& .MuiFormHelperText-root.Mui-error": {
                                position: "absolute",
                                top: "100%",
                              },
                            }}
                            error={Boolean(errors.name)}
                            helperText={errors.name}
                            name="name"
                            onChange={handleChange}
                            onBlur={() => handleSaveDatabaseValues(values, errors)}
                            onKeyDown={handleKeyPress}
                          />
                        )}
                      />
                    </Grid>
                  </Grid>
                </Grid>
                <Popover
                  id="popoverSettings"
                  open={Boolean(anchorEl)}
                  anchorEl={anchorEl}
                  onClose={() => setAnchorEl(null)}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "right",
                  }}
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "left",
                  }}
                >
                  <Typography sx={{ p: 2, maxWidth: 300 }}>
                    {popoverMessage}
                  </Typography>
                </Popover>
                <Grid item>
                  <Button
                    variant="contained"
                    size="small"
                    onClick={handleResetDatabase}
                    color="error"
                  >
                    Reset Database
                  </Button>
                  <Button
                    variant="contained"
                    size="small"
                    onClick={handleSeed}
                    sx={{
                      ml: 2,
                      backgroundColor: '#4caf50', // Green color
                      '&:hover': {
                        backgroundColor: '#388e3c', // Darker green on hover
                      }
                    }}
                    // color="primary"
                  >
                    Seed Database
                  </Button>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </Box>
      <Alert
        open={openAlert}
        // setOpen={setOpenAlert}
        onCancel={() => setOpenAlert(false)}
        onClose={() => setOpenAlert(false)}
        {...alert} />
    </Box>
  );
};
