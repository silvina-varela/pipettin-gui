import React, { useState, useEffect } from "react";
import { CircularProgress } from "@mui/material";
import Editor from "@monaco-editor/react";

const ControllerEditor = React.memo(
  ({ setValidation, handleSave, validation, formValues }) => {
    const [editor, setEditor] = useState(null);

    useEffect(() => {
      if (editor) {
        editor.onDidBlurEditorWidget(() => {
          if (!validation.length) {
            handleSave(formValues, "controller");
          }
        });
      }
    }, [editor, formValues.joystickUrl]);

    const handleUpdateForm = (editorValues) => {
      try {
        formValues.configuration = JSON.parse(editorValues);
        setValidation([]);
      } catch (error) {
        setValidation(error);
      }
    };

    const handleMount = (event) => {
      setEditor(event);
    };

    const containerStyle = {
      display: "flex",
      flexDirection: "column",
      height: "60vh", // Full viewport height
      border: "2px solid #1976d2", // Blue border
      borderRadius: "8px",
      padding: "5px",
      backgroundColor: "#f5f5f5",
    };

    return (
      <div style={containerStyle}>
        <Editor
          name="configuration"
          id="configuration"
          // height="100vh"
          width="100%"
          defaultLanguage="json"
          // theme={editorTheme ? undefined : "vs-dark"}
          value={JSON.stringify(formValues.configuration, null, 2)}
          onChange={(values) => handleUpdateForm(values)}
          onValidate={(values) => setValidation(values)}
          onMount={handleMount}
          options={{
            minimap: { enabled: false }, // Disable the minimap
            scrollbar: {
              verticalScrollbarSize: "6px",
              horizontalScrollbarSize: "6px",
            },
          }}
          loading={<CircularProgress />}
        />
      </div>
    );
  }
);

export default ControllerEditor;

ControllerEditor.displayName = "ControllerEditor";
