import { useState } from "react";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import {
  Typography,
  Box,
  TextField,
  Grid,
  IconButton,
  Popover,
  SvgIcon,
} from "@mui/material";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";

const validationSchema = Yup.object().shape({
  defaultPadding: Yup.object().shape({
    top: Yup.number().min(0, "Padding values cannot be negative").nullable(),
    bottom: Yup.number().min(0, "Padding values cannot be negative").nullable(),
    left: Yup.number().min(0, "Padding values cannot be negative").nullable(),
    right: Yup.number().min(0, "Padding values cannot be negative").nullable(),
  }),
  defaultWidth: Yup.number()
    .min(0, "Width value cannot be negative")
    .nullable(),
  defaultLength: Yup.number()
    .min(0, "Length value cannot be negative")
    .nullable(),
  defaultHeight: Yup.number(),
  zoom: Yup.number().min(0, "Zoom value cannot be negative").nullable(),
  roundUp: Yup.number().min(0, "Round up value cannot be negative").nullable(),
  incrementPosition: Yup.number()
    .min(0, "Position increment value cannot be negative")
    .nullable(),
});

export const WorkspaceSettingsForm = ({ handleSave, settings }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [popoverMessage, setPopoverMessage] = useState("");

  const handleKeyPress = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      event.target.blur();
    }
  };

  const handleInfo = (message) => {
    setPopoverMessage(message);
  };

  return (
    <Box sx={{ p: 3 }}>
      <Typography
        variant="h6"
        sx={{ marginBottom: "2rem", borderBottom: "1px solid #ccc" }}
      >
        Workspace Settings
      </Typography>
      <Box sx={{ marginTop: "2rem" }}>
        <Formik
          initialValues={settings.workspace}
          validationSchema={validationSchema}
        >
          {({ values, errors, handleChange }) => {
            return (
              <Form>
                <Grid container mb={2}>
                  <Grid item>
                    <Typography
                      variant="h8"
                      color="initial"
                      fontWeight="fontWeightBold"
                    >
                      Default Padding
                    </Typography>
                    <IconButton
                      onClick={(event) => {
                        setAnchorEl(event.currentTarget);
                        handleInfo(
                          "This is the default space around the workspace area that will be added when a new workspace is created"
                        );
                      }}
                      size="small"
                    >
                      <InfoOutlinedIcon sx={{ fontSize: 15 }} />
                    </IconButton>
                  </Grid>
                  <Grid container alignItems="center">
                    <Grid item ml={1} mr={1}>
                      <SvgIcon viewBox="0 0 70 50">
                        <path
                          d="M 5 0 L 5 35 Q 5 40, 10 40 L 70 40" // Adjust the x-coordinate and length of the line
                          stroke="#333"
                          strokeOpacity="0.6"
                          strokeWidth="6"
                          fill="none"
                        />
                      </SvgIcon>
                    </Grid>
                    <Grid item>
                      {/* TOP */}
                      <Typography variant="body2" color="initial">
                        Top:
                      </Typography>
                    </Grid>
                    <Grid item mr={1}>
                      <Field
                        as={TextField}
                        id="defaultPadding.top"
                        label=""
                        type="number"
                        InputLabelProps={{
                          shrink: false,
                        }}
                        value={values.defaultPadding.top}
                        variant="outlined"
                        size="small"
                        sx={{
                          maxWidth: "60px",
                          "& input": {
                            padding: 0.5,
                          },
                        }}
                        name="defaultPadding.top"
                        onChange={handleChange}
                        onBlur={() => handleSave(values, "workspace", errors)}
                        onKeyDown={handleKeyPress}
                        error={Boolean(errors.defaultPadding?.top)}
                      />
                    </Grid>
                    <Grid item>
                      {/* BOTTOM */}
                      <Typography variant="body2" color="initial">
                        Bottom:
                      </Typography>
                    </Grid>
                    <Grid item mr={1}>
                      <Field
                        as={TextField}
                        id="defaultPadding.bottom"
                        label=""
                        type="number"
                        InputLabelProps={{
                          shrink: false,
                        }}
                        value={values.defaultPadding.bottom}
                        variant="outlined"
                        size="small"
                        sx={{
                          maxWidth: "60px",
                          "& input": {
                            padding: 0.5,
                          },
                        }}
                        name="defaultPadding.bottom"
                        onChange={handleChange}
                        onBlur={() => handleSave(values, "workspace", errors)}
                        onKeyDown={handleKeyPress}
                        error={Boolean(errors.defaultPadding?.bottom)}
                      />
                    </Grid>
                    <Grid item>
                      {/* LEFT */}
                      <Typography variant="body2" color="initial">
                        Left:
                      </Typography>
                    </Grid>
                    <Grid item mr={1}>
                      <Field
                        as={TextField}
                        id="defaultPadding.left"
                        label=""
                        type="number"
                        InputLabelProps={{
                          shrink: false,
                        }}
                        value={values.defaultPadding.left}
                        variant="outlined"
                        size="small"
                        sx={{
                          maxWidth: "60px",
                          "& input": {
                            padding: 0.5,
                          },
                        }}
                        name="defaultPadding.left"
                        onChange={handleChange}
                        onBlur={() => handleSave(values, "workspace", errors)}
                        onKeyDown={handleKeyPress}
                        error={Boolean(errors.defaultPadding?.left)}
                      />
                    </Grid>
                    <Grid item>
                      {/* RIGHT */}
                      <Typography variant="body2" color="initial">
                        Right:
                      </Typography>
                    </Grid>
                    <Grid item mr={1}>
                      <Field
                        as={TextField}
                        id="defaultPadding.right"
                        label=""
                        type="number"
                        InputLabelProps={{
                          shrink: false,
                        }}
                        value={values.defaultPadding.right}
                        variant="outlined"
                        size="small"
                        sx={{
                          maxWidth: "60px",
                          "& input": {
                            padding: 0.5,
                          },
                        }}
                        name="defaultPadding.right"
                        onChange={handleChange}
                        onBlur={() => handleSave(values, "workspace", errors)}
                        onKeyDown={handleKeyPress}
                        error={Boolean(errors.defaultPadding?.right)}
                      />
                    </Grid>
                    <Grid item>
                      <Typography variant="body2" color="error">
                        {errors.defaultPadding?.top ||
                          errors.defaultPadding?.right ||
                          errors.defaultPadding?.bottom ||
                          errors.defaultPadding?.left}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid container alignItems="center" mb={2}>
                  <Grid item>
                    <Typography
                      variant="h8"
                      color="initial"
                      fontWeight="fontWeightBold"
                    >
                      Default Dimensions
                    </Typography>
                    <IconButton
                      onClick={(event) => {
                        setAnchorEl(event.currentTarget);
                        handleInfo(
                          "These are the default dimensions for a new workspace"
                        );
                      }}
                      size="small"
                    >
                      <InfoOutlinedIcon sx={{ fontSize: 15 }} />
                    </IconButton>
                  </Grid>
                  <Grid container alignItems="center">
                    <Grid item mr={1} pl={1}>
                      <SvgIcon viewBox="0 0 70 50">
                        <path
                          d="M 5 0 L 5 35 Q 5 40, 10 40 L 70 40" // Adjust the x-coordinate and length of the line
                          stroke="#333"
                          strokeOpacity="0.6"
                          strokeWidth="6"
                          fill="none"
                        />
                      </SvgIcon>
                    </Grid>
                    <Grid item>
                      {/* defaultWidth */}
                      <Typography variant="body2" color="initial">
                        Width:
                      </Typography>
                    </Grid>
                    <Grid item mr={1}>
                      <Field
                        as={TextField}
                        id="defaultWidth"
                        label=""
                        type="number"
                        InputLabelProps={{
                          shrink: false,
                        }}
                        value={values.defaultWidth}
                        variant="outlined"
                        size="small"
                        sx={{
                          maxWidth: "60px",
                          "& input": {
                            padding: 0.5,
                          },
                        }}
                        name="defaultWidth"
                        onChange={handleChange}
                        onBlur={() => handleSave(values, "workspace", errors)}
                        onKeyDown={handleKeyPress}
                        error={Boolean(errors.defaultWidth)}
                      />
                    </Grid>
                    <Grid item>
                      {/* defaultLength */}
                      <Typography variant="body2" color="initial">
                        Length:
                      </Typography>
                    </Grid>
                    <Grid item mr={1}>
                      <Field
                        as={TextField}
                        id="defaultLength"
                        label=""
                        type="number"
                        InputLabelProps={{
                          shrink: false,
                        }}
                        value={values.defaultLength}
                        variant="outlined"
                        size="small"
                        sx={{
                          maxWidth: "60px",
                          "& input": {
                            padding: 0.5,
                          },
                        }}
                        name="defaultLength"
                        onChange={handleChange}
                        onBlur={() => handleSave(values, "workspace", errors)}
                        onKeyDown={handleKeyPress}
                        error={Boolean(errors.defaultLength)}
                      />
                    </Grid>
                    <Grid item>
                      {/* defaultHeight */}
                      <Typography variant="body2" color="initial">
                        Height:
                      </Typography>
                    </Grid>
                    <Grid item mr={1}>
                      <Field
                        as={TextField}
                        id="defaultHeight"
                        label=""
                        type="number"
                        InputLabelProps={{
                          shrink: false,
                        }}
                        value={values.defaultHeight}
                        variant="outlined"
                        size="small"
                        sx={{
                          maxWidth: "60px",
                          "& input": {
                            padding: 0.5,
                          },
                        }}
                        name="defaultHeight"
                        onChange={handleChange}
                        onBlur={() => handleSave(values, "workspace", errors)}
                        onKeyDown={handleKeyPress}
                        error={Boolean(errors.defaultHeight)}
                      />
                    </Grid>
                    <Grid item>
                      <Typography variant="body2" color="error">
                        {errors.defaultWidth ||
                          errors.defaultLength ||
                          errors.defaultHeight}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid container alignItems="center" mb={2}>
                  <Grid item>
                    <Typography
                      variant="h8"
                      color="initial"
                      fontWeight="fontWeightBold"
                    >
                      Zoom
                    </Typography>
                    <IconButton
                      onClick={(event) => {
                        setAnchorEl(event.currentTarget);
                        handleInfo(
                          "This is the default zoom when you open a workspace"
                        );
                      }}
                      size="small"
                    >
                      <InfoOutlinedIcon sx={{ fontSize: 15 }} />
                    </IconButton>
                  </Grid>
                  <Grid item>
                    <Field
                      as={TextField}
                      id="zoom"
                      label=""
                      type="number"
                      InputLabelProps={{
                        shrink: false,
                      }}
                      value={values.zoom}
                      variant="outlined"
                      size="small"
                      sx={{
                        maxWidth: "60px",
                        "& input": {
                          padding: 0.5,
                        },
                      }}
                      onChange={handleChange}
                      onBlur={() => handleSave(values, "workspace", errors)}
                      onKeyDown={handleKeyPress}
                      error={Boolean(errors.zoom)}
                    />
                  </Grid>
                  <Grid item>
                    <Typography variant="body2" color="error">
                      {errors.zoom}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container alignItems="center" mb={2}>
                  <Grid item>
                    <Typography
                      variant="h8"
                      color="initial"
                      fontWeight="fontWeightBold"
                    >
                      Position round up
                    </Typography>
                    <IconButton
                      onClick={(event) => {
                        setAnchorEl(event.currentTarget);
                        handleInfo(
                          "Platform coordinates will be rounded up to this number"
                        );
                      }}
                      size="small"
                    >
                      <InfoOutlinedIcon sx={{ fontSize: 15 }} />
                    </IconButton>
                  </Grid>
                  <Grid item>
                    <Field
                      as={TextField}
                      id="roundUp"
                      label=""
                      type="number"
                      InputLabelProps={{
                        shrink: false,
                      }}
                      value={values.roundUp}
                      variant="outlined"
                      size="small"
                      sx={{
                        maxWidth: "60px",
                        "& input": {
                          padding: 0.5,
                        },
                      }}
                      onChange={handleChange}
                      onBlur={() => handleSave(values, "workspace", errors)}
                      onKeyDown={handleKeyPress}
                      error={Boolean(errors.roundUp)}
                    />
                  </Grid>
                  <Grid item>
                    <Typography variant="body2" color="error">
                      {errors.roundUp}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container alignItems="center" mb={2}>
                  <Grid item>
                    <Typography
                      variant="h8"
                      color="initial"
                      fontWeight="fontWeightBold"
                    >
                      Position increment
                    </Typography>
                    <IconButton
                      onClick={(event) => {
                        setAnchorEl(event.currentTarget);
                        handleInfo(
                          "Platform coordinates will increase by this number"
                        );
                      }}
                      size="small"
                    >
                      <InfoOutlinedIcon sx={{ fontSize: 15 }} />
                    </IconButton>
                  </Grid>
                  <Grid item>
                    <Field
                      as={TextField}
                      id="incrementPosition"
                      label=""
                      type="number"
                      InputLabelProps={{
                        shrink: false,
                      }}
                      value={values.incrementPosition}
                      variant="outlined"
                      size="small"
                      sx={{
                        maxWidth: "60px",
                        "& input": {
                          padding: 0.5,
                        },
                      }}
                      name="incrementPosition"
                      onChange={handleChange}
                      onBlur={() => handleSave(values, "workspace", errors)}
                      onKeyDown={handleKeyPress}
                      inputProps={{ step: 0.1 }}
                      error={Boolean(errors.incrementPosition)}
                    />
                  </Grid>
                  <Grid item>
                    <Typography variant="body2" color="error">
                      {errors.incrementPosition}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container alignItems="center" mb={2}>
                  <Grid item>
                    <Typography
                      variant="h8"
                      color="initial"
                      fontWeight="fontWeightBold"
                    >
                      Panel width
                    </Typography>
                    <IconButton
                      onClick={(event) => {
                        setAnchorEl(event.currentTarget);
                        handleInfo("This is the width of the side panels");
                      }}
                      size="small"
                    >
                      <InfoOutlinedIcon sx={{ fontSize: 15 }} />
                    </IconButton>
                  </Grid>
                  <Grid item>
                    <Field
                      as={TextField}
                      id="panelWidth"
                      label=""
                      type="number"
                      InputLabelProps={{
                        shrink: false,
                      }}
                      value={values.panelWidth}
                      variant="outlined"
                      size="small"
                      sx={{
                        maxWidth: "60px",
                        "& input": {
                          padding: 0.5,
                        },
                      }}
                      name="panelWidth"
                      onChange={handleChange}
                      onBlur={() => handleSave(values, "workspace", errors)}
                      onKeyDown={handleKeyPress}
                      inputProps={{ step: 0.1 }}
                      error={Boolean(errors.panelWidth)}
                    />
                  </Grid>
                  <Grid item>
                    <Typography variant="body2" color="error">
                      {errors.panelWidth}
                    </Typography>
                  </Grid>
                </Grid>
                <Popover
                  id="popoverSettings"
                  open={Boolean(anchorEl)}
                  anchorEl={anchorEl}
                  onClose={() => setAnchorEl(null)}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "right",
                  }}
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "left",
                  }}
                >
                  <Box p={1}>
                    <Typography variant="caption">{popoverMessage}</Typography>
                  </Box>
                </Popover>
              </Form>
            );
          }}
        </Formik>
      </Box>
    </Box>
  );
};
