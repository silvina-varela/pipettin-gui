import { useNavigate } from "react-router-dom";
import SettingsIcon from "@mui/icons-material/Settings";
import { IconButton, Tooltip } from "@mui/material";

export const SettingsButton = () => {
  const navigate = useNavigate();

  return (
    <Tooltip title="Settings">
      <IconButton onClick={() => navigate("/settings")}>
        <SettingsIcon fontSize="medium" />
      </IconButton>
    </Tooltip>
  );
};
