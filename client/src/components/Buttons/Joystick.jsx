import GamepadIcon from "@mui/icons-material/Gamepad";
import { IconButton, Tooltip } from "@mui/material";
import { useSelector } from "react-redux";

export const Joystick = () => {
  const { settings } = useSelector((state) => state.settingsSlice);

  const joystick = settings.controller?.joystickUrl;

  return (
    <Tooltip title="Joystick">
      <span>
        {joystick ? (
          <a href={joystick} target="_blank" rel="noreferrer">
            <IconButton>
              <GamepadIcon fontSize="medium" />
            </IconButton>
          </a>
        ) : (
          <IconButton disabled>
            <GamepadIcon fontSize="medium" />
          </IconButton>
        )}
      </span>
    </Tooltip>
  );
};
