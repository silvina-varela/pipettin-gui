import { Button, Tooltip, CircularProgress } from "@mui/material";

export const ActionButton = ({
  tooltip,
  name,
  startIcon,
  color,
  handleClick,
  isSubmitting,
}) => {
  return (
    <>
      <Tooltip title={tooltip}>
        <Button
          disabled={isSubmitting}
          color={color}
          variant="contained"
          startIcon={startIcon}
          onClick={handleClick}
        >
          {name}
          {isSubmitting && (
            <CircularProgress
              size={24}
              sx={{ position: "absolute" }}
            />
          )}
        </Button>
      </Tooltip>
    </>
  );
};
