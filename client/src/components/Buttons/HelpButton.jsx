import HelpIcon from "@mui/icons-material/Help";
import { IconButton, Tooltip } from "@mui/material";
import { useSelector } from "react-redux";

export const HelpButton = () => {
  const { settings } = useSelector((state) => state.settingsSlice);
  const helpLink = settings.controller?.helpLink;

  return (
    <Tooltip title="Help">
      <a href={helpLink} target="_blank" rel="noreferrer">
        <IconButton>
          <HelpIcon fontSize="medium" />
        </IconButton>
      </a>
    </Tooltip>
  );
};
