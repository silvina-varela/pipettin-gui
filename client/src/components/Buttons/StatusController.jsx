import LensRoundedIcon from "@mui/icons-material/LensRounded";
import { IconButton, Tooltip } from "@mui/material";
import { Chip, Dialog, DialogTitle, DialogContent, Typography, List, ListItem, ListItemText, Divider } from "@mui/material";
import { useState } from "react";

const statusToColor = {
    OK: "#00aa00",
    WARN: "orange",
    ERROR: "red",
    UNK: "yellow",
    OFF: "#e5e5e5",
    STANDBY: "#55aaff"
};
const statusToTextColor = {
    OK: "white",
    WARN: "white",
    ERROR: "white",
    UNK: "black",
    OFF: "black",
    STANDBY: "white"
};

// Helper function to render status details dynamically and recursively
const renderStatusDetails = (statusData, level = 0) => {
    if (!statusData || typeof statusData !== "object") return "No status data has been recently received.";

    return (
        <div style={{ marginLeft: `${level * 16}px` }}>
            {Object.keys(statusData).map((key) => {
                const value = statusData[key];

                // If the value is a boolean, render a Chip with red/green color
                if (typeof value === "boolean") {
                    return (
                        <ListItem key={key}>
                            <ListItemText primary={key} />
                            <Chip
                                label={value ? "YES" : "NO"}
                                style={{
                                    backgroundColor: value ? statusToColor["OK"] : statusToColor["ERROR"],
                                    color: "white",
                                }}
                            />
                        </ListItem>
                    );
                } else if (typeof value === "object" && value !== null) {
                // If the value is an object, recursively render it as a new section
                    return (
                        <div key={key}>
                            <Typography variant="subtitle1" style={{ marginTop: 8 }}>
                                {key.charAt(0).toUpperCase() + key.slice(1)}
                            </Typography>
                            {renderStatusDetails(value, level + 1)}
                            <Divider />
                        </div>
                    );
                } else {
                    return (
                        <ListItem key={key}>
                            <ListItemText primary={key} />
                            <Chip 
                                label={String(value)}
                                style={{
                                    backgroundColor: statusToColor[value] || "black",
                                    color: statusToTextColor[value] || "white",
                                }}
                            />
                        </ListItem>
                    );
                }
            })}
        </div>
    );
};

export const StatusController = ({ controllerStatus }) => {

    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const currentColor = statusToColor[controllerStatus.status] || "grey";

    return (
        <>
            {/* Tooltip with current status */}
            <Tooltip title={`Status: ${controllerStatus.status} (click for details).`}>
                <IconButton onClick={handleOpen}>
                    <LensRoundedIcon fontSize="small" style={{ color: currentColor }} />
                </IconButton>
            </Tooltip>

            {/* Modal to show details */}
            <Dialog open={open} onClose={handleClose} maxWidth="sm" fullWidth>
                <DialogTitle>Controller Status Details</DialogTitle>
                <DialogContent>
                    {renderStatusDetails(controllerStatus)}
                </DialogContent>
            </Dialog>
        </>
    );
};
