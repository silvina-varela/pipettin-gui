import { useState } from "react";
import { Container, TextField, IconButton } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import ClearIcon from "@mui/icons-material/Clear";

export const SearchBar = ({ searchInput }) => {
  const [searchTerm, setSearchTerm] = useState("");

  const handleSearch = (event) => {
    setSearchTerm(event.target.value);
    searchInput(event.target.value);
  };
  const handleClearClick = () => {
    setSearchTerm("");
    searchInput("");
  };
  return (
    <>
      <Container
        sx={{
          display: "flex",
          justifyContent: "flex-end",
          alignItems: "center",
          // maxWidth: "200px !important",
          pl: "0px !important",
          pr: "0px !important",
        }}
      >
        <TextField
          variant="standard"
          fullWidth
          placeholder="Search..."
          value={searchTerm}
          onChange={handleSearch}
          color="neutral"
          InputProps={{
            endAdornment: (
              <IconButton
                sx={{ visibility: searchTerm ? "visible" : "hidden" }}
                onClick={handleClearClick}
              >
                <ClearIcon />
              </IconButton>
            ),
            startAdornment: (
              <SearchIcon fontSize="md" sx={{ marginRight: 1 }} />
            ),
          }}
        />
      </Container>
    </>
  );
};
