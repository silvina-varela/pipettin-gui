import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { Button, TextField, Grid, Typography } from "@mui/material";
import { InputNumber } from "../Platforms/FormFields/InputNumber";
import { WorkspaceInherit } from "./Buttons/WorkspaceInherit";
import { useNavigate } from "react-router-dom";
import { createWorkspaceAsync, updateWorkspace } from "../../redux/slices";

const createWorkspaceSchema = Yup.object().shape({
  name: Yup.string()
    .min(3, "Workspace name must have at least 3 characters")
    .max(100, "Workspace name can't have more than 100 characters")
    .required("Please enter name"),
  description: Yup.string().max(
    1000,
    "Description can't have more than 1000 characters"
  ),
  width: Yup.number(),
  length: Yup.number(),
  height: Yup.number(),
  padding: Yup.object().shape({
    top: Yup.number(),
    bottom: Yup.number(),
    left: Yup.number(),
    right: Yup.number(),
  }),
});

export const WorkspaceForm = ({ handleClosePopup, isEditMode = false }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { workspace } = useSelector((state) => state.workspaceSlice);
  const { settings } = useSelector((state) => state.settingsSlice);

  const workspaceSettings = settings.workspace;

  const [workspaceToImport, setWorkspaceToImport] = useState("");
  const [inheritData, setInheritData] = useState({
    platforms: false,
    anchors: false,
    protocols: false,
  });

  const createFormInitialValues = {
    name: "",
    description: "",
    width: workspaceSettings.defaultWidth,
    length: workspaceSettings.defaultLength,
    height: workspaceSettings.defaultHeight,
    padding: {
      top: workspaceSettings.defaultPadding.top,
      bottom: workspaceSettings.defaultPadding.bottom,
      left: workspaceSettings.defaultPadding.left,
      right: workspaceSettings.defaultPadding.right,
    },
  };
  const initialValues = isEditMode
    ? workspace.padding
      ? workspace
      : {
        ...workspace,
        padding: {
          top: workspaceSettings.defaultPadding.top,
          bottom: workspaceSettings.defaultPadding.bottom,
          left: workspaceSettings.defaultPadding.left,
          right: workspaceSettings.defaultPadding.right,
        },
      }
    : createFormInitialValues;

  const handleSubmit = async (values) => {
    if (isEditMode) {
      dispatch(updateWorkspace({
        padding: values.padding,
        width: values.width,
        height: values.height,
        length: values.length,
        name: values.name,
        description: values.description
      }))
        handleClosePopup();      
    } else {
      dispatch(createWorkspaceAsync({ newWorkspace: values, inheritData: { workspaceID: workspaceToImport, data: inheritData } }))
      .unwrap()
      .then((response) => {
          handleClosePopup();
          navigate(`workspace/${response._id}`);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  return (
    <div>
      <Formik
        initialValues={initialValues}
        validationSchema={createWorkspaceSchema}
        onSubmit={handleSubmit}
      >
        {({
          values,
          touched,
          errors,
          handleChange,
          setValues,
        }) => (
          <Form id="workspaceForm">
            <Grid container direction={"column"} spacing={4}>
              <Grid item xs={12}>
                {/* NAME */}
                <TextField
                  fullWidth
                  variant="standard"
                  id="name"
                  name="name"
                  label="Name"
                  value={values.name}
                  onChange={handleChange}
                  error={touched.name && Boolean(errors.name)}
                  helperText={touched.name && errors.name}
                />
              </Grid>
              <Grid item xs={12}>
                {/* DESCRIPTION */}
                <TextField
                  fullWidth
                  variant="standard"
                  multiline
                  rows={10}
                  id="description"
                  name="description"
                  label="Description"
                  value={values.description}
                  onChange={handleChange}
                  error={touched.description && Boolean(errors.description)}
                  helperText={touched.description && errors.description}
                />
              </Grid>
              <Grid item>
                <Typography variant="overline">Dimensions</Typography>

                <Grid container spacing={4} mb={4}>
                  <Grid item>
                    {/* WIDTH */}
                    <InputNumber
                      inputWidth={100}
                      id="width"
                      name="width"
                      label="Width"
                      endAdornment={"mm"}
                      value={values.width}
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid item>
                    {/* LENGTH */}
                    <InputNumber
                      inputWidth={100}
                      id="length"
                      name="length"
                      label="Length"
                      endAdornment={"mm"}
                      value={values.length}
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid item>
                    {/* HEIGHT */}
                    <InputNumber
                      inputWidth={100}
                      id="height"
                      name="height"
                      label="Height"
                      endAdornment={"mm"}
                      value={values.height}
                      onChange={handleChange}
                    />
                  </Grid>
                </Grid>
                {/* PADDING */}
                <Typography variant="overline">Padding</Typography>
                <Grid container spacing={4}>
                  <Grid item>
                    <InputNumber
                      inputWidth={100}
                      id="padding.top"
                      name="padding.top"
                      label="Top"
                      value={values.padding.top}
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid item>
                    <InputNumber
                      inputWidth={100}
                      id="padding.bottom"
                      name="padding.bottom"
                      label="Bottom"
                      value={values.padding.bottom}
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid item>
                    <InputNumber
                      inputWidth={100}
                      id="padding.left"
                      name="padding.left"
                      label="Left"
                      value={values.padding.left}
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid item>
                    <InputNumber
                      inputWidth={100}
                      id="padding.right"
                      name="padding.right"
                      label="Right"
                      value={values.padding.right}
                      onChange={handleChange}
                    />
                  </Grid>
                </Grid>
              </Grid>
              {isEditMode ? null : (
                <Grid item>
                  <Typography variant={"overline"}>
                    Inherit data from another workspace{" "}
                  </Typography>
                  <WorkspaceInherit
                    setFieldValues={setValues}
                    formValues={values}
                    setWorkspaceToImport={setWorkspaceToImport}
                    inheritData={inheritData}
                    setInheritData={setInheritData}
                  />
                </Grid>
              )}
              <Grid item sx={{ textAlign: "right" }}>
                {/* SUBMIT */}
                <Button variant="contained" type="submit">
                  {isEditMode ? "Edit" : "Create"}
                </Button>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </div>
  );
};
