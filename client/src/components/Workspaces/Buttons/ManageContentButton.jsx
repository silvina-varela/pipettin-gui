import { useEffect, useState } from "react";
import { Button, Tooltip } from "@mui/material";
import { Alert } from "../../Dialogs/Alert";
import { useSelector, useDispatch } from "react-redux";
import { ManageContainerAlertMessage } from "./ManageContainerAlertMessage";
import { addNewContentToItem, deleteWorkspaceContent, selectPlatform, selectContainer } from "../../../redux/slices";


// Utility function to convert column and row indices to a "nice" name
function convertToNiceName(index, numRows, numCols) {
  // Calculate the row and column based on the index
  const colIndex = (index % numCols) + 1;
  const rowIndex = Math.floor(index / numCols) + 1;

  let rowLetter = "";

  let tempRowIndex = rowIndex;
  while (tempRowIndex > 0) {
    tempRowIndex--; // Adjust for zero-indexing
    rowLetter = String.fromCharCode((tempRowIndex % 26) + 65) + rowLetter;
    tempRowIndex = Math.floor(tempRowIndex / 26);
  }

  return `${rowLetter}${colIndex}`;
}

export const ManageContentButton = ({ mode, tooltipLabel, icon }) => {
  const dispatch = useDispatch();
  const { workspace, selectedPlatform, selectedContent } = useSelector(
    (state) => state.workspaceSlice
  );
  const { selectedContainer } = useSelector((state) => state.containerSlice);

  const [containerType, setContainerType] = useState("");
  const [inputValue, setInputValue] = useState(0);

  /** Alerts */
  const [alert, setAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const [alertInput, setAlertInput] = useState(false);
  const [yesMessage, setYesMessage] = useState("Yes");
  const [noMessage, setNoMessage] = useState("No");
  const [title, setTitle] = useState("");
  const [component, setComponent] = useState();
  const [disableConfirm, setDisableConfirm] = useState(false);

  useEffect(() => {
    if (workspace.items && workspace.items[selectedPlatform]) {
      workspace.items[selectedPlatform]?.platformData.type === "CUSTOM"
        ? setContainerType("slot")
        : setContainerType("well");
    }
  }, [selectedPlatform, selectedContent]);

  useEffect(() => {
    if (mode === "insert") {
      if (!selectedContent) {
        if (inputValue > 0 && selectedContainer) setDisableConfirm(false);
      } else if (selectedContent && selectedContainer) setDisableConfirm(false);
      else setDisableConfirm(true);
    }
  }, [inputValue, selectedContainer, selectedContent]);

  const setNoWellsSelectedAlert = () => {
    setTitle("Insert Container");
    setAlertMessage(null);
    setAlertInput(true);
    setYesMessage("Confirm");
    setNoMessage("Cancel");
    setAlert(true);
    setComponent(
      <ManageContainerAlertMessage
        inputNumber={true}
        containerType={containerType}
        setInputValue={setInputValue}
      />
    );
  };

  const setNoPlatformAlert = () => {
    setTitle("Oops...");
    setAlertMessage("You need to select a platform first.");
    setAlertInput(false);
    setYesMessage(false);
    setNoMessage("Ok");
    setAlert(true);
  };

  const setBucketAlert = () => {
    setTitle("Oops...");
    setAlertMessage("Contents can't be added to a bucket. Select another item.");
    setAlertInput(false);
    setYesMessage(false);
    setNoMessage("Ok");
    setAlert(true);
  };

  const setAnchorAlert = () => {
    setTitle("Oops...");
    setAlertMessage("Contents can't be added to an anchor. Select another item.");
    setAlertInput(false);
    setYesMessage(false);
    setNoMessage("Ok");
    setAlert(true);
  };

  const setPetriAlert = () => {
    setTitle("Oops...");
    setAlertMessage(
      "To add content to a petri dish, double click in the desired position inside the petri dish."
    );
    setAlertInput(false);
    setYesMessage(false);
    setNoMessage("Ok");
    setAlert(true);
  };

  const setEmptyAllAlert = () => {
    setTitle("Warning");
    setAlertMessage(
      "Do you want to empty the entire platform? This action can't be undone."
    );
    setAlertInput(false);
    setYesMessage("Confirm");
    setNoMessage("Cancel");
    setAlert(true);
  };

  const setEmptyCustomAlert = () => {
    setTitle("Oops...");
    setAlertMessage("There are no slots (yet) in this platform where to place contents.");
    setAlertInput(false);
    setYesMessage(false);
    setNoMessage("Ok");
    setAlert(true);
  };
  const setOneWellSelectedAlert = () => {
    setTitle("Insert Container");
    setAlertMessage(null);
    setAlertInput(false);
    setYesMessage("Confirm");
    setNoMessage("Cancel");
    setAlert(true);
    setComponent(
      <ManageContainerAlertMessage
        containerType={containerType}
        setInputValue={setInputValue}
      />
    );
  };

  const handleClick = (e) => {
    e.preventDefault();
    if (typeof selectedPlatform !== "number") {
      // No platform is selected
      setNoPlatformAlert();
      return;
    }

    // A platform is currently selected.
    const platform_type = workspace.items[selectedPlatform].platformData.type

    // Handle incompatible platforms.
    if (platform_type.includes("BUCKET")) {
      // Bucket is selected.
      setBucketAlert();
      return;
    }
    if (platform_type.includes("ANCHOR")) {
      // Anchor is selected.
      setAnchorAlert();
      return;
    }

    // Handle custom platform with no slots.
    if (
      platform_type.includes("CUSTOM") &&
      workspace.items[selectedPlatform].platformData.slots.length === 0
    ) {
      setEmptyCustomAlert();
      return;
    }

    // Handle content insertion.
    if (mode === "insert") {
      // Petri dish is selected
      if (platform_type.includes("PETRI")) {
        setPetriAlert();
        return;
      }
      // No wells are selected. Shows message to add content to x number of wells
      if (!selectedContent.length) {
        setNoWellsSelectedAlert();
        return;
      } else {
        // Specific wells are selected. Adds content to those wells
        setOneWellSelectedAlert();
        return;
      }
    } else if (mode === "empty") {
      // Empty entire platform
      if (!selectedContent.length) {
        setEmptyAllAlert();
        return;
      } else {
        // Empty selected content
        handleDeleteSelectedContent();
      }
    }
  };

  const handleConfirm = (event, returnedValue) => {
    event.preventDefault();
    if (mode === "insert") handleConfirmAddContent(returnedValue);
    if (mode === "empty") handleConfirmEmptyContent();
  };

  const handleClose = () => {
    setAlert(false);
  };

  // Gets number of wells from popup to add content to
  const handleConfirmAddContent = (inputValue) => {
    if (!inputValue) handleAddContentToSpecificWells();
    else {
      const currentPlatform = workspace.items[selectedPlatform];

      let emptyWells = document
        .querySelector(`#platform-${selectedPlatform}`)
        .querySelectorAll(".empty");

      let addContentToWells = [...emptyWells];

      // Selects available wells
      addContentToWells = addContentToWells.slice(0, parseInt(inputValue));
      if (containerType === "slot") {
        addContentToWells = addContentToWells.filter((slot) => {
          const index = slot.getAttribute("index");
          return currentPlatform.platformData.slots[index].containers.some(
            (container) => selectedContainer?.name === container.container
          );
        });
      }

      const newContent = addContentToWells.map((well) => {
        const index = parseInt(well.getAttribute("index")) + 1;

        // Add nice names for grid-type platforms (e.g. E4, B1, etc.).
        const numCols = currentPlatform.platformData.wellsColumns;
        const numRows = currentPlatform.platformData.wellsRows;
        const new_content_loc = currentPlatform.platformData.type === "TUBE_RACK" ? 
          "-" + convertToNiceName(parseInt(index - 1), numRows, numCols) :
          (parseInt(index))

        return {
          // TODO: Add nice names for grid-type platforms (e.g. E4, B1, etc.).
          name: selectedContainer?.type + new_content_loc,
          index: index,
          position:
            containerType === "slot"
              ? {
                  x: well.getAttribute("cx"),
                  y: well.getAttribute("cy"),
                }
              : {
                  col: well.getAttribute("col"),
                  row: well.getAttribute("row"),
                },
          tags: [],
          volume: 0,
          container: selectedContainer?.name,
          containerData: selectedContainer,
        };
      });

      dispatch(addNewContentToItem(selectedPlatform, newContent));
    }
    setAlert(false);
  };

  // Has selected wells to add content to
  const handleAddContentToSpecificWells = async () => {
    const platforms = workspace.items;
    const currentPlatform = platforms[selectedPlatform];

    let contentToAdd = [];
    await selectedContent.forEach((contentIndex) => {
      const well = document.querySelector(
        `#well-${selectedPlatform}-${contentIndex}`
      );
      let isEmpty = well.classList.contains("empty");

      if (containerType === "slot") {
        isEmpty = currentPlatform.platformData.slots[
          contentIndex
        ].containers.some(
          (container) => selectedContainer?.name === container.container
        );
      }
      
      // Add nice names for grid-type platforms (e.g. E4, B1, etc.).
      const numCols = currentPlatform.platformData.wellsColumns;
      const numRows = currentPlatform.platformData.wellsRows;
      const new_content_loc = currentPlatform.platformData.type === "TUBE_RACK" ? 
        "-" + convertToNiceName(parseInt(contentIndex), numRows, numCols) :
        (parseInt(contentIndex) + 1)

      if (isEmpty) {
        const newContent = {
          name: selectedContainer?.type + new_content_loc,
          index: contentIndex + 1,
          position:
            containerType === "slot"
              ? {
                  x: well.getAttribute("cx"),
                  y: well.getAttribute("cy"),
                }
              : {
                  col: well.getAttribute("col"),
                  row: well.getAttribute("row"),
                },
          tags: [],
          volume: 0,
          // maxVolume: selectedContainer?.maxVolume,
          container: selectedContainer?.name,
          containerData: selectedContainer,
        };

        contentToAdd.push(newContent);
      }
    });

    if (contentToAdd.length) {
      dispatch(addNewContentToItem(selectedPlatform, contentToAdd));
    }
    dispatch(selectPlatform({ platform: selectedPlatform, content: [] }));
  };

  // Empty entire platform
  const handleConfirmEmptyContent = () => {
    dispatch(deleteWorkspaceContent(selectedPlatform, []));
    dispatch(selectContainer(null));
    setAlert(false);
  };

  // Delete specific contents
  const handleDeleteSelectedContent = () => {
    dispatch(deleteWorkspaceContent(selectedPlatform, selectedContent));
    dispatch(selectPlatform({ platform: selectedPlatform, content: [] }));
  };

  return (
    <>
      <Tooltip title={tooltipLabel}>
        <Button
          sx={{
            fontSize: "small",
            "& .MuiSvgIcon-root": {
              fontSize: "1rem",
            },
            "&:hover": {
              filter: "brightness(95%)",
            },
          }}
          onClick={(e) => handleClick(e)}
        >
          {icon}
        </Button>
      </Tooltip>

      <Alert
        open={alert}
        yesMessage={yesMessage}
        noMessage={noMessage}
        message={alertMessage}
        inputNumber={alertInput}
        onConfirm={handleConfirm}
        onCancel={() => setAlert(false)}
        onClose={handleClose}
        title={title}
        component={component}
        inputValue={inputValue}
        setInputValue={setInputValue}
        disableConfirm={disableConfirm}
      />
    </>
  );
};
