import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Grid, IconButton } from "@mui/material";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";

export const PlatformPositionButtons = React.memo(
  ({ platformCoords, setPlatformCoords, handleSaveCoordinates }) => {
    const { workspace, selectedPlatform } = useSelector(
      (state) => state.workspaceSlice
    );
    const { settings } = useSelector((state) => state.settingsSlice);
    const [incPosition, setIncPosition] = useState(0);

    useEffect(() => {
      if (
        settings &&
        Object.prototype.hasOwnProperty.call(settings, "workspace") &&
        Object.prototype.hasOwnProperty.call(settings.workspace, "incrementPosition")
      ) {
        setIncPosition(settings.workspace.incrementPosition);
      }
    }, []);

    const updatePlatformCoordinates = (event, dx, dy) => {
      event.preventDefault();
      const platform = workspace.items[selectedPlatform]
      if(platform.locked) return
      const newX = platformCoords[0] + dx;
      const newY = platformCoords[1] + dy;
      const platformWidth = platform.platformData.width;
      const platformHeight = platform.platformData.length;
      setPlatformCoords(
        newX,
        newY,
        platformCoords[2],
        platformWidth,
        platformHeight
      );
      handleSaveCoordinates([newX, newY, platformCoords[2]]);
    };

    const handlePlatformButtonClick = (e) => {
      e.stopPropagation();
    };

    return (
      <Grid container onClick={handlePlatformButtonClick} direction="column">
        {/* Up Arrow */}
        <Grid item sx={{ alignSelf: "center" }}>
          <IconButton
            onClick={(e) => updatePlatformCoordinates(e, 0, -incPosition)}
            sx={{ p: 0 }}
            size="small"
          >
            <KeyboardArrowUpIcon />
          </IconButton>
        </Grid>
        {/* Left and Right Arrows */}
        <Grid item sx={{ alignSelf: "center" }}>
          <Grid container>
            <Grid item xs={6}>
              <IconButton
                onClick={(e) => updatePlatformCoordinates(e, -incPosition, 0)}
                sx={{ p: 0, mr: 1.5 }}
                size="small"
              >
                <KeyboardArrowLeftIcon />
              </IconButton>
            </Grid>
            <Grid item xs={6}>
              <IconButton
                onClick={(e) => updatePlatformCoordinates(e, incPosition, 0)}
                sx={{ p: 0, ml: 1.5 }}
                size="small"
              >
                <KeyboardArrowRightIcon />
              </IconButton>
            </Grid>
          </Grid>
        </Grid>
        {/* Down Arrow */}
        <Grid item sx={{ alignSelf: "center" }}>
          <IconButton
            onClick={(e) => updatePlatformCoordinates(e, 0, incPosition)}
            sx={{ p: 0 }}
            size="small"
          >
            <KeyboardArrowDownIcon />
          </IconButton>
        </Grid>
      </Grid>
    );
  }
);

PlatformPositionButtons.displayName = "PlatformPositionButtons";
