import { useState } from "react";
import { useSelector } from "react-redux";
import { Button, Tooltip } from "@mui/material";
import axios from "axios";
import { Alert } from "../../Dialogs/Alert";
import CenterFocusStrongIcon from "@mui/icons-material/CenterFocusStrong";
import { roundNumber } from "../../../utils/previewHelpers";

export const GoToButton = () => {
  const { workspace, selectedPlatform, selectedContent } = useSelector(
    (state) => state.workspaceSlice
  );
  const { selectedTool } = useSelector((state) => state.toolSlice);
  const { settings } = useSelector((state) => state.settingsSlice);
  const [alert, setAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const roundUp = settings.workspace?.roundUp;

  const setAlertPopup = (message) => {
    setAlertMessage(message);
    setAlert(true);
  };

  const handleClickGoTo = async (e) => {
    e.preventDefault();

    if (typeof selectedPlatform !== "number") {
      setAlertPopup("Please select a platform");
      return;
    }

    const platformType = workspace.items[selectedPlatform].platformData.type;
    const isPetriDish = platformType === "PETRI_DISH";

    if (platformType === "BUCKET") {
      setAlertPopup("Please select a rack");
      return;
    }

    let elementName = isPetriDish ? "colony" : "well";
    if (!selectedContent.length) {
      setAlertPopup(`Please select a ${elementName}`);
      return;
    }
    if (selectedContent.length !== 1) {
      setAlertPopup(`Please select only one ${elementName}`);
      return;
    }

    let positionx, positiony;
    if (elementName === "well") {
      const well = document.getElementById(
        `well-${selectedPlatform}-${selectedContent}`
      );
      positionx =
        parseFloat(well.getAttribute("cx")) +
        workspace.items[selectedPlatform].position.x;
      positiony =
        parseFloat(well.getAttribute("cy")) +
        workspace.items[selectedPlatform].position.y;
    } else {
      positionx =
        workspace.items[selectedPlatform].content[selectedContent].position
          .col + workspace.items[selectedPlatform].position.x;
      positiony =
        workspace.items[selectedPlatform].content[selectedContent].position
          .row + workspace.items[selectedPlatform].position.y;
    }

    const selections = {
      position: {
        x: roundNumber(positionx, roundUp),
        y: roundNumber(positiony, roundUp),
      },
      workspace: workspace.name,
      tool: selectedTool,
    };

    try {
      const response = await axios.post("/api/robot/goto", selections);
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <Tooltip title="Go to here">
        <Button
          onClick={handleClickGoTo}
          sx={{
            fontSize: "small",
          }}
        >
          <CenterFocusStrongIcon />
        </Button>
      </Tooltip>

      <Alert
        title="Go to"
        open={alert}
        noMessage={"OK"}
        yesMessage={""}
        message={alertMessage}
        onCancel={() => setAlert(false)}
        onClose={() => setAlert(false)}
      />
    </>
  );
};
