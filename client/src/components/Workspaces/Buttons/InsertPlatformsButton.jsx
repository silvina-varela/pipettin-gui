import React, { useState, useEffect } from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import CircularProgress from "@mui/material/CircularProgress";
import { useSelector, useDispatch } from "react-redux";
import { styled } from "@mui/material/styles";
import { Divider, Typography } from "@mui/material";
import { getUniqueName } from "../../../utils/previewHelpers";
import { addNewItemToWorkspace } from "../../../redux/slices";

const GroupHeader = styled("div")(() => ({
  position: "sticky",
  top: "-8px",
  paddingLeft: 15,
  paddingTop: 10,
  color: "gray",
  fontVariant: "all-small-caps",
  backgroundColor: "white",
}));

const GroupItems = styled("ul")({
  padding: 6,
  fontSize: "90%",
});

export const InsertPlatformsButton = () => {
  const dispatch = useDispatch();
  const { platforms } = useSelector((state) => state.platformSlice);
  const { workspace } = useSelector((state) => state.workspaceSlice);

  const [open, setOpen] = useState(false);
  const loading = open && !platforms.length;
  const [value, setValue] = useState(null);

  const insertPlatformToWorkspace = (platform) => {
    const newItem = {
      platform: platform.name,
      platformData: { ...platform },
      name: getUniqueName(workspace.items, platform.name),
      color: platform.color,
      type: platform.type,
      position: {
        x: 0,
        y: 0,
      },
      content: [],
    };

    if (platform.type === "ANCHOR") {
      newItem.locked = true;
    } else {
      newItem.locked = false;
    }

    dispatch(addNewItemToWorkspace(newItem));
  };

  useEffect(() => {
    if (value) {
      insertPlatformToWorkspace(value);
    }
  }, [value]);

  const options = !platforms.length
    ? []
    : platforms?.map((platform) => {
        const typeNormalizer = {
          TUBE_RACK: "Tube racks",
          TIP_RACK: "Tip racks",
          PETRI_DISH: "Petri dishes",
          BUCKET: "Buckets",
          CUSTOM: "Custom",
          NO_TYPE: "No type",
          ANCHOR: "Anchors",
        };

        if (!platform.type) platform.type = "NO_TYPE";
        if (!platform.name) platform.name = "Unnamed platform";
        return {
          newType: typeNormalizer[platform.type],
          ...platform,
        };
      });

  function compare(a, b) {
    const typeValues = {
      "Tube racks": 1,
      "Tip racks": 2,
      "Petri dishes": 3,
      Custom: 4,
      Buckets: 5,
      Anchors: 6,
      "No type": 7,
    };
    // Compare by type value
    if (typeValues[a.newType] < typeValues[b.newType]) {
      return -1;
    }
    if (typeValues[a.newType] > typeValues[b.newType]) {
      return 1;
    }
    // If types are the same, compare by name
    if (a.name.toLowerCase() < b.name.toLowerCase()) {
      return -1;
    }
    if (a.name.toLowerCase() > b.name.toLowerCase()) {
      return 1;
    }
    // Objects are equal
    return 0;
  }

  return (
    <Autocomplete
      id="insert platforms"
      disableCloseOnSelect
      sx={{
        backgroundColor: "lightgray",
      }}
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
        setValue(null);
      }}
      isOptionEqualToValue={(option, value) => option._id === value._id}
      getOptionLabel={(option) => option.name}
      loading={loading}
      options={options.sort(compare)}
      groupBy={(option) => option.newType}
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      renderInput={(params) => (
        <TextField
          {...params}
          color="neutral"
          label={
            <Typography
              variant="body2"
              style={{ fontSize: "0.8rem", userSelect: "none" }}
            >
              Insert platform...
            </Typography>
          }
          size="small"
          sx={{
            width: 200,
            "& input[type=text]": {
              fontSize: "0.8rem",
              height: 13,
            },
          }}
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
      renderOption={(props, option) => {
        return (
          <li {...props} key={option._id}>
            {option.name}
          </li>
        );
      }}
      renderGroup={(params) => {
        return (
          <li key={params.key}>
            <GroupHeader>{params.group}</GroupHeader>
            <GroupItems>{params.children}</GroupItems>
            <Divider />
          </li>
        );
      }}
    />
  );
};
