import React, { useState, useEffect } from "react";
import { TextField, Chip, Box, Grid, Autocomplete } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { updateWorkspaceContents } from "../../../redux/slices";
import { colorHash, getLuma } from "../../../utils/colorHash";

export const BulkTagging = () => {
  const [newTag, setNewTag] = useState("");
  const [tags, setTags] = useState([]);
  const [tagsAll, setTagsAll] = useState([]);

  const { workspace, selectedPlatform, selectedContent } = useSelector(
    (state) => state.workspaceSlice
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (selectedPlatform !== null && selectedContent.length) {
      const currentPlatform = workspace.items[selectedPlatform];
      
      // Every tag in all contents.
      const allTags = currentPlatform.content.reduce((acc, curr) => {
        return [...acc, ...(curr.tags || [])];
      }, []);
      const allTagsDedup = Array.from(new Set(allTags));
      console.log("allTags:", allTagsDedup);
      setTagsAll(allTagsDedup);
      
      // Tags in selected contents.
      const selectedContents = currentPlatform.content.filter((content) => {
        return selectedContent.includes(content.index - 1);
      });
      const allSelectedTags = selectedContents.reduce((acc, curr) => {
        return [...acc, ...(curr.tags || [])];
      }, []);

      const newAllTags = allSelectedTags.filter((tag, tagIndex, self) => self.indexOf(tag) === tagIndex);
      console.log("newAllTags:", newAllTags);
      setTags(newAllTags);

    } else {
      setTags([]);
    }
  }, [selectedPlatform, selectedContent, workspace]);

  const handleTagChange = (event, value, reason) => {
    // The reason argument is provided by onInputChange.
    // Only update the input value if the reason is not "reset" (which happens after Enter is pressed).
    if (reason !== "reset") {
      setNewTag(value);
    }
  };

  const addUniqueTag = (tags, newTag) => {
    const tagSet = new Set(tags);
    tagSet.add(newTag);
    return Array.from(tagSet);
  };

  const handleTagUpdate = () => {
    // Get the contents.
    const platformContents = workspace.items[selectedPlatform].content;

    // Get the "list indexes" for the selected platform contents.
    const selectedContentIndexes = platformContents.reduce((indexes, content, current_index) => {
      if(selectedContent.includes(content.index - 1)){
        indexes.push(current_index);
      }
      return indexes;
    }, []) // Pass an empty list as initial value.
    // Get the list of contents to update.
    const selectedContents = selectedContentIndexes.map((i) => platformContents[i])

    // Create an array of updated content objects based on the filtered indexes
    const updatedContents = selectedContents.map((content) => {
      return {
        ...content,
        tags: addUniqueTag(content?.tags || [], newTag),
      };
    });

    // Dispatch the updateWorkspaceContents action with the platform, filtered indexes, and updated content
    dispatch(updateWorkspaceContents(selectedPlatform, selectedContentIndexes, updatedContents));

    // Clear the newTag input field
    console.log(`Clearing newTag`)
    setNewTag("");
  }

  const handleKeyDown = (e) => {
    // Check if the pressed key is "Enter" and the newTag has any content (not just whitespace)
    if (e.key === "Enter" && newTag.trim()) {
      handleTagUpdate();
    }
  };

  const handleTagDelete = (tagToDelete) => {
    const updatedTags = tags.filter((tag) => tag !== tagToDelete);
    setTags(updatedTags);

    const updatedContents = selectedContent.map((index) => {
      const currentContent = workspace.items[selectedPlatform].content[index];
      const currentTags = currentContent.tags.filter((tag) => tag !== tagToDelete);
      return {
        ...currentContent,
        tags: currentTags,
      };
    });

    dispatch(updateWorkspaceContents(selectedPlatform, selectedContent, updatedContents));
  };

  // height of the TextField
  const height = 18;
  const width = 120;
  
  return (
    <>
    <Grid container direction="row" spacing={1}>
      <Grid item>
          <Autocomplete
            freeSolo
            options={tagsAll} // Use tags as the list of options for autocomplete
            inputValue={selectedContent.length === 0 ? "" : newTag} // Controlled input value
            onInputChange={handleTagChange} // Handle input changes
            onChange={(event, newValue) => {
              // Handle when an option is selected from the drop-down list.
              if (newValue) {
                handleTagChange(event, newValue);
              }
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                // value={params.inputValue}
                size="small"
                placeholder="Manage tags..."
                onKeyDown={handleKeyDown}
                style={{ height, width }}
                InputLabelProps={{
                  style: { fontSize: 13, height, width },
                }}
                inputProps={{
                  ...params.inputProps,
                  style: { fontSize: 13, height, width, padding: "0 2px" },
                }}
                />
              )}
            disabled={selectedContent.length === 0} // Disable if no content selected
          />
      </Grid>
      <Grid item>
        {/* DISPLAY TAGS OF SELECTED CONTENTS */}
        <Box sx={{ display: "flex", flexWrap: "wrap" }}>
          {tags.map((tag, index) => {
            const tagColor = colorHash(tag).hex;
            const luma = parseInt(getLuma(tagColor), 10);
            
            return (
              <Chip
                key={index}
                label={tag}
                sx={{
                  backgroundColor: tagColor,
                  color: luma < 85 ? "white" : "black",
                  height: 28,
                }}
                onDelete={() => handleTagDelete(tag)}
              />
            );
          })}
        </Box>
      </Grid>
    </Grid>
    </>
  );
};
