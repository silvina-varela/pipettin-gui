import { useState, useEffect } from "react";
import { Button, Tooltip } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import { getRobotPosition } from "../../../redux/slices";

/*
This ReactJS component, "RobotPositionButton", is a functional component that uses various
hooks and components from React, Redux, and Material-UI to create a button that toggles
the visibility of a robot's position on a UI.
*/
export const RobotPositionButton = () => {

  // Retrieve the dispatch function from the Redux store, used to send actions to the Redux store.
  // Actions are objects that describe a change to the state,
  // and they are processed by reducers to update the state.
  const dispatch = useDispatch();

  // Extract the "showRobotPosition" state from the Redux store.
  const { showRobotPosition } = useSelector(
    // The useSelector hook allows the component to access and select parts of the state from the Redux store.
    // It takes a selector function as an argument, which receives the entire Redux state and returns the part
    // of the state that the component needs.
    (state) => state.workspaceSlice
  );

  // Initialize a local state variable show to "false".
  const [show, setShow] = useState(false);
  // Initialize a state variable "intervalId".
  const [intervalId, setIntervalId] = useState(null);

  // Sync the local "show" state with the "showRobotPosition" state
  // from the Redux store, whenever "showRobotPosition" changes.
  useEffect(() => {
    console.log("useEffect: setting 'show' to showRobotPosition=" + JSON.stringify(showRobotPosition) + " in RobotPositionButton.")
    setShow(showRobotPosition);
  }, [showRobotPosition]);

  useEffect(() => {
    let id = null;

    if (showRobotPosition) {
      id = setInterval(() => {
        dispatch(getRobotPosition());
      }, 1000);
      setIntervalId(id);
    } else {
      clearInterval(intervalId);
      setIntervalId(null);
    }

    // Clean up interval on unmount and when showRobotPosition changes
    return () => {
      clearInterval(id);
    };
  }, [showRobotPosition, dispatch]);


  const handleClickShow = () => {
    // Dispatch the getRobotPosition action to show the robot's position.
    dispatch(getRobotPosition())
  };

  const handleClickHide = () => {
    // Dispatch the getRobotPosition action with a "hide" parameter to hide the robot's position.
    dispatch(getRobotPosition("hide"));
  };

  // console.log("RobotPositionButton current show state: " + JSON.stringify(show))

  // The value of show is updated via the useEffect hook in the RobotPositionButton component.
  return (
    <>
      {show ? (
        <Tooltip title="Hide robot position">
          <Button
            onClick={handleClickHide}
            sx={{
              fontSize: "small",
              "& .MuiSvgIcon-root": {
                fontSize: "medium",
              },
            }}
          >
            <VisibilityOffIcon />
          </Button>
        </Tooltip>
      ) : (
        <Tooltip title="View robot position">
          <Button
            onClick={handleClickShow}
            sx={{
              fontSize: "small",
              "& .MuiSvgIcon-root": {
                fontSize: "medium",
              },
            }}
          >
            <VisibilityIcon />
          </Button>
        </Tooltip>
      )}
    </>
  );
};
