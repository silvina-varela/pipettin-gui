import React, { useState, useEffect } from "react";
import { TextField, Grid } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { updateWorkspaceContents } from "../../../redux/slices";
import { compose } from "redux";


export const BulkVolumes = () => {
  const [volume, setVolume] = useState(""); // State variable
  const [volumeSum, setVolumeSum] = useState(0); // State variable
  
  const { workspace, selectedPlatform, selectedContent } = useSelector(
    (state) => state.workspaceSlice
  );
  const dispatch = useDispatch();
  
  const handleVolChange = (event) => {
    setVolume(event.target.value); // Update state
  };

  const handleVolUpdate = () => {
    // Get the selected contents.
    const platformContents = workspace.items[selectedPlatform].content;

    // Get the "list indexes" for the selected platform contents.
    const selectedContentIndexes = platformContents.reduce((indexes, content, current_index) => {
      if(selectedContent.includes(content.index - 1)){
        indexes.push(current_index);
      }
      return indexes;
    }, []) // Pass an empty list as initial value.

    // Get the list of contents to update.
    const selectedContents = selectedContentIndexes.map((i) => platformContents[i])

    // Create an array of updated content objects based on the filtered indexes
    const updatedContents = selectedContents.map((content) => {
      return {...content, volume: volume};
    });

    // Dispatch the updateWorkspaceContents action with the platform, filtered indexes, and updated content
    dispatch(updateWorkspaceContents(selectedPlatform, selectedContentIndexes, updatedContents));

    // Clear the input field
    setVolume("");
  }

  const handleKeyDown = (e) => {
    // Check if the pressed key is "Enter" and the newTag has any content (not just whitespace)
    if (e.key === "Enter" && volume.trim()) {
      handleVolUpdate();
    }
  };

  // height of the TextField
  const height = 31;
  const width = 90;

  useEffect(() => {
    if (selectedPlatform !== null && selectedContent.length) {
      // Get the contents of the platform item.
      const platformContents = workspace.items[selectedPlatform].content;

      // Get the "list indexes" for the selected platform contents.
      const selectedContentIndexes = platformContents.reduce((indexes, content, current_index) => {
        if(selectedContent.includes(content.index - 1)){
          indexes.push(current_index);
        }
        return indexes;
      }, []) // Pass an empty list as initial value.

      // Get the list of selected contents.
      const totalVol = selectedContentIndexes
        .map((i) => platformContents[i])
        .reduce((accumulatedVol, content) => {
          return parseFloat(content.volume) + accumulatedVol
        }, 0); // Initial value for "accumulatedVol".

      // TODO: Make checks for maxVolume (e.g. try using "content.containerData?.maxVolume").
      setVolumeSum(totalVol);

    } else {
      setVolumeSum(0);
    }
  }, [selectedPlatform, selectedContent, workspace]);
  
  return (
    <>
      <Grid item>
        <TextField
          value={volume}
          size="small"
          placeholder={selectedContent.length === 0 ? "Set volume..." : "Sum: " + volumeSum + " uL"}
          onChange={handleVolChange} // Handle input changes
          onKeyDown={handleKeyDown}
          inputProps={{
            style: { fontSize: 13, height, width, padding: "0 5px" },
          }}
          disabled={selectedContent.length === 0} // Disable if no content selected
        />
      </Grid>
    </>
  );
};
