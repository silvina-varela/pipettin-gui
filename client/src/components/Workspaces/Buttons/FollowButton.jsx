import { useEffect, useState } from "react";
import { Button, Tooltip } from "@mui/material";
import { useSelector } from "react-redux";
import { Alert } from "../../Dialogs/Alert";
import NearMeIcon from "@mui/icons-material/NearMe";

export const FollowButton = ({ setMovingPlatform }) => {
  const [alert, setAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const [follow, setFollow] = useState(false);

  const { workspace, selectedPlatform, selectedContent } = useSelector(
    (state) => state.workspaceSlice
  );

  const setAlertPopup = (message) => {
    setAlertMessage(message);
    setAlert(true);
  };

  const handleClick = (e) => {
    e.preventDefault();
    setFollow(!follow);
  };

  const handleFollow = async () => {
    if (typeof selectedPlatform !== "number") {
      setAlertPopup("Please select a platform");
      setFollow(false);
      return;
    }

    const platformType = workspace.items[selectedPlatform].platformData.type;
    const isPetriDish = platformType === "PETRI_DISH";

    if (platformType === "BUCKET") {
      setAlertPopup("Please select a rack");
      setFollow(false);
      return;
    }

    let elementName = isPetriDish ? "colony" : "well";
    if (!selectedContent.length) {
      setAlertPopup(`Please select a ${elementName}`);
      setFollow(false);
      return;
    }
    if (selectedContent.length !== 1) {
      setAlertPopup(`Please select only one ${elementName}`);
      setFollow(false);
      return;
    }
    if (selectedContent.length === 1) {
      if (follow) {
        if (isPetriDish) {
          const currentContent = workspace.items[selectedPlatform].content.find(
            (content) => content.index - 1 === selectedContent
          );
          setMovingPlatform({
            platformName: workspace.items[selectedPlatform].name,
            element: elementName,
            selectedPlatform: selectedPlatform,
            selectedContent: selectedContent,

            colonyPosition: {
              x: currentContent?.position.col,
              y: currentContent?.position.row,
            },
          });
        } else {
          setMovingPlatform({
            platformName: workspace.items[selectedPlatform].name,
            element: elementName,
            selectedPlatform: selectedPlatform,
            selectedContent: selectedContent[0],
          });
        }
      }

      return;
    }
  };

  useEffect(() => {
    if (follow) handleFollow();
    if (!follow) setMovingPlatform({});
  }, [follow]);

  useEffect(() => {
    setFollow(false);
  }, [selectedContent, selectedPlatform]);

  return (
    <>
      <Tooltip title="Follow content">
        <Button
          onClick={handleClick}
          sx={{
            fontSize: "small",
            backgroundColor: follow ? "#f3e5f5" : null,
            "&:hover": {
              backgroundColor: follow ? "#f3e5f5" : null,
            },
          }}
        >
          <NearMeIcon />
        </Button>
      </Tooltip>
      <Alert
        open={alert}
        noMessage={"OK"}
        yesMessage={""}
        message={alertMessage}
        onCancel={() => setAlert(false)}
        onClose={() => setAlert(false)}
      />
    </>
  );
};
