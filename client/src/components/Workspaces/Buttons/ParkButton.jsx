import { useState } from "react";
import { useSelector } from "react-redux";
import { Button, Tooltip } from "@mui/material";
import axios from "axios";
import { Alert } from "../../Dialogs/Alert";
import PlumbingIcon from '@mui/icons-material/Plumbing';

export const ParkButton = () => {
  const { workspace, selectedPlatform } = useSelector(
    (state) => state.workspaceSlice
  );
  const { selectedTool } = useSelector((state) => state.toolSlice);
  const [alert, setAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");

  const setAlertPopup = (message) => {
    setAlertMessage(message);
    setAlert(true);
  };

  const handleClickPark = async (e) => {
    e.preventDefault();

    const selections = {
      workspace: workspace.name,
    // NOTE: An empty "tool" is valid in this case,
    // indicating that the controller shoudl know
    // that its toolhead is empty (i.e. has no tool).
      tool: selectedTool,
    };

    try {
      const response = await axios.post("/api/robot/park", selections);
      console.log(response);
    } catch (error) {
      console.log(error);
      setAlertPopup(error.message);
    }
  };

  return (
    <>
      <Tooltip title="Park tool">
        <Button
          onClick={handleClickPark}
          sx={{
            fontSize: "small",
          }}
        >
          <PlumbingIcon />
        </Button>
      </Tooltip>

      <Alert
        title="Park Tool"
        open={alert}
        noMessage={"OK"}
        yesMessage={""}
        message={alertMessage}
        onCancel={() => setAlert(false)}
        onClose={() => setAlert(false)}
      />
    </>
  );
};
