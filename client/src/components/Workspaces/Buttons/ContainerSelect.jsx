import { useEffect, useMemo, useState } from "react";
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { selectContainer } from "../../../redux/slices";

export const ContainerSelect = () => {
  const [container, setContainer] = useState("");
  const dispatch = useDispatch();
  const { workspace, selectedPlatform, selectedContent } = useSelector(
    (state) => state.workspaceSlice
  );
  const { containers, selectedContainer } = useSelector(
    (state) => state.containerSlice
  );

  useEffect(() => {
    const selectedContainer = containers.find((c) => c.name === container);
    dispatch(selectContainer(selectedContainer));
  }, [dispatch, container, containers]);

  const getContainerNames = () => {
    if (workspace.items[selectedPlatform]?.platformData.type === "CUSTOM") {
      const uniqueNames = [];

      return workspace.items[selectedPlatform]?.platformData.slots
        .flatMap((slot, index) => {
          if (!selectedContent.length || selectedContent.includes(index)) {
            return slot.containers.map((container) => {
              const name = container.container;
              if (name && !uniqueNames.includes(name)) {
                uniqueNames.push(name);
                return name;
              }
            });
          }
        })
        .filter(Boolean);
    } else {
      const containers = workspace.items[selectedPlatform]?.platformData.containers
      if (containers){
        return containers
          .map((container) => container.container)
          .filter(Boolean);
      } else {
        return [];
      }
    }
  };

  const containerNames = useMemo(
    () => getContainerNames(),
    [
      selectedPlatform,
      selectedContent,
      JSON.stringify(
        workspace.items[selectedPlatform]?.platformData.containers
      ),
      JSON.stringify(workspace.items[selectedPlatform]?.platformData.slots),
    ]
  );

  // Select a default container if there is only one option.
  useEffect(() => {
    if (containerNames.length === 1) {
      setContainer(containerNames[0]);
    }
  }, [containerNames]);

  const handleChange = (event) => {
    setContainer(event.target.value);
  };

  return (
    <FormControl
      variant="standard"
      sx={{ minWidth: 120 }}
      error={Boolean(container.length && !selectedContainer)}
    >
      <InputLabel id="content-select-label" sx={{ color: "#c7c7c7" }}>
        Container
      </InputLabel>
      <Select
        labelId="content-select-label"
        id="content-select"
        value={container}
        onChange={handleChange}
        label="Container"
      >
        {containerNames?.map((name, index) => (
          <MenuItem key={index} value={name}>
            {name}
          </MenuItem>
        ))}
      </Select>
      <FormHelperText>
        {container.length && !selectedContainer
          ? "Missing from the database"
          : " "}
      </FormHelperText>
    </FormControl>
  );
};
