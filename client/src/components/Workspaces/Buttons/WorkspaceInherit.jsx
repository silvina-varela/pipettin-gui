import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Autocomplete,
  TextField,
  Switch,
  Typography,
  Grid,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import { getWorkspacesAsync } from "../../../redux/slices";

const CustomSwitch = styled(Switch)(({ theme }) => ({
  padding: 8,
  "& .MuiSwitch-track": {
    borderRadius: 22 / 2,
    "&:before, &:after": {
      content: '""',
      position: "absolute",
      top: "50%",
      transform: "translateY(-50%)",
      width: 16,
      height: 16,
    },
    "&:before": {
      backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="16" width="16" viewBox="0 0 24 24"><path fill="${encodeURIComponent(
        theme.palette.getContrastText(theme.palette.primary.main)
      )}" d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z"/></svg>')`,
      left: 12,
    },
    "&:after": {
      backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="16" width="16" viewBox="0 0 24 24"><path fill="${encodeURIComponent(
        theme.palette.getContrastText(theme.palette.primary.main)
      )}" d="M19,13H5V11H19V13Z" /></svg>')`,
      right: 12,
    },
  },
  "& .MuiSwitch-thumb": {
    boxShadow: "none",
    width: 16,
    height: 16,
    margin: 2,
  },
}));

export const WorkspaceInherit = ({
  setFieldValues,
  formValues,
  setWorkspaceToImport,
  inheritData,
  setInheritData,
}) => {
  const dispatch = useDispatch();
  const { workspaces } = useSelector((state) => state.workspaceSlice);

  const [selectedWorkspace, setSelectedWorkspace] = useState(null);

  const [inheritDimensions, setInheritDimensions] = useState(false);

  useEffect(() => {
    dispatch(getWorkspacesAsync())
  }, [dispatch]);

  const handleSelectedWorkspace = (workspace) => {
    setSelectedWorkspace(workspace);
    setInheritDimensions(false);
    setInheritData({
      platforms: false,
      anchors: false,
      protocols: false,
    });
    if (workspace) setWorkspaceToImport(workspace._id);
  };

  const handleInheritDimensions = (event) => {
    setInheritDimensions(event.target.checked);
    if (event.target.checked && selectedWorkspace) {
      setFieldValues({
        ...formValues,
        width: selectedWorkspace.width || 0,
        length: selectedWorkspace.length || 0,
        height: selectedWorkspace.height || 0,
        padding: {
          top: selectedWorkspace.padding.top || 0,
          bottom: selectedWorkspace.padding.bottom || 0,
          left: selectedWorkspace.padding.left || 0,
          right: selectedWorkspace.padding.right || 0,
        },
      });
    }
  };

  const handleSwitchChange = (event, element) => {
    setInheritData({ ...inheritData, [element]: event.target.checked });
  };

  return (
    <>
      <Autocomplete
        disablePortal
        id="select-workspace"
        options={workspaces}
        sx={{ width: "50%" }}
        getOptionLabel={(option) => option.name}
        renderInput={(params) => (
          <TextField {...params} label="Select workspace" variant="standard" />
        )}
        isOptionEqualToValue={(option) =>
          option._id === selectedWorkspace._id
        }
        value={selectedWorkspace}
        onChange={(event, newValue) => {
          handleSelectedWorkspace(newValue);
        }}
        renderOption={(props, option) => {
          return (
            <li {...props} key={option._id}>
              {option.name}
            </li>
          );
        }}
      />
      <Grid
        container
        direction={"row"}
        columns={2}
        alignItems={"center"}
        spacing={2}
        mt={1}
      >
        <Grid
          item
          display={"flex"}
          justifyContent={"space-between"}
          sm={1}
          alignItems={"center"}
        >
          <Typography>Inherit dimensions and padding</Typography>
          <CustomSwitch
            checked={inheritDimensions}
            onChange={handleInheritDimensions}
            disabled={!selectedWorkspace}
          />
        </Grid>
        <Grid
          item
          display={"flex"}
          justifyContent={"space-between"}
          sm={1}
          alignItems={"center"}
        >
          <Typography>Inherit anchors</Typography>
          <CustomSwitch
            checked={inheritData.anchors}
            onChange={(event) => handleSwitchChange(event, "anchors")}
            disabled={!selectedWorkspace}
          />
        </Grid>
        <Grid
          item
          display={"flex"}
          justifyContent={"space-between"}
          sm={1}
          alignItems={"center"}
        >
          <Typography>Inherit platforms</Typography>
          <CustomSwitch
            checked={inheritData.platforms}
            onChange={(event) => handleSwitchChange(event, "platforms")}
            disabled={!selectedWorkspace}
          />
        </Grid>
        <Grid
          item
          display={"flex"}
          justifyContent={"space-between"}
          sm={1}
          alignItems={"center"}
        >
          <Typography>Inherit protocols</Typography>
          <CustomSwitch
            checked={inheritData.protocols}
            onChange={(event) => handleSwitchChange(event, "protocols")}
            disabled={!selectedWorkspace}
          />
        </Grid>
      </Grid>
    </>
  );
};
