/* eslint-disable react/no-unescaped-entities */
import { useEffect, useState } from "react";
import { Box, Grid, TextField, Typography } from "@mui/material";
import { ContainerSelect } from "./ContainerSelect";
import { PlatformForm } from "../../Platforms/PlatformForm";
import { useSelector } from "react-redux";
import { Popup } from "../../Dialogs/Popup";

export const ManageContainerAlertMessage = ({
  inputNumber = false,
  containerType,
  setInputValue,
}) => {
  const [openPopup, setOpenPopup] = useState(false);
  const [platform, setPlatform] = useState({});
  const [number, setNumber] = useState(0);

  const { workspace, selectedPlatform } = useSelector(
    (state) => state.workspaceSlice
  );

  useEffect(() => {
    if (workspace.items && workspace.items[selectedPlatform]) {
      const platform = workspace.items[selectedPlatform].platformData;
      setPlatform(platform);
    }
  }, [workspace, selectedPlatform]);

  const handleEditContainers = () => {
    setOpenPopup(true);
  };

  const handleNumberChange = (e) => {
    setNumber(e.target.value);
    setInputValue(e.target.value);
  };

  return (
    <>
      <Box>
        <Grid container alignItems="center">
          <Typography
            sx={{
              display: "inline-block",
              marginRight: 1,
            }}
          >
            Add
          </Typography>
          <Box>
            <ContainerSelect />
          </Box>
          <Typography
            sx={{
              display: "inline-block",
              marginRight: 1,
              ml: 1,
            }}
          >
            to
          </Typography>
          {inputNumber ? (
            <>
              <TextField
                id="outlined-number"
                type="number"
                variant="standard"
                sx={{ maxWidth: "60px", marginLeft: 1 }}
                size="small"
                value={number}
                inputProps={{
                  min: 0,
                  max: 100,
                }}
                onChange={handleNumberChange}
              />
              <Typography
                sx={{
                  display: "inline-block",
                  marginLeft: 1,
                }}
              >
                {containerType}(s)
              </Typography>
            </>
          ) : (
            <Typography
              sx={{
                display: "inline-block",
              }}
            >
              the selected {containerType}(s).
            </Typography>
          )}
        </Grid>
        <Typography fontSize={13} mt={0} color={"#686868"}>
          Link more containers to the '{platform.name}' by clicking{" "}
          <a href="#"
             onClick={handleEditContainers}
             style={{ color: "primary", textDecoration: "underline" }}
          >here</a>.
        </Typography>
      </Box>
      <Popup
        title="Edit Platform"
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        component={
          <PlatformForm isEditMode platform={platform} editContainers formStep={1}/>
        }
        width="md"
      />
    </>
  );
};
