import { useMemo } from "react";
import { useNavigate } from "react-router-dom";
import {
  Card,
  CardContent,
  Typography,
  CardActionArea,
  CardMedia,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import moment from "moment";

const WorkspaceCard = ({ workspace, handleCardClick }) => {
  const navigate = useNavigate();

  const getThumbnailDataURL = () => {
    const svgElement = document.createElement("div");
    svgElement.innerHTML = workspace.thumbnail;

    // Convert the SVG to a data URL
    const svgDataUrl = `data:image/svg+xml;base64,${btoa(
      svgElement.innerHTML
    )}`;
    return svgDataUrl;
  };

  const svgDataURL = useMemo(() => {
    if (workspace?.thumbnail) {
      return getThumbnailDataURL();
    }
    return null;
  }, [workspace?.thumbnail]);

  if (!workspace) {
    return (
      <Card sx={{ maxWidth: 345 }} elevation={0} onClick={handleCardClick}>
        <CardActionArea>
          <CardMedia
            sx={{
              border: "1px solid black",
              minHeight: 100,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <AddIcon />
          </CardMedia>
          <CardContent>
            <Typography>New workspace</Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    );
  }
  return (
    <Card
      sx={{ maxWidth: 345 }}
      elevation={0}
      onClick={() => navigate(`workspace/${workspace._id}`)}
    >
      <CardActionArea>
        {svgDataURL ? (
          <CardMedia
            component="img"
            src={svgDataURL}
            alt={workspace.name}
            sx={{
              objectFit: "scale-down",
              height: 100,
              backgroundColor: "lightgrey",
            }}
          />
        ) : (
          <CardMedia
            sx={{
              minHeight: 100,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "lightgrey",
            }}
          >
            {" "}
          </CardMedia>
        )}
        <CardContent>
          <Typography>{workspace.name}</Typography>
          <Typography variant="caption">
            Last updated:{" "}
            {workspace.updatedAt
              ? moment(workspace.updatedAt).fromNow()
              : workspace.createdAt
              ? moment(workspace.createdAt).fromNow()
              : "no data available"}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export { WorkspaceCard };
