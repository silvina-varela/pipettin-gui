import React, { useState } from "react";
import {
  Typography,
  IconButton,
  Tooltip,
  Grid,
  Popover,
  Paper,
  Fade,
} from "@mui/material";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { useNavigate } from "react-router-dom";
import { Alert } from "../Dialogs/Alert";
import { useDispatch, useSelector } from "react-redux";
import { WorkspaceForm } from "./WorkspaceForm";
import { Popup } from "../Dialogs/Popup";
import SaveOutlinedIcon from "@mui/icons-material/SaveOutlined";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import axios from "axios";
import { useSnackAlert } from "../../hooks/useSnackAlert";
import { formatDate } from "../../utils/formatDate";
import { deleteWorkspaceAsync, saveWorkspaceAsync } from "../../redux/slices";

export const WorkspaceHeader = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const addSnack = useSnackAlert();

  const [alert, setAlert] = useState({
    open: false,
    title: "",
    message: "",
    switch: false,
    yesMessage: "Yes",
    noMessage: "No",
    onConfirm: () => {},
    onCancel: () => {},
    onClose: () => {},
  });

  const { workspace } = useSelector((state) => state.workspaceSlice);
  const { loading } = useSelector((state) => state.globalSlice);

  const [openEditPopup, setOpenEditPopup] = useState(false);
  const [editPopupTitle, setEditPopupTitle] = useState("");
  const [openEditComponent, setOpenEditComponent] = useState("");

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  /**MENU HANDLERS */
  const handleMenuClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleCloseMenu = () => {
    setAnchorEl(null);
  };

  const handleDeleteAlert = (e) => {
    e.preventDefault();
    setAlert({
      open: true,
      title: "Delete workspace",
      type: "delete",
      message: "Are you sure you want to permanently delete the workspace?",
      switch: false,
      yesMessage: "Delete workspace",
      noMessage: "Cancel",
      onConfirm: (e) => handleDeleteWorkspace(e),
      onCancel: () => setAlert({ open: false }),
      onClose: () => setAlert({ open: false }),
    });
    handleCloseMenu();
  };

  const handleCloseAlert = () => {
    setAlert({
      open: true,
      title: "Close workspace",
      message: "Do you want to save this workspace before closing it?",
      switch: false,
      onConfirm: () => saveWorkspaceAndNavigate(),
      onCancel: () => goToDashboard(),
      onClose: () => setAlert({ open: false }),
    });
    handleCloseMenu();
  };

  /**SAVE, DUPLICATE & DELETE BUTTONS */
  const goToDashboard = () => {
    navigate(`/`);
  };

  const handleDeleteWorkspace = () => {
    dispatch(deleteWorkspaceAsync(workspace._id));
    goToDashboard();
  };



  const saveWorkspaceAndNavigate = () => {
    dispatch(saveWorkspaceAsync())
      .unwrap()
      .then(() => {
        goToDashboard();
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setAlert({ open: false });
      });
  };

  const handleDuplicateWorkspace = async (e, checked) => {
    e.preventDefault();

    await axios
      .post(`/api/workspaces/duplicate?withProtocol=${checked}`, workspace)
      .then((workspaceResponse) => {
        setAlert({
          open: true,
          title: " ",
          message:
            "Your workspace has been successfully duplicated. Would you like to open the new workspace?",
          onConfirm: () => {
            navigate(`/workspace/${workspaceResponse.data._id}`);
            setAlert({ open: false });
          },
          onCancel: () => setAlert({ open: false }),
          onClose: () => setAlert({ open: false }),
        });
      })
      .catch((error) => console.log(error.response.data));
  };

  const handleDuplicateAlert = () => {
    setAlert({
      open: true,
      title: "Duplicate workspace",
      message: "Do you want to duplicate this workspace?",
      switch: true,
      onConfirm: (e, checked) => handleDuplicateWorkspace(e, checked),
      onCancel: () => setAlert({ open: false }),
      onClose: () => setAlert({ open: false }),
    });
    handleCloseMenu();
  };

  const handleSaveWorkspace = (e) => {
    e.preventDefault();

    dispatch(saveWorkspaceAsync())
      .unwrap()
      .then(() => {
        addSnack("Workspace saved", "success");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleOpenEditPopup = (event, title, component) => {
    event.preventDefault();
    setEditPopupTitle(title);
    setOpenEditComponent(component);
    setOpenEditPopup(true);
    handleCloseMenu();
  };

  const handleCloseEditPopup = () => {
    setOpenEditPopup(false);
    setEditPopupTitle("");
    setOpenEditComponent("");
  };

  const [anchorDetails, setAnchorDetails] = useState(null);
  const openPopper = Boolean(anchorDetails);
  const id = open ? "simple-popper" : undefined;

  const openDetails = (e) => {
    e.preventDefault();

    setAnchorDetails(anchorDetails ? null : e.currentTarget);
  };

  if(!loading) return (
    <>
      <Grid
        container
        direction="row"
        sx={{
          display: "flex",
          alignItems: "center",
          pl: 1,
          mt: 1,
        }}
      >
        <Grid item>
          <Grid container sx={{ display: "flex", alignItems: "center" }}>
            <Grid item>
              <Typography component="div" style={{ flexGrow: 2 }}>
                {workspace ? workspace.name : "Untitled Workspace"}
              </Typography>
            </Grid>
            <Popover
              id={id}
              open={openPopper}
              TransitionComponent={Fade}
              anchorEl={anchorDetails}
              onClose={openDetails}
              anchorOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
            >
              <Paper sx={{ maxWidth: "20vw", p: 2 }}>
                <Typography variant="subtitle2">{workspace.name} </Typography>
                <Typography variant="caption" component="p" fontSize={"70%"}>
                  {" "}
                  Created: {formatDate(workspace.createdAt, true)}
                </Typography>
                <Typography variant="caption" component="p" fontSize={"70%"}>
                  {" "}
                  Last updated: {formatDate(workspace.updatedAt, true)}
                </Typography>
                <Typography variant="body2" mt={1}>
                  {" "}
                  {workspace.description}
                </Typography>
              </Paper>
            </Popover>
            <Grid item ml={2}>
              <Tooltip title="Workspace details">
                <IconButton aria-label="save" onClick={openDetails}>
                  <InfoOutlinedIcon fontSize="small" />
                </IconButton>
              </Tooltip>
            </Grid>
            <Grid item>
              <Tooltip title="Save">
                <IconButton aria-label="save" onClick={handleSaveWorkspace}>
                  <SaveOutlinedIcon fontSize="small" />
                </IconButton>
              </Tooltip>
            </Grid>
            <Grid item>
              <IconButton
                id="basic-button"
                aria-controls={open ? "basic-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
                onClick={handleMenuClick}
              >
                <MoreVertIcon fontSize="small" />
              </IconButton>
            </Grid>
          </Grid>
        </Grid>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleCloseMenu}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "right",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}
        >
          <MenuItem
            aria-label="edit"
            onClick={(event) =>
              handleOpenEditPopup(
                event,
                "Edit workspace",
                <WorkspaceForm
                  handleClosePopup={handleCloseEditPopup}
                  isEditMode={true}
                />
              )
            }
          >
            Edit
          </MenuItem>
          <MenuItem aria-label="duplicate" onClick={handleDuplicateAlert}>
            Duplicate
          </MenuItem>
          <MenuItem aria-label="delete" onClick={handleDeleteAlert}>
            Delete
          </MenuItem>
          <MenuItem onClick={handleCloseAlert}>Close</MenuItem>
        </Menu>
      </Grid>

      <Alert
        type={alert.type}
        title={alert.title}
        open={alert.open}
        message={alert.message}
        switchOn={alert.switch}
        onConfirm={alert.onConfirm}
        onCancel={alert.onCancel}
        onClose={alert.onClose}
        yesMessage={alert.yesMessage}
        noMessage={alert.noMessage}
      />
      <Popup
        title={editPopupTitle}
        openPopup={openEditPopup}
        setOpenPopup={setOpenEditPopup}
        component={openEditComponent}
      />
    </>
  );
};
