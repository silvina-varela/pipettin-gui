import React, { useEffect, useState } from "react";
import { Typography } from "@mui/material";
import TextField from "@mui/material/TextField";
import { useSelector } from "react-redux";

export const PlatformCoordinates = React.memo(
  ({ platformCoords, setPlatformCoords, handleSaveCoordinates }) => {
    const [editXCoordinates, setEditXCoordinates] = useState(false);
    const [editYCoordinates, setEditYCoordinates] = useState(false);
    const [editZCoordinates, setEditZCoordinates] = useState(false);

    const { workspace, selectedPlatform } = useSelector(
      (state) => state.workspaceSlice
    );
    const { settings } = useSelector((state) => state.settingsSlice);

    const [updatedPlatform, setUpdatedPlatform] = useState(null);
    const [incPosition, setIncPosition] = useState(0);
    const [platformWidth, setPlatformWidth] = useState(0);
    const [platformHeight, setPlatformHeight] = useState(0);

    useEffect(() => {
      if (
        workspace &&
        Object.prototype.hasOwnProperty.call(workspace, "items")
        &&
        selectedPlatform >= 0
      ) {
        setUpdatedPlatform(workspace?.items[selectedPlatform]);
      }

      if (
        settings &&
        Object.prototype.hasOwnProperty.call(settings, "workspace") &&
        Object.prototype.hasOwnProperty.call(settings.workspace, "incrementPosition")
      ) {
        setIncPosition(settings.workspace.incrementPosition);
      }
    }, []);

    useEffect(() => {
      if (updatedPlatform && Object.prototype.hasOwnProperty.call(updatedPlatform, "platformData")) {
        setPlatformWidth(updatedPlatform.platformData.width);
        setPlatformHeight(updatedPlatform.platformData.length);
      }
    }, [updatedPlatform]);

    const handleBlur = (event) => {
      event.target.blur();
      handleSaveCoordinates(platformCoords);
      setEditXCoordinates(false);
      setEditYCoordinates(false);
      setEditZCoordinates(false);
    };

    const handleXCoord = (e) => {
      let newCoords = [...platformCoords];
      newCoords[0] = e.target.value;
      setPlatformCoords(
        newCoords[0],
        newCoords[1],
        newCoords[2],
        platformWidth,
        platformHeight
      );
    };

    const handleYCoord = (e) => {
      let newCoords = [...platformCoords];
      newCoords[1] = e.target.value;
      setPlatformCoords(
        newCoords[0],
        newCoords[1],
        newCoords[2],
        platformWidth,
        platformHeight
      );
    };

    const handleZCoord = (e) => {
      let newCoords = [...platformCoords];
      newCoords[2] = e.target.value;
      setPlatformCoords(
        newCoords[0],
        newCoords[1],
        newCoords[2],
        platformWidth,
        platformHeight
      );
    };

    const handleKeyDown = (event) => {
      if (event.key === "Enter") {
        event.target.blur();
      }
    };

    const handleCoordsClick = (e, coord) => {
      e.preventDefault();
      e.stopPropagation();

      const currentPlatform = workspace?.items[selectedPlatform];

      if (!currentPlatform?.locked) {
        switch (coord) {
          case "x":
            setEditXCoordinates(true);
            break;
          case "y":
            setEditYCoordinates(true);
            break;
          case "z":
            setEditZCoordinates(true);
            break;
          default:
            return;
        }
      }
    };

    const handleEditClick = (e) => {
      e.preventDefault();
      e.stopPropagation();
    };

    return (
      <>
        {editXCoordinates ? (
          <TextField
            type="number"
            variant="standard"
            sx={{
              maxWidth: "55px",
              "& .MuiInput-underline:after": {
                borderBottomColor: "black",
              },
            }}
            size="small"
            value={platformCoords[0]}
            inputProps={{
              style: { fontSize: "0.7rem" },
              step: incPosition,
            }}
            onClick={handleEditClick}
            onChange={(e) => handleXCoord(e)}
            onKeyDown={handleKeyDown}
            onBlur={handleBlur}
          />
        ) : (
          <Typography
            variant="caption"
            sx={{
              cursor: "pointer",
              ml: 0.5,
            }}
            onClick={(e) => handleCoordsClick(e, "x")}
          >
            x: {platformCoords[0]}
          </Typography>
        )}
        <Typography
          variant="caption"
          sx={{
            ml: 0.5,
          }}
        >
          /
        </Typography>
        {editYCoordinates ? (
          <TextField
            type="number"
            variant="standard"
            sx={{
              maxWidth: "55px",
              "& .MuiInput-underline:after": {
                borderBottomColor: "black",
              },
            }}
            size="small"
            inputProps={{
              style: { fontSize: "0.7rem" },
              step: incPosition,
            }}
            value={platformCoords[1]}
            onClick={handleEditClick}
            onChange={(e) => handleYCoord(e)}
            onKeyDown={handleKeyDown}
            onBlur={handleBlur}
          />
        ) : (
          <Typography
            variant="caption"
            sx={{
              cursor: "pointer",
              ml: 0.5,
            }}
            onClick={(e) => handleCoordsClick(e, "y")}
          >
            y: {platformCoords[1]}
          </Typography>
        )}
        <Typography
          variant="caption"
          sx={{
            ml: 0.5,
          }}
        >
          /
        </Typography>
        {editZCoordinates ? (
          <TextField
            type="number"
            variant="standard"
            sx={{
              maxWidth: "55px",
              "& .MuiInput-underline:after": {
                borderBottomColor: "black",
              },
            }}
            size="small"
            inputProps={{
              style: { fontSize: "0.7rem" },
              step: incPosition,
            }}
            value={platformCoords[2]}
            onClick={handleEditClick}
            onChange={(e) => handleZCoord(e)}
            onKeyDown={handleKeyDown}
            onBlur={handleBlur}
          />
        ) : (
          <Typography
            variant="caption"
            sx={{
              cursor: "pointer",
              ml: 0.5,
            }}
            onClick={(e) => handleCoordsClick(e, "z")}
          >
            z: {platformCoords[2]}
          </Typography>
        )}
      </>
    );
  }
);

PlatformCoordinates.displayName = "PlatformCoordinates";
