import { useRef, useEffect, useState } from "react";
import {
  IconButton,
  LinearProgress,
  Typography,
  Tooltip,
  Box,
} from "@mui/material";
import ZoomInIcon from "@mui/icons-material/ZoomIn";
import ZoomOutIcon from "@mui/icons-material/ZoomOut";
import RestoreZoom from "@mui/icons-material/YoutubeSearchedFor";
import { Platform } from "../Graphics/Platform";
import { select } from "d3-selection";
import { useSelector, useDispatch } from "react-redux";
import { roundNumber } from "../../utils/previewHelpers";
import RestorePageIcon from "@mui/icons-material/RestorePage";
import { PlatformCoordinates } from "./PlatformCoordinates";
import { PlatformPositionButtons } from "./Buttons/PlatformPositionButtons";
import { PositionData } from "./PositionData";
import { useFollowPlatform } from "../../hooks/useFollowPlatform";
import { deleteWorkspaceItem, selectPlatform, updatePlatformPosition } from "../../redux/slices";

export const WorkspacePreview = ({ movingPlatform }) => {
  const { workspace, showRobotPosition, robotPosition, selectedPlatform } =
    useSelector((state) => state.workspaceSlice);
  const { selectedTool } = useSelector((state) => state.toolSlice);
  const { settings } = useSelector((state) => state.settingsSlice);
  const { loading } = useSelector((state) => state.globalSlice);

  const followPlatform = useFollowPlatform();
  const { width, length, height } = workspace;
  const workspaceZoom = settings.workspace?.zoom;
  const zoom = workspaceZoom ? workspaceZoom / 100 : 1;
  const workspaceWidth = width;
  const workspaceLength = length;

  const padding = workspace.padding
    ? workspace.padding
    : {
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
      };

  const dispatch = useDispatch();
  const previewRef = useRef(null);
  const svgRef = useRef(null);
  const containerRef = useRef(null);
  const [roundUp, setRoundUp] = useState(2);

  useEffect(() => {
    if (Object.keys(settings).length) {
      setRoundUp(settings.workspace.roundUp);
    }
  }, [JSON.stringify(settings)]);

  /** Robot position crosshair */
  useEffect(() => {
    // Select the SVG element using d3.select and svgRef.current
    const svg = select(svgRef.current);

    // Check if showRobotPosition is true and robotPosition is not null or undefined
    if (showRobotPosition && robotPosition) {
      console.log("Drawing and animating robot position crosshair: " + JSON.stringify(robotPosition));

      // Select existing group with class 'robotPosition' or create a new one if it doesn't exist
      let position = svg.select(".robotPosition");
      if (position.empty()) {
        position = svg.append("g").classed("robotPosition", true);
      }

    // Create or update a circle element for the center point of the crosshair
    position
      .selectAll("circle")
      .data([robotPosition]) // Bind data to the circle element
      .join("circle") // Join data to elements
      .attr("cx", d => d.x) // Set the x-coordinate of the circle
      .attr("cy", d => d.y) // Set the y-coordinate of the circle
      .attr("r", 5) // Set the radius of the circle
      .style("stroke", "rgba(255, 0, 0, 0.5)"); // Set the stroke color (transparent red)

    // Create or update a vertical line element for the crosshair
    position
      .selectAll("line.vertical")
      .data([robotPosition]) // Bind data to the line element
      .join(
        enter => enter.append("line").classed("vertical", true), // Append a new line element for enter selection
        update => update, // Update existing line elements
        exit => exit.remove() // Remove line elements that are no longer needed
      )
      .attr("x1", d => d.x) // Set the starting x-coordinate of the line
      .attr("y1", d => d.y - 7) // Set the starting y-coordinate of the line
      .attr("x2", d => d.x) // Set the ending x-coordinate of the line
      .attr("y2", d => d.y + 7) // Set the ending y-coordinate of the line
      .style("stroke", "rgba(0, 0, 0, 0.5)") // Set the stroke color (transparent black)
      .transition() // Apply transition for smooth animation
      .duration(500) // Set the duration of the transition animation
      .attr("y1", d => d.y - 7) // Animate the starting y-coordinate of the line
      .attr("y2", d => d.y + 7); // Animate the ending y-coordinate of the line

    // Create or update a horizontal line element for the crosshair
    position
      .selectAll("line.horizontal")
      .data([robotPosition]) // Bind data to the line element
      .join(
        enter => enter.append("line").classed("horizontal", true), // Append a new line element for enter selection
        update => update, // Update existing line elements
        exit => exit.remove() // Remove line elements that are no longer needed
      )
      .attr("x1", d => d.x - 7) // Set the starting x-coordinate of the line
      .attr("y1", d => d.y) // Set the starting y-coordinate of the line
      .attr("x2", d => d.x + 7) // Set the ending x-coordinate of the line
      .attr("y2", d => d.y) // Set the ending y-coordinate of the line
      .style("stroke", "rgba(0, 0, 0, 0.5)") // Set the stroke color (transparent black)
      .transition() // Apply transition for smooth animation
      .duration(500) // Set the duration of the transition animation
      .attr("x1", d => d.x - 7) // Animate the starting x-coordinate of the line
      .attr("x2", d => d.x + 7); // Animate the ending x-coordinate of the line


    } else {
      // If showRobotPosition is false or robotPosition is null or undefined, remove the 'robotPosition' group.
      svg.select(".robotPosition").remove();
    }
  }, [showRobotPosition, robotPosition]);

  /** Mouse and platform coordinates info */
  const [mouseCoords, setMouseCoords] = useState([0.0, 0.0]);
  const [platformCoords, setPlatformCoords] = useState([0.0, 0.0, 0.0]);
  const [mouseWarning, setMouseWarning] = useState(true);
  const [platformWarning, setPlatformWarning] = useState(true);
  const [incPosition, setIncPosition] = useState(0);

  useEffect(() => {
    if (
      settings &&
      Object.prototype.hasOwnProperty.call(settings, "workspace") &&
      Object.prototype.hasOwnProperty.call(settings.workspace, "incrementPosition")
    ) {
      setIncPosition(settings.workspace.incrementPosition);
    }
  }, [selectedPlatform, workspace, settings]);

  const isInsideWorkspace = (x, y) => {
    if (x > workspaceWidth || x < 0 || y > workspaceLength || y < 0)
      setMouseWarning(true);
    else setMouseWarning(false);
  };

  const isPlatformInsideWorkspace = (x, y, platformWidth, platformHeight) => {
    if (
      x + platformWidth > workspaceWidth ||
      x < 0 ||
      y + platformHeight > workspaceLength ||
      y < 0
    )
      setPlatformWarning(true);
    else setPlatformWarning(false);
  };

  const [lastClickedIndex, setLastClickedIndex] = useState(null);

  const handleSetPlatformCoords = (x, y, z, platformWidth, platformHeight) => {
    if (z === undefined) z = 0;
    isPlatformInsideWorkspace(x, y, platformWidth, platformHeight);
    setPlatformCoords([
      roundNumber(x, roundUp),
      roundNumber(y, roundUp),
      roundNumber(z, roundUp),
    ]);
  };

  useEffect(() => {
    //VER
    const handleArrowKey = (event) => {
        if (typeof selectedPlatform === "number") {
          const currentPlatform = workspace?.items[selectedPlatform];
          const [x, y, z] = platformCoords;
          const currentPlatformWidth = currentPlatform?.platformData?.width || 0;
          const currentPlatformHeight =
            currentPlatform?.platformData?.length || 0;

          if (!currentPlatform.locked) {
            switch (event.key) {
              case "ArrowUp":
                handleSetPlatformCoords(
                  x,
                  y - incPosition,
                  z,
                  currentPlatformWidth,
                  currentPlatformHeight
                );
                handleSaveCoordinates([x, y - incPosition, z]);
                break;
              case "ArrowDown":
                handleSetPlatformCoords(
                  x,
                  y + incPosition,
                  z,
                  currentPlatformWidth,
                  currentPlatformHeight
                );
                handleSaveCoordinates([x, y + incPosition, z]);
                break;
              case "ArrowLeft":
                handleSetPlatformCoords(
                  x - incPosition,
                  y,
                  z,
                  currentPlatformWidth,
                  currentPlatformHeight
                );
                handleSaveCoordinates([x - incPosition, y, z]);
                break;
              case "ArrowRight":
                handleSetPlatformCoords(
                  x + incPosition,
                  y,
                  z,
                  currentPlatformWidth,
                  currentPlatformHeight
                );
                handleSaveCoordinates([x + incPosition, y, z]);
                break;
              case "PageUp":
                handleSetPlatformCoords(
                  x,
                  y,
                  z + incPosition,
                  currentPlatformWidth,
                  currentPlatformHeight
                );
                handleSaveCoordinates([x, y, z + incPosition]);
                break;
              case "PageDown":
                handleSetPlatformCoords(
                  x,
                  y,
                  z - incPosition,
                  currentPlatformWidth,
                  currentPlatformHeight
                );
                handleSaveCoordinates([x, y, z - incPosition]);
                break;
              default:
                break;
            }
          }
        }
    };

    window.addEventListener("keydown", handleArrowKey);
    return () => {
      window.removeEventListener("keydown", handleArrowKey);
    };
  }, [selectedPlatform, platformCoords, movingPlatform]);

  const handleSaveCoordinates = async (coords) => {
    dispatch(updatePlatformPosition({x: coords[0], y: coords[1],  z: coords[2]}))

    if (Object.keys(movingPlatform).length > 0)
      followPlatform(
        movingPlatform,
        {x: coords[0], y: coords[1],  z: coords[2]},
        selectedTool,
        roundUp
      );
  };

  useEffect(() => {
    const container = containerRef.current;
    const svgElement = previewRef.current;

    const handleMouseMove = (event) => {
      const { left, top, width, height } = svgElement.getBoundingClientRect();
      const widthDif = workspaceWidth / width;
      const lengthDif = workspaceLength / height;

      const x = (event.clientX - left) * widthDif;
      const y = (event.clientY - top) * lengthDif;

      isInsideWorkspace(x, y);
      setMouseCoords([roundNumber(x, roundUp), roundNumber(y, roundUp)]);
    };

    const handleMouseOut = () => {
      setMouseCoords([0.0, 0.0]);
    };

    if (container && svgElement) {
      container.addEventListener("mousemove", handleMouseMove);
      container.addEventListener("mouseout", handleMouseOut);
    }
    return () => {
      if (container && svgElement) {
        container.removeEventListener("mousemove", handleMouseMove);
        container.removeEventListener("mouseout", handleMouseOut);
      }
    };
  }, [
    previewRef.current,
    containerRef.current,
    loading,
    JSON.stringify(settings),
  ]);

  /** Panning */
  const [panningCoords, setPanningCoords] = useState({ x: 0, y: 0 });

  // Scroll down
  const handlePanDown = () => {
    setPanningCoords({ x: panningCoords.x, y: panningCoords.y + 5 }); // Adjustable speed
  };

  // Scroll up
  const handlePanUp = () => {
    setPanningCoords({ x: panningCoords.x, y: panningCoords.y - 5 }); // Adjustable speed
  };

  const [isPanning, setIsPanning] = useState(false);
  const [panStartX, setPanStartX] = useState(0);
  const [panStartY, setPanStartY] = useState(0);

  // Panning speed
  const panSpeed = 0.45;

  const handlePanStart = (event) => {
    setIsPanning(true);
    setPanStartX(event.clientX);
    setPanStartY(event.clientY);
  };

  const handlePanMove = (event) => {
    if (isPanning) {
      const deltaX = event.clientX - panStartX;
      const deltaY = event.clientY - panStartY;
      setPanStartX(event.clientX);
      setPanStartY(event.clientY);

      // Update the coordinates based on the panning movement
      setPanningCoords({
        x: panningCoords.x + deltaX * panSpeed,
        y: panningCoords.y + deltaY * panSpeed,
      });
    }
  };

  const handlePanEnd = () => {
    setIsPanning(false);
  };

  const resetPanning = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setPanningCoords({
      x: 0,
      y: 0,
    });
  };

  // Scroll and move listeners
  useEffect(() => {
    const handleScroll = (event) => {
      if (
        event.srcElement.id === "workspaceSVG" ||
        (event.srcElement.ownerSVGElement &&
          event.srcElement.ownerSVGElement.id === "workspaceSVG")
      ) {
        event.preventDefault();
        if (event.deltaY < 0) {
          handlePanDown();
        } else {
          handlePanUp();
        }
      }
    };
    window.addEventListener("mousemove", handlePanMove);
    window.addEventListener("wheel", handleScroll, { passive: false });

    return () => {
      window.removeEventListener("mousemove", handlePanMove);
      window.removeEventListener("wheel", handleScroll);
    };
  }, [isPanning, panStartX, panStartY, panningCoords]);

  // Stop moving when mouse outside container
  useEffect(() => {
    const handleMouseOutsideContainer = (event) => {
      if (isPanning && !containerRef.current.contains(event.target)) {
        setIsPanning(false);
      }
    };

    window.addEventListener("mousemove", handleMouseOutsideContainer);

    return () => {
      window.removeEventListener("mousemove", handleMouseOutsideContainer);
    };
  }, [isPanning]);

  /** Zoom */

  const [scale, setScale] = useState(zoom); // Initial scale value

  useEffect(() => {
    // Update the scale value when workspaceZoom changes
    if (workspaceZoom) {
      setScale(workspaceZoom / 100);
    }
  }, [workspaceZoom]);

  const handleZoomIn = (e) => {
    e.stopPropagation();
    setScale(scale + 0.1); // Increase the scale by 0.1
  };

  const handleZoomOut = (e) => {
    e.stopPropagation();
    if (scale > 0.1) {
      setScale(scale - 0.1); // Decrease the scale by 0.1 (minimum scale of 0.1)
    }
  };

  const handleResetZoom = (e) => {
    e.stopPropagation();
    setScale(zoom);
  };

  useEffect(() => {
    const handleMouseWheel = (event) => {
      if (event.ctrlKey) {
        event.preventDefault();

        if (event.deltaY < 0) {
          setScale(scale + 0.1);
        } else {
          setScale(scale - 0.1);
        }
      }
    };

    window.addEventListener("wheel", handleMouseWheel, { passive: false });
    return () => {
      window.removeEventListener("wheel", handleMouseWheel);
    };
  }, [scale]);

  /** Delete platform */
  const handlePlatformDelete = (platformIndex) => {
    dispatch(deleteWorkspaceItem(platformIndex));
  };

  /** Deselect platform when clicking outside */
  const handleClickOutside = (e) => {
    e.preventDefault();
    if (typeof selectedPlatform === "number") {
      dispatch(selectPlatform({ platform: "", content: [] }));
    }
  };
  if (loading || !Object.values(settings).length)
    return (
      <LinearProgress
        sx={{
          marginTop: 20,
          marginX: 10,
        }}
      />
    );
  else
    return (
      <Box
        ref={containerRef}
        onMouseDown={handlePanStart}
        onMouseUp={handlePanEnd}
        onClick={handleClickOutside}
        sx={{
          cursor: isPanning ? "grabbing" : "grab",
          width: "auto",
          height: "85%",
          position: "relative",
        }}
      >
        <Box
          sx={{
            width: "100%",
            height: "100%",
            backgroundColor: "#ededed",
            backgroundSize: "1rem 1rem",
            backgroundImage:
              "radial-gradient(circle at 0.5rem 0.5rem, lightgrey 0.07rem, transparent 0px)",
            position: "relative",
          }}
        ></Box>
        <Box>
          <svg
            id="workspaceSVG"
            preserveAspectRatio="xMinYMin meet"
            viewBox={`-${padding.left} -${padding.top} ${
              width ? width + padding.left + padding.right : 300
            } ${length ? length + padding.top + padding.bottom + 15 : 180}`}
            style={{
              width: "100%",
              height: "100%",
              position: "absolute",
              top: 0,
              left: 0,
            }}
          >
            <g
              style={{
                transform: `scale(${scale}) translate(${panningCoords.x}px, ${panningCoords.y}px) `,
                transformOrigin: "left top",
              }}
              ref={svgRef}
              id="workspacePreview"
            >
              <rect
                ref={previewRef}
                width={width}
                height={length}
                x={0}
                y={0}
                fill="transparent"
                strokeWidth="1px"
                stroke="#252525"
                strokeDasharray="5, 10"
              />
              <text
                x={width && width}
                y={length && length + 5}
                textAnchor="end"
                dominantBaseline="hanging"
                fontSize="8px"
                style={{ userSelect: "none" }}
              >
                {`${width || 0} x ${length || 0} x ${height || 0} mm`}
              </text>

              {[
                ...(workspace.items?.filter(
                  (platform) => platform?.platformData?.type === "ANCHOR"
                ) || []),
                ...(workspace.items?.filter(
                  (platform) => platform?.platformData?.type !== "ANCHOR"
                ) || []),
              ].map((platform, index) => {
                const stableIndex = workspace.items.findIndex(
                  (item) => item.name === platform.name
                );

                return (
                  <Platform
                    key={index}
                    platform={platform}
                    index={stableIndex}
                    setPlatformCoords={handleSetPlatformCoords}
                    movingPlatform={movingPlatform}
                    deletePlatform={handlePlatformDelete}
                    allItems={workspace?.items || []}
                    lastClickedIndex={lastClickedIndex}
                    setLastClickedIndex={setLastClickedIndex}
                  />
                );
              })}
            </g>
          </svg>
        </Box>
        <Box
          style={{
            position: "absolute",
            top: 5,
          }}
        >
          <PositionData />
        </Box>
        <Box
          style={{
            position: "absolute",
            top: 5,
            right: 10,
            display: "flex",
            flexDirection: "row",
            zIndex: 1,
          }}
        >
          {typeof selectedPlatform !== "number" ? (
            <>
              <Typography
                variant="caption"
                sx={{ userSelect: "none" }}
                color={
                  mouseWarning && mouseCoords[0] !== 0 ? "error" : "inherit"
                }
              >
                Mouse coordinates
              </Typography>
            </>
          ) : (
            <>
              <Typography
                variant="caption"
                sx={{ userSelect: "none", display: "inline-block" }}
                color={platformWarning ? "error" : "inherit"}
              >
                Platform coordinates
              </Typography>
            </>
          )}
        </Box>
        <Box
          style={{
            position: "absolute",
            top: 25,
            right: 10,
            display: "flex",
            flexDirection: "row",
            zIndex: 1,
          }}
        >
          {typeof selectedPlatform !== "number" ? (
            <>
              <Typography
                variant="caption"
                color={
                  mouseWarning && mouseCoords[0] !== 0 ? "error" : "inherit"
                }
                sx={{ userSelect: "none" }}
              >
                x: {mouseCoords[0]} / y: {mouseCoords[1]}
              </Typography>
            </>
          ) : (
            <>
              <Typography
                variant="caption"
                color={platformWarning ? "error" : "inherit"}
                sx={{ userSelect: "none" }}
              >
                <PlatformCoordinates
                  platformCoords={platformCoords}
                  setPlatformCoords={handleSetPlatformCoords}
                  handleSaveCoordinates={handleSaveCoordinates}
                />
              </Typography>
            </>
          )}
        </Box>
        <Box
          style={{
            position: "absolute",
            top: 45,
            right: 35,
            display: "flex",
            flexDirection: "row",
            zIndex: 1,
          }}
        >
          {typeof selectedPlatform !== "number" ? null : (
            <PlatformPositionButtons
              platformCoords={platformCoords}
              setPlatformCoords={handleSetPlatformCoords}
              handleSaveCoordinates={handleSaveCoordinates}
            />
          )}
        </Box>
        <Box
          style={{
            position: "absolute",
            top: 45,
            right: 4,
            display: "flex",
            flexDirection: "column",
            zIndex: 1,
          }}
        >
          <Tooltip title="Zoom In">
            <IconButton
              variant="contained"
              color="neutral"
              onClick={handleZoomIn}
            >
              <ZoomInIcon fontSize="small" />
            </IconButton>
          </Tooltip>
          <Tooltip title="Zoom Out">
            <IconButton
              variant="contained"
              color="neutral"
              onClick={handleZoomOut}
            >
              <ZoomOutIcon fontSize="small" />
            </IconButton>
          </Tooltip>
          <Tooltip title="Reset Zoom">
            <IconButton
              variant="contained"
              color="neutral"
              onClick={handleResetZoom}
            >
              <RestoreZoom fontSize="small" />
            </IconButton>
          </Tooltip>
          <Tooltip title="Restore panning">
            <IconButton
              variant="contained"
              color="neutral"
              onClick={resetPanning}
            >
              <RestorePageIcon fontSize="small" />
            </IconButton>
          </Tooltip>
        </Box>
      </Box>
    );
};
