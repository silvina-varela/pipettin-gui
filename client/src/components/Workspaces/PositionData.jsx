import { Grid, Typography } from "@mui/material";
import { useSelector } from "react-redux";

export const PositionData = () => {
  const { robotPosition, showRobotPosition } = useSelector(
    (state) => state.workspaceSlice
  );
  return (
    <Grid container ml={3}>
      {showRobotPosition && (
        <Typography variant="caption" sx={{ userSelect: "none" }}>
          Robot position: x: {robotPosition.x} y: {robotPosition.y} z:{" "}
          {robotPosition.z}
        </Typography>
      )}
    </Grid>
  );
};
