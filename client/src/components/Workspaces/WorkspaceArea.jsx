import { useState } from "react";
import { ButtonGroup, Grid } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { WorkspaceHeader } from "./WorkspaceHeader";
import { InsertPlatformsButton } from "./Buttons/InsertPlatformsButton";
import { ManageContentButton } from "./Buttons/ManageContentButton";
import { RobotPositionButton } from "./Buttons/RobotPositionButton";
import { GoToButton } from "./Buttons/GoToButton";
import { ParkButton } from "./Buttons/ParkButton";
import { WorkspacePreview } from "./WorkspacePreview";
import { FollowButton } from "./Buttons/FollowButton";
import { SelectTool } from "./Buttons/SelectTool";
import { BulkTagging } from  "./Buttons/BulkTagging";
import { BulkVolumes } from "./Buttons/BulkVolumes";

export const WorkspaceArea = () => {
  const [movingPlatform, setMovingPlatform] = useState({});
  return (
    <>
      <WorkspaceHeader />

      <Grid container direction="row" mt={1} mb={1} pl={1}>
        <Grid item container xs={9}>
          <Grid container spacing={1} alignContent={"center"}>
            <Grid item>
              <InsertPlatformsButton />
            </Grid>
            <Grid item>
              <ButtonGroup
                variant="outlined"
                color="neutral"
                size="medium"
                sx={{ minHeight: 30, maxWidth: 200 }}
              >
                <ManageContentButton
                  mode="insert"
                  tooltipLabel="Insert container"
                  icon={<AddIcon />}
                />
                <ManageContentButton
                  mode="empty"
                  tooltipLabel="Empty container"
                  icon={<RemoveIcon />}
                />
              </ButtonGroup>
            </Grid>
            <BulkVolumes /> {/* FILL SELECTED CONTENTS IN BULK */}
            <Grid item>
              <BulkTagging /> {/* TAG SELECTED CONTENTS IN BULK */}
            </Grid>
          </Grid>
        </Grid>
        <Grid
          item
          container
          spacing={1}
          xs={3}
          sx={{
            display: "flex",
            justifyContent: "flex-end",
            pr: 1 ,
            // alignItems: "stretch", // Ensure all buttons stretch to same height
          }}
          
        >
          <Grid item>
            <ButtonGroup variant="outlined" color="neutral" size="small" sx={{ minHeight: "31px" }}> 
              <RobotPositionButton /> {/* LIVE TOOL COORDINATES */}
            </ButtonGroup>
          </Grid>
          <Grid item>
            <SelectTool sx={{ height: "50px" }}/> {/* TOOL SELECT */}
          </Grid>
          <Grid item>
            <ButtonGroup variant="outlined" color="neutral" size="small" sx={{ minHeight: "31px" }}>
              <GoToButton /> {/* GO-TO CONTENT BUTTON */}
              <FollowButton  setMovingPlatform={setMovingPlatform}/> {/* FOLLOW CONTENT BUTTON */}
              <ParkButton /> {/* PARK TOOL */}
            </ButtonGroup>
          </Grid>
        </Grid>
      </Grid>

      <WorkspacePreview  movingPlatform={movingPlatform}/>
    </>
  );
};
