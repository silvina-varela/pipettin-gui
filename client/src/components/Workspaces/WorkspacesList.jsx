import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { ListView } from "../List/ListView";
import { useExportData } from "../../hooks/useExportData";
import axios from "axios";
import { Popup } from "../Dialogs/Popup";
import { WorkspaceForm } from "./WorkspaceForm";
import { GenericImport } from "../NavBar/GenericImport";
import {
  deleteWorkspaceAsync,
  getWorkspacesAsync,
  deselectProtocol,
} from "../../redux/slices";

export const WorkspacesList = ({ closePopup, changeTo }) => {
  const dispatch = useDispatch();
  const { workspaces } = useSelector((state) => state.workspaceSlice);
  const navigate = useNavigate();
  const exportData = useExportData();

  /**Popup settings */
  const [openPopup, setOpenPopup] = useState(false);
  const [popupTitle, setPopupTitle] = useState("");
  const [popupComponent, setPopupComponent] = useState(null);
  const [popupWidth, setPopupWidth] = useState("");

  useEffect(() => {
    dispatch(getWorkspacesAsync());
  }, [dispatch]);

  /** Delete workspaces */
  const deleteAlert = {
    title: "Delete workspace",
    message:
      "Are you sure you want to delete the selected workspace(s)? This process cannot be undone",
    type: "delete",
    yesMessage: "Delete",
  };

  const handleDeleteWorkspaces = async (workspaces) => {
    if (workspaces.length) {
      await Promise.all(
        workspaces.map((element) => dispatch(deleteWorkspaceAsync(element._id)))
      );
    }
  };

  /** Export workspaces */
  const handleExport = async (workspaces) => {
    try {
      const workspacesToExport = await axios.post(
        "/api/workspaces/export",
        workspaces
      );
      exportData(workspacesToExport.data, "Workspaces");
    } catch (error) {
      console.log(error);
    }
  };

  /**Create new workspace */
  const handleNewWorkspace = () => {
    changeTo(
      "Create new workspace",
      <WorkspaceForm handleClosePopup={closePopup} />,
      "sm"
    );
  };

  const handleOpenWorkspace = (e, workspace) => {
    e.preventDefault();
    navigate(`workspace/${workspace._id}`);
    dispatch(deselectProtocol());
    closePopup();
  };

  /** Import workspaces */

  const handleImportWorkspaces = () => {
    setPopupTitle("Import worksapces");
    setPopupComponent(<GenericImport type="workspaces" />);
    setPopupWidth("xs");
    setOpenPopup(true);
  };

  const columns = [
    {
      id: "name",
      numeric: false,
      disablePadding: false,
      label: "Name",
      width: 300,
    },
    {
      id: "description",
      numeric: false,
      disablePadding: false,
      label: "Description",
      width: 350,
    },
    {
      id: "updatedAt",
      numeric: false,
      disablePadding: false,
      label: "Last edited",
      width: 160,
    },
    {
      id: "createdAt",
      numeric: false,
      disablePadding: false,
      label: "Created",
      width: 160,
    },
  ];

  return (
    <>
      <ListView
        data={workspaces}
        handleDelete={handleDeleteWorkspaces}
        deleteAlert={deleteAlert}
        handleExport={handleExport}
        handleClickRow={handleOpenWorkspace}
        handleNew={handleNewWorkspace}
        handleImport={handleImportWorkspaces}
        columns={columns}
        word="workspaces"
      />
      <Popup
        title={popupTitle}
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        component={popupComponent}
        width={popupWidth}
      />
    </>
  );
};
