import React, { useState } from "react";
import * as d3 from "d3";
import { colorHash } from "../../utils/colorHash";
import { useDispatch, useSelector } from "react-redux";
import { selectPlatform } from "../../redux/slices";

export const WellSlot = React.memo(
  ({
    index,
    platformIndex,
    content,
    setPlatformCoords,
    position,
    type,
    positionProps,
    diameter,
    setLastClickedIndex,
    lastClickedIndex,
  }) => {
    const dispatch = useDispatch();
    const { selectedPlatform, selectedContent } = useSelector(
      (state) => state.workspaceSlice
    );

    const gradientId = content && `grad-${platformIndex}-${content.index}`;
    const fillPorc =
      content &&
      content.volume &&
      parseInt(
        (content?.volume * 100) / (content?.containerData?.maxVolume || 0),
        10
      );

    const handleClick = (e) => {
      e.preventDefault();
      e.stopPropagation();

      if (platformIndex !== selectedPlatform) {
        // Click on a different platform.
        dispatch(selectPlatform({ platform: platformIndex, content: [index] }));
        setPlatformCoords(position.x, position.y, position.z);
      } else {
        // Click on the currently selected platform.
        let newContentSelected = [];
        // Handle cases.
        if (e.shiftKey && lastClickedIndex !== null) {
          // Prevent the default action that would select text in the page.
          e.preventDefault();
          // Multiple-selection with Shift key.
          const start = Math.min(lastClickedIndex, index);
          const end = Math.max(lastClickedIndex, index);
          const range = Array.from({ length: end - start + 1 }, (_, i) => start + i);
          newContentSelected = [...new Set([...selectedContent, ...range])];
        } else if (selectedContent.includes(index)) {
          // Click on a selected content.
          newContentSelected = selectedContent.filter((el) => el !== index);
        } else {
          // Click on a non-selected content.
          newContentSelected = [...selectedContent, index];
        }
        
        dispatch(
          selectPlatform({
            platform: platformIndex,
            content: newContentSelected,
          })
        );
      }

      setLastClickedIndex(index);
    };

    const drawTags = content && content.tags && content.tags.length > 0;
    const semiCircles = content && content.tags && content.tags.length;
    const arcLength = semiCircles && (Math.PI * 2) / semiCircles;
    let startAngle = 0;
    let endAngle = arcLength;

    // Convert 1rem to pixels based on the root font size
    const remToPx = (rem) => rem * parseFloat(getComputedStyle(document.documentElement).fontSize);

    return (
      <>
        {/* Draw the content's liquid level */}
        {content && (
          <g>
            <defs>
              <linearGradient id={gradientId} x1="0%" x2="0%" y1="100%" y2="0%">
                <stop offset={`${fillPorc || 0}%`} stopColor="lightblue" />
                <stop offset="0%" stopColor="white" />
              </linearGradient>
            </defs>
          </g>
        )}
        <circle
          {...positionProps}
          r={diameter / 2 || 0}
          index={index} //! do not remove!
          fill={content && content.volume ? `url(#${gradientId})` : "#ffffff"}
          type={type}
          fillOpacity={0.3}
          stroke="#ffffff"
          strokeWidth={"0.2px"}
          onClick={handleClick}
          style={{ cursor: "pointer" }}
          id={"well-" + platformIndex + "-" + index}
          // Style the circle to highlight selected contents.
          className={
            "well" +
            " " +
            (content ? "filled" : "empty") +
            " " +
            (selectedPlatform === platformIndex &&
            selectedContent.includes(index)
              ? "wellSelected"
              : "")
          }
        >
          {/* Add a tooltip using the title tag.
              Note that it must be a child tag.
              The 'title attribute' has no effect. */}
          <title>
            {content
              ? `Content: ${content.name}${
                  content.tags && content.tags.length > 0
                    ? `\nTags: ${content.tags.join(", ")}.`
                    : ""
                }`
              : `Empty (${index+1})`}
          </title>
        </circle>

        {/* Draw a dark arc behind the tags as a background for contrast */}
        {drawTags && (
          (() => {
            const arc = d3
              .arc()
              .innerRadius(diameter / 2 - remToPx(0.01))
              .outerRadius(diameter / 2 + remToPx(0.05) + remToPx(0.01));

            return (
              <path
                d={arc({
                  startAngle: 0,
                  endAngle: Math.PI * 2, // Full circle in radians
                })}
                transform={`translate(${positionProps.cx},${positionProps.cy})`}
                style={{
                  zIndex: 100,
                  fill: "black", // Set the fill color
                }}
                className="tag"
              />
            );
          })()
        )}

        {/* Draw the tags as coloured arcs around the contents. */}
        {drawTags &&
          content.tags.map((tag, i) => {
            const arc = d3
              .arc()
              // Now using non-null arc diameter and colouring the fill, instead of stroke.
              .innerRadius(diameter / 2)
              .outerRadius(diameter / 2 + remToPx(0.05));

            return (
              <path
                key={i}
                d={arc({
                  startAngle: startAngle + i * arcLength,
                  endAngle: endAngle + i * arcLength,
                })}
                transform={`translate(${positionProps.cx},${positionProps.cy})`}
                style={{
                  zIndex: 100,
                  // Now using non-null arc diameter and colouring the fill, instead of stroke.
                  fill: colorHash(tag).hex,
                  // Colouring the stroke black caused the flat ends of the arcs to be visible.
                  // stroke: "black",
                  // strokeWidth: "0.01rem",
                }}
                className="tag"
              />
            );
          })}
      </>
    );
  }
);

WellSlot.displayName = "WellSlot";
