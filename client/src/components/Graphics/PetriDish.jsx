import React from "react";
import { roundNumber } from "../../utils/previewHelpers";
import * as d3 from "d3";
import { Colony } from "./Colony";
import { useDispatch, useSelector } from "react-redux";
import { addNewContentToItem } from "../../redux/slices";

export const PetriDish = React.memo(({ platform, index, selectedPlatform }) => {
  const dispatch = useDispatch();

  const { settings } = useSelector((state) => state.settingsSlice);
  const roundUp = settings.workspace?.roundUp;

  /** Add colony */
  const handleAddColony = (event) => {
    if (!event.shiftKey) return;
    event.preventDefault();
    event.stopPropagation();

    let colonyPosition = {
      x: roundNumber(d3.pointer(event)[0], roundUp),
      y: roundNumber(d3.pointer(event)[1], roundUp),
    };
    let newIndex = platform.content.length
      ? platform.content[platform.content.length - 1].index + 1
      : 1;
    let newColony = {
      index: newIndex,
      name: "colony" + newIndex,
      position: { ...colonyPosition },
      tags: [],
      type: "colony",
    };

    dispatch(addNewContentToItem(index, [newColony]));
  };

  return (
    <g onClick={handleAddColony}>
      <circle
        r={platform.platformData.diameter / 2}
        fill={platform.platformData.color}
        fillOpacity={0.5}
        className={index === selectedPlatform ? "selected" : ""}
      />
      {platform.content?.map((colony, i) => {
        return <Colony key={i} platformIndex={index} content={colony} />;
      })}
    </g>
  );
});

PetriDish.displayName = "PetriDish";
