import React from "react";
import { WellSlot } from "./WellSlot";

export const Slot = React.memo(
  ({
    slotData,
    index,
    platformIndex,
    content,
    setPlatformCoords,
    position,
    setLastClickedIndex,
    lastClickedIndex,
  }) => {
  const {
    slotPosition,
    slotSize,
  } = slotData;

  const positionProps = {
    cx: slotPosition.slotX,
    cy: slotPosition.slotY,
  };

  return (
    <WellSlot
      data={slotData}
      index={index}
      platformIndex={platformIndex}
      content={content}
      setPlatformCoords={setPlatformCoords}
      position={position}
      type="slot"
      positionProps={positionProps}
      diameter={slotSize}
      setLastClickedIndex={setLastClickedIndex}
      lastClickedIndex={lastClickedIndex}
    />
  );
});

Slot.displayName = "Slot"
