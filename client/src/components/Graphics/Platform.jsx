import React, { useRef, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { select } from "d3-selection";
import { drag } from "d3-drag";
import { PetriDish } from "./PetriDish";
import { Well } from "./Well";
import { roundNumber } from "../../utils/previewHelpers";
import { useFollowPlatform } from "../../hooks/useFollowPlatform";
import { Slot } from "./Slot";
import LockIcon from "@mui/icons-material/Lock";
import {
  deleteWorkspaceContent,
  selectPlatform,
  updatePlatformPosition,
} from "../../redux/slices";
import { text } from "d3";

function getQuadrant(angle) {
  // Normalize the angle to be between 0 and 360 degrees
  angle = angle % 360;
  if (angle < 0) {
      angle += 360; // Make angle positive if it's negative
  }

  // Determine quadrant based on the angle
  if (angle >= 0 && angle < 90) {
      return 1;
  } else if (angle >= 90 && angle < 180) {
      return 2;
  } else if (angle >= 180 && angle < 270) {
      return 3;
  } else {
      return 4;
  }
}

// This object is imported in "WorkspacePreview.jsx".
export const Platform = React.memo(
  ({
    platform, // NOTE: This is a platform item, not the platform definition.
    index,
    setPlatformCoords,
    deletePlatform,
    movingPlatform,
    allItems,
    setLastClickedIndex,
    lastClickedIndex,
  }) => {
    const { platformData } = platform;
    const followPlatform = useFollowPlatform();
    const position = useMemo(
      () => ({ ...platform.position }),
      [platform.position.x, platform.position.y, platform.position.z]
    );

    const rotation = (platformData && platformData.rotation) || 0;
    const dispatch = useDispatch();
    const { selectedPlatform, selectedContent } = useSelector(
      (state) => state.workspaceSlice
    );
    const { selectedTool } = useSelector((state) => state.toolSlice);
    const { settings } = useSelector((state) => state.settingsSlice);

    const roundUp = useMemo(
      () => settings.workspace?.roundUp,
      [settings.workspace?.roundUp]
    );

    // Sets number of wells
    const determineRange = () => {
      let range = [];
      for (
        let index = 0;
        index < platformData.wellsRows * platformData.wellsColumns;
        index++
      ) {
        range.push(index);
      }
      return range;
    };
    const range = platformData
      ? platformData.type === "CUSTOM"
        ? platformData.slots
        : determineRange()
      : [];

    /** Platform selection */
    const handlePlatformSelect = (e) => {
      e.preventDefault();

      dispatch(selectPlatform({ platform: index, content: [] }));

      setPlatformCoords(
        position.x,
        position.y,
        position.z,
        platformData.width,
        platformData.length
      );
    };

    /** DRAG */
    const platformRef = useRef(null);
    const element = select(platformRef.current);

    const [dragging, setDragging] = useState(false);
    const [snappedAnchor, setSnappedAnchor] = useState(null);

    // Handle changes in drag-and-drop of the platform item.
    useEffect(() => {
      // Save the item's position, only if the item has just been dropped.
      if (!dragging && selectedPlatform === index) {
        dispatch(updatePlatformPosition({ ...position, snappedAnchor: snappedAnchor }));
        if (
          Object.keys(movingPlatform).length > 0 &&
          movingPlatform.platformName === platform.name
        ) {
          // Dispatch a follow event if required.
          followPlatform(movingPlatform, position, selectedTool, roundUp);
        }
      }
    }, [dragging, index]);

    const dragHandler = drag()
      .on("start", () => {
        setDragging(true);
        if (selectedPlatform === index) {
          element.style("cursor", "all-scroll");
        }
      })
      .on("drag", (event) => {
        if (selectedPlatform === index) {
          position.x = roundNumber(position.x + event.dx, roundUp);
          position.y = roundNumber(position.y + event.dy, roundUp);

          setPlatformCoords(
            position.x,
            position.y,
            position.z,
            platformData?.width,
            platformData?.length
          );
        }
      })
      .on("end", async () => {
        // Item snapping to anchors.
        element.style("cursor", "pointer");
        if (selectedPlatform === index) {
          // Get all anchors in the workspace.
          const anchors = allItems.filter(
            (wItem) => wItem?.platformData?.type === "ANCHOR"
          );

          // Map anchors to their distances.
          const anchorsWithDistances = anchors
            .filter((anchor) => anchor.name !== platform.name) // Exclude self-anchoring
            .map((anchor) => {
              // NOTE: To snap to the "offset corner" instead of the origin, add here
              //       "anchor.platformData.anchorOffsetX/Y" to each position.
              const anchorX = anchor.position.x;
              const anchorY = anchor.position.y;

              // Snap from different corners to rotated anchors.
              // Calculate the quadrant for snapping based on rotation.
              const anchorQ = getQuadrant(anchor.platformData.rotation); // degrees
              // Construct the boolean indicator variables.
              // These will tell which corner of the item is to be compared,
              // according to the rotation of the anchor we're looking at.
              const anchorOffX = 1 * [2, 3].includes(anchorQ);
              const anchorOffY = 1 * [3, 4].includes(anchorQ);

              // Calculate the delta between platform and anchor positions.
              const deltaX =
                position.x + platform.platformData.width * anchorOffX - anchorX;
              const deltaY =
                position.y + platform.platformData.length * anchorOffY - anchorY;

              // Calculate the distance to the anchor.
              const distance = Math.sqrt(deltaX ** 2 + deltaY ** 2);

              return { anchor, distance };
            });

            // Sort the anchors by distance.
            const sortedAnchors = anchorsWithDistances.sort(
              (a, b) => a.distance - b.distance
            );

          // Find the nearest anchor within snapping distance (threshold of 10).
          const nearAnchor = sortedAnchors.find(({ distance }) => distance < 10)?.anchor;

          // Update the position of the platform if a "near" anchor was found.
          if (nearAnchor) {
            // TODO: snap from different corners to rotated anchors
            const anchorQ = getQuadrant(nearAnchor.platformData.rotation); // degrees
            const anchorOffX = 1*[2, 3].includes(anchorQ);
            const anchorOffY = 1*[3, 4].includes(anchorQ);

            // Calculate new position.
            position.x = roundNumber(
              // NOTE: To snap to the offset corner instead,
              //       add "nearAnchor.platformData.anchorOffsetX" to the position.
              nearAnchor.position.x - platform.platformData.width * anchorOffX,
              roundUp
            );
            position.y = roundNumber(
              // nearAnchor.position.y + platform.platformData.length * anchorSin,
              nearAnchor.position.y - platform.platformData.length * anchorOffY,
              roundUp
            );
            // Apply new position.
            setPlatformCoords(
              position.x,
              position.y,
              position.z,
              platformData?.width,
              platformData?.length
            );

            // Update platform to store the snapped anchor name.
            setSnappedAnchor(nearAnchor.name);
          } else {
            // Update platform to clear the snapped anchor name.
            setSnappedAnchor(null);
          }
        }

        setDragging(false);
      });

    useEffect(() => {
      const element = select(platformRef.current);
      if (!platform.locked) {
        /** DRAG EVENT */
        element.call(dragHandler);
      } else {
        element.on("mousedown.drag", null);
      }
    }, [
      index,
      selectedPlatform,
      position.x,
      position.y,
      movingPlatform,
      selectedTool,
      platform.locked,
      element,
    ]);

    useEffect(() => {
      /** DELETE KEY EVENT */
      const handleKeyDown = (event) => {
        // TODO: Check if this needs unification with the "handleDeleteKeyDown" hook in "PlatformAccordion.jsx".
        if (
          event.key === "Delete" &&
          index === selectedPlatform &&
          !selectedContent.length
        ) {
          console.log("Delete key pressed (platform). Delete platform with index:", index);
          deletePlatform(index);
        } else if (
          event.key === "Delete" &&
          index === selectedPlatform &&
          selectedContent.length
        ) {
          console.log("Delete key pressed. Delete selected content");

          dispatch(deleteWorkspaceContent(selectedPlatform, selectedContent));
          dispatch(selectPlatform({ platform: selectedPlatform, content: [] }));
        } else if (
          event.key === "a" &&
          index === selectedPlatform &&
          event.ctrlKey
        ) {
          console.log("Select all.");
          // Prevent the default 'select all' action that would select all text in the page.
          event.preventDefault();
          // Compute the total amount of contents that the platform can have.
          const ncols = platform.platformData.wellsColumns || 0;
          const nrows = platform.platformData.wellsRows || 0;
          const slotsList = platformData.slots || [];
          const ncontents = ncols * nrows + slotsList.length
          // Select all the contents (even if empty)!
          dispatch(
            selectPlatform({
              platform: selectedPlatform,
              content: Array.from(Array(ncontents).keys()),
            })
          );
        }
      };
      if (platformRef.current) {
        platformRef.current.setAttribute("tabindex", "0");

        platformRef.current.addEventListener("keydown", handleKeyDown);
      }

      //Cleanup
      return () => {
        if (platformRef.current)
          platformRef.current.removeEventListener("keydown", handleKeyDown);
      };
    }, [index, selectedPlatform, JSON.stringify(selectedContent)]);

    const drawPlatform = () => {
      if (platformData.type === "PETRI_DISH") {
        return (
          <PetriDish
            platform={platform}
            selectedPlatform={selectedPlatform}
            index={index}
          />
        );
      } else if (platformData.type === "BUCKET") {
        return (
          <rect
            width={platformData.width}
            height={platformData.length}
            fillOpacity={0.9}
            fill={platformData.color}
            className={index === selectedPlatform ? "selected" : ""}
          />
        );
      } else if (platformData.type === "CUSTOM" && platformData.diameter > 0) {
        return (
          <>
            <circle
              r={platformData.diameter / 2}
              fillOpacity={0.9}
              fill={platformData.color}
              className={index === selectedPlatform ? "selected" : ""}
            />
            {
              // SLOTS
              range.map((slot, i) => {
                return (
                  <Slot
                    slotData={slot}
                    key={i}
                    index={i}
                    platformIndex={index}
                    content={platform.content.find((el) => el.index === i + 1)}
                    setPlatformCoords={setPlatformCoords}
                    position={position}
                    setLastClickedIndex={setLastClickedIndex}
                    lastClickedIndex={lastClickedIndex}
                  />
                );
              })
            }
          </>
        );
      } else if (platformData.type === "CUSTOM" && !platformData.diameter) {
        return (
          <>
            <rect
              width={platformData.width}
              height={platformData.length}
              fillOpacity={0.9}
              fill={platformData.color}
              className={index === selectedPlatform ? "selected" : ""}
            />
            {
              // SLOTS
              range.map((slot, i) => {
                return (
                  <Slot
                    slotData={slot}
                    key={i}
                    index={i}
                    platformIndex={index}
                    content={platform.content.find((el) => el.index === i + 1)}
                    setPlatformCoords={setPlatformCoords}
                    position={position}
                    setLastClickedIndex={setLastClickedIndex}
                    lastClickedIndex={lastClickedIndex}
                  />
                );
              })
            }
          </>
        );
      } else if (platformData.type === "ANCHOR") {
        // Define SVG graphics group for an anchor.
        return (
          <g>
            <rect
              x={platformData.anchorOffsetX}
              y={platformData.anchorOffsetY}
              width={platformData.width - platformData.anchorOffsetX}
              height={platformData.length - platformData.anchorOffsetY}
              fill="#hhhhhh"
              fillOpacity={0.5}
            />
            <rect
              width={platformData.width}
              height={platformData.length}
              fill={platformData.color}
              className={index === selectedPlatform ? "selected" : ""}
              fillOpacity={0.5}
            />
          </g>
        );
      } else {
        return (
          <>
            <rect
              width={platformData.width}
              height={platformData.length}
              fillOpacity={0.9}
              fill={platformData.color}
              className={index === selectedPlatform ? "selected" : ""}
            />
            {
              // WELLS
              range.map((el, i) => {
                return (
                  <Well
                    wellData={platformData}
                    key={i}
                    index={i}
                    platformIndex={index}
                    content={platform.content.find((el) => el.index === i + 1)}
                    setPlatformCoords={setPlatformCoords}
                    position={position}
                    setLastClickedIndex={setLastClickedIndex}
                    lastClickedIndex={lastClickedIndex}
                  />
                );
              })
            }
          </>
        );
      }
    };

    const labelFontSize = 7;

    const getTextPositionX = (platformData) => {
      // Calculate quadrant.
      const q = getQuadrant(platformData.rotation);

      if (platformData.diameter > 0) {
        // Round platforms.
        if (platform.locked) {
          return platformData.diameter / 2 - 5;
        } else {
          return platformData.diameter / 2;
        }
      } else {
        // Square platforms.
        const base_offset = [
          // 0º
          platformData.width,
          // 90º
          -platformData.anchorOffsetX || 0,
          // 180º
          -platformData.anchorOffsetX || 0,
          // 270º
          platformData.width][q-1];

        if (platform.locked) {
          // return platformData.width - 5;
          return base_offset - 5;
        } else {
          // return platformData.width;
          return base_offset;
        }
      }
    };

    const getTextPositionY = (platformData) => {
      // Calculate quadrant.
      const q = getQuadrant(platformData.rotation);
      // Get translation.
      if (platformData.diameter > 0) {
        // Round platforms.
        return platformData.diameter / 2 + 4;
      } else {
        // Square platforms.
        return [
          // 0º
          (platformData.anchorOffsetY || 0) -labelFontSize -1,
          // 90º
          (platformData.anchorOffsetX || 0) -labelFontSize -1,
          // 180º
          -platformData.anchorOffsetY+1,
          // 270º
          -platformData.anchorOffsetY+1][q-1];
      }
    };

    const textPositionX = useMemo(
      () => platformData && getTextPositionX(platformData),
      [
        platformData?.diameter,
        platformData?.type,
        platformData?.width,
        platformData?.rotation,
        platform?.locked,
      ]
    );
    const textPositionY = useMemo(
      () => platformData && getTextPositionY(platformData),
      [
        platformData?.diameter,
        platformData?.type,
        platformData?.length,
        platformData?.rotation,
      ]
    );

    if (platform && platform.platformData) {
      return (
        <g
          ref={platformRef}
          transform={`translate(${roundNumber(position.x, roundUp)}, ${roundNumber(position.y, roundUp)}) rotate(${rotation}, 0, 0)`}
          className={"platform"}
          onClick={handlePlatformSelect}
          id={"platform-" + index}
          style={{ cursor: "pointer" }}
        >

          {drawPlatform()}

          <g
            // Label and lock icon SVG group.
            transform={`rotate(${-rotation})`}
          >
            {platform.locked && (
              <svg
                // Lock icon indicator.
                x={textPositionX}
                y={textPositionY}
                height={5.7}
                width={6}
              >
                <LockIcon />
              </svg>
            )}
            <text
              // Label text.
              x={textPositionX}
              y={textPositionY}
              textAnchor="end"
              dominantBaseline="hanging"
              fontSize={`${labelFontSize}px`}
              style={{ userSelect: "none" }}
            >
              {platform.name}
            </text>
          </g>

        </g>
      );
    }
  }
);

Platform.displayName = "Platform";
