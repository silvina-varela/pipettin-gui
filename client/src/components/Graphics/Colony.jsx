import React from "react";
import * as d3 from "d3";
import { colorHash } from "../../utils/colorHash";
import { useDispatch, useSelector } from "react-redux";
import { selectPlatform } from "../../redux/slices";

export const Colony = React.memo(({ platformIndex, content }) => {
  const dispatch = useDispatch();

  const { selectedPlatform, selectedContent } = useSelector(
    (state) => state.workspaceSlice
  );

  const handleClickColony = (e) => {
    e.preventDefault();
    e.stopPropagation();
    if (platformIndex !== selectedPlatform) {
      dispatch(
        selectPlatform({
          platform: platformIndex,
          content: [content.index - 1],
        })
      );
    } else {
      if (selectedContent.includes(content.index - 1)) {
        let newContentSelected = selectedContent.filter(
          (el) => el !== content.index - 1
        );
        dispatch(
          selectPlatform({
            platform: platformIndex,
            content: newContentSelected,
          })
        );
      } else {
        let newContentSelected = [...selectedContent, content.index - 1];
        dispatch(
          selectPlatform({
            platform: platformIndex,
            content: newContentSelected,
          })
        );
      }
    }
  };

  /**Tags */
  const drawTags = content && content.tags && content.tags.length > 0;
  const semiCircles = content && content.tags && content.tags.length;
  const arcLength = semiCircles && (Math.PI * 2) / semiCircles;
  let startAngle = 0;
  let endAngle = arcLength;

  return (
    <>
      <circle
        cx={content.position.x}
        cy={content.position.y}
        r={2}
        className={
          "colony" +
          " " +
          (selectedPlatform === platformIndex &&
          selectedContent.includes(content.index - 1)
            ? "selectedColony"
            : "")
        }
        id={"colony-" + platformIndex + "-" + (content.index - 1)}
        onClick={handleClickColony}
      />
      {drawTags &&
        content.tags.map((tag, i) => {
          const arc = d3.arc().innerRadius(3).outerRadius(3);
          return (
            <path
              key={i}
              d={arc({
                startAngle: startAngle + i * arcLength,
                endAngle: endAngle + i * arcLength,
              })}
              transform={`translate(${content.position.x || 0},${
                content.position.y || 0
              })`}
              style={{
                zIndex: 100,
                stroke: colorHash(tag).hex,
                strokeWidth: "0.1rem",
              }}
              className="colonyTag"
            />
          );
        })}
    </>
  );
});

Colony.displayName = "Colony"