import React from "react";
import { WellSlot } from "./WellSlot";

export const Well = React.memo(
  ({
    wellData,
    index,
    platformIndex,
    content,
    setPlatformCoords,
    position,
    setLastClickedIndex,
    lastClickedIndex,
  }) => {
    const {
      firstWellCenterX,
      wellsColumns,
      wellSeparationX,
      firstWellCenterY,
      wellSeparationY,
      wellDiameter,
      type,
    } = wellData;

    /** Well position */
    const translateX =
      firstWellCenterX + (index % wellsColumns) * wellSeparationX;
    const translateY =
      firstWellCenterY + Math.floor(index / wellsColumns) * wellSeparationY;

    const wellType = type === "TUBE_RACK" ? "tube" : "tip";

    const positionProps = {
      cx: translateX,
      cy: translateY,
      row: Math.floor(index / wellsColumns) + 1,
      col: (index % wellsColumns) + 1,
    };

    return (
      <WellSlot
        index={index}
        platformIndex={platformIndex}
        content={content}
        setPlatformCoords={setPlatformCoords}
        position={position}
        type={wellType}
        positionProps={positionProps}
        diameter={wellDiameter}
        setLastClickedIndex={setLastClickedIndex}
        lastClickedIndex={lastClickedIndex}
      />
    );
  }
);

Well.displayName = "Well";
