import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ListView } from "../List/ListView";
import { TemplateForm } from "./TemplateForm";
import { useExportData } from "../../hooks/useExportData";
import { Popup } from "../Dialogs/Popup";
import { deleteProtocolTemplatesAsync, getProtocolTemplatesAsync } from "../../redux/slices";

export const TemplateList = () => {
  const dispatch = useDispatch();
  const exportData = useExportData();

  /**Popup settings */
  const [openPopup, setOpenPopup] = useState(false);
  const [popupTitle, setPopupTitle] = useState("");
  const [popupComponent, setPopupComponent] = useState(null);
  const [popupWidth, setPopupWidth] = useState("");

  const { protocolTemplates } = useSelector((state) => state.protocolSlice);
  
  useEffect(() => {
    dispatch(getProtocolTemplatesAsync());
  }, [dispatch]);

  /** Delete templates */
  const deleteAlert = {
    title: "Delete template",
    message:
      "Are you sure you want to delete the selected template(s)? This process cannot be undone",
    type: "delete",
    yesMessage: "Delete",
  };

  const handleDeleteTemplates = async (templates) => {
    try {
      if (templates.length) {
        await Promise.all(
          templates.map((element) =>
            dispatch(deleteProtocolTemplatesAsync(element._id))
          )
        );
      }
    } catch (error) {
      console.log(error);
    }
  };


  /** Export templates */
  const handleExport = (templates) => {
    exportData(templates, "Protocol Templates");
  };

  const handleClosePopup = () => {
    setOpenPopup(false);
    setPopupTitle("");
    setPopupComponent("");
  };

  /** Edit template */
  const handleEdit = (template) => {
    setPopupTitle("Edit template");
    setPopupComponent(<TemplateForm isEditMode={true} oldTemplate={template} closePopup={handleClosePopup} />);
    setPopupWidth("xl");
    setOpenPopup(true);
  };

  /**Create new template */
  const handleNewTemplate = () => {
    setPopupTitle("Create new template");
    setPopupComponent(<TemplateForm closePopup={handleClosePopup} />);
    setPopupWidth("xl");
    setOpenPopup(true);
  };

  const columns = [
    {
      id: "name",
      numeric: false,
      disablePadding: false,
      label: "Name",
      width: 250,
    },
    {
      id: "description",
      numeric: false,
      disablePadding: false,
      label: "Description",
      width: 300,
    },
    {
      id: "updatedAt",
      numeric: false,
      disablePadding: false,
      label: "Last edited",
      width: 160,
    },
    {
      id: "createdAt",
      numeric: false,
      disablePadding: false,
      label: "Created",
      width: 160,
    },
  ];

  return (
    <>
      <ListView
        data={protocolTemplates}
        handleDelete={handleDeleteTemplates}
        deleteAlert={deleteAlert}
        handleExport={handleExport}
        handleEdit={handleEdit}
        handleNew={handleNewTemplate}
        columns={columns}
        word="templates"
      />
      <Popup
        title={popupTitle}
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        component={popupComponent}
        width={popupWidth}
      />
    </>
  );
};
