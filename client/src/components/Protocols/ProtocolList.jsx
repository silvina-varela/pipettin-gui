import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { ListView } from "../List/ListView";
import { useExportData } from "../../hooks/useExportData";
import {
  deleteProtocolAsync,
  getAllProtocolsAsync,
  getWorkspaceByNameAsync,
  getProtocolByIdAsync,
} from "../../redux/slices";

export const ProtocolList = ({ closePopup }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { allProtocols } = useSelector((state) => state.protocolSlice);
  const exportData = useExportData();

  useEffect(() => {
    dispatch(getAllProtocolsAsync());
  }, [dispatch]);

  /** Navigate */
  const handleOpenProtocol = async (e, protocol) => {
    e.preventDefault();

    dispatch(getWorkspaceByNameAsync(protocol.workspace))
      .unwrap()
      .then((response) => {
        dispatch(getProtocolByIdAsync(protocol._id));
        return response;
      })
      .then((response) => {
        navigate(`workspace/${response._id}`);
      })
      .then(() => closePopup())
      .catch((error) => console.log(error));
  };

  /** Delete protocols */
  const deleteAlert = {
    title: "Delete protocol",
    message:
      "Are you sure you want to delete the selected protocol(s)? This process cannot be undone",
    type: "delete",
    yesMessage: "Delete",
  };

  const handleDeleteProtocols = async (templates) => {
    try {
      if (templates.length) {
        await Promise.all(
          templates.map((element) => dispatch(deleteProtocolAsync(element._id)))
        );
      }
      dispatch(getAllProtocolsAsync());
    } catch (error) {
      console.log(error);
    }
  };

  /** Export protocols */
  const handleExport = (protocols) => {
    exportData(protocols, "Protocols");
  };

  const columns = [
    {
      id: "name",
      numeric: false,
      disablePadding: false,
      label: "Name",
      width: 200,
    },
    {
      id: "workspace",
      numeric: false,
      disablePadding: false,
      label: "Workspace",
      width: 200,
    },
    {
      id: "description",
      numeric: false,
      disablePadding: false,
      label: "Description",
      width: 250,
    },
    {
      id: "updatedAt",
      numeric: false,
      disablePadding: false,
      label: "Last edited",
      width: 160,
    },
    {
      id: "createdAt",
      numeric: false,
      disablePadding: false,
      label: "Created",
      width: 160,
    },
  ];

  return (
    <ListView
      data={allProtocols}
      handleDelete={handleDeleteProtocols}
      deleteAlert={deleteAlert}
      handleExport={handleExport}
      handleClickRow={handleOpenProtocol}
      columns={columns}
      orderTableBy="updatedAt"
      orderTableDirection="desc"
      word="protocols"
    />
  );
};
