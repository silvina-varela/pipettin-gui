import React, { useEffect, useState } from "react";
import axios from "axios";
import {
  Box,
  Button,
  CircularProgress,
  Fade,
  Grid,
  IconButton,
  Menu,
  MenuItem,
  Popover,
  Stack,
  Tooltip,
  Typography,
} from "@mui/material";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { FormInputs } from "../ProtocolsSidePanel/Forms/FormInputs";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import RestartAltIcon from "@mui/icons-material/RestartAlt";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import LightModeIcon from "@mui/icons-material/LightMode";
import Editor from "@monaco-editor/react";
import { useSnackAlert } from "../../hooks/useSnackAlert";
import { useDispatch } from "react-redux";
import { getProtocolTemplatesAsync } from "../../redux/slices";

export const TemplateForm = ({
  closePopup,
  isEditMode = false,
  oldTemplate = {},
}) => {
  const addSnack = useSnackAlert();

  const dispatch = useDispatch();
  
  const setEditTemplate = () => {
    const templateToEdit = { ...oldTemplate };

    delete templateToEdit._id;
    delete templateToEdit.createdAt;
    delete templateToEdit.updatedAt;
    delete templateToEdit.__v;

    return templateToEdit;
  };

  const [template, setTemplate] = useState(
    isEditMode
      ? setEditTemplate()
      : { name: "template", description: "", fields: [] }
  );
  const [editorTheme, setEditorTheme] = useState(true);

  const handleEditorTheme = (event) => {
    event.preventDefault();
    setEditorTheme(!editorTheme);
  };

  const [anchorInfo, setAnchorInfo] = useState(null);
  const [popoverMessage, setPopoverMessage] = useState("");

  const handleInfo = (message) => {
    setPopoverMessage(message);
  };

  const fieldTypes = {
    number: {
      label: "Number",
      description: "",
      properties: {
        field_id: "",
        field_label: "",
        field_mandatory: false,
        field_initialValue: "",
        field_placeholder: "",
        field_type: "number",
        endAdornment: "",
        startAdornment: false,
        min: 0,
        max: false,
      },
    },
    text: {
      label: "Text",
      description: "",
      properties: {
        field_id: "",
        field_label: "",
        field_mandatory: false,
        field_initialValue: "",
        field_placeholder: "",
        field_type: "text",
      },
    },
    largeText: {
      label: "Large text",
      description: "",
      properties: {
        field_id: "",
        field_label: "",
        field_mandatory: false,
        field_placeholder: "",
        helperText: "",
        field_type: "largeText",
      },
    },
    select: {
      label: "Select",
      description: "Select from predefined options",
      properties: {
        field_id: "",
        field_label: "",
        field_mandatory: false,
        field_placeholder: "",
        field_initialValue: "",
        field_type: "definedSelect",
        select_options: [
          {
            option_label: "option 1",
          },
          {
            option_label: "option 2",
          },
        ],
      },
    },
    selectFromItems: {
      label: "Select from workspace",
      description: "Select from items in workspace",
      properties: {
        field_id: "",
        field_label: "",
        field_mandatory: false,
        field_placeholder: "",
        field_type: "selectDataFromItems",
        field_select_by: "TUBE_RACK",
      },
    },
    toolSelect: {
      label: "Select tool",
      description: "Select tool",
      properties: {
        field_id: "",
        field_label: "",
        field_mandatory: false,
        field_placeholder: "",
        field_type: "toolSelect",
        field_select_by: "Micropipette",
      },
    },
    checkbox: {
      label: "Checkbox",
      properties: {
        field_id: "",
        field_label: "",
        field_type: "checkbox",
        field_initialValue: false,
        field_mandatory: false,
      },
    },
    toggle: {
      label: "Toggle",
      properties: {
        field_id: "",
        field_value: "",
        field_placeholder: "",
        field_initialValue: "",
        field_type: "toggle",
        field_options: [
          {
            option_label: "label 1",
          },
          {
            option_label: "label 2",
          },
        ],
      },
    },
    switch: {
      label: "Switch",
      properties: {
        field_id: "",
        field_label: "",
        field_mandatory: false,
        field_placeholder: "",
        field_type: "switch",
        field_initialValue: false,
      },
    },
    list: {
      label: "List",
      description: "Field for comma separated elements",
      properties: {
        field_id: "",
        field_value: "",
        field_label: "",
        field_mandatory: false,
        field_initialValue: "",
        field_placeholder: "",
        field_type: "list",
      },
    },
    listGroup: {
      label: "List group",
      description: "Group of fields",
      properties: {
        field_id: "",
        field_label: "",
        field_mandatory: true,
        field_initialValue: 1,
        helperText: "Use comma to define multiple combinations",
        field_type: "listGroup",
        items_label: "Component",
        fields: [
          {
            field_id: "",
            field_value: "",
            field_label: "",
            field_mandatory: false,
            field_initialValue: "",
            field_placeholder: "",
            field_type: "list",
          },
        ],
      },
    },
    fileUpload: {
      label: "File Upload",
      description: "Allows the user to upload a file",
      properties: {
        field_id: "",
        field_label: "Upload File",
        field_type: "fileUpload",
        field_mandatory: false,
        accepted_file_types: "", // e.g., ".jpg,.png,.pdf"
        max_file_size: 0, // Maximum file size in bytes
      },
    },
  };

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const addNewField = (e, type) => {
    e.preventDefault();
    handleClose();
    setTemplate({
      ...template,
      fields: [...template.fields, fieldTypes[type].properties],
    });
  };

  const [validation, setValidation] = useState([]);
  useEffect(() => console.log({ validation }), [validation]);

  const handleUpdateForm = (values) => {
    try {
      setTemplate(JSON.parse(values));
    } catch (error) {
      setValidation(error);
    }
  };

  const exampleWorkspace = {
    name: "Example Workspace",
    items: [],
  };

  const validate = (values) => {
    const emptyID = values.findIndex((value) => !value.field_id?.length);
    return emptyID;
  };

  const validationText = () => {
    if (validation[0].message?.length) {
      if (Object.prototype.hasOwnProperty.call(validation[0], "endLineNumber")) {
        return `There is an error on line ${validation[0].endLineNumber}: ${validation[0].message}`;
      } else if (Object.prototype.hasOwnProperty.call(validation[0], "element")) {
        if (validation[0].element === "all") return validation[0].message;
        else
          return `There is an error on field ${validation[0].element + 1}: ${
            validation[0].message
          }`;
      }
    } else return "";
  };

  const submitTemplate = async () => {
    if (!isEditMode) {
      await axios
        .post("/api/templates", template)
        .then((response) => {
          console.log("Protocol template created successfully");
          console.log(response);
        })
        .then(() => dispatch(getProtocolTemplatesAsync()))
        .then(() => {
          closePopup();
        })
        .catch((error) => {
          console.error(`[ERROR]: ${error}`);
          addSnack("There was an error creating the template", "error");
        });
    } else {
      const editedTemplate = { ...template, _id: oldTemplate._id };
      await axios
        .put("/api/templates", editedTemplate)
        .then((response) => {
          console.log("Protocol template edited successfully");
          console.log(response);
        })
        .then(() => dispatch(getProtocolTemplatesAsync()))
        .then(() => {
          closePopup();
        })
        .catch((error) => {
          console.error(`[ERROR]: ${error}`);
          addSnack("There was an error updating the template", "error");
        });
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (!template.fields?.length) {
      setValidation([
        {
          element: "all",
          message: "You can't create a template without fields",
        },
      ]);
    } else {
      const invalidField = validate(template.fields);
      console.log({ invalidField });
      if (invalidField !== -1) {
        setValidation([
          {
            element: invalidField,
            message: "The field is missing the property 'field_id'",
          },
        ]);
      } else {
        submitTemplate();
      }
    }
  };

  const handleReset = (event) => {
    event.preventDefault();
    if (isEditMode) setTemplate(setEditTemplate());
    else setTemplate({ name: "template", description: "", fields: [] });
  };

  return (
    <>
      <Stack direction={"row"} spacing={3}>
        <Box width={"50%"}>
          <Box justifyContent={"space-between"} display={"flex"} ml={2}>
            <Box>
              <Tooltip title="Reset">
                <IconButton onClick={handleReset} id="reset-template-button">
                  <RestartAltIcon fontSize="small" />
                </IconButton>
              </Tooltip>
              <Tooltip title="Theme">
                <IconButton
                  onClick={handleEditorTheme}
                  id="editor-theme-button"
                >
                  {!editorTheme ? (
                    <LightModeIcon fontSize="small" />
                  ) : (
                    <DarkModeIcon fontSize="small" />
                  )}
                </IconButton>
              </Tooltip>
            </Box>
            <Button
              id="add-field-button"
              color="neutral"
              aria-controls={open ? "fade-menu" : undefined}
              aria-haspopup="true"
              aria-expanded={open ? "true" : undefined}
              onClick={handleClick}
              endIcon={<KeyboardArrowDownIcon />}
            >
              Add field
            </Button>
            <Menu
              id="fade-menu"
              MenuListProps={{
                "aria-labelledby": "fade-button",
              }}
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
              TransitionComponent={Fade}
              PaperProps={{
                style: {
                  maxHeight: 160,
                  width: "25ch",
                },
              }}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "right",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
            >
              {Object.keys(fieldTypes).map((option, optionIndex) => (
                <MenuItem
                  key={optionIndex}
                  value={option}
                  onClick={(event) => addNewField(event, option)}
                  sx={{ display: "flex", justifyContent: "space-between" }}
                >
                  {fieldTypes[option].label}

                  {fieldTypes[option].description &&
                    fieldTypes[option].description.length && (
                      <IconButton
                        onClick={(event) => {
                          event.stopPropagation();
                          event.preventDefault();
                          setAnchorInfo(event.currentTarget);
                          handleInfo(fieldTypes[option].description);
                        }}
                        size="small"
                      >
                        <InfoOutlinedIcon sx={{ fontSize: 15 }} />
                      </IconButton>
                    )}
                </MenuItem>
              ))}
            </Menu>
          </Box>
          <Editor
            height="50vh"
            width="100%"
            defaultLanguage="json"
            theme={editorTheme ? undefined : "vs-dark"}
            value={JSON.stringify(template, null, 2)}
            onChange={(values) => handleUpdateForm(values)}
            onValidate={(values) => setValidation(values)}
            options={{
              scrollbar: {
                verticalScrollbarSize: "6px",
                horizontalScrollbarSize: "6px",
              },
            }}
            loading={<CircularProgress />}
          />
        </Box>
        <Box width={"50%"}>
          <Typography variant="subtitle1">Preview</Typography>
          <Box maxHeight={"50vh"} overflow={"scroll"}>
            <Grid display={"flex"} flexWrap={"wrap"} mt={2}>
              {template.fields?.map((field, i) => {
                const date = new Date();
                return (
                  <Box key={date.getTime() + i}>
                    <FormInputs
                      field={{...field}}
                      workspace={exampleWorkspace}
                      handleChange={() => {}}
                      preview={true}
                      setFieldValue={() => {}}
                    />
                  </Box>
                );
              })}
            </Grid>
          </Box>
        </Box>
      </Stack>
      <Box mt={2} height={validation.length ? "auto" : 24}>
        <Typography color="error" variant="caption">
          {!validation.length ? " " : validationText()}
        </Typography>
      </Box>
      <Popover
        id="popoverSettings"
        open={Boolean(anchorInfo)}
        anchorEl={anchorInfo}
        onClose={() => setAnchorInfo(null)}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
      >
        <Typography variant="body2" p={1}>
          {popoverMessage}
        </Typography>
      </Popover>
      <Box sx={{ textAlign: "right" }}>
        {isEditMode ? (
          <Button type="submit" onClick={handleSubmit} variant="contained">
            Edit
          </Button>
        ) : (
          <Button type="submit" onClick={handleSubmit} variant="contained">
            Create
          </Button>
        )}
      </Box>
    </>
  );
};
