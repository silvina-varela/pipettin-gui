import { at } from 'lodash';
import { useField } from 'formik';
import { TextField } from "@mui/material";

export const InputField = (props) => {
  const { ...rest } = props;
  const [field, meta] = useField(props);

  const renderHelperText = () => {
    const [touched, error] = at(meta, 'touched', 'error');
    if (touched && error) {
      return error;
    }
  }

  return (
    <TextField
      type="text"
      variant="standard"
      error={meta.touched && meta.error && true}
      helperText={renderHelperText()}
      {...field}
      {...rest}
    />
  );
}
