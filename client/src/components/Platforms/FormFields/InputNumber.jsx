import { at } from "lodash";
import { useField } from "formik";
import { TextField, InputAdornment } from "@mui/material";

export const InputNumber = (props) => {
  const { endAdornment, inputWidth, slot = false, ...rest } = props;
  const [field, meta] = useField(props);

  const renderHelperText = () => {
    const [touched, error] = at(meta, "touched", "error");
    if (touched && error && !slot) {
      return error;
    }
  };

  return (
    <TextField
      type="number"
      variant="standard"
      error={meta.touched && meta.error && true}
      helperText={renderHelperText()}
      sx={{ maxWidth: "170px", width: inputWidth || undefined }}
      // InputLabelProps={{ style: { fontSize: 12 } }}
      InputProps={{
        endAdornment: <InputAdornment position="end">{endAdornment}</InputAdornment>,
      }}
      {...field}
      {...rest}
    />
  );
};
