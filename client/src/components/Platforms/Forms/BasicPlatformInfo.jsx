import { Grid } from "@mui/material";
import { InputField } from "../FormFields/InputField";
import { SelectField } from "../FormFields/SelectField";

const types = [
  {
    value: "TUBE_RACK",
    label: "Tube rack",
  },
  {
    value: "TIP_RACK",
    label: "Tip rack",
  },
  {
    value: "PETRI_DISH",
    label: "Petri dish",
  },
  {
    value: "BUCKET",
    label: "Bucket",
  },
  {
    value: "CUSTOM",
    label: "Custom",
  },
  {
    value: "ANCHOR",
    label: "Anchor",
  },
];

export const BasicPlatformInfo = (props) => {
  const {
    formField: { name, description, type },
  } = props;

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <InputField
            id="formPlatformName"
            name={name.name}
            label={name.label}
            fullWidth
            autoComplete="off"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <SelectField
            id="formPlatformType"
            name={type.name}
            label={type.label}
            data={types}
            fullWidth
            autoComplete="off"
            disabled={props.isEditMode}
          />
        </Grid>
        <Grid item xs={12}>
          <InputField
            id="formPlatformDescription"
            multiline
            rows={10}
            name={description.name}
            label={description.label}
            fullWidth
            autoComplete="off"
          />
        </Grid>
      </Grid>
    </>
  );
};
