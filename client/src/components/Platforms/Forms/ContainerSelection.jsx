import {
  Autocomplete,
  Button,
  Grid,
  IconButton,
  TextField,
} from "@mui/material";
import { FieldArray, useField } from "formik";
import { useSelector } from "react-redux";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import CloseIcon from "@mui/icons-material/Close";
import { InputNumber } from "../FormFields/InputNumber";
import React from "react";
import { styled } from "@mui/material/styles";

const CustomTextField = styled((props) => <TextField {...props} />)(
  () => ({
    "& .MuiFormHelperText-root": {
      color: "error",
    },
  })
);

export const ContainerSelection = ({
  values,
  setFieldValue,
  selectedSlot,
  slot = false,
}) => {
  const { containers } = useSelector((state) => state.containerSlice);
  const options = !containers.length
    ? []
    : containers?.map((container) => {
        if (!container.type) container.type = "no type";
        if (!container.name) container.name = "unnamed container";

        return container;
      });

  function compare(a, b) {
    const typeValues = {
      tube: 1,
      tip: 2,
      well: 3,
      colony: 4,
      "no type": 5,
    };
    // Compare by type value
    if (typeValues[a.type] < typeValues[b.type]) {
      return -1;
    }
    if (typeValues[a.type] > typeValues[b.type]) {
      return 1;
    }
    // If types are the same, compare by name
    if (a.name.toLowerCase() < b.name.toLowerCase()) {
      return -1;
    }
    if (a.name.toLowerCase() > b.name.toLowerCase()) {
      return 1;
    }
    // Objects are equal
    return 0;
  }

  return (
    <FieldArray
      name={slot ? `slots[${selectedSlot}].containers` : "containers"}
    >
      {({ remove, push }) => {
        return (
          <div>
            <Grid container spacing={1}>
              {values.containers?.length > 0 &&
                values.containers.map((container, index) => {
                  const selectedValue = options.find(
                    (option) => option.name === container.container
                  );
                  return (
                    <React.Fragment key={"container" + index}>
                      <Grid container spacing={1}>
                        <Grid item mb={2}>
                          <Autocomplete
                            id="choose-container"
                            name={
                              slot
                                ? `slots[${selectedSlot}].containers[${index}].container`
                                : `containers[${index}].container`
                            }
                            options={options.sort(compare)}
                            groupBy={(option) => option.type}
                            getOptionLabel={(option) => option.name}
                            value={
                              selectedValue
                                ? selectedValue
                                : container.container.length
                                ? { name: container.container }
                                : null
                            }
                            isOptionEqualToValue={(option, value) =>
                              option?.name === value?.name ||
                              container.container
                            }
                            onChange={(_, value) => {
                              slot
                                ? setFieldValue(
                                    `slots[${selectedSlot}].containers[${index}].container`,
                                    value?.name ?? ""
                                  )
                                : setFieldValue(
                                    `containers[${index}].container`,
                                    value?.name ?? ""
                                  );
                            }}
                            sx={{ width: 173 }}
                            renderInput={(params) => {
                              const fieldPath = slot
                                ? `slots[${selectedSlot}].containers[${index}].container`
                                : `containers[${index}].container`;

                              const [field, meta] = useField(fieldPath);
                              const error = meta.error;
                              const touch = meta.touched;
                              return (
                                <CustomTextField
                                  {...params}
                                  color={touch && error ? "error" : "neutral"}
                                  variant="standard"
                                  label="Container"
                                  error={
                                    touch && error
                                      ? true
                                      : !selectedValue &&
                                        container.container.length
                                      ? true
                                      : null
                                  }
                                  helperText={
                                    touch && error
                                      ? error
                                      : !selectedValue &&
                                        container.container.length
                                      ? "Missing from the database"
                                      : null
                                  }
                                />
                              );
                            }}
                            renderOption={(props, option) => {
                              return (
                                <li {...props} key={option._id}>
                                  {option.name}
                                </li>
                              );
                            }}
                          />
                        </Grid>
                        <Grid item>
                          <InputNumber
                            id={`container-offsetZ-${index}`}
                            name={
                              slot
                                ? `slots[${selectedSlot}].containers[${index}].containerOffsetZ`
                                : `containers[${index}].containerOffsetZ`
                            }
                            label="Z Offset"
                          />
                        </Grid>
                        <Grid item mt={2}>
                          <IconButton
                            id={"button" + index}
                            onClick={() => remove(index)}
                          >
                            <CloseIcon sx={{ fontSize: 22 }} />
                          </IconButton>
                        </Grid>
                      </Grid>
                    </React.Fragment>
                  );
                })}
              <Grid item mt={1} ml={-1}>
                <Button
                  color="inherit"
                  size="small"
                  variant="text"
                  startIcon={
                    <AddCircleOutlineIcon sx={{ fontSize: "small" }} />
                  }
                  onClick={() => {
                    push({ container: "", containerOffsetZ: "" });
                  }}
                >
                  Add Container
                </Button>
              </Grid>
            </Grid>
          </div>
        );
      }}
    </FieldArray>
  );
};
