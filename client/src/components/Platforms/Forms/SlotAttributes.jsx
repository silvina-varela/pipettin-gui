import { Box, Button, Grid, IconButton, Typography } from "@mui/material";
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { SlotModal } from "./SlotModal";
import { useEffect, useState } from "react";
import CloseIcon from '@mui/icons-material/Close';
import { FieldArray, useFormikContext } from "formik";

export const SlotAttributes = ({ slots, values, setFieldValue }) => {
    const [openModal, setOpenModal] = useState(false);
    const [selectedSlot, setSelectedSlot] = useState({});
    const [slotErrors, setSlotErrors] = useState(false);

    const { errors } = useFormikContext();


    const handleOpenModal = (e, index) => {
        e.preventDefault();
        setOpenModal(true);
        setSelectedSlot(index)
    };

    useEffect(() =>{
        if(errors.slots){
            console.log("Errors in slot form:", errors.slots)
            setSlotErrors(true);
        } else {
            setSlotErrors(false);
        }
    }, [JSON.stringify(errors.slots)])


    const slotInitialValues = {
        slotName: "",
        slotPosition: {
            slotX: 0,
            slotY: 0,
        },
        slotActiveHeight: 2,
        slotSize: 10,
        slotHeight: 10,
        contentLength: 50,
        contentMaxVolume: 200,
        contentLoadBottom: 1,
    };

    return (
        <>
            <FieldArray
                name="slots"
                render={arrayHelpers => (
                    <div>
                        <Grid container alignItems="center">
                            <Button
                                onClick={() => arrayHelpers.push({ ...slotInitialValues })}
                                color="inherit"
                                size="small"
                                variant="text"
                                startIcon={<AddCircleOutlineIcon sx={{ fontSize: "small" }} />}
                            >
                                Add slot
                            </Button>
                            {slotErrors ? 
                            <Typography variant="body2" color="error" ml={2}>
                                You need to fill all the required slot fields
                            </Typography> : null}
                        </Grid>
                        <Grid container>
                            {values.slots?.length > 0 &&
                                values.slots.map((slot, index) => (
                                    <Grid item key={index}>
                                        <Box>
                                            <Button variant="text" color="primary" onClick={(e) => handleOpenModal(e, index)}>
                                                {"slot " + (index + 1)}
                                            </Button>
                                            <IconButton onClick={() => arrayHelpers.remove(index)}>
                                                <CloseIcon sx={{ fontSize: 12 }} />
                                            </IconButton>
                                        </Box>
                                    </Grid>
                                ))}
                        </Grid>
                    </div>
                )}
            />
            <SlotModal
                openModal={openModal}
                setOpenModal={setOpenModal}
                slotFields={slots}
                selectedSlot={selectedSlot}
                setFieldValue={setFieldValue}
                values={values.slots}
               />
        </>
    )
}