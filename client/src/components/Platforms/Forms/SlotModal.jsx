import { FieldArray, useFormikContext } from 'formik';
import { Box, Button, Dialog, DialogContent, Grid, IconButton, Typography } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { InputNumber } from '../FormFields/InputNumber';
import { InputField } from '../FormFields/InputField';
import { ContainerSelection } from './ContainerSelection';

export const SlotModal = ({ openModal, setOpenModal, slotFields, selectedSlot, setFieldValue, values }) => {

    const { setFieldTouched, errors } = useFormikContext();

    const handleOkButton = () => {
        if (Object.keys(errors).length > 0 && errors.slots[selectedSlot] && Object.keys(errors.slots[selectedSlot]).length > 0) {
            setFieldTouched(`slots[${selectedSlot}].slotName`, true)
            values[selectedSlot].containers?.map((container, index) => {
                setFieldTouched(`slots[${selectedSlot}].containers[${index}].containerOffsetZ`, true);
                setFieldTouched(`slots[${selectedSlot}].containers[${index}].container`, true)
            })
        } else {
            setOpenModal(false);
        }
    };
    const handleCloseButton = () => {
        setOpenModal(false)
    }

    return (
        <Dialog
            open={openModal}
            onClose={handleCloseButton}
            aria-labelledby="slot-modal-title"
            aria-describedby="slot-modal-description"
            fullWidth
            scroll='body'
        >
            <DialogContent>
                <IconButton onClick={handleCloseButton} sx={{ position: 'absolute', top: '8px', right: '8px' }}>
                    <CloseIcon sx={{ fontSize: 20 }} />
                </IconButton>
                <Grid item sm={12} mr={2}>
                    <Grid container spacing={1} direction="column">
                        <Grid item>
                            <FieldArray
                                name="slots"
                                render={() => (
                                    <>
                                        <Grid container spacing={1} mt={1}>
                                            <Grid item>
                                                <Typography variant="overline">
                                                    Slot attributes
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                        <Grid container spacing={1}>
                                        <Grid item>
                                                <InputField
                                                    name={`slots[${selectedSlot}].slotName`}
                                                    label={slotFields.slotName.label}
                                                    fullWidth autoComplete="off"
                                                    required
                                                />
                                            </Grid>
                                            <Grid item>
                                                <InputField
                                                    sx={{ minWidth: 350 }}
                                                    name={`slots[${selectedSlot}].slotDescription`}
                                                    label={slotFields.slotDescription.label}
                                                />
                                            </Grid>
                                            <Grid item>
                                                <Grid container alignItems="flex-end" spacing={1} direction="row">
                                                    <Grid item xs={2.5}>
                                                        <Typography fontSize={15} variant='body2'>
                                                            Coordinates:
                                                        </Typography>
                                                    </Grid>
                                                    <Grid item xs={2}>
                                                        <InputNumber
                                                            name={`slots[${selectedSlot}].slotPosition.slotX`}
                                                            label={slotFields.slotPosition.slotX.label}
                                                            error={Boolean(errors.slots ? errors.slots[selectedSlot]?.slotPosition?.slotX : false)}
                                                            slot={true}
                                                        />
                                                    </Grid>
                                                    <Grid item xs={2}>
                                                        <InputNumber
                                                            name={`slots[${selectedSlot}].slotPosition.slotY`}
                                                            label={slotFields.slotPosition.slotY.label}
                                                            error={Boolean(errors.slots ? errors.slots[selectedSlot]?.slotPosition?.slotY : false)}
                                                            slot={true}
                                                        />
                                                    </Grid>
                                                    <Grid item xs={3.5}>
                                                        <Typography variant="body2" color="error">
                                                            {errors.slots ? (errors.slots[selectedSlot]?.slotPosition?.slotX ||
                                                                errors.slots[selectedSlot]?.slotPosition?.slotY) : null}
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                            <Grid item>
                                                <InputNumber
                                                    name={`slots[${selectedSlot}].slotActiveHeight`}
                                                    label={slotFields.slotActiveHeight.label}
                                                    endAdornment={"mm"}
                                                />
                                            </Grid>
                                            <Grid item>
                                                <InputNumber
                                                    name={`slots[${selectedSlot}].slotSize`}
                                                    label={slotFields.slotSize.label}
                                                    endAdornment={"mm"}
                                                />
                                            </Grid>
                                            <Grid item>
                                                <InputNumber
                                                    name={`slots[${selectedSlot}].slotHeight`}
                                                    label={slotFields.slotHeight.label}
                                                    endAdornment={"mm"}
                                                />
                                            </Grid>
                                            <Grid item mt={1} ml={1}>
                                                <ContainerSelection values={values[selectedSlot]} setFieldValue={setFieldValue} selectedSlot={selectedSlot} slot={true} />
                                            </Grid>
                                        </Grid>
                                    </>
                                )}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Box display="flex" justifyContent="flex-end" mt={3}>
                    <Button variant="contained" onClick={handleOkButton} color="primary"> OK </Button>
                </Box>
            </DialogContent>
        </Dialog>
    );
}