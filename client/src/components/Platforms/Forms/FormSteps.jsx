import { useEffect, useState } from "react";
import platformFormModel from "../FormModel/platformFormModel";
import { BasicPlatformInfo } from "./BasicPlatformInfo";
import { PlatformAttributes } from "./PlatformAttributes";
import { useFormikContext } from "formik";
import { LinearProgress } from "@mui/material";

export const FormSteps = ({ activeStep, isEditMode, setValues, 
                            updatePlatformShape, updatePlatformType}) => {
  const { formField } = platformFormModel;
  const { values, setFieldValue, errors } = useFormikContext();

  // Set the initial value of "currentPlatformShape" from the form's values if set,
  // else initialize by default to a rectangular shape.
  const [currentPlatformShape, setCurrentPlatformShape] = useState(
    values.platformShape || "rectangular"
  );

  useEffect(() => {
    // Propagate the new "platformShape" value upstream.
    // It is used by the "validationSchema" for validation logic.
    updatePlatformShape(currentPlatformShape);
    updatePlatformType(values.type);
  }, [currentPlatformShape])

  useEffect(() => {
    // Propagate the new "platformType" value upstream.
    // It is used by the "validationSchema" for validation logic.
    // Depending on "activeStep" in addition to the above because
    // the update was not triggering immediately.
    updatePlatformType(values.type);
  }, [activeStep])

  useEffect(() => {
    if (isEditMode) {
      const newPlatformShape = values.diameter > 0 ? "circular" : "rectangular";
      setCurrentPlatformShape(newPlatformShape);
      // Store the platforms' shape in the form's "platformShape" value.
      setFieldValue("platformShape", newPlatformShape);
    }
  }, [isEditMode]);

  const formInitialValues = {
    CUSTOM:
      currentPlatformShape === "rectangular"
        ? {
            width: 200,
            length: 60,
            height: 40,
          }
        : {
            height: 15,
            diameter: 90,
          },

    TUBE_RACK: {
      width: 215,
      length: 72,
      height: 2,
      firstWellCenterX: 10,
      firstWellCenterY: 10,
      wellDiameter: 10,
      wellSeparationX: 13.2,
      wellSeparationY: 13.2,
      wellsColumns: 16,
      wellsRows: 5,
    },
    TIP_RACK: {
      width: 122,
      length: 86,
      height: 80,
      firstWellCenterX: 12,
      firstWellCenterY: 10,
      wellDiameter: 5,
      wellSeparationX: 9.0,
      wellSeparationY: 9.0,
      wellsColumns: 12,
      wellsRows: 8,
    },
    PETRI_DISH: {
      height: 15,
      diameter: 90,
      maxVolume: 25000,
      activeHeight: 10,
      color: "#d7d7d7"
    },
    BUCKET: {
      width: 215,
      length: 72,
      height: 2,
    },
    ANCHOR: {
      width: 50,
      length: 50,
      height: 0,
      anchorOffsetX: -10,
      anchorOffsetY: -10,
      anchorOffsetZ: 10,
    },
  };

  async function fetchInitialValues(type) {
    await new Promise((r) => setTimeout(r, 100));
    return formInitialValues[type];
  }

  const setInitialValues = () => {
    if (values.type) {
      const fixedInitialValues = {
        name: values.name,
        type: values.type,
        description: values.description,
        color: values.color || "8a1eF6",
        rotation: values.rotation || 0,
        activeHeight: values.activeHeight || 0,
        slots: values.type !== "CUSTOM" ? [] : values.slots || [],
        containers: values.type !== "CUSTOM" ? values.containers || [] : [],
        platformShape: currentPlatformShape,
      };

      fetchInitialValues(values.type).then((typeInitialValues) => {
        setValues({ ...fixedInitialValues, ...typeInitialValues });
      });
    }
  };

  useEffect(() => {
    if (!isEditMode) setInitialValues();
  }, [currentPlatformShape, values.type]);

  useEffect(() => {
    if (errors) {
      // https://formik.org/docs/api/formik#errors--field-string-string-
      // If the object is "empty" there may not be an error.
      console.log("Current form errors:", errors);
    }
  }, [errors]);

  const renderStepContent = (step) => {
    switch (step) {
      case 0:
        return (
          <BasicPlatformInfo formField={formField} isEditMode={isEditMode} />
        );
      case 1:
        return (
          <PlatformAttributes
            formField={formField}
            setValues={setValues}
            setPlatformShape={setCurrentPlatformShape}
            isEditMode={isEditMode}
          />
        );
      default:
        return (
          <LinearProgress
            variant="determinate"
            value={50}
            sx={{
              marginTop: 10,
              marginX: 10,
            }}
          />
        );
    }
  };

  return <div>{renderStepContent(activeStep)}</div>;
};
