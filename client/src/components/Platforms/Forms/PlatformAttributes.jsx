import { useState } from "react";
import { useFormikContext } from "formik";
import { HexColorPicker } from "react-colorful";
import { styled } from "@mui/material/styles";
import SquareRoundedIcon from "@mui/icons-material/SquareRounded";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import {
  IconButton,
  Grid,
  MenuItem,
  Menu,
  InputAdornment,
  Typography,
  Box,
  Popover,
  Select,
  FormControl,
  InputLabel,
} from "@mui/material";
import { InputField } from "../FormFields/InputField";
import { InputNumber } from "../FormFields/InputNumber";
import { PlatformPreview } from "../PlatformPreview";
import { SlotAttributes } from "./SlotAttributes";
import { ContainerSelection } from "./ContainerSelection";

// Menu for color picker
const StyledMenu = styled((props) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "right",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "right",
    }}
    {...props}
  />
))(({ theme }) => ({
  "& .MuiPaper-root": {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    boxShadow:
      "rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px",
    "& .MuiMenu-list": {
      padding: "4px 0",
    },
  },
}));

// Area of the platform form to edit its attributes ("step 2").
export const PlatformAttributes = ({ formField, setPlatformShape, isEditMode }) => {
  const {
    color,
    width,
    length,
    height,
    diameter,
    firstWellCenterX,
    firstWellCenterY,
    wellDiameter,
    wellSeparationX,
    wellSeparationY,
    wellsColumns,
    wellsRows,
    activeHeight,
    maxVolume,
    rotation,
    anchorOffsetX,
    anchorOffsetY,
    anchorOffsetZ,
    slots,
  } = formField;

  const { values, setFieldValue } = useFormikContext();

  // COLOR PICKER
  const [anchorElColor, setAnchorElColor] = useState(null);
  const setColor = (color) => {
    setFieldValue("color", color);
  };

  // POPOVERS
  const [anchorEl, setAnchorEl] = useState(null);
  const [popoverMessage, setPopoverMessage] = useState("");
  const handleInfo = (message) => {
    setPopoverMessage(message);
  };

  const attributesMessage = (
    <Typography variant="caption">Info about platform measurements</Typography>
  );
  const slotAttributesMessage = (
    <Typography variant="caption">Info about slot measurements</Typography>
  );
  const containersMessage = <img src="/images/platformTip.png" />;

  const platformTypes = {
    TUBE_RACK: "tube rack",
    TIP_RACK: "tip rack",
    BUCKET: "bucket",
    PETRI_DISH: "petri dish",
    CUSTOM: "custom platform",
    ANCHOR: "anchor",
  };

  const handleChangePlatformShape = (event) => {
    setPlatformShape(event.target.value);
  };

  return (
    <Grid container sx={{ display: "flex", flexDirection: "row-reverse" }}>
      <Grid item xs={6}>
        <PlatformPreview values={values} platformShape={values.platformShape} />
      </Grid>
      <Grid item xs={6} sx={{ maxWidth: "50vh" }}>
        <Box>
          <Grid container spacing={4}>
            <Grid item sm={12}>
              {/* Platform attributes */}
              <Grid container direction="column" spacing={2}>
                <Grid item sm={12}>
                  <Typography variant="overline">
                    {platformTypes[values.type]} attributes
                  </Typography>

                  <IconButton
                    onClick={(event) => {
                      setAnchorEl(event.currentTarget);
                      handleInfo(attributesMessage);
                    }}
                    size="small"
                  >
                    <InfoOutlinedIcon fontSize="inherit" />
                  </IconButton>
                </Grid>
                <Grid item>
                  <Grid container spacing={1}>
                    {/* COLOR */}
                    <Grid item>
                      <InputField
                        name={color.name}
                        label={color.label}
                        value={values.color}
                        sx={{ maxWidth: "170px" }}
                        onChange={(event) => {
                          setFieldValue("color", event.target.value);
                        }}
                        inputProps={{ maxLength: 7 }}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                id="color-picker"
                                aria-controls={
                                  anchorElColor
                                    ? "color-picker"
                                    : undefined
                                }
                                aria-haspopup="true"
                                aria-expanded={
                                  anchorElColor ? "true" : undefined
                                }
                                onClick={(event) =>
                                  setAnchorElColor(event.currentTarget)
                                }
                                variant="elevated"
                              >
                                <SquareRoundedIcon
                                  sx={{ color: values.color }}
                                  fontSize="medium"
                                />
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                      <StyledMenu
                        id="color-picker"
                        MenuListProps={{
                          "aria-labelledby": "color-picker",
                        }}
                        anchorEl={anchorElColor}
                        open={Boolean(anchorElColor)}
                        onClose={() => {
                          setAnchorElColor(null);
                        }}
                      >
                        <MenuItem disableRipple>
                          <HexColorPicker
                            color={values.color}
                            onChange={setColor}
                          />
                        </MenuItem>
                      </StyledMenu>
                    </Grid>
                    {/* ROTATION, ACTIVE HEIGHT - all types */}
                    <Grid item>
                      <InputNumber
                        name={rotation.name}
                        label={rotation.label}
                        inputProps={{
                          // Set the step of the rotation field to 90, easier input due to form validation.
                          // https://stackoverflow.com/a/66763263
                          step: 90
                        }}
                      />
                    </Grid>
                    <Grid item>
                      <InputNumber
                        name={activeHeight.name}
                        label={activeHeight.label}
                        endAdornment={"mm"}
                      />
                    </Grid>
                    {/* SHAPE - only CUSTOM */}
                    {values.type === "CUSTOM" && (
                      <Grid item>                     
                        <FormControl variant="standard" sx={{ minWidth: 170 }}>
                          <InputLabel id="platform-shape">
                            Platform Shape
                          </InputLabel>
                          {/* Note that "platformShape" is not controller by Formik, 
                              and is not part of the "validationSchema" it is a derived
                              value for all platforms, except custom. */}
                          <Select
                            labelId="platform-shape"
                            id="platform-shape"
                            value={values.platformShape}
                            onChange={handleChangePlatformShape}
                            label="Shape"
                            disabled={isEditMode}
                          >
                            <MenuItem value="rectangular">Rectangular</MenuItem>
                            <MenuItem value="circular">Circular</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                    )}
                    {/* WIDTH - all but PETRI */}
                    {(values.type === "TIP_RACK" ||
                      values.type === "TUBE_RACK" ||
                      values.type === "BUCKET" ||
                      values.type == "ANCHOR" ||
                      (values.type === "CUSTOM" &&
                       values.platformShape === "rectangular")
                      ) && (
                      <Grid item>
                        <InputNumber
                          name={width.name}
                          label={width.label}
                          endAdornment={"mm"}
                        />
                      </Grid>
                    )}
                    {/* LENGTH - all but PETRI */}
                    {(values.type === "TIP_RACK" ||
                      values.type === "TUBE_RACK" ||
                      values.type === "BUCKET" ||
                      values.type == "ANCHOR" ||
                      (values.type === "CUSTOM" &&
                       values.platformShape === "rectangular")
                      ) && (
                      <Grid item>
                        <InputNumber
                          name={length.name}
                          label={length.label}
                          endAdornment={"mm"}
                        />
                      </Grid>
                    )}

                    {/* HEIGHT  all types*/}
                    <Grid item>
                      <InputNumber
                        name={height.name}
                        label={height.label}
                        endAdornment={"mm"}
                      />
                    </Grid>
                    {/* Anchor only*/}
                    {values.type == "ANCHOR" && (
                      <>
                        <Grid item>
                          <InputNumber
                            name={anchorOffsetX.name}
                            label={anchorOffsetX.label}
                            endAdornment={"mm"}
                          />
                        </Grid>
                        <Grid item>
                          <InputNumber
                            name={anchorOffsetY.name}
                            label={anchorOffsetY.label}
                            endAdornment={"mm"}
                          />
                        </Grid>
                        <Grid item>
                          <InputNumber
                            name={anchorOffsetZ.name}
                            label={anchorOffsetZ.label}
                            endAdornment={"mm"}
                          />
                        </Grid>
                      </>
                    )}
                    {/* DIAMETER only petri & circular custom*/}
                    {(values.type === "PETRI_DISH" ||
                      (values.type === "CUSTOM" &&
                        values.platformShape === "circular")) && (
                      <>
                        <Grid item>
                          <InputNumber
                            name={diameter.name}
                            label={diameter.label}
                            endAdornment={"mm"}
                          />
                        </Grid>
                      </>
                    )}
                    {/* ONLY PETRI */}
                    {values.type === "PETRI_DISH" && (
                      <>
                        <Grid item>
                          <InputNumber
                            name={maxVolume.name}
                            label={maxVolume.label}
                            endAdornment={"µL"}
                          />
                        </Grid>
                      </>
                    )}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            {/* SLOT ATTRIBUTES - only custom platforms*/}
            <Grid item sm={12}>
              <Grid container spacing={1} direction="column" pt={0}>
                {values.type === "CUSTOM" && (
                  <SlotAttributes
                    slots={slots}
                    values={values}
                    setFieldValue={setFieldValue}
                  />
                )}
              </Grid>
            </Grid>
            {/* WELL INPUTS - only racks*/}
            {(values.type === "TUBE_RACK" || values.type === "TIP_RACK") && (
              <>
                <Grid item sm={12}>
                  <Grid container direction="column">
                    <Grid item>
                      {" "}
                      <Typography variant="overline">
                        Slot attributes
                      </Typography>
                      <IconButton
                        onClick={(event) => {
                          setAnchorEl(event.currentTarget);
                          handleInfo(slotAttributesMessage);
                        }}
                        size="small"
                      >
                        <InfoOutlinedIcon fontSize="inherit" />
                      </IconButton>
                    </Grid>

                    <Grid item>
                      <Grid container spacing={1}>
                        <Grid item>
                          <InputNumber
                            name={wellsColumns.name}
                            label={wellsColumns.label}
                          />
                        </Grid>
                        <Grid item>
                          <InputNumber
                            name={wellsRows.name}
                            label={wellsRows.label}
                          />
                        </Grid>
                        <Grid item>
                          <InputNumber
                            name={wellDiameter.name}
                            label={wellDiameter.label}
                            endAdornment={"mm"}
                          />
                        </Grid>
                        <Grid item>
                          <InputNumber
                            name={firstWellCenterX.name}
                            label={firstWellCenterX.label}
                            endAdornment={"mm"}
                          />
                        </Grid>
                        <Grid item>
                          <InputNumber
                            name={firstWellCenterY.name}
                            label={firstWellCenterY.label}
                            endAdornment={"mm"}
                          />
                        </Grid>
                        <Grid item>
                          <InputNumber
                            name={wellSeparationX.name}
                            label={wellSeparationX.label}
                            endAdornment={"mm"}
                          />
                        </Grid>
                        <Grid item>
                          <InputNumber
                            name={wellSeparationY.name}
                            label={wellSeparationY.label}
                            endAdornment={"mm"}
                          />
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item sm={12}>
                  <Grid container spacing={1} direction="column">
                    {/* CONTAINER SELECTION */}
                    <Grid item>
                      {" "}
                      <Typography variant="overline">Containers</Typography>
                      <IconButton
                        onClick={(event) => {
                          setAnchorEl(event.currentTarget);
                          handleInfo(containersMessage);
                        }}
                        size="small"
                      >
                        <InfoOutlinedIcon fontSize="inherit" />
                      </IconButton>
                    </Grid>
                    <Grid item>
                      <Grid container>
                        <Grid item ml={1}>
                          <ContainerSelection
                            values={values}
                            setFieldValue={setFieldValue}
                          />
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </>
            )}
          </Grid>
        </Box>
      </Grid>

      <Popover
        id="popoverPlatformForm"
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={() => setAnchorEl(null)}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        sx={{ maxWidth: "60vw", maxHeight: "50vh" }}
      >
        <Box p={1}>{popoverMessage}</Box>
      </Popover>
    </Grid>
  );
};
