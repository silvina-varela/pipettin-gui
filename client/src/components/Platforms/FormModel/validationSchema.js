import * as Yup from "yup";
import axios from "axios";
import platformFormModel from "./platformFormModel";

const {
  formField: {
    name,
    description,
    type,
    color,
    width,
    length,
    height,
    diameter,
    firstWellCenterX,
    firstWellCenterY,
    wellDiameter,
    wellSeparationX,
    wellSeparationY,
    wellsColumns,
    wellsRows,
    activeHeight,
    maxVolume,
    rotation,
    tipLength,
    anchorOffsetX,
    anchorOffsetY,
    anchorOffsetZ,
    containers,
    slots,
  },
} = platformFormModel;

const validationSchema = ({ initialValues, platformShape, platformType }) => {

  console.log("platformType:", platformType)
  console.log("platformShape:", platformShape)

  return [
    // Platform validation schema for step 1: "Basic data"
    Yup.object().shape({
      [name.name]: Yup.string()
        .required(`${name.requiredErrorMsg}`)
        .min(4, `${name.invalidErrorMsg}`)
        .max(40, `${name.invalidErrorMsg}`)
        .test("Unique", "A platform with that name already exists", (value) => {
          if (value === initialValues.name) {
            // console.log("Edit Mode. Name hasn't changed");
            return true;
          } else {
            return new Promise((resolve) => {
              axios
                .get(`/api/platforms/check/${value}`)
                .then((response) => {
                  if (response.data.exists) {
                    console.log("Name is not available");
                    resolve(false);
                  } else {
                    console.log("Name is available");
                    resolve(true);
                  }
                })
                .catch((error) => {
                  console.log(error.response.data);
                });
            });
          }
        }),
      [description.name]: Yup.string().max(
        1000,
        `${description.invalidErrorMsg}`
      ),
      [type.name]: Yup.string().required(`${type.requiredErrorMsg}`),
    }),

    // Platform validation schema for step 2: "Attributes"
    Yup.object().shape({
      [color.name]: Yup.string(),
      [width.name]: Yup.number()
        .when([], {
          is: (value) => platformShape === "rectangular",
          then: (schema) => schema.moreThan(0.0, "Must be positive.").required(),
          otherwise: (schema) => schema.nullable(),
        }),
      [length.name]: Yup.number()
        .when([], {
          is: (value) => platformShape === "rectangular",
          then: (schema) => schema.moreThan(0.0, "Must be positive.").required(),
          otherwise: (schema) => schema.nullable(),
        }),
        [diameter.name]: Yup.number()
        .when([], {
          is: (value) => platformShape === "circular",
          then: (schema) => schema.moreThan(0.0, "Must be positive.").required(),
          otherwise: (schema) => schema.nullable(),
        }),
      [height.name]: Yup.number().positive("Must be positive.").required(),
      [firstWellCenterX.name]: Yup.number().positive("Must be positive."),
      [firstWellCenterY.name]: Yup.number().positive("Must be positive."),
      [wellDiameter.name]: Yup.number().positive("Must be positive."),
      [wellSeparationX.name]: Yup.number().positive("Must be positive."),
      [wellSeparationY.name]: Yup.number().positive("Must be positive."),
      [wellsColumns.name]: Yup.number().positive("Must be positive."),
      [wellsRows.name]: Yup.number().positive("Must be positive."),
      [activeHeight.name]: Yup.number(),

      // Conditionally require maxVolume based on the platform type
      [maxVolume.name]: Yup.number()
        .when([], {
          is: (value) => platformType === "PETRI_DISH",
          then: (schema) => schema.moreThan(0.0, "Must be positive.").required("Must set a limit."),
          otherwise: (schema) => schema.nullable(),
        }),

      [rotation.name]: Yup.number().test(
        'is-multiple-of-90',
        'Only multiples of 90 degrees.',
        angle => Math.abs(angle) % 90 === 0
      ),
      [tipLength.name]: Yup.number(),
      [anchorOffsetX.name]: Yup.number().max(0.0, "Must be negative or 0."),
      [anchorOffsetY.name]: Yup.number().max(0.0, "Must be negative or 0."),
      [anchorOffsetZ.name]: Yup.number(),
      containers: Yup.array().of(
        Yup.object().shape({
          [containers.container.name]: Yup.string().required(
            "Must choose a container"
          ),
          [containers.containerOffsetZ.name]: Yup.number().required(
            "Must enter a number"
          ),
        })
      ),
      slots: Yup.array().of(
        Yup.object().shape({
          [slots.slotName.name]: Yup.string().required(
            "Name is required"
          ),
          [slots.slotSize.name]: Yup.number().required(
            "Must enter a number"
          ),
          [slots.slotHeight.name]: Yup.number().required(
            "Must enter a number"
          ),
          [slots.slotActiveHeight.name]: Yup.number().required(
            "Must enter a number"
          ),
          slotPosition: Yup.object().shape({
            [slots.slotPosition.slotX.name]: Yup.number().required( 
              "Must enter a number"
            ),
            [slots.slotPosition.slotY.name]: Yup.number().required(
              "Must enter a number"
            ),
          }),
          containers: Yup.array().of(
            Yup.object().shape({
              [slots.containers.container.name]: Yup.string().required(
                "Must choose a container"
              ),
              [slots.containers.containerOffsetZ.name]: Yup.number().required(
                "Must enter a number"
              ),
            })
          ),
        })
      ),
    }),
  ];
};

export default validationSchema;
