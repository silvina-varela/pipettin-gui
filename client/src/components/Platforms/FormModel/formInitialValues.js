import platformFormModel from "./platformFormModel";

const {
  formField: {
    name,
    description,
    type,
    color,
  },
} = platformFormModel;

const formInitialValues = {
  [name.name]: "",
  [description.name]: "",
  [type.name]: "",
  [color.name]: "#8a1eF6",
  slots: [],
  containers: [],
};

export default formInitialValues;
