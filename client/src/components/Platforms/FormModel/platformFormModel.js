export default {
  formId: "platformForm",
  formField: {
    name: {
      name: "name",
      label: "Name*",
      requiredErrorMsg: "Name is required",
      invalidErrorMsg: "Name must be between 4 and 20 characters",
    },
    description: {
      name: "description",
      label: "Description",
      invalidErrorMsg: "Description can't have more than 1000 characters",
    },
    type: {
      name: "type",
      label: "Type*",
      requiredErrorMsg: "Type is required",
    },
    color: {
      name: "color",
      label: "Color",
    },
    width: {
      name: "width",
      label: "Width",
    },
    length: {
      name: "length",
      label: "Length",
    },
    height: {
      name: "height",
      label: "Height",
    },
    diameter: {
      name: "diameter",
      label: "Diameter",
    },
    firstWellCenterX: {
      name: "firstWellCenterX",
      label: "X offset",
    },
    firstWellCenterY: {
      name: "firstWellCenterY",
      label: "Y offset",
    },
    wellDiameter: {
      name: "wellDiameter",
      label: "Well diameter",
    },
    wellSeparationX: {
      name: "wellSeparationX",
      label: "X spacing",
    },
    wellSeparationY: {
      name: "wellSeparationY",
      label: "Y spacing",
    },
    wellsColumns: {
      name: "wellsColumns",
      label: "Columns",
    },
    wellsRows: {
      name: "wellsRows",
      label: "Rows",
    },
    activeHeight: {
      name: "activeHeight",
      label: "Active Height",
    },
    maxVolume: {
      name: "maxVolume",
      label: "Max volume",
    },
    rotation: {
      name: "rotation",
      label: "Rotation",
    },
    tipLength: {
      name: "tipLength",
      label: "Tip length",
    },
    anchorOffsetX: {
      name: "anchorOffsetX",
      label: "X Anchor Offset",
    },
    anchorOffsetY: {
      name: "anchorOffsetY",
      label: "Y Anchor Offset",
    },
    anchorOffsetZ: {
      name: "anchorOffsetZ",
      label: "Z Anchor Offset",
    },
    slots: {
      slotName: {
        name: "slotName",
        label: "Name",
      },
      slotDescription: {
        name: "slotDescription",
        label: "Description",
      },
      slotPosition: {
        slotX: {
          name: "slotX",
          label: "X",
        },
        slotY: {
          name: "slotY",
          label: "Y",
        },
      },
      slotActiveHeight: {
        name: "slotActiveHeight",
        label: "Active Height",
      },
      slotSize: {
        name: "slotSize",
        label: "Size",
      },
      slotHeight: {
        name: "slotHeight",
        label: "Height",
      },
      containers: {
        container: {
          name: "container",
          label: "Container",
        },
        containerOffsetZ: {
          name: "containerOffsetZ",
          label: "Z Offset",
        },
      },
    },
    containers: {
      container: {
        name: "container",
        label: "Container",
      },
      containerOffsetZ: {
        name: "containerOffsetZ",
        label: "Z Offset",
      },
    },
  },
};
