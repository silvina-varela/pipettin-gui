import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { PlatformForm } from "./PlatformForm";
import { ListView } from "../List/ListView";
import { useExportData } from "../../hooks/useExportData";
import { Popup } from "../Dialogs/Popup";
import { GenericImport } from "../NavBar/GenericImport";
import { deletePlatformAsync, getPlatformsAsync, saveWorkspaceAsync } from "../../redux/slices";

export const PlatformsList = () => {
  const dispatch = useDispatch();
  const exportData = useExportData();


  /**Popup settings */
  const [openPopup, setOpenPopup] = useState(false);
  const [popupTitle, setPopupTitle] = useState("");
  const [popupComponent, setPopupComponent] = useState(null);
  const [popupWidth, setPopupWidth] = useState("");

  const { platforms } = useSelector((state) => state.platformSlice);
  const { workspace } = useSelector((state) => state.workspaceSlice);

  useEffect(() => {
    dispatch(getPlatformsAsync());
  }, [dispatch]);

  /** Delete platforms */
  const deleteAlert = {
    title: "Delete platforms",
    message:
      "Are you sure you want to delete the selected platform(s)? This process cannot be undone",
    type: "delete",
    yesMessage: "Delete",
  };

  const handleDeletePlatforms = async (platforms) => {
    try {
      if (platforms.length) {
        await Promise.all(
          platforms.map((element) => dispatch(deletePlatformAsync(element._id)))
        );
        if(workspace?._id) dispatch(saveWorkspaceAsync())
      }
    } catch (error) {
      console.log(error);
    }
  };

  /** Export platforms */
  const handleExport = (platforms) => {
    exportData(platforms, "Platforms")
  };

  const handleClosePopup = () => {
    setOpenPopup(false);
  };

  /** Edit platform */
  const handleEdit = (platform) => {
    setPopupTitle("Edit platform");
    setPopupComponent(<PlatformForm isEditMode={true} platform={platform} formStep={0} closePopup={handleClosePopup}/>);
    setPopupWidth("md");
    setOpenPopup(true);
  };


  /**Create new platform */
  const handleNewPlatform = () => {
    setPopupTitle("Create new platform");
    setPopupComponent(<PlatformForm closePopup={handleClosePopup} formStep={0}/>);
    setPopupWidth("md");
    setOpenPopup(true);
  };

  /** Import platforms */
  const handleImportPlatforms = () => {
    setPopupTitle("Import platforms");
    setPopupComponent(<GenericImport type="platforms" />);
    setPopupWidth("xs");
    setOpenPopup(true);
  };

  const columns = [
    {
      id: "name",
      numeric: false,
      disablePadding: false,
      label: "Name",
      width: 300,
    },
    {
      id: "type",
      numeric: false,
      disablePadding: false,
      label: "Type",
      width: 150,
    },
    {
      id: "description",
      numeric: false,
      disablePadding: false,
      label: "Description",
      width: 350,
    },
  ];

  return (
    <>
      <ListView
        data={platforms}
        handleDelete={handleDeletePlatforms}
        deleteAlert={deleteAlert}
        handleExport={handleExport}
        handleEdit={handleEdit}
        handleNew={handleNewPlatform}
        handleImport={handleImportPlatforms}
        columns={columns}
        word="platforms"
      />
      <Popup
        title={popupTitle}
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        component={popupComponent}
        width={popupWidth}
      />
    </>
  );
};
