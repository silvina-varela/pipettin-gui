import { useState } from "react";
import { Button, CircularProgress } from "@mui/material";
import { Formik, Form } from "formik";
import validationSchema from "./FormModel/validationSchema";
import platformFormModel from "./FormModel/platformFormModel";
import formInitialValues from "./FormModel/formInitialValues";
import { useDispatch } from "react-redux";
import { FormSteps } from "./Forms/FormSteps";
import { createPlatformAsync, updatePlatformAsync } from "../../redux/slices";

const steps = ["Basic data", "Attributes"];
const { formId } = platformFormModel;

export const PlatformForm = ({ isEditMode = false, platform = {}, closePopup, formStep }) => {
  const [activeStep, setActiveStep] = useState(formStep);
  const dispatch = useDispatch();

  // In edit mode, the initial values are the values of the platform being edited.
  const initialValues = isEditMode ? platform : formInitialValues;

  // Defining a "platformShape" value here, that can be updated by "FormSteps"
  // through "updatePlatformShape". This is needed for logic in "validationSchema".
  const [platformShape, updatePlatformShape] = useState("rectangular");
  const [platformType, updatePlatformType] = useState(initialValues.type);

  // Get the list of schemas for validating each step of the form.
  const getSchema = validationSchema({
    initialValues: initialValues,
    platformShape: platformShape,
    platformType: platformType
  });

  // Select the schema for the current form step.
  const currentValidationSchema = getSchema[activeStep];

  // Flag for last step (before submitting).
  const isLastStep = activeStep === steps.length - 1;

  const _sleep = (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
  };

  const submitForm = async (values, actions) => {
    await _sleep(1000);
    // console.log(JSON.stringify(values, null, 2));
    dispatch(createPlatformAsync(values))
    closePopup();
    actions.setSubmitting(false);
  };

  const submitEdit = async (values, actions) => {
    await _sleep(1000);
    // console.log(JSON.stringify(values, null, 2));
    dispatch(updatePlatformAsync(values))
    closePopup();
    actions.setSubmitting(false);
  };

  // This "handleSubmit" is called each time the form "advances".
  const handleSubmit = async (values, actions) => {
    if (isLastStep) {
      if (isEditMode) submitEdit(values, actions);
      else submitForm(values, actions);
    } else {
      setActiveStep(activeStep + 1);
      actions.setTouched({});
      actions.setSubmitting(false);
    }
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  return (
    <>
        <Formik
          initialValues={initialValues}
          validationSchema={currentValidationSchema}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting, setValues }) => (
            <Form id={formId}>
              <FormSteps
                activeStep={activeStep}
                isEditMode={isEditMode}
                setValues={setValues}
                updatePlatformShape={updatePlatformShape}
                updatePlatformType={updatePlatformType}
              />

              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                {activeStep !== 0 && (
                  <Button
                    onClick={()=>{handleBack()}}
                    sx={{ marginTop: 3, marginLeft: 1 }}
                    color="primary"
                    variant="text"
                  >
                    Back
                  </Button>
                )}
                <div style={{ margin: 1, position: "relative" }}>
                  <Button
                    disabled={isSubmitting}
                    type="submit"
                    variant="contained"
                    color="primary"
                    sx={{ marginTop: 3, marginLeft: 1 }}
                  >
                    {isLastStep ? "Submit" : "Next"}
                  </Button>

                  {isSubmitting && (
                    <CircularProgress
                      size={24}
                      sx={{ position: "absolute", top: "50%", left: "40%" }}
                    />
                  )}
                </div>
              </div>
            </Form>
          )}
        </Formik>
    </>
  );
};
