import { useRef, useEffect, useState } from "react";
import * as d3 from "d3";

const Well = ({ wellData, index }) => {
  const {
    firstWellCenterX,
    wellsColumns,
    wellSeparationX,
    firstWellCenterY,
    wellSeparationY,
    wellDiameter,
  } = wellData;

  return (
    <circle
      cx={firstWellCenterX + ((index - 1) % wellsColumns) * wellSeparationX}
      cy={
        firstWellCenterY +
        Math.floor((index - 1) / wellsColumns) * wellSeparationY
      }
      r={wellDiameter / 2 || 0}
      fill="#ffffff"
      fillOpacity={0.3}
      stroke="#ffffff"
      strokeWidth={"0.2px"}
    />
  );
};

const Slot = ({ slotData }) => {
  const { slotPosition, slotSize } = slotData;

  const renderSlot = () => {
    if (slotSize) {
      return (
        <circle
          cx={slotPosition.slotX}
          cy={slotPosition.slotY}
          r={slotSize / 2 || 0}
          fill="#ffffff" // Set the fill color as needed
          fillOpacity={0.3} // Set the fill opacity as needed
          stroke="#ffffff" // Set the stroke color as needed
          strokeWidth={"0.2px"}
        />
      );
    }
    return null;
  };
  return <>{renderSlot()}</>;
};

const Platform = ({ platform, position, platformShape }) => {
  let range = d3.range(platform.wellsRows * platform.wellsColumns);

  platform.position = { x: 0, y: 0 };

  const drawPlatform = () => {
    if (platform.type === "PETRI_DISH") {
      return (
        <circle
          r={platform.diameter / 2 || 0}
          fill={platform.color}
          transform={`translate(${position.x}, ${position.y}) rotate(${
            platform.rotation || 0
          }, 0, 0)`}
        />
      );
    } else if (platform.type === "BUCKET") {
      return (
        <rect
          width={platform.width}
          height={platform.length}
          fill={platform.color}
          transform={`translate(${position.x}, ${position.y}) rotate(${
            platform.rotation || 0
          }, 0, 0)`}
        />
      );
    } else if (platform.type === "CUSTOM" && platformShape === "circular") {
      return (
        <g
          transform={`translate(${position.x}, ${position.y}) rotate(${
            platform.rotation || 0
          }, 0, 0)`}
        >
          <circle r={platform.diameter / 2 || 0} fill={platform.color} />
          {platform.slots?.map((slot, index) => {
            return <Slot slotData={slot} key={index} />;
          })}
        </g>
      );
    } else if (platform.type === "CUSTOM" && platformShape === "rectangular") {
      return (
        <g
          transform={`translate(${position.x}, ${position.y}) rotate(${
            platform.rotation || 0
          }, 0, 0)`}
        >
          <rect
            width={platform.width}
            height={platform.length}
            fill={platform.color}
          />
          {platform.slots?.map((slot, index) => {
            return <Slot slotData={slot} key={index} />;
          })}
        </g>
      );
    } else if (platform.type === "ANCHOR") {
      return (
        <g transform={`translate(${position.x}, ${position.y}) rotate(${platform.rotation || 0}, 0, 0)`}>
          <rect
            x={platform.anchorOffsetX}
            y={platform.anchorOffsetY}
            width={platform.width - platform.anchorOffsetX}
            height={platform.length - platform.anchorOffsetY}
            fill="000000"
          />
          <rect
            width={platform.width}
            height={platform.length}
            fill={platform.color}
            fillOpacity={0.9}
          />
        </g>
      );
    } else {
      return (
        <g
          transform={`translate(${position.x}, ${position.y}) rotate(${
            platform.rotation || 0
          }, 0, 0)`}
        >
          <rect
            width={platform.width}
            height={platform.length}
            fill={platform.color}
          />
          {
            // WELLS
            range.map((el, index) => {
              return <Well wellData={platform} key={index} index={index + 1} />;
            })
          }
        </g>
      );
    }
  };

  return <>{drawPlatform()}</>;
};

export const PlatformPreview = ({ values, platformShape }) => {
  const previewRef = useRef(null);
  const [position, setPosition] = useState(null);

  useEffect(() => {
    if (previewRef.current) {
      let height = previewRef.current.getBoundingClientRect().height;
      let width = previewRef.current.getBoundingClientRect().width;

      const x =
        values.type === "PETRI_DISH" ||
        (values.type === "CUSTOM" && platformShape === "circular")
          ? width / 2
          : width / 2 - values.width / 2;
      const y =
        values.type === "PETRI_DISH" ||
        (values.type === "CUSTOM" && platformShape === "circular")
          ? height / 2
          : height / 2 - values.length;

      setPosition({ x, y });
    }
  }, [previewRef, values]);

  return (
    <svg ref={previewRef} style={{ height: "100%", width: "100%" }}>
      {position && (
        <Platform
          platform={values}
          position={position}
          platformShape={platformShape}
        />
      )}
    </svg>
  );
};
