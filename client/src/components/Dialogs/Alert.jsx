import { useEffect, useState } from "react";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
  IconButton,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import Switch from "@mui/material/Switch";
import { useSnackAlert } from "../../hooks/useSnackAlert";

export const Alert = ({
  open,
  title,
  message,
  onConfirm,
  onCancel,
  onClose,
  inputValue,
  setInputValue,
  switchOn,
  yesMessage = "Yes",
  noMessage = "No",
  type,
  component,
  disableConfirm = false,
  dialogWidth,
}) => {
  const [checked, setChecked] = useState(false);
  const [isSubmitting, setSubmitting] = useState(false);

  const closeSnackAlert = useSnackAlert();

  useEffect(() => {
    if (open) {
      closeSnackAlert(null)
    }
  })
  
  const handleConfirm = async (e) => {
    e.preventDefault();

    const resolveAction = async () => {
      if (inputValue) {
        await onConfirm(e, inputValue);
        setInputValue(0);
      } else if (switchOn) {
        await onConfirm(e, checked);
        setChecked(false);
      } else {
        await onConfirm(e);
      }
    };

    setSubmitting(true);
    resolveAction().then(() => setSubmitting(false));
  };

  // Log the message.
  // console.log("Rendering Alert component with message: ");
  // console.log(message);

  // Replace newline characters with <br> tags
  let formattedMessage;
  if (typeof message === 'string') {
    // Handle string message
    formattedMessage = message.split('\n').map((line, index) => (
      <span key={index}>
        {line}
        <br />
      </span>
    ));
  } else {
    // Handle React element (e.g., React.Fragment).
    // This is needed to process the "message" from DatabaseSettingsFrom,
    // which are created with the "<>...</>" shorthand for "React.Fragment".
    formattedMessage = message;
  }

  // console.log("dialogWidth: " + dialogWidth)

  return (
    <>
      <Dialog
        open={open}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        onClose={onClose}
        // maxWidth='lg' // Change this to 'xl' if 'lg' is not wide enough
        maxWidth={dialogWidth ? dialogWidth : 'lg'}
        // https://mui.com/material-ui/api/dialog/
        // fullWidth // Ensures the Dialog uses the full width specified by maxWidth
      >
        <DialogTitle
          sx={{
            display: "flex",
            alignItems: "baseline",
            justifyContent: "space-between",
          }}
        >
          <Box>
            <Typography variant="h6" component="div" style={{ flexGrow: 1}}>
              {title}
            </Typography>
          </Box>
          <Box>
            <IconButton color="neutral" onClick={onClose} size="small">
              <CloseIcon fontSize="small" />
            </IconButton>
          </Box>
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description" variant="body1">
            {formattedMessage}
          </DialogContentText>
          {switchOn ? (
            <Box
              sx={{
                display: "flex",
                flexWrap: "nowrap",
                alignItems: "center",
                mt: 2,
              }}
            >
              <Typography variant="body2"> Include protocols </Typography>
              <Switch onChange={() => setChecked(!checked)} />
            </Box>
          ) : null}
          {component ? component : null}
        </DialogContent>
        <DialogActions>
          {yesMessage && (
            <Button
              onClick={handleConfirm}
              variant={type === "delete" ? "contained" : "contained"}
              color={type === "delete" ? "error" : "info"}
              disabled={isSubmitting || disableConfirm}
              sx={{
                "&:hover": {
                  backgroundColor: type === "delete" ? "darkred" : undefined,
                },
              }}
            >
              {yesMessage}
            </Button>
          )}
          {noMessage && (
            <Button
              onClick={onCancel}
              autoFocus
              color={type === "delete" ? "neutral" : "neutral"}
            >
              {noMessage}
            </Button>
          )}
        </DialogActions>
      </Dialog>
    </>
  );
};

