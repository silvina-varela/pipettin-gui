import { useEffect } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Typography,
  IconButton,
  Box,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { useSnackAlert } from "../../hooks/useSnackAlert";

export const Popup = ({ title, openPopup, setOpenPopup, component, width }) => {
  const closeSnackAlert = useSnackAlert();

  useEffect(() => {
    closeSnackAlert(null);
  }, [openPopup]);

  const handleClose = () => {
    setOpenPopup(false);
  };

  return (
    <Dialog
      open={openPopup}
      fullWidth
      maxWidth={width}
      onClose={handleClose}
      scroll="body"
    >
      <DialogTitle
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Box>
          <Typography variant="h6" component="div" style={{ flexGrow: 1 }}>
            {title}
          </Typography>
        </Box>
        <Box>
          <IconButton
            color="neutral"
            onClick={() => {
              setOpenPopup(false);
            }}
          >
            <CloseIcon />
          </IconButton>
        </Box>
      </DialogTitle>
      <DialogContent dividers>{component}</DialogContent>
    </Dialog>
  );
};
