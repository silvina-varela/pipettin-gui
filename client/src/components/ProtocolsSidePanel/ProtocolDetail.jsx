import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Typography,
  Box,
  Tooltip,
  IconButton,
  Divider,
  Paper,
  Popover,
  Skeleton,
  Grid,
} from "@mui/material";
import SaveOutlinedIcon from "@mui/icons-material/SaveOutlined";
import InfoOutlinedIcon from "@mui/icons-material/InfoOutlined";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import ManageHistoryIcon from "@mui/icons-material/ManageHistory";
import EditOffIcon from "@mui/icons-material/EditOff";
import { AddStepMenu } from "./AddStepMenu";
import { StepCard } from "./Steps/StepCard";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { ProtocolExecution } from "./ProtocolExecution";
import { formatDate } from "../../utils/formatDate";
import { Popup } from "../Dialogs/Popup";
import { ProtocolForm } from "./ProtocolForm";
import { ProtocolExecutionList } from "./ProtocolExecutionList";
import { useSocketContext } from "../../hooks/useSocket";
import {
  addNewStepToProtocol,
  deleteProtocolSteps,
  deselectProtocol,
  setSteps,
} from "../../redux/slices";

export const ProtocolDetail = ({ saveProtocol, openDrawer }) => {
  const dispatch = useDispatch();
  const { protocol, steps, loadingProtocol } = useSelector(
    (state) => state.protocolSlice
  );
  const [openPopup, setOpenPopup] = useState(false);
  const [openList, setOpenList] = useState(false);
  const [protocolRunning, setProtocolRunning] = useState();
  const [protocolStatus, setProtocolStatus] = useState(null);
  const [editOff, setEditOff] = useState(false);

  // A dict with one entry per high-level step, each a list of
  // the action indexes that belong to that high-level step.
  const [stepActionsCount, setStepActionsCount] = useState({});
  
  // Dict of "status" data for each high-level step, indexed by "_id".
  const [stepsStatus, setStepsStatus] = useState(null);

  // Execution status messages from the controller.
  const { executionStatus, handleProtocolExecution } = useSocketContext();

  useEffect(() => {
    return () => {
      dispatch(deselectProtocol());
    };
  }, []);

  /** Checkbox steps */
  const [selectedSteps, setSelectedSteps] = useState([]);

  const isSelected = (step) =>
    selectedSteps.findIndex((i) => i.order === step.order) !== -1;

  const handleStepSelection = (step) => {
    let selectedIndex = selectedSteps.findIndex((i) => i.order === step.order);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selectedSteps, step);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selectedSteps.slice(1));
    } else if (selectedIndex === selectedSteps.length - 1) {
      newSelected = newSelected.concat(selectedSteps.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selectedSteps.slice(0, selectedIndex),
        selectedSteps.slice(selectedIndex + 1)
      );
    }

    setSelectedSteps(newSelected);
  };

  /** Create new step */
  const defaultStepDefinitions = {
    HUMAN: { text: "" },
    COMMENT: { text: "" },
    WAIT: { seconds: 0 },
    MIX: {
      target: {
        item: "",
        by: "name",
        value: "",
      },
      mix: {
        type: "",
        percentage: 50,
        count: 3,
      },
      tip: {
        item: "",
        discardItem: "",
        mode: "reuse",
      },
      tool: "",
    },
    TRANSFER: {
      source: {
        item: "",
        by: "name",
        value: "",
        treatAs: "",
      },
      target: {
        item: "",
        by: "name",
        value: "",
      },
      volume: {
        type: "fixed_each",
        value: 0,
        tag: "",
      },
      tip: {
        item: "",
        discardItem: "",
        mode: "reuse",
      },
      // It is important that this is "" and not null, because the
      // backend validates these properties length (see protocolValidations.js).
      tool: "",
    },
    JSON: {},
  };

  const handleAddStep = (stepType) => {
    const stepOrder = steps?.length ? steps[steps.length - 1].order + 1 : 1;

    const getNextStepOrder = (stepType) => {
      const stepsOfType = steps?.filter((step) => step.type === stepType);
      return stepsOfType?.length ? stepsOfType?.length + 1 : 1;
    };

    const getUniqueStepName = (stepType) => {
      let nameOrder = getNextStepOrder(stepType);

      while (nameOrder) {
        const nameExists = steps?.some(
          (step) => step.name === `${stepType.toLowerCase()}${nameOrder}`
        );
        if (!nameExists) {
          return `${stepType.toLowerCase()}${nameOrder}`;
        }
        nameOrder++;
      }
    };

    const newStep = {
      type: stepType,
      order: stepOrder,
      name: getUniqueStepName(stepType),
      definition: defaultStepDefinitions[stepType],
    };

    dispatch(addNewStepToProtocol(newStep));

    setSelectedSteps([]);
  };

  /** Delete steps */
  const handleDeleteSteps = async () => {
    try {
      dispatch(deleteProtocolSteps(selectedSteps)).then(() =>
        setSelectedSteps([])
      );
    } catch (error) {
      console.log("Error:", error);
    }
  };

  /** Saves to DB */
  const handleSaveProtocol = (e) => {
    e.preventDefault();
    saveProtocol();
  };

  const handleDragEnd = async (result) => {
    if (!result.destination) return;
    const { source, destination } = result;

    const newSteps = [...steps];
    const [movedStep] = newSteps.splice(source.index, 1);
    newSteps.splice(destination.index, 0, movedStep);
    const updatedSteps = newSteps.map((step, i) => ({ ...step, order: i + 1 }));

    dispatch(setSteps(updatedSteps));
  };

  const [anchorDetails, setAnchorDetails] = useState(null);
  const open = Boolean(anchorDetails);
  const id = open ? "simple-popper" : undefined;

  const openDetails = (e) => {
    e.preventDefault();

    setAnchorDetails(anchorDetails ? null : e.currentTarget);
  };

  const handleEditProtocol = () => {
    setOpenPopup(true);
  };

  const handleShowProtocolList = () => {
    setOpenList(true);
  };

  useEffect(() => {
    return () => {
      handleProtocolExecution(null);
    };
  }, []);

  const resetStates = () => {
    setProtocolRunning(null);
    setStepsStatus(null);
    setProtocolStatus(null);
    setStepActionsCount({});
  };
  useEffect(() => {
    if (protocolRunning?.actions && steps.length) {
      const actionsCount = {};

      // Initialize list of action indexes for each
      // high-level step (indexed by its "_id").
      steps.forEach((step) => {
        actionsCount[step._id] = [];
      });

      // Iterate through actions to count actions for each step
      protocolRunning.actions.forEach((action, index) => {
        // The index of each action is added to the list of
        // indexes, that corresponds to a high-level step id.
        actionsCount[action.stepID]?.push(index);
      });

      // This sets "stepActionsCount".
      setStepActionsCount(actionsCount);
    }
  }, [protocolRunning?.name]);

  useEffect(() => {
    if (executionStatus) {
      const stepID = executionStatus.action?.stepID || "Home";
      const status = executionStatus.status || undefined;
      const command = executionStatus.action?.cmd;
      setProtocolStatus({
        message:
          status === "error"
            ? `Error in action ${executionStatus.action_index} with command '${command}'`
            : `Action ${executionStatus.action_index} is ${status} with command '${command}'`,
        severity: status === "error" ? "error" : "green", // Set color (red / green).
        protocol: executionStatus.hLprotocol,
      });

      setStepsStatus((prev) => {
        return {
          // Preserve the previous step-status values.
          ...prev,
          // Override the status of the current high-level step.
          [stepID]: {
            // Save the incoming status.
            status,
            // Look for the action index currently being executed (i.e. status update received).
            currentAction:
              stepActionsCount[stepID]?.indexOf(executionStatus.action_index) + 1 || 0,
            // Count total actions for the current high-level step.
            totalActions: stepActionsCount[stepID]?.length || 0,
          },
        };
      });
    }
  }, [executionStatus, useSocketContext]);

  const handleEditOff = () => {
    setEditOff(!editOff);
  };

  if (loadingProtocol)
    return (
      <>
        <Grid
          container
          pt={3}
          px={1}
          direction="row"
          justifyContent="space-between"
        >
          <Grid item>
            <Skeleton variant="rectangular" height={35} width={100} />
          </Grid>
          <Grid item>
            <Skeleton variant="rectangular" height={35} width={100} />
          </Grid>
        </Grid>
        <Skeleton
          sx={{ position: "relative", marginTop: 10, marginX: 1 }}
          variant="rectangular"
          height={30}
        />
        <Skeleton
          sx={{ position: "relative", marginTop: 1, marginX: 1 }}
          variant="rectangular"
          height={30}
        />
        <Skeleton
          sx={{ position: "relative", marginTop: 1, marginX: 1 }}
          variant="rectangular"
          height={30}
        />
        <Skeleton
          sx={{ position: "relative", marginTop: 1, marginX: 1 }}
          variant="rectangular"
          height={30}
        />
      </>
    );
  else
    return (
      <>
        <Box
          display={"flex"}
          flexDirection={"column"}
          sx={{ display: !openDrawer && "none" }}
        >
          <Box
            display={"flex"}
            ml={2}
            mr={1}
            mt={2}
            justifyContent={"space-between"}
            alignItems={"center"}
          >
            <Typography variant="subtitle1" noWrap style={{ fontSize: 14 }}>
              {protocol.name}
            </Typography>
            <Box
              display={"flex"}
              justifyContent={"space-between"}
              alignItems={"center"}
            >
              <Tooltip title="Protocol details">
                <IconButton
                  aria-label="save"
                  color="inherit"
                  onClick={openDetails}
                >
                  <InfoOutlinedIcon sx={{ fontSize: 14 }} />
                </IconButton>
              </Tooltip>
              <Tooltip title="Edit protocol">
                <IconButton
                  aria-label="save"
                  color="inherit"
                  onClick={handleEditProtocol}
                >
                  <EditIcon sx={{ fontSize: 14 }} />
                </IconButton>
              </Tooltip>
              <Tooltip title="Save protocol">
                <IconButton
                  aria-label="save"
                  color="inherit"
                  onClick={handleSaveProtocol}
                >
                  <SaveOutlinedIcon sx={{ fontSize: 14 }} />
                </IconButton>
              </Tooltip>
              <Tooltip title="Protocol executions">
                <IconButton
                  aria-label="save"
                  color="inherit"
                  onClick={handleShowProtocolList}
                >
                  <ManageHistoryIcon sx={{ fontSize: 14 }} />
                </IconButton>
              </Tooltip>
            </Box>
          </Box>
          <ProtocolExecution
            protocolStatus={protocolStatus}
            setEditOff={setEditOff}
            setProtocolRunning={setProtocolRunning}
            resetStates={resetStates}
          />
          <Divider sx={{ marginY: 1 }} />

          {/* Add step menu */}
          <Box
            display={"flex"}
            ml={2}
            mr={1}
            mb={1}
            justifyContent={"space-between"}
            alignItems={"center"}
          >
            <Typography variant="subtitle2">STEPS</Typography>
            <Box
              display={"flex"}
              justifyContent={"space-between"}
              alignItems={"center"}
            >
              <Tooltip title={"Edit " + (editOff ? "on" : "off")}>
                <IconButton onClick={handleEditOff}>
                  {editOff ? (
                    <EditOffIcon fontSize="small" />
                  ) : (
                    <EditIcon fontSize="small" />
                  )}
                </IconButton>
              </Tooltip>

              <Tooltip title="Delete step">
                <IconButton onClick={handleDeleteSteps}>
                  <DeleteIcon fontSize="small" />
                </IconButton>
              </Tooltip>
              <AddStepMenu handleAddStep={handleAddStep} />
            </Box>
          </Box>
        </Box>

        {/* STEPS */}
        <DragDropContext onDragEnd={handleDragEnd}>
          <Droppable droppableId="droppable-list">
            {(provided) => (
              <Box
                {...provided.droppableProps}
                ref={provided.innerRef}
                sx={{ overflowY: "auto", display: !openDrawer && "none" }}
              >
                {steps
                  ? [...steps]
                      .sort((a, b) => a.order - b.order)
                      .map((step, index) => (
                        <Draggable
                          key={step.name}
                          draggableId={step.name}
                          index={index}
                          isDragDisabled={editOff}
                        >
                          {(provided, snapshot) => (
                            <Box
                              className={snapshot.isDragging ? "dragging" : ""}
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                            >
                              {/* STEP */}
                              <StepCard
                                key={index}
                                step={step}
                                index={index}
                                isSelected={isSelected}
                                handleStepSelection={handleStepSelection}
                                stepsStatus={stepsStatus}
                                protocolRunning={protocolRunning}
                              />
                            </Box>
                          )}
                        </Draggable>
                      ))
                  : null}
                {provided.placeholder}
              </Box>
            )}
          </Droppable>
        </DragDropContext>

        <Popover
          id={id}
          open={open}
          anchorEl={anchorDetails}
          onClose={openDetails}
          anchorOrigin={{
            vertical: "top",
            horizontal: "left",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
        >
          <Paper sx={{ maxWidth: "20vw", p: 2 }}>
            <Typography variant="subtitle2">{protocol.name} </Typography>
            <Typography variant="caption" component="p" fontSize={"70%"}>
              {" "}
              Created: {formatDate(protocol.createdAt, true)}
            </Typography>
            <Typography variant="caption" component="p" fontSize={"70%"}>
              {" "}
              Last updated: {formatDate(protocol.updatedAt, true)}
            </Typography>
            <Typography variant="body2" mt={1}>
              {" "}
              {protocol.description}
            </Typography>
          </Paper>
        </Popover>
        <Popup
          title={"Edit protocol"}
          openPopup={openPopup}
          setOpenPopup={setOpenPopup}
          component={
            <ProtocolForm
              openSelectedProtocol={() => setOpenPopup(true)}
              handleCloseForm={() => setOpenPopup(false)}
              isEditMode={true}
            />
          }
        />
        <Popup
          title={"Protocol Execution List"}
          openPopup={openList}
          setOpenPopup={setOpenList}
          component={
            <ProtocolExecutionList
              hLprotocol={protocol.name}
              workspaceName={protocol.workspace}
              fileName={protocol.name}
            />
          }
          width="md"
        />
      </>
    );
};
