import { useState, useEffect } from "react";
import { Formik, Form } from "formik";
import { Button, CircularProgress } from "@mui/material";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { ProtocolTemplateForm } from "./Forms/ProtocolTemplateForm";
import { BasicProtocolInfo } from "./Forms/BasicProtocolInfo";
import {
  getWorkspaceByIdAsync,
  createProtocolAsync,
  getProtocolTemplatesAsync,
  saveProtocolAsync,
} from "../../redux/slices";

const createProtocolSchema = Yup.object().shape({
  name: Yup.string()
    .min(3, "Protocol name must have at least 3 characters")
    .max(100, "Protocol name can't have more than 100 characters")
    .required("Please enter name"),
  description: Yup.string().max(
    1000,
    "Description can't have more than 1000 characters"
  ),
});

export const ProtocolForm = ({
  isEditMode = false,
  openSelectedProtocol,
  handleCloseForm,
}) => {
  const dispatch = useDispatch();
  const { workspace } = useSelector((state) => state.workspaceSlice);
  const { platforms } = useSelector((state) => state.platformSlice);
  const { protocol, protocolTemplates } = useSelector(
    (state) => state.protocolSlice
  );

  const [activeStep, setActiveStep] = useState(0);
  const [templates, setTemplates] = useState([]);
  const [platformsToInsert, setPlatformsToInsert] = useState({});
  const steps = ["New protocol", "Template"];

  const renderStepContent = (
    step,
    values,
    handleChange,
    touched,
    errors,
    isEditMode
  ) => {
    switch (step) {
      case 0:
        return (
          <BasicProtocolInfo
            templates={templates}
            values={values}
            handleChange={handleChange}
            touched={touched}
            errors={errors}
            isEditMode={isEditMode}
          />
        );
      case 1:
        return (
          <ProtocolTemplateForm
            templates={templates}
            values={values}
            template={values.template}
            workspace={workspace}
            handleChange={handleChange}
            touched={touched}
            errors={errors}
            setPlatformsToInsert={setPlatformsToInsert}
            platformsToInsert={platformsToInsert}
            isEditMode={isEditMode}
          />
        );
      default:
        return <div>Not Found</div>;
    }
  };

  /** Get templates */
  useEffect(() => {
    dispatch(getProtocolTemplatesAsync());
  }, [dispatch]);

  useEffect(() => {
    setTemplates(protocolTemplates);
  }, [JSON.stringify(protocolTemplates)]);

  const createFormInitialValues = {
    name: "",
    description: "",
    template: "",
    templateDefinition: {},
    templateFields: "",
  };

  const initialValues = isEditMode ? protocol : createFormInitialValues;
  const currentValidationSchema = !activeStep && createProtocolSchema;
  const isLastStep = activeStep === steps.length - 1;

  const createProtocol = async (values, workspaceValues) => {
    dispatch(createProtocolAsync({ values, workspaceValues }))
      .unwrap()
      .then(() => {
        if (workspace)
          dispatch(getWorkspaceByIdAsync(workspace._id));
      })
      .then(() => {
        openSelectedProtocol();
        handleCloseForm();
      })
      .catch((error) => console.log(error))
      .finally(() => {
        handleCloseForm();
      });
  };

  const updateProtocol = async (values, workspaceValues) => {
    dispatch(saveProtocolAsync({ values, workspaceValues }))
      .unwrap()
      .then(() => {
        if (workspace)
          dispatch(getWorkspaceByIdAsync(workspace._id));
      })
      .then(() => {
        openSelectedProtocol();
        handleCloseForm();
      })
      .catch((error) => console.log(error))
      .finally(() => {
        handleCloseForm();
      });
  };

  const submitForm = async (values) => {
    if (
      Object.prototype.hasOwnProperty.call(values, "templateFields") &&
      Object.keys(values.templateFields).length
    ) {
      // console.log("Submitting templated protocol for workspace: " + workspace.name)
      // console.log("Template definition:");
      // console.log(values);  // values.templateDefinition.file_upload1 is a "File" object.
      const workspaceValues = {
        currentWorkspace: workspace,
        allPlatforms: platforms,
        platformsToInsert: platformsToInsert,
      };
      isEditMode
        ? updateProtocol(values, workspaceValues)
        : createProtocol({ ...values, workspace: workspace.name }, workspaceValues);
    } else {
      // console.log("Submitting protocol for workspace: " + workspace.name)
      isEditMode
        ? updateProtocol(values)
        : createProtocol({ ...values, workspace: workspace.name });
    }
  };

  const handleSubmit = (values, actions) => {
    if (
      Object.prototype.hasOwnProperty.call(values, "templateFields") &&
      Object.keys(values.templateFields).length &&
      !isLastStep
    ) {
      // console.log("Continiung to template form.");
      setActiveStep(activeStep + 1);
      actions.setFieldValue("template", values.templateFields.name);
      actions.setTouched({});
      actions.setSubmitting(false);
    } else {
      // console.log("Submitting protocol with values:");
      // console.log(values);
      actions.setFieldValue("workspace", workspace.name);
      submitForm(values);
    }
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={currentValidationSchema}
      onSubmit={handleSubmit}
    >
      {({ values, touched, errors, isSubmitting, handleChange }) => (
        <Form id="protocolForm">
          {renderStepContent(
            activeStep,
            values,
            handleChange,
            touched,
            errors,
            isEditMode
          )}
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            {activeStep !== 0 && (
              <Button onClick={handleBack} sx={{ marginTop: 3, marginLeft: 1 }}>
                Back
              </Button>
            )}
            <div style={{ margin: 1, position: "relative" }}>
              <Button
                disabled={isSubmitting}
                type="submit"
                variant="contained"
                sx={{ marginTop: 3, marginLeft: 1 }}
              >
                {isLastStep
                  ? isEditMode
                    ? "Edit"
                    : "Create"
                  : Object.prototype.hasOwnProperty.call(
                      values,
                      "templateFields"
                    ) && Object.keys(values.templateFields).length
                  ? "Next"
                  : isEditMode
                  ? "Edit"
                  : "Create"}
              </Button>

              {isSubmitting && (
                <CircularProgress
                  size={24}
                  sx={{ position: "absolute", top: "50%", left: "40%" }}
                />
              )}
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};
