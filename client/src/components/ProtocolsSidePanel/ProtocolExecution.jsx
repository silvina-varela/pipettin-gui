import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { Box, Button, ButtonGroup, Tooltip } from "@mui/material";
import DoneOutlineOutlinedIcon from "@mui/icons-material/DoneOutlineOutlined";
import PlayArrowOutlinedIcon from "@mui/icons-material/PlayArrowOutlined";
import StopIcon from "@mui/icons-material/Stop";
import StartIcon from "@mui/icons-material/Start";
import PauseIcon from "@mui/icons-material/Pause";
import { ProtocolLog } from "./ProtocolLog";
import { useSnackAlert } from "../../hooks/useSnackAlert";
import { saveProtocolAsync, saveWorkspaceAsync } from "../../redux/slices";
import { useSocketContext } from "../../hooks/useSocket";

export const ProtocolExecution = ({
  protocolStatus,
  setEditOff,
  setProtocolRunning,
  resetStates,
}) => {
  const { protocol } = useSelector((state) => state.protocolSlice);

  const addSnack = useSnackAlert();

  const dispatch = useDispatch();
  const { controllerStatus } = useSocketContext();

  const [disableButton, setDisableButton] = useState(false);
  const [protocolLog, setProtocolLog] = useState([]);

  const constructLog = (message, severity, protocol) => {
    const date = new Date();

    const formattedDate = Intl.DateTimeFormat("en-GB", {
      dateStyle: "short",
      timeStyle: "long",
    }).format(date);

    setProtocolLog([
      ...protocolLog,
      {
        message,
        severity,
        date: formattedDate,
        protocol,
      },
    ]);
  };

  useEffect(() => {
    if (protocolStatus)
      constructLog(
        protocolStatus.message,
        protocolStatus.severity,
        protocolStatus.protocol
      );
  }, [JSON.stringify(protocolStatus)]);

  const handleValidate = async (e) => {
    e.preventDefault();

    setDisableButton(true);
    await Promise.all([dispatch(saveProtocolAsync({ values: protocol })), dispatch(saveWorkspaceAsync())])
    addSnack("Workspace and protocol saved", "success");

    await axios
      .post("/api/protocols/validate", { protocolID: protocol._id })
      .then(() => {
        constructLog(
          "Validation completed. Your protocol is ready to run!",
          "green",
          protocol.name
        );
      })
      .catch((error) => {
        constructLog(error.response.data, "error", protocol.name);
      })
      .finally(() => setDisableButton(false));
  };
  const handleRun = async (e) => {
    e.preventDefault();
    setDisableButton(true);

    setEditOff(true);
    resetStates()
    try {
      if (controllerStatus.status === "OFF") {
        addSnack("The controller is not connected", "error");
        setDisableButton(false);
        return;
      }
  
      await Promise.all([dispatch(saveProtocolAsync({ values: protocol })), dispatch(saveWorkspaceAsync())])
      addSnack("Workspace and protocol saved", "success");

      const response = await axios.post("/api/robot/run", { protocolID: protocol._id });
      setProtocolRunning(response?.data);
      constructLog("Your protocol is running!", "green");
    } catch (error) {
      constructLog(error.response.data, "error", protocol.name);
    } finally {
      setDisableButton(false);
    }    
  };

  const handleStopProtocol = async (e) => {
    e.preventDefault();
    setDisableButton(true);

    await axios
      .post("/api/robot/stop_protocol", {
        protocolID: protocol._id,
        protocol: protocol.name,
      })
      .then(() => {
        constructLog("Protocol stopped", "neutral");
      })
      .catch((error) => {
        constructLog(error.response.data, "error");
      })
      .finally(() => setDisableButton(false));
  };

  const handlePauseProtocol = async (e) => {
    e.preventDefault();
    setDisableButton(true);

    await axios
      .post("/api/robot/pause_protocol", {
        protocolID: protocol._id,
        protocol: protocol.name,
      })
      .then(() => {
        constructLog("Protocol paused", "neutral");
      })
      .catch((error) => {
        constructLog(error.response.data, "error");
      })
      .finally(() => setDisableButton(false));
  };

  const handleContinueProtocol = async (e) => {
    e.preventDefault();
    setDisableButton(true);

    await axios
      .post("/api/robot/continue_protocol", {
        protocolID: protocol._id,
        protocol: protocol.name,
      })
      .then(() => {
        constructLog("Protocol paused", "neutral");
      })
      .catch((error) => {
        constructLog(error.response.data, "error");
      })
      .finally(() => setDisableButton(false));
  };

  return (
    <Box>
      <Box
        mt={2}
        mx={2}
        mb={0.5}
        display="flex"
        justifyContent={"space-between"}
      >
        <Box>
          <ButtonGroup variant="outlined" color="neutral" size="small">
            <Tooltip title="Validate protocol">
              <Button
                color="primary"
                onClick={handleValidate}
                disabled={disableButton}
              >
                <DoneOutlineOutlinedIcon fontSize="small" />
              </Button>
            </Tooltip>
          </ButtonGroup>
        </Box>
        <Box>
          <ButtonGroup variant="outlined" color="neutral" size="small">
            {/* START EXECUTION */}
            <Tooltip title="Run protocol">
              <Button
                color="success"
                onClick={handleRun}
                disabled={disableButton}
              >
                <PlayArrowOutlinedIcon fontSize="small" />
              </Button>
            </Tooltip>
            {/* PAUSE EXECUTION */}
            <Tooltip title="Pause execution">
              <Button
                color="warning"
                onClick={handlePauseProtocol}
                disabled={disableButton}
              >
                <PauseIcon fontSize="small" />
              </Button>
            </Tooltip>
            {/* STOP EXECUTION */}
            <Tooltip title="Stop execution">
              <Button
                color="error"
                onClick={handleStopProtocol}
                disabled={disableButton}
              >
                <StopIcon fontSize="small" />
              </Button>
            </Tooltip>
            {/* CONTINUE EXECUTION */}
            <Tooltip title="Continue execution">
              <Button
                  color="info"
                  onClick={handleContinueProtocol}
                  disabled={disableButton}
                >
                <StartIcon fontSize="small" />
              </Button>
            </Tooltip>
          </ButtonGroup>
        </Box>
      </Box>

      <ProtocolLog protocolLog={protocolLog} setProtocolLog={setProtocolLog} />
    </Box>
  );
};
