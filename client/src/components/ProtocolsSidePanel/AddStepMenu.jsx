import { useState } from "react";
import { styled, alpha } from "@mui/material/styles";
import { Menu, MenuItem, IconButton, Tooltip } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";

const StyledMenu = styled((props) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "right",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "right",
    }}
    {...props}
  />
))(({ theme }) => ({
  "& .MuiPaper-root": {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color:
      theme.palette.mode === "light"
        ? "rgb(55, 65, 81)"
        : theme.palette.grey[300],
    boxShadow:
      "rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px",
    "& .MuiMenu-list": {
      padding: "4px 0",
    },
    "& .MuiMenuItem-root": {
      "& .MuiSvgIcon-root": {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      "&:active": {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity
        ),
      },
    },
  },
}));

export const AddStepMenu = ({ handleAddStep }) => {
  const [anchorMenu, setAnchorMenu] = useState(null);
  const openMenu = Boolean(anchorMenu);


  const handleClickMenu = (event) => {
    setAnchorMenu(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorMenu(null);
  };

  const handleClickItem = (event) => {
    event.preventDefault();
    handleAddStep(event.target.id);
    setAnchorMenu(null);
  };

  const renderMenuItem = (id, label) => (
    <MenuItem key={id} id={id} onClick={handleClickItem}>
      {label}
    </MenuItem>
  );

  const menuItems = [
    { id: 'TRANSFER', label: 'Transfer' },
    { id: 'WAIT', label: 'Wait' },
    { id: 'HUMAN', label: 'Human intervention' },
    { id: 'COMMENT', label: 'Comment' },
    { id: 'MIX', label: 'Mix' },
    { id: 'JSON', label: 'JSON' },
  ];

  return (
    <>
      <Tooltip title="Add step">
        <IconButton
          aria-label="add"
          color="inherit"
          onClick={(event) => handleClickMenu(event)}
        >
          <AddIcon fontSize="small" />
        </IconButton>
      </Tooltip>
      <StyledMenu
        anchorEl={anchorMenu}
        open={openMenu}
        onClose={handleCloseMenu}
      >
         {menuItems.map(({ id, label }) => renderMenuItem(id, label))}
      </StyledMenu>
    </>
  );
};
