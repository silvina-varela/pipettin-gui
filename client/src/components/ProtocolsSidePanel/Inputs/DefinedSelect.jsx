import {
  MenuItem,
  FormControl,
  Select,
  Typography,
} from "@mui/material";

export const DefinedSelect = ({
  options,
  handleChange,
  value,
}) => {

  const handleSelection = (event) => {
    handleChange(event.target.value);
  };

  return (
    <FormControl variant="standard" sx={{
      width: "100%",
    }}>
      <Select
        value={value}
        onChange={handleSelection}
        defaultValue=""
        sx={{height: 25}}
        displayEmpty // Needed to display "Choose one"
        renderValue={(selected) => {
          if (selected === "") {
            return <Typography variant={"caption"} color="grey">Choose one</Typography>;
          }
          return selected;
        }}
      >
        {options?.map((option, index) => {
          return (
            <MenuItem key={index} value={option.value}>
              <Typography variant={"caption"}>{option.label}</Typography>
            </MenuItem>
          );
        })}
      </Select>
     
    </FormControl>
  );
};
