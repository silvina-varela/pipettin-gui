import { useEffect } from "react";
import {
  MenuItem,
  FormControl,
  Select,
  Typography,
} from "@mui/material";

export const InputSelect = ({ options, handleChange, value }) => {

  const handleSelection = (event) => {
    handleChange(event.target.value);
  };

  useEffect(() => {
    // If value is undefined or empty, and there is only one option, set it as the default value.
    if ((!value || value === "") && options.length === 1) {
      handleChange(options[0]);
    }
  }, [value, options, handleChange]);

  return (
    <FormControl
      variant="standard"
      sx={{
        width: "100%",
      }}
      error
    >
      <Select
        value={value || ""}
        displayEmpty // Needed to display "Choose one"
        onChange={handleSelection}
        renderValue={(selected) => {
          if (selected === "") {
            return <Typography variant={"caption"} color="grey">Choose one</Typography>;
          }
          return selected;
        }}
        // defaultValue=""
        sx={{
          height: 25,
        }}
        error={Boolean(value?.length && !options.includes(value))}
      >
        {/* <MenuItem  value=""> */}
          {/* <Typography variant={"caption"}>None</Typography> */}
        {/* </MenuItem> */}
        {options?.map((option, index) => {
          return (
            <MenuItem key={index} value={option}>
              <Typography variant={"caption"}>{option}</Typography>
            </MenuItem>
          );
        })}
        {value?.length ? !options.includes(value) && (
          <MenuItem value={value}>
            <Typography variant={"caption"} color={"error"}>{value}</Typography>
          </MenuItem>
        ) : null}
      </Select>
      {value?.length ? !options.includes(value) && (
        <Typography
          variant={"caption"}
          color={"error"}
          position={"absolute"}
          mt={3.2}
          fontSize="0.6rem"
        >
          Missing item
        </Typography>
      ) : null}
    </FormControl>
  );
};
