import { ToggleButtonGroup, ToggleButton } from "@mui/material";

export const Toggle = ({ option1, option2, handleChange, toggleValue }) => {
  return (
    <ToggleButtonGroup
      color="primary"
      value={toggleValue}
      exclusive
      onChange={(event) => handleChange(event.target.value)}
      size="small"
      sx={{ height: 20 }}
    >
      <ToggleButton
        value={option1}
        sx={{
          textTransform: "lowercase",
          p: 0.5,
        }}
      >
        {option1}
      </ToggleButton>
      <ToggleButton
        value={option2}
        sx={{
          textTransform: "lowercase",
          p: 0.5,
        }}
      >
        {option2}
      </ToggleButton>
    </ToggleButtonGroup>
  );
};
