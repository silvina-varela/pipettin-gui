import { useState, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Box,
  Typography,
  AccordionDetails,
  Accordion as MuiAccordion,
  AccordionSummary as MuiAccordionSummary,
  Checkbox,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { styled } from "@mui/material/styles";
import { CommentStep } from "./CommentStep";
import { HumanStep } from "./HumanStep";
import WaitStep from "./WaitStep";
import TransferStep from "./TransferStep";
import { MixStep } from "./MixStep";
import { JsonStep } from "./JsonStep";
import { getUniqueName } from "../../../utils/previewHelpers";
import { updateProtocolStep } from "../../../redux/slices";
import { createSelector } from 'reselect';

const selectWorkspaceItems = createSelector(
  (state) => {
    // console.log("INPUT")
    // console.log(state.workspaceSlice)
    return state.workspaceSlice.workspace;
  },
  (workspace) => {
    // console.log("OUTPUT")
    // console.log(workspace)
    return workspace.items;
  }
);

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  "&:not(:last-child)": {
    borderBottom: 0,
  },
  "&:before": {
    display: "none",
  },
}));

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={<ExpandMoreIcon sx={{ fontSize: "0.9rem" }} />}
    {...props}
  />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === "dark"
      ? "rgba(255, 255, 255, .05)"
      : "rgba(0, 0, 0, .03)",
  "& .MuiAccordionSummary-content": {
    maxWidth: "100%",
    right: 0,
    left: 0,
  },
}));

export const StepCard = ({
  step,
  index,
  isSelected,
  handleStepSelection,
  stepsStatus,
  protocolRunning,
}) => {
  const dispatch = useDispatch();
  const { steps } = useSelector((state) => state.protocolSlice);
  const items = useSelector(selectWorkspaceItems);

  /** Step name editing */
  const [isStepNameEditing, setIsStepNameEditing] = useState(false);
  const [newStepName, setNewStepName] = useState(step.name);

  const handleSaveStepName = () => {
    if(step.name !== newStepName && newStepName.length){
      const uniqueName = getUniqueName(steps, newStepName);
      const updatedStep = { ...step, name: uniqueName };
      setNewStepName(updatedStep)
      dispatch(
        updateProtocolStep(index, updatedStep)
      ).then(() => {
        setIsStepNameEditing(false)
      });
    } else {
      setIsStepNameEditing(false)
      setNewStepName(step.name)

    }
  };

  const handleNameDoubleClick = (event) => {
    event.stopPropagation();
    setIsStepNameEditing(true);
  };

  const handleStepNameChange = (event) => {
    setNewStepName(event.target.value);
  };

  const handleStepNameBlur = () => {
    handleSaveStepName();
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      handleStepNameBlur();
    }
  };

  /** Checkbox */
  const handleCheckbox = (event) => {
    event.stopPropagation();
    handleStepSelection(step);
  };

  /** Save step definition */
  const handleSaveStep = (definition) => {
    dispatch(
      updateProtocolStep(index, {
        ...steps[index],
        definition: definition,
      })
    );
  };

  /** Get current platforms data */
  /** Get all platform names from workspace */
  const allPlatformNames = useMemo(() => {
    if (items?.length) {
      return items.map((platform) => platform.name);
    } else return [];
  }, [JSON.stringify(items)]);

  /** Get all non-tip rack and non-trash platform names from workspace */
  const contentlyPlatformTypes = ["TUBE_RACK", "PETRI_DISH", "CUSTOM"]
  const sourcePlatformNames = useMemo(() => {
    if (items?.length) {
      return items
        .filter(
          (platform) =>
            platform.platformData && contentlyPlatformTypes.includes(platform.platformData.type)
        )
        .map((platform) => platform.name);
    } else {
      return [];
    }
  }, [JSON.stringify(items)]);

  /** Get all tip racks names from workspace */
  const tipNames = useMemo(() => {
    if (items?.length) {
      return items
        .filter(
          (platform) =>
            platform.platformData && platform.platformData.type === "TIP_RACK"
        )
        .map((platform) => platform.name);
    }
    return [];
  }, [JSON.stringify(items)]);

  /** Get all buckets names from workspace */
  const bucketNames = useMemo(() => {
    if (items?.length) {
      return items
        .filter(
          (platform) =>
            platform.platformData && platform.platformData.type === "BUCKET"
        )
        .map((platform) => platform.name);
    }
    return [];
  }, [JSON.stringify(items)]);

  /** Gather results */
  const platformsNames = {
    allPlatforms: allPlatformNames,
    tips: tipNames,
    buckets: bucketNames,
    sources: sourcePlatformNames
  };

  // Get data from specific platform name
  const getPlatformSelected = (item) => {
    return items?.find((platform) => platform.name === item);
  };

  // Get content by name or tag from specific platform name
  const getContentOptions = (item, by) => {
    if (item?.length && by?.length) {
      const platform = getPlatformSelected(item);
      if (!platform) return [];
      if (by === "name") {
        const nameOptions = platform.content?.map((content) => content.name);
        return nameOptions;
      } else if (by === "tag") {
        const tags = new Set();

        platform.content?.forEach((content) => {
          content.tags.forEach((tag) => {
            tags.add(tag);
          });
        });
        const tagOptions = Array.from(tags);

        return tagOptions;
      }
    } else return [];
  };

  const getCountContentSelected = (item, by, value) => {
    if (by === "name" && value?.length) return 1;
    else if (by === "tag" && value?.length) {
      const platform = getPlatformSelected(item);
      if (!platform) return 0;
      let sameTags = platform.content?.filter((content) =>
        content.tags.includes(value)
      );
      return sameTags.length;
    } else return 0;
  };

  const contentTargetOptions = useMemo(
    () =>
      getContentOptions(
        step.definition?.target?.item,
        step.definition?.target?.by
      ),
    [
      step.definition?.target?.by,
      step.definition?.target?.item,
      JSON.stringify(items),
    ]
  );

  const contentTargetAmount = useMemo(
    () =>
      getCountContentSelected(
        step.definition?.target?.item,
        step.definition?.target?.by,
        step.definition?.target?.value
      ),
    [
      step.definition?.target?.by,
      step.definition?.target?.value,
      JSON.stringify(items),
    ]
  );

  // Return step components
  const stepDefinition = {
    TRANSFER: (
      <TransferStep
        definition={step.definition}
        handleSaveStep={handleSaveStep}
        platformsNames={platformsNames}
        getContentOptions={getContentOptions}
        getCountContentSelected={getCountContentSelected}
        contentOptions={contentTargetOptions}
        contentAmount={contentTargetAmount}
      />
    ),
    MIX: (
      <MixStep
        definition={step.definition}
        handleSaveStep={handleSaveStep}
        platformsNames={platformsNames}
        contentOptions={contentTargetOptions}
        contentAmount={contentTargetAmount}
      />
    ),
    WAIT: (
      <WaitStep definition={step.definition} handleSaveStep={handleSaveStep} />
    ),
    COMMENT: (
      <CommentStep
        definition={step.definition}
        handleSaveStep={handleSaveStep}
      />
    ),
    HUMAN: (
      <HumanStep definition={step.definition} handleSaveStep={handleSaveStep} />
    ),
    JSON: (
      <JsonStep definition={step.definition} handleSaveStep={handleSaveStep} />
    ),
  };

  const backgroundColor = (status) => {
    switch (status) {
      case "error":
        return "#FF000050"; // red
      case "running":
        return "#00800050"; // orange
      case "done":
        return "#00800050"; // green
      default:
        return undefined;
    }
  };

  const stepPercentage = useMemo(() => {
    if (stepsStatus && stepsStatus[step._id]) {
      const totalActions = stepsStatus[step._id].totalActions;
      const currentAction = stepsStatus[step._id].currentAction;
      const progressPercentage = (currentAction / totalActions) * 100;
      return progressPercentage;
    }
  }, [JSON.stringify(stepsStatus), JSON.stringify(protocolRunning), step._id]);

  return (
    <Accordion>
      <AccordionSummary expandIcon={null}>
        <Box
          width="100%"
          display="flex"
          flexDirection={"row"}
          overflow={"hidden"}
          alignItems={"center"}
          justifyContent={"space-between"}
        >
          <Box display="flex" flexDirection={"row"} overflow={"hidden"}>
            <Checkbox
              checked={isSelected(step)}
              onClick={handleCheckbox}
              size="small"
            />
            <Box display={"flex"} flexDirection={"column"} overflow={"hidden"}>
              <Typography sx={{ fontSize: "9px" }}>
                STEP {step.order}
              </Typography>
              {isStepNameEditing ? (
                <input
                  type="text"
                  value={newStepName}
                  onChange={handleStepNameChange}
                  onBlur={handleStepNameBlur}
                  onKeyDown={handleKeyDown}
                  onClick={(e) => e.stopPropagation()}
                  style={{ maxWidth: "85%" }}
                />
              ) : (
                <Typography
                  onDoubleClick={handleNameDoubleClick}
                  onClick={(e) => e.stopPropagation()}
                  sx={{
                    fontSize: "15px",
                    whiteSpace: "nowrap",
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                  }}
                >
                  {step.name}
                </Typography>
              )}
            </Box>
          </Box>
          <Box>
            <Typography sx={{ fontSize: "8.5px" }}>{step.type}</Typography>
          </Box>
        </Box>
        <Box
          position="absolute"
          bottom={0}
          left={0}
          width={`${stepPercentage}%`}
          height="100%"
          bgcolor={
            stepsStatus && stepsStatus[step._id]
              ? backgroundColor(stepsStatus[step._id]?.status)
              : undefined
          }
          sx={{ transition: "width 0.5s ease-in-out", opacity: 0.5 }}
        />
      </AccordionSummary>
      <AccordionDetails>{stepDefinition[step.type]}</AccordionDetails>
    </Accordion>
  );
};
