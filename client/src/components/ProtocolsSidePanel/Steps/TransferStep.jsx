import { useMemo, memo } from "react";
import { Stack } from "@mui/material";
import { SourceSection } from "./Sections/SourceSection";
import { TargetSection } from "./Sections/TargetSection";
import { TipsSection } from "./Sections/TipsSection";
import { VolumeSectionForTransfer } from "./Sections/VolumeSectionForTransfer";
import { useSelector } from "react-redux";

const TransferStep = ({
  definition,
  handleSaveStep,
  platformsNames,
  getContentOptions,
  getCountContentSelected,
  contentOptions,
  contentAmount,
}) => {
  const { allPlatforms, tips, buckets, sources } = platformsNames;
  const { workspace } = useSelector((state) => state.workspaceSlice);

  /** Data for Tips Section */
  const behavior = [
    {
      value: "reuse",
      label: "Always re-use",
    },
    {
      value: "isolated_source_only",
      label: "Avoid target contamination",
    },
    {
      value: "isolated_targets",
      label: "Avoid source contamination",
    },
    {
      value: "reuse_same_source",
      label: "Re-use only for same source",
    },
  ];

  const behaviorDescription = {
    reuse: "(Will always re-use the tip in this step)",
    isolated_source_only: "(Will change the tip after touching a target)",
    isolated_targets:
      "(Will change the tip before touching each a source. May touch several targets with the same tip)",
    reuse_same_source:
      "(Will re-use the tip to load multiple times from the same source if target is the same. Will change the tip for different targets)",
  };

  // Save the updated definitions
  const handleSaveDefinition = (property, value) => {
    const newDefinition = {
      ...definition,
      [property]: value,
    };
    handleSaveStep(newDefinition);
  };

  // Get all tags from the selected target item
  const tagOptions = useMemo(
    () => getContentOptions(definition.target.item, "tag"),
    [definition.target.item, workspace.items]
  );

  const volumeForEachTargetTagAmount = useMemo(
    () =>
      getCountContentSelected(
        definition.target.item,
        "tag",
        definition.volume.tag
      ),
    [definition.target.item, definition.volume.tag, workspace.items]
  );

  return (
    <Stack spacing={3}>
      <SourceSection
        definition={definition.source}
        handleSaveDefinition={handleSaveDefinition}
        platformNames={sources || []}
        getContentOptions={getContentOptions}
        getCountContentSelected={getCountContentSelected}
      />
      <TargetSection
        definition={definition.target}
        handleSaveDefinition={handleSaveDefinition}
        platformNames={sources || []}
        contentOptions={contentOptions}
        contentAmount={contentAmount}
      />
      <VolumeSectionForTransfer
        definition={definition.volume}
        handleSaveDefinition={handleSaveDefinition}
        tagOptions={tagOptions}
        contentAmount={contentAmount}
        volumeForEachTargetTagAmount={volumeForEachTargetTagAmount}
      />
      <TipsSection
        definition={definition.tip}
        toolDefinition={definition.tool}
        handleSaveDefinition={handleSaveDefinition}
        behavior={behavior}
        behaviorDescription={behaviorDescription}
        tipNames={tips}
        bucketNames={buckets}
      />
    </Stack>
  );
};

export default memo(TransferStep);
