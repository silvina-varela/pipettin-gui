import { useEffect, useState } from "react";
import { Typography, TextField, Box } from "@mui/material";

export const CommentStep = ({ definition, handleSaveStep }) => {
  const [text, setText] = useState("");

  useEffect(() => {
    setText(definition.text)
  }, [definition.text]);

  const handleOnBlur = () => {
    handleSaveStep({text: text})
  }

  const handleChange = (event) => {
    setText(event.target.value)
  }

  return (
    <>
      <Typography variant={"caption"} sx={{whiteSpace: "pre-line",}}>
        This step is just a comment in the Intermediate Level Protocol. And will
        be ignored.
      </Typography>
      <Box mt={2}>
    
      <TextField
        multiline
        fullWidth
        rows={4}
        placeholder="Write comment..."
        onChange={handleChange}
        onBlur={handleOnBlur}
        value={text}
        inputProps={{style: {fontSize: 14}}} 
      />
      </Box>
    </>
  );
};
