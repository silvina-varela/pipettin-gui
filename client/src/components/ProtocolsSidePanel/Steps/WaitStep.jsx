import { useEffect, useState, memo } from "react";
import { TextField, Typography } from "@mui/material";

const WaitStep = ({ definition, handleSaveStep }) => {
  const [time, setTime] = useState(0);

  useEffect(() => {
    setTime(definition.seconds);
  }, [definition.seconds]);

  const handleOnBlur = () => {
    handleSaveStep({ seconds: time });
  };

  return (
    <>
      <Typography variant={"caption"} mr={1}>
        Wait for{" "}
      </Typography>
      <TextField
        type={"number"}
        variant="standard"
        sx={{ maxWidth: 50 }}
        size="small"
        value={time}
        inputProps={{
          min: 0,
          style: { fontSize: "13px" },
        }}
        onChange={(event) => setTime(event.target.value)}
        onBlur={handleOnBlur}
      />
      <Typography variant={"caption"} ml={1}>
        {" "}
        seconds
      </Typography>
    </>
  );
};

export default memo(WaitStep);
