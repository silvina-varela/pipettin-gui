import { Stack } from "@mui/material";
import { TargetSection } from "./Sections/TargetSection";
import { TipsSection } from "./Sections/TipsSection";
import { VolumeSectionForMix } from "./Sections/VolumeSectionForMix";

export const MixStep = ({
  definition,
  handleSaveStep,
  platformsNames,
  contentOptions,
  contentAmount,
}) => {
  const { allPlatforms, tips, buckets } = platformsNames;
 
  const behavior = [
    {
      value: "reuse",
      label: "Always re-use",
    },
    {
      value: "isolated_source_only",
      label: "Avoid target contamination",
    },
  ];

  const behaviorDescription = {
    reuse:
      "(Will always re-use the tip, mixing multiple targets during this step)",
    isolated_source_only: "(Will change the tip after touching a target)",
  };

  const handleSaveDefinition = (property, value) => {
    const newDefinition = {
      ...definition,
      [property]: value,
    };
    handleSaveStep(newDefinition);
  };

  return (
    <Stack spacing={3}>
      <TargetSection
        definition={definition.target}
        handleSaveDefinition={handleSaveDefinition}
        platformNames={allPlatforms || []}
        contentOptions={contentOptions}
        contentAmount={contentAmount}
      />
      <VolumeSectionForMix
        definition={definition.mix}
        handleSaveDefinition={handleSaveDefinition}
      />
      <TipsSection
        definition={definition.tip}
        toolDefinition={definition.tool}
        handleSaveDefinition={handleSaveDefinition}
        behavior={behavior}
        behaviorDescription={behaviorDescription}
        tipNames={tips}
        bucketNames={buckets}
      />
    </Stack>
  );
};
