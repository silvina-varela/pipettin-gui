/* eslint-disable react/no-unescaped-entities */
import { useEffect, useRef, useState } from "react";
import {
  Box,
  Button,
  CircularProgress,
  IconButton,
  Tooltip,
  Typography,
} from "@mui/material";
import Editor from "@monaco-editor/react";
import RestartAltIcon from "@mui/icons-material/RestartAlt";
import FileUploadIcon from "@mui/icons-material/FileUpload";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import LightModeIcon from "@mui/icons-material/LightMode";
import HelpOutlineIcon from "@mui/icons-material/HelpOutline";
import { Alert } from "../../../Dialogs/Alert";

export const JsonEditorSection = ({
  setOpenJsonEditor,
  handleSaveStep,
  definition,
}) => {
  const [jsonValue, setJsonValue] = useState(JSON.stringify({}, null, 2));
  const [validation, setValidation] = useState([]);
  const [editorTheme, setEditorTheme] = useState(true);
  const [openHelp, setOpenHelp] = useState(false);

  useEffect(() => {
    if (definition) setJsonValue(JSON.stringify(definition, null, 2));
  }, [definition]);

  useEffect(() => console.log({ validation }), [validation]);

  const handleEditorTheme = (event) => {
    event.preventDefault();
    setEditorTheme(!editorTheme);
  };

  const inputRef = useRef(null);

  const handleJsonImport = (event) => {
    event.preventDefault();
    inputRef.current.click();
  };

  const handleFileUpload = (event) => {
    const file = event.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.onload = (e) => {
        try {
          const parsedData = JSON.parse(e.target.result);
          setJsonValue(JSON.stringify(parsedData, null, 2));
        } catch (error) {
          console.error("Error parsing JSON file:", error);
          setValidation(error.message);
        }
      };

      reader.readAsText(file);
    }
  };

  const handleReset = (event) => {
    event.preventDefault();
    setJsonValue(JSON.stringify({}, null, 2));
  };

  const handleClickHelpButton = (event) => {
    event.preventDefault();
    setOpenHelp(true);
  };

  const handleUpdateForm = (values) => {
    try {
      setJsonValue(values);
    } catch (error) {
      setValidation(error);
    }
  };

  const validationText = () => {
    if (validation[0].message?.length) {
      if (Object.prototype.hasOwnProperty.call(validation[0], "endLineNumber")) {
        return `There is an error on line ${validation[0].endLineNumber}: ${validation[0].message}`;
      } else {
        return validation[0].message;
      }
    } else return "";
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    try {
      console.log({ jsonValue });
      if (!validation?.length) {
        if (!jsonValue?.length) {
          handleSaveStep({});
        } else {
          handleSaveStep(JSON.parse(jsonValue));
        }
        setOpenJsonEditor(false);
      }
    } catch (error) {
      console.error(error);
      if (!validation.length) {
        setValidation([{ message: error.message }]);
      }
    }
  };

  return (
    <>
      <input
        style={{ display: "none" }}
        ref={inputRef}
        type="file"
        value=""
        name="files"
        accept=".json"
        onChange={handleFileUpload}
      />
      <Alert
        open={openHelp}
        onClose={() => setOpenHelp(false)}
        yesMessage=""
        noMessage=""
        message={
          <>
            <Typography variant="body1" paragraph>
              You can create or import a JSON file to add a custom step.
            </Typography>
            <Typography variant="body1" paragraph>
              This step will be transformed into a 'JSON' command for the robot,
              unless you explicitly specify a 'cmd' property in your JSON.
            </Typography>
            <Typography variant="body1" paragraph>
              Additionally, you can include a 'description' property in the
              JSON, which will be displayed in the protocol step details. If the
              JSON is a list, only the description of the first element will be
              displayed.
            </Typography>
          </>
        }
      />

      <Box mb={1} display={"flex"} justifyContent={"space-between"}>
        <Box>
          <Tooltip title="Help">
            <IconButton onClick={handleClickHelpButton}>
              <HelpOutlineIcon fontSize="small" />
            </IconButton>
          </Tooltip>
        </Box>
        <Box>
          <Tooltip title="Import">
            <IconButton onClick={handleJsonImport} id="import-template-button">
              <FileUploadIcon fontSize="small" />
            </IconButton>
          </Tooltip>
          <Tooltip title="Reset">
            <IconButton onClick={handleReset} id="reset-template-button">
              <RestartAltIcon fontSize="small" />
            </IconButton>
          </Tooltip>
          <Tooltip title="Theme">
            <IconButton onClick={handleEditorTheme} id="editor-theme-button">
              {!editorTheme ? (
                <LightModeIcon fontSize="small" />
              ) : (
                <DarkModeIcon fontSize="small" />
              )}
            </IconButton>
          </Tooltip>
        </Box>
      </Box>
      <Box>
        <Editor
          height="50vh"
          width="100%"
          defaultLanguage="json"
          value={jsonValue}
          theme={editorTheme ? undefined : "vs-dark"}
          onChange={(values) => handleUpdateForm(values)}
          onValidate={(values) => setValidation(values)}
          options={{
            scrollbar: {
              verticalScrollbarSize: "6px",
              horizontalScrollbarSize: "6px",
            },
          }}
          loading={<CircularProgress />}
        />
      </Box>
      <Box mt={2} height={validation.length ? "auto" : 24}>
        <Typography color="error" variant="caption">
          {!validation.length ? " " : validationText()}
        </Typography>
      </Box>
      <Box sx={{ textAlign: "right" }}>
        <Button type="submit" onClick={handleSubmit} variant="contained">
          OK
        </Button>
      </Box>
    </>
  );
};
