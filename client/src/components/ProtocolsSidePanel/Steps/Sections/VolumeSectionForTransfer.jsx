import { useEffect, useMemo, useState } from "react";
import {
  Typography,
  Box,
  Stack,
  TextField,
  InputAdornment,
} from "@mui/material";
import { DefinedSelect } from "../../Inputs/DefinedSelect";
import { InputSelect } from "../../Inputs/InputSelect";

export const VolumeSectionForTransfer = ({
  definition,
  handleSaveDefinition,
  tagOptions,
  contentAmount,
  volumeForEachTargetTagAmount,
}) => {
  const { type, value, tag } = definition;

  const handleChange = (property, value) => {
    handleSaveDefinition("volume", {
      ...definition,
      [property]: value,
    });
  };

  const volumeOptions = [
    { value: "fixed_each", label: "to each target" },
    { value: "fixed_total", label: "in total" },
    { value: "for_each_target_tag", label: "for each" },
  ];

  const getVolumeToEachTarget = () => {
    const round = (num) => Math.round((num + Number.EPSILON) * 100) / 100;
    if (type === "fixed_each") {
      return value;
    } else if (type === "fixed_total") {
      const amount = value / contentAmount || 0;
      return round(amount);
    } else if (type === "for_each_target_tag") {
      const amount = value * volumeForEachTargetTagAmount;
      return round(amount);
    } else return 0;
  };

  const volumeToEachTarget = useMemo(
    () => getVolumeToEachTarget(),
    [value, type, tag, contentAmount, volumeForEachTargetTagAmount]
  );

  const [volumeState, setVolumeState] = useState(0);

  useEffect(() => {
    setVolumeState(value);
  }, [value]);

  const handleOnBlur = () => {
    handleChange("value", volumeState);
  };

  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      e.target.blur();
    }
  }

  return (
    <Box>
      <Typography variant="overline" fontWeight={"bold"}>
        Volume
      </Typography>
      <Stack spacing={2}>
        <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
          <Box sx={{ mr: 1 }}>
            <TextField
              type={"number"}
              variant="standard"
              sx={{ maxWidth: 70 }}
              size="small"
              value={volumeState}
              inputProps={{
                min: 0,
                style: { fontSize: "13px" },
              }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Typography variant="body2" style={{ fontSize: "13px" }}>
                      µl
                    </Typography>
                  </InputAdornment>
                ),
              }}
              onChange={(event) => setVolumeState(event.target.value)}
              onBlur={handleOnBlur}
              onKeyDown={handleKeyDown}
            />
          </Box>
          <Box sx={{ mr: 1 }}>
            <DefinedSelect
              options={volumeOptions}
              handleChange={(value) => handleChange("type", value)}
              value={type}
            />
          </Box>
        </Box>

        {type === "for_each_target_tag" ? (
          <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
            <Box sx={{ mr: 1 }}>
              <Typography
                variant={"caption"}
                sx={{ mr: 1 }}
                color="grey"
                fontWeight="bold"
              >
                Content with tag
              </Typography>
            </Box>

            <Box sx={{ mr: 1 }}>
              <InputSelect
                label="Select tag"
                options={tagOptions}
                handleChange={(value) => handleChange("tag", value)}
                value={tag}
              />
            </Box>
          </Box>
        ) : null}
        <Box>
          <Typography variant={"caption"} sx={{ fontSize: 9 }}>
            {volumeToEachTarget} µl to each target
          </Typography>
        </Box>
      </Stack>
    </Box>
  );
};
