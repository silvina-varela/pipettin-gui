import { Typography, Box, Stack } from "@mui/material";
import { DefinedSelect } from "../../Inputs/DefinedSelect";
import { InputSelect } from "../../Inputs/InputSelect";
import { useSelector } from "react-redux";

export const TipsSection = ({
  definition,
  toolDefinition,
  handleSaveDefinition,
  behavior,
  behaviorDescription,
  tipNames,
  bucketNames,
}) => {
  const { item, discardItem, mode } = definition;
  const { tools } = useSelector((state) => state.toolSlice);

  // Save changes
  const handleChange = (property, value) => {
    if (property === "tool") {
      // Saving the tool name is a special case (or hack).
      // It is in the "tip" section, but ends up in the
      // root of the step definition (see "defaultStepDefinitions").
      handleSaveDefinition("tool", value);
    } else {
      handleSaveDefinition("tip", {
        ...definition,
        [property]: value,
      });
    }
  };

  // Offer trash options only if the pipette can actually eject.
  const currentTool = tools.find(t => t.name === toolDefinition);
  const can_eject = currentTool?.parameters?.can_eject === true
  const trashOptions = can_eject ? bucketNames : ["None"];
  
  const toolNames = tools
    .filter((tool) => tool.type == "Micropipette")
    .filter((tool) => tool?.enabled === undefined || tool.enabled)
    .map((tool) => tool.name);

  return (
    <Box>
      <Typography variant="overline" fontWeight="bold">
        Tips
      </Typography>
      <Stack spacing={2}>
        <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
          <Box>
            <Typography
              variant={"caption"}
              sx={{ mr: 1 }}
              fontWeight="bold"
              color="grey"
            >
              Tip rack
            </Typography>
          </Box>
          <Box sx={{ mr: 1 }} width={"60%"}>
            <InputSelect
              label="Select tip rack"
              options={tipNames}
              handleChange={(value) => handleChange("item", value)}
              value={item}
            />
          </Box>
        </Box>
        <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
          <Box>
            <Typography
              variant={"caption"}
              sx={{ mr: 1 }}
              fontWeight="bold"
              color="grey"
            >
              Tool
            </Typography>
          </Box>
          <Box sx={{ mr: 1 }} width={"60%"}>
            <InputSelect
              label="Select tool"
              options={toolNames}
              handleChange={(value) => handleChange("tool", value)}
              value={toolDefinition}
            />
          </Box>
        </Box>
        <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
          <Box>
            <Typography
              variant={"caption"}
              sx={{ mr: 1 }}
              fontWeight="bold"
              color="grey"
            >
              Discard item
            </Typography>
          </Box>
          <Box sx={{ mr: 1 }} width={"60%"}>
            <InputSelect
              label="Select bucket"
              options={trashOptions}
              handleChange={(value) => handleChange("discardItem", value)}
              value={discardItem}
            />
          </Box>
        </Box>

        <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
          <Box>
            <Typography
              variant={"caption"}
              sx={{ mr: 1 }}
              fontWeight="bold"
              color="grey"
            >
              Behavior
            </Typography>
          </Box>
          <Box sx={{ mr: 1 }} width={"60%"}>
            <DefinedSelect
              options={behavior}
              handleChange={(value) => handleChange("mode", value)}
              value={mode}
            />
          </Box>
          <Typography
            variant={"caption"}
            sx={{
              fontSize: 9,
              display: "inline-block",
              whiteSpace: "pre-line",
            }}
            color="grey"
          >
            {behaviorDescription[mode]}
          </Typography>
        </Box>
      </Stack>
    </Box>
  );
};
