import { useMemo } from "react";
import { useSelector } from "react-redux";
import { Typography, Box, Stack } from "@mui/material";
import { InputSelect } from "../../Inputs/InputSelect";
import { Toggle } from "../../Inputs/Toggle";
import { DefinedSelect } from "../../Inputs/DefinedSelect";

export const SourceSection = ({
  definition,
  handleSaveDefinition,
  platformNames,
  getContentOptions,
  getCountContentSelected,
}) => {
  const { workspace } = useSelector((state) => state.workspaceSlice);
  const { item, by, value, treatAs } = definition;

  const contentOptions = useMemo(
    () => getContentOptions(item, by),
    [by, item, workspace.items]
  );

  const contentAmount = useMemo(
    () => getCountContentSelected(item, by, value),
    [by, value, workspace.items]
  );

  //   Defines the selection options of behavior
  const multipleSelectContent = [
    { value: "same", label: "Treat all like unique sources" },
    { value: "for_each", label: "Iterate this step for each source" },
  ];

  // Save changes
  const handleChange = (property, value) => {
    if (property === "item" || property === "by") {
      handleSaveDefinition("source", {
        ...definition,
        [property]: value,
        value: "",
      });
    } else {
      handleSaveDefinition("source", {
        ...definition,
        [property]: value,
      });
    }
  };

  return (
    <Box>
      <Typography variant="overline" fontWeight="bold">
        Source
      </Typography>
      <Stack spacing={2}>
        <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
          <Box>
            <Typography
              variant={"caption"}
              sx={{ mr: 1 }}
              fontWeight="bold"
              color="grey"
            >
              Platform
            </Typography>
          </Box>
          <Box sx={{ mr: 1 }} width={"60%"}>
            <InputSelect
              options={platformNames}
              value={item}
              handleChange={(value) => handleChange("item", value)}
            />
          </Box>
        </Box>
        <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
          <Box>
            <Typography
              variant={"caption"}
              sx={{ mr: 1 }}
              fontWeight="bold"
              color="grey"
            >
              Select by
            </Typography>
          </Box>
          <Box sx={{ mr: 1 }}>
            <Toggle
              option1="name"
              option2="tag"
              toggleValue={by}
              handleChange={(value) => handleChange("by", value)}
            />
          </Box>
        </Box>
        <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
          <Typography
            variant={"caption"}
            sx={{ mr: 1 }}
            fontWeight="bold"
            color="grey"
          >
            Content with {by}
          </Typography>
          <Box sx={{ mr: 1 }} width={"50%"}>
            <InputSelect
              label={`Select ${by}`}
              options={contentOptions}
              value={value}
              handleChange={(value) => handleChange("value", value)}
            />
          </Box>
          {contentAmount > 1 ? (
            <Box display="flex" sx={{ mt: 2 }} flexDirection="column">
              <Typography variant={"caption"} sx={{ color: "red" }}>
                Multiple sources behaviour:
              </Typography>
              <DefinedSelect
                options={multipleSelectContent}
                handleChange={(value) => handleChange("treatAs", value)}
                value={treatAs}
              />
            </Box>
          ) : null}
        </Box>
      </Stack>
    </Box>
  );
};
