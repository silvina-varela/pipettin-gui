import { Typography, Box, Stack } from "@mui/material";
import { InputSelect } from "../../Inputs/InputSelect";
import { Toggle } from "../../Inputs/Toggle";

export const TargetSection = ({
  definition,
  handleSaveDefinition,
  platformNames,
  contentAmount,
  contentOptions,
}) => {

  const { item, by, value } = definition;
 
  // Save changes
  const handleChange = (property, value) => {
    if (property === "item" || property === "by") {
      handleSaveDefinition("target", {
        ...definition,
        [property]: value,
        value: "",
      });
    } else {
      handleSaveDefinition("target", {
        ...definition,
        [property]: value,
      });
    }
  };

  return (
    <Box>
      <Typography variant="overline" fontWeight="bold">
        Target
      </Typography>
      <Stack spacing={2}>
        <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
          <Box>
            <Typography
              variant={"caption"}
              sx={{ mr: 1 }}
              fontWeight="bold"
              color="grey"
            >
              Platform
            </Typography>
          </Box>
          <Box sx={{ mr: 1 }} width={"60%"}>
              <InputSelect
                label="Select platform"
                options={platformNames}
                value={item}
                handleChange={(value) => handleChange("item", value)}
              />
          </Box>
        </Box>
        <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
          <Box>
            <Typography
              variant={"caption"}
              sx={{ mr: 1 }}
              fontWeight="bold"
              color="grey"
            >
              Select by
            </Typography>
          </Box>
          <Box sx={{ mr: 1 }}>
            <Toggle
              option1="name"
              option2="tag"
              toggleValue={by}
              handleChange={(value) => handleChange("by", value)}
            />
          </Box>
        </Box>
        <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
          <Typography
            variant={"caption"}
            sx={{ mr: 1 }}
            fontWeight="bold"
            color="grey"
          >
            Content with {by}
          </Typography>
          <Box sx={{ mr: 1 }} width={"50%"}>
              <InputSelect
                label={`Select ${by}`}
                options={contentOptions}
                value={value}
                handleChange={(value) => handleChange("value", value)}
              />           
          </Box>
          <Box>
            <Typography variant={"caption"} sx={{ fontSize: 9 }}>
              Selected item(s): {contentAmount}
            </Typography>
          </Box>
        </Box>
      </Stack>
    </Box>
  );
};
