import { useEffect, useState } from "react";
import { Typography, Box, Stack, TextField, InputAdornment } from "@mui/material";
import { DefinedSelect } from "../../Inputs/DefinedSelect";

export const VolumeSectionForMix = ({ definition, handleSaveDefinition }) => {

  const { type, percentage, count } = definition;

  const handleChange = (property, value) => {
      handleSaveDefinition("mix", {
        ...definition,
        [property]: value,
      });
  }; 
  
  const mixTypes = [
    { value: "content", label: "content volume" },
    { value: "tip", label: "tip volume" },
  ];
  
  const [percentageState, setPercentageState] = useState(0);

  useEffect(() => {
    setPercentageState(percentage);
  }, [percentage]);

  const handleOnBlur = () => {
    handleChange("percentage", percentageState);
  };

  const [countState, setCountState] = useState(0);

  useEffect(() => {
    setCountState(count);
  }, [count]);

  const handleOnBlurCount = () => {
    handleChange("count", countState);
  };

  return (
    <Box>
      <Typography variant="overline" fontWeight={"bold"} >
        Mix
      </Typography>
      <Stack spacing={2}>
      <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
        <Box sx={{mr: 1 }}>
            <TextField
              type={"number"}
              variant="standard"
              sx={{ maxWidth: 70 }}
              size="small"
              value={percentageState}
              inputProps={{
                min: 0,
                max: 100,
                style: { fontSize: "13px" },
              }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Typography variant="body2" style={{ fontSize: "13px" }}>
                    %
                    </Typography>
                  </InputAdornment>
                ),
              }}
              onChange={(event) => setPercentageState(event.target.value)}
              onBlur={handleOnBlur}
            />
        </Box>
        <Box>
          <Typography variant={"caption"} sx={{ mr: 1 }} color="grey" fontWeight="bold">of</Typography>
        </Box>
        {/* Types of mixing */}
        <Box sx={{mr: 1 }}>
          <DefinedSelect
              options={mixTypes}
              handleChange={(value) => handleChange("type", value)}
              value={type}
          />
        </Box>
      </Box>
      <Box display="flex" alignItems="flex-end" flexWrap={"wrap"}>
     
      <Box sx={{mr: 1 }}>
        <TextField
              type={"number"}
              variant="standard"
              sx={{ maxWidth: 70 }}
              size="small"
              value={countState}
              inputProps={{
                min: 0,
                style: { fontSize: "13px" },
              }}
              onChange={(event) => setCountState(event.target.value)}
              onBlur={handleOnBlurCount}
            />
      </Box>
      <Typography variant={"caption"} color="grey" sx={{ mr: 1 }} fontWeight="bold">time(s)</Typography>
     </Box> 
      </Stack>
    </Box>
  );
};
