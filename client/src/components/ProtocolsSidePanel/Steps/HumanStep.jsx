import { useEffect, useState } from "react";
import { Typography, TextField, Box } from "@mui/material";

export const HumanStep = ({ definition, handleSaveStep }) => {
  const [text, setText] = useState("");

  useEffect(() => {
    setText(definition.text)
  }, [definition.text])

  const handleOnBlur = () => {
    handleSaveStep({text: text})
  }

  const handleChange = (event) => {
    setText(event.target.value)
  }

  return (
    <>
      <Typography variant={"caption"} sx={{whiteSpace: "pre-line",}}>
      This step will pause the tool and show the following instructions. It will wait for a confirmation to continue with the next step.
      </Typography>
      <Box mt={2}>    
      <TextField
        multiline
        fullWidth
        rows={4}
        placeholder="Write instructions..."
        onChange={handleChange}
        onBlur={handleOnBlur}
        value={text}
        inputProps={{style: {fontSize: 14}}} 
      />
      </Box>
    </>
  );
};
