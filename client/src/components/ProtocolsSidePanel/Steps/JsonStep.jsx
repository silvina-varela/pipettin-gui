import { useState } from "react";
import { Typography, Box, Button, Stack } from "@mui/material";
import { Popup } from "../../Dialogs/Popup";
import { JsonEditorSection } from "./Sections/JsonEditorSection";
import EditIcon from "@mui/icons-material/Edit";
import AddIcon from "@mui/icons-material/Add";

export const JsonStep = ({ definition, handleSaveStep }) => {
  const [openJsonEditor, setOpenJsonEditor] = useState(false);

  const openJson = () => {
    setOpenJsonEditor(true);
  };

  return (
    <>
      <Stack direction={"column"} spacing={2}>
        <Box maxHeight={100} overflow={"scroll"}>
          <Typography variant={"caption"} sx={{ whiteSpace: "pre-line" }}>
            {definition &&
              (Array.isArray(definition)
                ? definition[0].description
                : definition?.description)}
          </Typography>
        </Box>
        {!definition ? (
          <Button onClick={openJson} variant="outlined" startIcon={<AddIcon />} size="small">
            Add json
          </Button>
        ) : (
          <Button
            onClick={openJson}
            variant="outlined"
            startIcon={<EditIcon />}
            size="small"
          >
            Edit json
          </Button>
        )}
      </Stack>
      <Popup
        title={"JSON editor"}
        openPopup={openJsonEditor}
        setOpenPopup={setOpenJsonEditor}
        width="lg"
        component={
          <JsonEditorSection
            handleSaveStep={handleSaveStep}
            definition={definition}
            setOpenJsonEditor={setOpenJsonEditor}
          />
        }
      />
    </>
  );
};
