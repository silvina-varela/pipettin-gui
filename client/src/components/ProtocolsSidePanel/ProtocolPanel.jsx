import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Typography, Box, IconButton, Tooltip } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { ProtocolList } from "./ProtocolList";
import { ProtocolDetail } from "./ProtocolDetail";
import { ProtocolForm } from "./ProtocolForm";
import { Popup } from "../Dialogs/Popup";
import { Alert } from "../Dialogs/Alert";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { useSnackAlert } from "../../hooks/useSnackAlert";
import { GenericImport } from "../NavBar/GenericImport";
import {
  getProtocolByIdAsync,
  getWorkspaceProtocolsAsync,
  saveProtocolAsync,
} from "../../redux/slices";
import { createSelector } from 'reselect';

const selectWorkspaceId = createSelector(
  (state) => {
    // console.log("INPUT")
    // console.log(state.workspaceSlice)
    return state.workspaceSlice.workspace;
  },
  (workspace) => {
    // console.log("OUTPUT")
    // console.log(workspace)
    return workspace._id;
  }
);

export const ProtocolPanel = ({
  openDrawer,
  handleDrawerClose,
  handleDrawerOpen,
}) => {
  const dispatch = useDispatch();

  const addSnack = useSnackAlert();

  const [popup, setPopup] = useState({
    open: false,
    title: "",
    component: "",
  });

  const handleClickAddNew = (e) => {
    e.preventDefault();

    setPopup({
      open: true,
      title: "Create New Protocol",
      component: (
        <ProtocolForm
          openSelectedProtocol={() => setOpenProtocol(true)}
          handleCloseForm={() =>
            setPopup({
              open: false,
              title: "",
              component: "",
            })
          }
        />
      ),
    });
  };

  const handleClickImport = (e) => {
    e.preventDefault();
    setPopup({
      open: true,
      title: "Import protocol",
      component: (
        <GenericImport
          closePopup={() =>
            setPopup({
              open: false,
              title: "",
              component: "",
            })
          }
          type="protocols"
        />
      ),
    });
  };

  const { protocol, protocolUpdated } = useSelector(
    (state) => state.protocolSlice
  );
  const workspace_id = useSelector(selectWorkspaceId);

  const [openProtocol, setOpenProtocol] = useState(false);

  useEffect(() => {
    setOpenProtocol(Boolean(Object.keys(protocol).length));
  }, [Object.keys(protocol).length]);

  const [alert, setAlert] = useState({
    open: false,
    title: "",
    message: "",
    onConfirm: () => {},
    onCancel: () => {},
    onClose: () => {},
    yesMessage: "",
    type: "",
  });

  const handleSelectProtocol = async (event, protocol) => {
    event.preventDefault();
    setOpenProtocol(true);
    dispatch(getProtocolByIdAsync(protocol._id))
      .unwrap()
      .catch((error) => {
        setOpenProtocol(false);
        console.log(error);
      });
  };

  const handleCloseProtocol = async () => {
    dispatch(getWorkspaceProtocolsAsync(workspace_id));
    setOpenProtocol(false);
    setAlert({ open: false });
  };

  const saveCurrentProtocol = async () => {
    try {
      await dispatch(saveProtocolAsync({ values: protocol })).then(() =>
        addSnack("Protocol saved", "success")
      );
    } catch (error) {
      console.log(error);
    }
  };

  const handleGoBackArrow = (event) => {
    event.preventDefault();

    if (protocolUpdated) {
      setAlert({
        open: true,
        title: "Close protocol",
        message: "Do you want to save this protocol before closing it?",
        onConfirm: () =>
          saveCurrentProtocol().then(() => handleCloseProtocol()),
        onCancel: () => handleCloseProtocol(),
        onClose: () => setAlert({ open: false }),
        yesMessage: "Yes",
        noMessage: "No",
      });
    } else {
      handleCloseProtocol();
    }
  };

  return (
    <>
      <Box
        sx={{
          alignItems: "center",
          display: "flex",
          zIndex: 1,
          backgroundColor: openDrawer && "lightgrey",
          padding: 1,
        }}
      >
        <Box position={"absolute"} right={openDrawer ? 0 : 10}>
          <Tooltip
            title={
              openDrawer ? "Collapse protocol panel" : "Expand protocol panel"
            }
          >
            <IconButton
              onClick={
                openDrawer
                  ? (e) => handleDrawerClose(e, "right")
                  : (e) => handleDrawerOpen(e, "right")
              }
            >
              {openDrawer ? <ChevronRightIcon /> : <ChevronLeftIcon />}
            </IconButton>
          </Tooltip>
        </Box>
        <Typography
          variant="h6"
          sx={{
            flexGrow: 1,
            textAlign: "center",
            color: "#252525",
            userSelect: "none",
            visibility: !openDrawer && "hidden",
          }}
        >
          {openProtocol ? "Protocol" : "Protocols"}
        </Typography>
        {openProtocol ? (
          <IconButton
            onClick={handleGoBackArrow}
            sx={{
              flex: "0 1 auto",
              marginRight: "auto",
              position: "absolute",
              zIndex: 10,
              display: !openDrawer && "none",
            }}
          >
            <ArrowBackIcon fontSize="small" aria-label="back" />
          </IconButton>
        ) : null}
      </Box>
      {!openProtocol ? (
        <ProtocolList
          handleAddNew={handleClickAddNew}
          handleImport={handleClickImport}
          handleSelectProtocol={handleSelectProtocol}
          setAlert={setAlert}
          openDrawer={openDrawer}
        />
      ) : (
        <ProtocolDetail
          saveProtocol={saveCurrentProtocol}
          openDrawer={openDrawer}
        />
      )}

      <Alert
        title={alert.title}
        open={alert.open}
        message={alert.message}
        onConfirm={alert.onConfirm}
        onCancel={alert.onCancel}
        onClose={alert.onClose}
        yesMessage={alert.yesMessage}
        type={alert.type}
      />

      <Popup
        title={popup.title}
        openPopup={popup.open}
        setOpenPopup={() => setPopup({ open: false, title: "", component: "" })}
        component={popup.component}
      />
    </>
  );
};
