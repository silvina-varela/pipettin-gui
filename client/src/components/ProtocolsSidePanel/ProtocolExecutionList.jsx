import { useEffect } from "react";
import { ListView } from "../List/ListView";
import { useExportData } from "../../hooks/useExportData";
import { useDispatch, useSelector } from "react-redux";
import { getProtocolExecutionListAsync } from "../../redux/slices";

export const ProtocolExecutionList = ({
  hLprotocol,
  workspaceName,
  fileName,
}) => {
  const dispatch = useDispatch();
  const { protocolExecutionList } = useSelector((state) => state.protocolSlice);
  const exportData = useExportData();

  useEffect(() => {
    dispatch(getProtocolExecutionListAsync({hLprotocol, workspaceName}));
  }, []);

  const columns = [
    {
      id: "name",
      numeric: false,
      disablePadding: false,
      label: "Name",
      width: 400,
    },
    {
      id: "date",
      numeric: false,
      disablePadding: false,
      label: "Date",
      width: 500,
    },
  ];

  /** Export  executed protocols */
  const handleExport = (protocols) => {
    exportData(protocols, fileName);
  };

  return (
    <ListView
      data={protocolExecutionList}
      handleExport={handleExport}
      columns={columns}
      word="protocols"
      showSearchBar={false}
      orderTableBy="date"
      orderTableDirection="desc"
    />
  );
};
