import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { alpha } from "@mui/material/styles";
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Toolbar,
  Typography,
  Checkbox,
  IconButton,
  Tooltip,
  Grid,
  Button,
  Skeleton,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import FileUploadIcon from "@mui/icons-material/FileUpload";
import AddIcon from "@mui/icons-material/Add";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { deleteProtocolAsync } from "../../redux/slices";

export const ProtocolList = ({
  handleSelectProtocol,
  handleAddNew,
  handleImport,
  setAlert,
  openDrawer,
}) => {
  const HeaderButtons = ({ handleAddNew, handleImport }) => {
    return (
      <Grid container direction="row" justifyContent="space-between">
        <Grid item>
          <Button
            onClick={handleImport}
            disableRipple
            color="inherit"
            startIcon={
              <FileUploadIcon
                sx={{ fontSize: "medium", justifyItems: "flex-start" }}
              />
            }
          >
            Import
          </Button>
        </Grid>
        <Grid item>
          <Button
            onClick={handleAddNew}
            color="inherit"
            startIcon={<AddIcon sx={{ fontSize: "medium" }} />}
          >
            Add new
          </Button>
        </Grid>
      </Grid>
    );
  };

  function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  function getComparator(order, orderBy) {
    return order === "desc"
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) {
        return order;
      }
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  function TableToolbar({
    handleDelete,
    numSelected,
    selected,
    setAlert,
    handleAddNew,
    handleImport,
    handleSelectAll,
    protocols,
  }) {
    const alertmessage =
      numSelected === 1
        ? `Are you sure you want to delete the protocol "${selected[0].name}"?`
        : `Are you sure you want to delete the ${numSelected} selected protocols?`;

    const handleDeleteAlert = (e) => {
      e.preventDefault();
      setAlert({
        open: true,
        title: "Delete protocols",
        message: alertmessage + " This process cannot be undone.",
        onConfirm: () => handleDeleteProtocol(e),
        onCancel: () => setAlert({ open: false }),
        onClose: () => setAlert({ open: false }),
        yesMessage: "Delete",
        type: "delete",
      });
    };

    /** Export protocols */
    const exportData = ({ data, fileName, fileType }) => {
      const blob = new Blob([data], { type: fileType });
      const a = document.createElement("a");
      a.download = fileName;
      a.href = window.URL.createObjectURL(blob);
      const clickEvt = new MouseEvent("click", {
        view: window,
        bubbles: true,
        cancelable: true,
      });
      a.dispatchEvent(clickEvt);
      a.remove();
    };

    const handleExport = () => {
      const jsonToExport = selected.map((protocol) => {
        const modifiedProtocol = { ...protocol };
        delete modifiedProtocol._id;
        return modifiedProtocol;
      });

      exportData({
        data: JSON.stringify(jsonToExport),
        fileName: "Protocols.json",
        fileType: "text/json",
      });
    };

    const handleDeleteProtocol = async (event) => {
      event.preventDefault();
      await handleDelete();
      setAlert({ open: false });
    };

    return (
      <Toolbar
        sx={{
          pl: { sm: 1 },
          pr: { xs: 1, sm: 1 },
          ...(numSelected > 0 && {
            bgcolor: (theme) =>
              alpha(
                theme.palette.primary.main,
                theme.palette.action.activatedOpacity
              ),
          }),
          display: !openDrawer && "none",
        }}
      >
        {numSelected > 0 ? (
          <>
            <Tooltip
              title={
                protocols.length > selected.length
                  ? "Select all"
                  : "Deselect all"
              }
              // sx={{height: "7vh"}}
            >
              <Checkbox
                size="small"
                checked={protocols.length === selected.length}
                onChange={handleSelectAll}
                indeterminate={protocols.length > selected.length}
              />
            </Tooltip>
            <Typography
              sx={{ flex: "1 1 100%", fontSize: "12px" }}
              color="inherit"
              variant="subtitle1"
              component="div"
            >
              {numSelected} Selected
            </Typography>
          </>
        ) : (
          <HeaderButtons
            handleAddNew={handleAddNew}
            handleImport={handleImport}
          />
        )}
        {numSelected > 0 ? (
          <Tooltip title="Delete">
            <IconButton onClick={handleDeleteAlert} aria-label="delete">
              <DeleteIcon size="small" sx={{ width: 16, height: 16 }} />
            </IconButton>
          </Tooltip>
        ) : null}

        {numSelected > 0 ? (
          <Tooltip title="Export">
            <IconButton onClick={handleExport}>
              <FileDownloadIcon size="small" sx={{ width: 16, height: 16 }} />
            </IconButton>
          </Tooltip>
        ) : null}
      </Toolbar>
    );
  }

  const dispatch = useDispatch();

  const { protocols, loadingProtocols } = useSelector(
    (state) => state.protocolSlice
  );

  /** Checkbox */
  const [selected, setSelected] = useState([]);

  const handleCheck = (event, protocol) => {
    event.preventDefault();

    let selectedIndex = selected.findIndex((i) => i._id === protocol._id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, protocol);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleSelectAll = (event) => {
    if (event.target.checked) {
      const newSelected = protocols.map((n) => n);
      setSelected(newSelected);
      return;
    }
    setSelected([]);
  };

  const isSelected = (protocol) =>
    selected.findIndex((i) => i._id === protocol._id) !== -1;

  /** Delete selected protocols */
  const handleDeleteProtocol = async () => {
    try {
      await Promise.all(
        selected.map((element) => dispatch(deleteProtocolAsync(element._id)))
      )
        .then(() => setSelected([]));
    } catch (error) {
      console.log("Error:", error);
    }
  };

  if (loadingProtocols)
    // Load skeleton
    return (
      <>
        <Grid
          container
          pt={3}
          px={1}
          direction="row"
          justifyContent="space-between"
        >
          <Grid item>
            <Skeleton variant="rectangular" height={35} width={100} />
          </Grid>
          <Grid item>
            <Skeleton variant="rectangular" height={35} width={100} />
          </Grid>
        </Grid>
        <Skeleton
          sx={{ position: "relative", marginTop: 10, marginX: 1 }}
          variant="rectangular"
          height={30}
        />
        <Skeleton
          sx={{ position: "relative", marginTop: 1, marginX: 1 }}
          variant="rectangular"
          height={30}
        />
        <Skeleton
          sx={{ position: "relative", marginTop: 1, marginX: 1 }}
          variant="rectangular"
          height={30}
        />
        <Skeleton
          sx={{ position: "relative", marginTop: 1, marginX: 1 }}
          variant="rectangular"
          height={30}
        />
      </>
    );
  // Load protocols
  else
    return (
      <>
        <TableToolbar
          numSelected={selected.length}
          selected={selected}
          handleDelete={handleDeleteProtocol}
          setAlert={setAlert}
          handleAddNew={handleAddNew}
          handleImport={handleImport}
          handleSelectAll={handleSelectAll}
          protocols={protocols}
        />
        {!protocols?.length ? (
          <Box m={2} display={!openDrawer && "none"}>
            <Typography
              variant="body1"
              color="#333333"
              mt={4}
              sx={{ userSelect: "none", whiteSpace: "pre-line" }}
            >
              There are no protocols associated with this workspace
            </Typography>
          </Box>
        ) : (
          <Box
            sx={{
              overflowY: "auto",
              display: !openDrawer && "none",
            }}
          >
            <TableContainer>
              <Table>
                <TableBody>
                  {stableSort(protocols, getComparator("asc", "name")).map(
                    (protocol, index) => {
                      const isItemSelected = isSelected(protocol);
                      const labelId = `enhanced-table-checkbox-${index}`;

                      return (
                        <TableRow
                          key={index}
                          hover
                          role="checkbox"
                          aria-checked={isItemSelected}
                          tabIndex={-1}
                          selected={isItemSelected}
                        >
                          <TableCell padding="checkbox">
                            <Checkbox
                              size="small"
                              checked={isItemSelected}
                              onClick={(event) => handleCheck(event, protocol)}
                              inputProps={{
                                "aria-labelledby": labelId,
                              }}
                            />
                          </TableCell>
                          <TableCell
                            onClick={(event) =>
                              handleSelectProtocol(event, protocol)
                            }
                            sx={{
                              fontSize: 12,
                              cursor: "pointer",
                              whiteSpace: "nowrap",
                              overflow: "hidden",
                              textOverflow: "ellipsis",
                              maxWidth: 0,
                            }}
                          >
                            {protocol.name}
                          </TableCell>
                        </TableRow>
                      );
                    }
                  )}
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
        )}
      </>
    );
};
