import { useState, useEffect } from "react";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";

export const DefinedSelectInput = (props) => {
  const {
    field: {
      field_id,
      field_label,
      field_value,
      field_initialValue,
      field_mandatory,
      select_options,
    },
    handleChange,
  } = props;

  const [inputValue, setInputValue] = useState("");

  useEffect(() => {
    if (props.preview) {
      setInputValue(field_initialValue || "");
    } else {
      setInputValue(field_value || "");
    }
  }, [field_value]);

  return (
    <>
      <InputLabel shrink required>
        {field_label}
      </InputLabel>
      <FormControl size="small" fullWidth required>
        <Select
          fullWidth
          required={field_mandatory}
          id={field_id}
          name={field_id}
          value={inputValue}
          onChange={(event) => handleChange(field_id, event.target.value)}
        >
          {select_options.map((option, index) => (
            <MenuItem key={index} value={option.option_label}>
              {option.option_label}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </>
  );
};
