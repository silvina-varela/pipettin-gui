import { FormGroup, FormControlLabel, Checkbox } from "@mui/material";

export const CheckboxLabel = (props) => {
  const {
    field: { field_id, field_label, field_mandatory },
    handleChange,
  } = props;

  return (
    <>
      <FormGroup>
        <FormControlLabel
          required={field_mandatory}
          control={
            <Checkbox
              id={field_id}
              onChange={(event) => handleChange(field_id, event.target.checked)}
            />
          }
          label={field_label}
        />
      </FormGroup>
    </>
  );
};
