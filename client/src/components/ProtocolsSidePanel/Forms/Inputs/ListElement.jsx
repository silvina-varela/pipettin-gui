import { FormInputs } from "../FormInputs";

export const ListElement = ({
  listIndex,
  group,
  listFields,
  setListFields,
  preview,
  isEditMode,
}) => {

  const handleGroupChange = (id, value) => {
    const updatedGroup = group.map((field) =>
      field.field_id === id ? { ...field, field_value: value } : field
    );

    const updatedList = listFields.map((groupItem) =>
      groupItem.id === listIndex
        ? { ...groupItem, fields: updatedGroup }
        : groupItem
    );

    setListFields(updatedList);
  };

  return (
    <>
      {group.map((field, fieldIndex) => (
        <FormInputs
          field={field}
          key={fieldIndex}
          handleChange={handleGroupChange}
          preview={preview}
          isEditMode={isEditMode}
        />
      ))}
    </>
  );
};
