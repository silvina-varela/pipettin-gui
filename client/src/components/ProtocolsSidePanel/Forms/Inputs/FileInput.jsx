import { useState, useEffect, useRef } from "react";
import { TextField, InputLabel, InputAdornment, Button } from "@mui/material";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";

export const FileInput = (props) => {
  const {
    field: {
      field_id,
      field_label,
      field_value,
      field_initialValue,
      field_mandatory,
      accept,
    },
    handleChange,
  } = props;

  const [fileName, setFileName] = useState(field_value || ""); // Initialize with an empty string if undefined
  const fileInputRef = useRef(null);

  useEffect(() => {
    // Set fileName based on preview mode or actual field_value
    if (props.preview) {
      // Use field_initialValue in preview mode
      setFileName(field_initialValue || "");
    } else {
      if (field_value && typeof field_value === 'object' && field_value.name) {
        setFileName(field_value.name); // Use file name if field_value is a file
      } else {
        // setFileName(""); // Clear fileName if no file is selected
        setFileName(field_value || "");  // Original.
      }
    }
  }, [field_value, field_initialValue, props.preview]);

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    setFileName(file ? file.name : ""); // Ensure fileName is always a string
    handleChange(field_id, file);
  };

  const triggerFileInput = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click();
    }
  };

  return (
    <>
      <InputLabel shrink required={field_mandatory}>
        {field_label}
      </InputLabel>
      <input
        id={field_id}
        ref={fileInputRef}
        type="file"
        onChange={handleFileChange}
        accept={accept}
        style={{ display: "none" }}
      />
      <TextField
        value={fileName || ""} // Ensure fileName is always a string
        placeholder="No file selected"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <CloudUploadIcon />
            </InputAdornment>
          ),
          endAdornment: (
            <InputAdornment position="end">
              <Button
                variant="contained"
                color="primary"
                onClick={triggerFileInput}
              >
                Browse...
              </Button>
            </InputAdornment>
          ),
          readOnly: true,
        }}
        fullWidth
        required={field_mandatory}
      />
      {fileName && (
        <p style={{ marginTop: "5px", fontStyle: "italic" }}>
          Selected file: {fileName}
        </p>
      )}
    </>
  );
};
