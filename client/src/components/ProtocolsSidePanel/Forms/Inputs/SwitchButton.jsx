import { FormGroup, FormControlLabel, Switch } from "@mui/material";

export const SwitchLabel = (props) => {
  const {
    field: { field_id, field_label, field_initialValue, field_mandatory },
    handleChange,
  } = props;

  return (
    <>
      <FormGroup>
        <FormControlLabel
          required={field_mandatory}
          control={
            <Switch
              id={field_id}
              defaultChecked={field_initialValue}
              onChange={(event) => handleChange(field_id, event.target.checked)}
            />
          }
          label={field_label}
        />
      </FormGroup>
    </>
  );
};
