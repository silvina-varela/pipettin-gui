import { useEffect, useState } from "react";
import { Box, Grid, Typography, IconButton } from "@mui/material";
import { ListElement } from "./ListElement";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";

export const ListGroup = (props) => {
  const {
    isEditMode,
    field: { field_id, field_label, helperText, fields, items_label },
    field,
    handleChange,
  } = props;

  const [listFields, setListFields] = useState([]);

  const handleAddField = (e) => {
    e.preventDefault();
    const newFieldGroup = { id: Date.now(), fields };
    setListFields((prevFields) => [...prevFields, newFieldGroup]);
  };

  useEffect(() => {
    if (
      field &&
      Object.prototype.hasOwnProperty.call(field, "field_groupValue") &&
      field.field_groupValue.length
    ) {
      setListFields(field.field_groupValue);
    }
  }, [field]);

  const handleDelete = (e, id) => {
    e.preventDefault();
    setListFields((prevFields) =>
      prevFields.filter((fieldGroup) => fieldGroup.id !== id)
    );
  };

  useEffect(() => {
    const listValues = listFields.map((group) => {
      const newGroup = {};
      group.fields.map((field) => {
        newGroup[field.field_id] = field.field_value;
      });
      return newGroup;
    });
    handleChange(field_id, {value: listValues, groupValue: listFields});
  }, [JSON.stringify(listFields)]);

  return (
    <>
      <Box
        sx={{
          display: "flex",
          alignItems: "baseline",
        }}
      >
        <Box>
          <Typography variant="subtitle1" mb={1}>
            {field_label}
          </Typography>
        </Box>
        <Box>
          <IconButton onClick={handleAddField}>
            <AddCircleOutlineIcon />
          </IconButton>
        </Box>
      </Box>
      <Box mb={2}>
        <Typography variant="caption">{helperText}</Typography>
      </Box>
      <Grid display="flex" flexWrap="wrap" flexDirection={"column"}>
        {listFields.map((group, i) => (
          <Box key={group.id}>
            <Box
              key={group.id}
              sx={{
                display: "flex",
                alignItems: "center",
                mb: 1,
              }}
            >
              <Typography variant="subtitle2">
                {items_label} {i + 1}
              </Typography>
              <IconButton
                onClick={(e) => handleDelete(e, group.id)}
                size="small"
              >
                <RemoveCircleOutlineIcon fontSize="inherit" />
              </IconButton>
            </Box>
            <Grid
              container
              sx={{
                display: "flex",
                alignItems: "flex-start",
              }}
            >
              <ListElement
                listIndex={group.id}
                group={group.fields}
                listFields={listFields}
                setListFields={setListFields}
                isEditMode={isEditMode}
              />
            </Grid>
          </Box>
        ))}
      </Grid>
    </>
  );
};
