import { useState, useEffect } from "react";
import {
  TextField,
  InputAdornment,
  InputLabel,
  FormControl,
} from "@mui/material";

export const NumberInput = (props) => {
  const {
    field: {
      field_id,
      startAdornment,
      endAdornment,
      min,
      max,
      field_label,
      field_placeholder,
      helperText,
      field_value,
      field_initialValue,
      field_mandatory,
    },
    handleChange,
  } = props;

  const [inputValue, setInputValue] = useState("");

  useEffect(() => {
    if (props.preview) {
      setInputValue(field_initialValue || "");
    } else {
      // console.log(props.field)
      setInputValue(field_value);
    }
  }, [field_value]);


  // Prevents arrows from changing the input
  const stopKeyArrows = (event) => {
    if (event.code.includes("Arrow")) {
      event.preventDefault();
      event.target.blur();
    }
  };
  return (
    <>
      <InputLabel shrink required>
        {field_label}
      </InputLabel>
      <FormControl size="small" fullWidth>
        <TextField
          id={field_id}
          value={inputValue}
          type="number"
          variant="standard"
          required={field_mandatory}
          placeholder={field_placeholder?.toString()}
          helperText={helperText}
          onChange={(event) =>
            handleChange(field_id, Number(event.target.value))
          }
          InputProps={{
            startAdornment: startAdornment ? (
              <InputAdornment position="start">{startAdornment}</InputAdornment>
            ) : undefined,
            endAdornment: endAdornment ? (
              <InputAdornment position="end">{endAdornment}</InputAdornment>
            ) : undefined,
            inputProps: {
              min: min ? min : undefined,
              max: max ? max : undefined,
              step: "any",
            },
          }}
          onKeyDown={stopKeyArrows}
          onWheel={(event) => {
            event.target.blur();
          }}
        />
      </FormControl>
    </>
  );
};
