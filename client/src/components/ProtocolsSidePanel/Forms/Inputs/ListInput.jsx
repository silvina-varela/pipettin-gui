import { useState, useEffect } from "react";
import { TextField, InputLabel, FormControl } from "@mui/material";

export const ListInput = (props) => {
  const {
    field: {
      field_id,
      field_label,
      field_placeholder,
      field_value,
      field_initialValue,
      field_mandatory,
      helperText,
    },
    handleChange,
  } = props;

  const [inputValue, setInputValue] = useState("");

  useEffect(() => {
    if (props.preview) {
      setInputValue(field_initialValue || "");
    } else {
      setInputValue(field_value);
    }
  }, [field_value]);
  


  return (
    <>
      <InputLabel shrink required>
        {field_label}
      </InputLabel>
      <FormControl size="small" required fullWidth>
        <TextField
          id={field_id}
          required={field_mandatory}
          value={inputValue}
          type="text"
          variant="standard"
          placeholder={field_placeholder}
          helperText={helperText}
          onChange={(event) =>
            handleChange(
              field_id,
              event.target.value.split(",").map((item) => item.trim())
            )
          }
        />
      </FormControl>
    </>
  );
};
