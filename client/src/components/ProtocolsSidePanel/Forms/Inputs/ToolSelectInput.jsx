import { useState, useEffect } from "react";
import {
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import { useSelector } from "react-redux";

export const ToolSelectInput = (props) => {
  const {
    field: { field_id, field_label, field_mandatory, field_value, field_select_by },
    handleChange,
  } = props;

  const [inputValue, setInputValue] = useState("");
  const { tools } = useSelector((state) => state.toolSlice);

  const toolNames = tools
    .filter((tool) => tool.type == field_select_by)
    .map((tool) => tool.name);

    useEffect(() => {
        if (!props.preview) {
          setInputValue(field_value || "");
        }
      }, [field_value, props.preview]);

  return (
    <>
      <InputLabel>{field_label}</InputLabel>
      <Select
        fullWidth
        required={field_mandatory}
        id={field_id}
        name={field_id}
        value={inputValue}
        size="small"
        onChange={(event) => handleChange(field_id, event.target.value)}  // Calls "handleTemplateChange".
      >
        {toolNames.map((option, index) => (
          <MenuItem key={index} value={option}>
            {option}
          </MenuItem>
        ))}
      </Select>
    </>
  );
};
