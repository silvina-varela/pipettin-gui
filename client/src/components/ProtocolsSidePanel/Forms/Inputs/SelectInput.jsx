import { useState, useEffect } from "react";
import {
  FormControl,
  InputLabel,
  ListSubheader,
  MenuItem,
  Select,
} from "@mui/material";
import { useSelector } from "react-redux";

export const SelectInput = (props) => {
  const {
    field: { field_id, field_label, field_mandatory, field_value, field_select_by },
    sections,
    handleChange,
  } = props;

  const [inputValue, setInputValue] = useState("");
  const [optionsFromWorkspace, setOptionsFromWorkspace] = useState([]);
  const [optionsFromLab, setOptionsFromLab] = useState([]);
  const { platforms } = useSelector((state) => state.platformSlice);
  const { workspace } = useSelector((state) => state.workspaceSlice);

  useEffect(() => {
    if (platforms.length && workspace) {
      const currentContents = workspace.items
        ?.filter(
          (item) =>
            item.platformData &&
            item.platformData.type === field_select_by
        )
        .map((item) => item.name);
      if (currentContents) setOptionsFromWorkspace(currentContents);

      const allPlatforms = platforms
        ?.filter((platform) => platform.type === field_select_by)
        .map((platform) => platform.name);
      if (currentContents) setOptionsFromLab(allPlatforms);
    }
  }, [platforms]);

  useEffect(() => {
    if (!props.preview) {
      setInputValue(field_value || "");
    }
  }, [field_value, props.preview]);

  const handleSelect = (event) => {
    event.preventDefault();
    let platformToInsert = "";
    let platform = event.target.value;
    if (event.target.value.startsWith("###")) {
      const name = event.target.value;
      platformToInsert = name.substring(3);
    }
    handleChange("insertPlatforms", { [field_id]: platformToInsert });
    handleChange(field_id, platform);
  };
  if (sections) {
    return (
      <>
        <InputLabel shrink required>
          {field_label}
        </InputLabel>
        <FormControl size="small" fullWidth required>
          <Select
            fullWidth
            required={field_mandatory}
            id={field_id}
            name={field_id}
            value={inputValue}
            onChange={handleSelect}
          >
            <ListSubheader>Currently in workspace</ListSubheader>
            {optionsFromWorkspace.map((option, index) => (
              <MenuItem key={index} value={option}>
                {option}
              </MenuItem>
            ))}
            <ListSubheader>Insert one for me</ListSubheader>
            {optionsFromLab.map((option, index) => (
              <MenuItem key={index} value={"###" + option}>
                {option}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </>
    );
  } else
    return (
      <>
        <InputLabel>{field_label}</InputLabel>
        <Select
          fullWidth
          required={field_mandatory}
          id={field_id}
          name={field_id}
          value={field_value}
          size="small"
          onChange={(event) => handleChange(field_id, event)}
        >
          {optionsFromWorkspace.map((option, index) => (
            <MenuItem key={index} value={option}>
              {option}
            </MenuItem>
          ))}
        </Select>
      </>
    );
};
