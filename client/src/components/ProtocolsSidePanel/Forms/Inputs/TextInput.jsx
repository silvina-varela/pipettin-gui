import { useState, useEffect } from "react";
import { TextField, InputLabel, FormControl } from "@mui/material";

export const TextInput = (props) => {
  const { handleChange, field: {field_id, field_type, field_value, field_label, field_placeholder, field_initialValue, field_mandatory, helperText} } = props;

  const [inputValue, setInputValue] = useState("");

   useEffect(() => {
    if(props.preview){
      setInputValue(field_initialValue || "")
    } else{
      setInputValue(field_value || "");
    }
   }, [field_value]);

  return (
    <>
      <InputLabel shrink required>{field_label}</InputLabel>
      <FormControl size="small" required fullWidth>
        {(field_type === "largeText") ?
          <TextField
            id={field_id}
            type="text"
            value={inputValue}
            required={field_mandatory}
            variant="standard"
            multiline
            placeholder={field_placeholder}
            helperText={helperText}
            onChange={(event) => handleChange(field_id, event.target.value)}
          />
          :
          <TextField
            id={field_id}
            value={inputValue}
            required={field_mandatory}
            type="text"
            variant="standard"
            placeholder={field_placeholder}
            helperText={helperText}
            onChange={(event) => handleChange(field_id, event.target.value)}
          />
        }
      </FormControl>
    </>
  );
};
