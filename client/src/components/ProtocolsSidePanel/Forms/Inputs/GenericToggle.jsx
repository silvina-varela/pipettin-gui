import { ToggleButton, ToggleButtonGroup } from "@mui/material";

export const GenericToggle = (props) => {
  const { field: {field_id, field_value, field_options, field_initialValue}, handleChange } = props;


  return (
    <ToggleButtonGroup
      id={field_id}
      color="primary"
      value={props.preview ? field_initialValue : field_value}
      exclusive
      onChange={(event) => handleChange(field_id, event.target.value)}
      size="small"
    >
      {field_options.map((field, i) => (
        <ToggleButton
          key={i}
          value={field.option_label} 
          sx={{
            textTransform: "lowercase",
            p: 0.5,
          }}
        >
          {field.option_label}
        </ToggleButton>
      ))}
    </ToggleButtonGroup>
  );
};
