import { Grid } from "@mui/material";
import { SelectInput } from "./Inputs/SelectInput";
import { ToolSelectInput } from "./Inputs/ToolSelectInput";
import { NumberInput } from "./Inputs/NumberInput";
import { ListGroup } from "./Inputs/ListGroup";
import { TextInput } from "./Inputs/TextInput";
import { ListInput } from "./Inputs/ListInput";
import { CheckboxLabel } from "./Inputs/Checkbox";
import { GenericToggle } from "./Inputs/GenericToggle";
import { SwitchLabel } from "./Inputs/SwitchButton";
import { DefinedSelectInput } from "./Inputs/DefinedSelectInput";
import { FileInput } from "./Inputs/FileInput";  // Import the new input component

export const FormInputs = ({
  field,
  handleChange,
  preview = false,
  isEditMode,
}) => {
  switch (field.field_type) {
    case "text":
      return (
        <Grid item xs={5.5} mr={2} mb={5}>
          <TextInput
            handleChange={handleChange}
            preview={preview}
            field={field}
            isEditMode={isEditMode}
          />
        </Grid>
      );
    case "list":
      return (
        <Grid item xs={5.5} mr={2} mb={5}>
          <ListInput
            handleChange={handleChange}
            preview={preview}
            field={field}
            isEditMode={isEditMode}
          />
        </Grid>
      );
    case "number":
      return (
        <Grid item xs={5.5} mr={2} mb={5}>
          <NumberInput
            handleChange={handleChange}
            preview={preview}
            field={field}
            isEditMode={isEditMode}
          />
        </Grid>
      );
    case "listGroup":
      return (
        <Grid item xs={12} mb={5}>
          <ListGroup
            handleChange={handleChange}
            preview={preview}
            field={field}
            isEditMode={isEditMode}
          />
        </Grid>
      );
    case "selectDataFromItems":      
      return (
        <Grid item xs={5.5} mr={2} mb={5}>
          <SelectInput
            handleChange={handleChange}
            sections={true}
            preview={preview}
            field={field}
            isEditMode={isEditMode}
          />
        </Grid>
      );
      case "toolSelect":      
      return (
        <Grid item xs={5.5} mr={2} mb={5}>
          <ToolSelectInput
            handleChange={handleChange}
            sections={true}
            preview={preview}
            field={field}
            isEditMode={isEditMode}
          />
        </Grid>
      );
    case "checkbox":
      return (
        <Grid
          item
          sx={{ display: "flex", alignItems: "center" }}
          xs={5.5}
          mr={2}
          mb={2.5}
        >
          <CheckboxLabel
            handleChange={handleChange}
            preview={preview}
            field={field}
            isEditMode={isEditMode}
          />
        </Grid>
      );
    case "switch":
      return (
        <Grid
          item
          sx={{ display: "flex", alignItems: "center" }}
          xs={5.5}
          mr={2}
          mb={2.5}
        >
          <SwitchLabel
            handleChange={handleChange}
            preview={preview}
            field={field}
            isEditMode={isEditMode}
          />
        </Grid>
      );
    case "toggle":
      return (
        <Grid
          item
          sx={{ display: "flex", alignItems: "center" }}
          xs={5.5}
          mr={2}
          mb={5}
        >
          <GenericToggle
            handleChange={handleChange}
            preview={preview}
            field={field}
            isEditMode={isEditMode}
          />
        </Grid>
      );
    case "largeText":
      return (
        <Grid item xs={12} mr={2} mb={5}>
          <TextInput
            handleChange={handleChange}
            preview={preview}
            field={field}
            isEditMode={isEditMode}
          />
        </Grid>
      );
    case "definedSelect":
      return (
        <Grid item xs={5.5} mr={2} mb={5}>
          <DefinedSelectInput
            handleChange={handleChange}
            preview={preview}
            field={field}
            isEditMode={isEditMode}
          />
        </Grid>
      );
    case "fileUpload":
      return (
        <Grid item xs={5.5} mr={2} mb={5}>
          <FileInput
            handleChange={handleChange}
            preview={preview}
            field={field}
            isEditMode={isEditMode}
          />
        </Grid>
      );
    default:
      return null;
  }
};
