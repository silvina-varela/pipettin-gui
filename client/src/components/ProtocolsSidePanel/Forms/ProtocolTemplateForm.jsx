import { useEffect, useState } from "react";
import { Grid, Typography } from "@mui/material";
import { FormInputs } from "./FormInputs";
import { useFormikContext } from "formik";

export const ProtocolTemplateForm = ({
  workspace,
  values,
  platformsToInsert,
  setPlatformsToInsert,
  isEditMode,
}) => {
  const [selectedTemplate, setTemplate] = useState(values.templateFields);

  useEffect(() => {
    if (selectedTemplate && !isEditMode) {
      const templateDefinition = {};
      let updatedFields = [...selectedTemplate.fields];
      updatedFields = updatedFields.map((field) => {
        const newField = { ...field };
        newField.field_value = field.field_initialValue;
        templateDefinition[field.field_id] = field.field_initialValue;
        return newField;
      });
      setFieldValue("templateDefinition", {
        ...values.templateDefinition,
        ...templateDefinition,
      });
      setFieldValue("templateFields", {
        ...values.templateFields,
        fields: updatedFields,
      });
      setTemplate((prev) => ({
        ...prev,
        fields: updatedFields,
        templateDefinition,
      }));
    }
  }, []);

  const { setFieldValue } = useFormikContext();

  const handleTemplateChange = (id, value) => {
    if (id === "insertPlatforms") {
      setPlatformsToInsert({ ...platformsToInsert, ...value });
    }
  
    if (id === "components") {
      setFieldValue("templateDefinition", {
        ...values.templateDefinition,
        [id]: value.value,
      });
    } else if (id === "fileUpload") {
      // Handle file upload separately
      setFieldValue("templateDefinition", {
        ...values.templateDefinition,
        [id]: value,  // value should be a file object here
      });
    } else {
      setFieldValue("templateDefinition", {
        ...values.templateDefinition,
        [id]: value,
      });
    }
  
    let newValues = { ...selectedTemplate };
    newValues.fields = newValues.fields.map((field) => {
      const { field_id } = field;
      const newField = { ...field };
      if (id === field_id) {
        if (id === "components") {
          newField["field_groupValue"] = value.groupValue;
          newField["field_value"] = value.value;
        } else {
          newField["field_value"] = value;
        }
      }
      return newField;
    });
  
    setFieldValue("templateFields", {
      ...values.templateFields,
      fields: newValues.fields,
    });
    setTemplate(newValues);
  };

  if (selectedTemplate)
    return (
      <>
        <Typography variant="h5" mb={3}>
          {selectedTemplate.name}
        </Typography>
        <Grid container display={"flex"} flexWrap={"wrap"}>
          {selectedTemplate.fields?.map((field, i) => (
            <FormInputs
              key={i}
              field={field}
              workspace={workspace}
              handleChange={handleTemplateChange}
              isEditMode={isEditMode}
            />
          ))}
        </Grid>
      </>
    );
};
