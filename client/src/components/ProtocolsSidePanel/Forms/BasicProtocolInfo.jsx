import { TextField, Select, MenuItem, InputLabel, Grid } from "@mui/material";

export const BasicProtocolInfo = ({
  values,
  handleChange,
  touched,
  errors,
  templates,
  isEditMode
}) => {

  return (
    <Grid container direction={"column"} spacing={4}>
      <Grid item xs={12}>
        <TextField
          fullWidth
          variant="standard"
          id="name"
          name="name"
          label="Name"
          value={values.name}
          onChange={handleChange}
          error={touched.name && Boolean(errors.name)}
          helperText={touched.name && errors.name}
        />
      </Grid>

      <Grid item xs={12}>
        <TextField
          fullWidth
          multiline
          rows={10}
          variant="standard"
          id="description"
          name="description"
          label="Description"
          value={values.description}
          onChange={handleChange}
          error={touched.description && Boolean(errors.description)}
          helperText={touched.description && errors.description}
        />
      </Grid>
      {isEditMode ? null :
        <Grid item xs={12}>
          <InputLabel>Create from template</InputLabel>
          <Select
            id="templateFields"
            name="templateFields"
            value={values.templateFields}
            displayEmpty
            defaultValue={""}
            onChange={handleChange}
            fullWidth
            placeholder="Select"
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            {templates?.map((template) => (
              <MenuItem key={template._id} value={template}>
                {template.name}
              </MenuItem>
            ))}
          </Select>
        </Grid>}

    </Grid>
  );
};
