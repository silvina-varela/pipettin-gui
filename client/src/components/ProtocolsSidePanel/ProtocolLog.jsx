import { useRef, useEffect, useState } from "react";
import { Box, IconButton, Tooltip, Typography } from "@mui/material";
import FileCopyIcon from "@mui/icons-material/FileCopy";
import DeleteIcon from "@mui/icons-material/Delete";
import { useSelector } from "react-redux";

export const ProtocolLog = ({ protocolLog, setProtocolLog }) => {
  const boxRef = useRef();
  const [showButtons, setShowButtons] = useState(false);
  const { protocol } = useSelector((state) => state.protocolSlice);

  useEffect(() => {
    // Scroll to the bottom of the box when new messages appear
    boxRef.current.scrollTop = boxRef.current.scrollHeight;
  }, [protocolLog]);

  const handleCopyToClipboard = (e) => {
    e.preventDefault();
    const formattedLogs = protocolLog.map(
      (log) => `[${log.date}]: ${log.message}`
    );
    const copiedText = formattedLogs.join("\n");
    navigator.clipboard.writeText(copiedText);
  };

  const handleClearMessages = (e) => {
    e.preventDefault();
    setProtocolLog([]);
  };

  return (
    <Box
      ref={boxRef}
      sx={{
        border: "1px solid black",
        overflowY: "auto",
        height: "10vh",
        width: "auto",
        marginX: 2,
      }}
      onMouseEnter={() => setShowButtons(true)}
      onMouseLeave={() => setShowButtons(false)}
    >
      {showButtons && (
        <Box
          sx={{
            position: "absolute",
            width: "-webkit-fill-available",
            marginRight: 2.8,
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
          }}
        >
          <Tooltip title="Copy to Clipboard">
            <IconButton
              onClick={handleCopyToClipboard}
              color="neutral"
              size="small"
            >
              <FileCopyIcon sx={{ fontSize: 16 }} />
            </IconButton>
          </Tooltip>
          <Tooltip title="Clear Messages">
            <IconButton
              onClick={handleClearMessages}
              color="neutral"
              size="small"
            >
              <DeleteIcon sx={{ fontSize: 16 }} />
            </IconButton>
          </Tooltip>
        </Box>
      )}

      {protocolLog.map((log, index) => {
        if (protocol?.name === log?.protocol)
          return (
            <Typography
              variant="caption"
              component="p"
              key={index}
              color={log.severity}
              sx={{ whiteSpace: "pre-line" }}
            >
              [{log.date}]: {log.message}
            </Typography>
          );
      })}
    </Box>
  );
};
