import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import fs from 'fs'

const data = JSON.parse(fs.readFileSync("../config.json", "utf8"));
const { UI_PORT } = data;
const ui_port = UI_PORT || 3000;

export default defineConfig(() => {
  return {
    plugins: [react()],
    server: {
      // Specify which IP addresses the server should listen on.
      // Set to 0.0.0.0 to listen on all addresses, including LAN and public addresses.
      host: true,
      // Specify development server port (i.e. for "npm run dev" or "vite serve").
      // Note if the port is already being used, Vite will automatically try the next available port,
      // so this may not be the actual port the server ends up listening on.
      // The actual port is printed to the console when starting the app.
      port: ui_port,
      // The port used by "serve" when running "npm start" is 3000 by default, and is not affected
      // by the port set here. The "serve" port can be changed by altering the "start" command in
      // the "package.json" file of the client app.
    },
  };
});
