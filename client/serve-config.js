import { exec } from 'child_process';
import fs from 'fs'
import os from 'os';

const data = JSON.parse(fs.readFileSync("../config.json", "utf8"));
const { UI_PORT, USE_CORS } = data;
const ui_port = UI_PORT || 3000;

// Function to get the IP address
const getIPAddress = () => {
  const interfaces = os.networkInterfaces();
  for (const interfaceName in interfaces) {
    for (const iface of interfaces[interfaceName]) {
      if (iface.family === 'IPv4' && !iface.internal) {
        return iface.address;
      }
    }
  }
  return 'localhost'; // Fallback if no external IP is found
};

if(!USE_CORS){
    console.log("USE_CORS disabled in config.json, attempting to auto-configure HOST address.");
    try {
        const serverIP = getIPAddress();
        data.HOST = serverIP;  // Replace HOST with server IP.
        console.log("Updated HOST in config data to:", serverIP);
        
        // Write the updated config data.
        fs.writeFileSync("./dist/config.json", JSON.stringify(data, null, 2), 'utf8');
        console.log("Written updated config to dist's config.json:", data);
    } catch (error) {
        console.error("Error loading or writing config:", error);
    }
}

// Run the serve command with the specified port.
console.log("Serving UI on port:", ui_port)
const command = `serve -s dist -l ${ui_port}`;

exec(command, (err, stdout, stderr) => {
    if (err) {
        console.error(`Error executing serve: ${err}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
    console.error(`stderr: ${stderr}`);
});
