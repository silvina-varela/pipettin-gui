# App Components

> (WIP)
>
> This README provides an overview of the components, and an example of how to modify the code in a React app.
>
> It is probably incomplete.

The [`components` folder](../client/src/components) contains two types of components:

1. Function-based Components:
   - [`Platforms`](../client/src/components/Platforms)
   - [`Workspaces`](../client/src/components/Workspaces)
   - [`ProtocolsSidePanel`](../client/src/components/ProtocolsSidePanel)
   - [`PlatformsSidePanel`](../client/src/components/PlatformsSidePanel)
   - [`NavBar`](../client/src/components/NavBar)
2. Reusable Components:
   - [`Buttons`](../client/src/components/Buttons)
   - [`Dialogs`](../client/src/components/Dialogs)

Each folder mentioned above contains components divided according to their specific use.

Additionally, the `Platforms`, `ProtocolsSidePanel`, and `Workspaces` folders have their own reusable component subfolders, tailored to their functionalities. These subfolders contain components designed for reusability within their respective features.

This organization ensures a clear separation between the app's specific functionality and the more generic reusable components, promoting modularity and code reusability.

Feel free to explore these components and customize them to suit your app's requirements.

> Note: Please be aware that the specific implementation details and usage instructions for each component may vary.

## Main Components

Learn more at:

- [ProtocolSidePanel.md](./ProtocolSidePanel.md): protocols side panel.
- [PlatformSidePanel.md](./PlatformSidePanel.md): platforms side panel.
- Dashboard: *to-do*.
- Workspace editor: *to-do*.
- Platform editor: *to-do*.

## Platform forms

The forms used to edit and create platforms are [here](../client/src/components/Platforms) including graphics, initial values, and validation.

## Platform item graphics

The objects drawn on the workspace are called "items". Their graphics are defined [here](../client/src/components/Graphics). They are:

- [Colony](../client/src/components/Graphics/Colony.jsx)
- [PetriDish](../client/src/components/Graphics/PetriDish.jsx)
- [Platform](../client/src/components/Graphics/Platform.jsx)
- [Slot](../client/src/components/Graphics/Slot.jsx)
- [Well](../client/src/components/Graphics/Well.jsx)
- [WellSlot](../client/src/components/Graphics/WellSlot.jsx)

## Example: Modifying the Code

Before starting to edit the code, make sure you have a basic understanding of React and its jsx syntax extension, which is a special way of writing JavaScript. If you are new to React, you can refer to the official page for learning: <https://react.dev/learn>.

Additionally, ensure you are familiar with JavaScript: <https://javascript.info/>.

In this app, components from Mui (Material-UI) are used. You can learn how to use Material-UI components from its official page: <https://mui.com/material-ui/getting-started/>.

Let's say you want to add a new number input to the WorkspaceForm component, located in the Workspaces folder at [client/src/components/Workspaces/WorkspaceForm.jsx].<!-- Nico: why is this component not mentioned in the list above?(Porque es un componente que está dentro de otras carpetas, en caso de mencionarlo arriba, habría que escribir todas otros componentes)--> Here's a step-by-step guide on how to do it:

1. Identify the appropriate location in the component where you want to add the new form field. For this example, let's assume you want to add it next to the "Height" field. <!-- Nico: What does "after" mean in this context? Does it mean on the right? below? or somewhere in the code? -->
![Alt text](<../../../Screen Shot 2023-08-01 at 10.54.14.png>)
<!-- Nico: insert an image of "the component" here for reference. -->

2. Inside the return statement of the WorkspaceForm component, locate the section where the "Height" input is rendered. <!-- Nico: What is "Height" in this context? In which file is the "WorkspaceForm component" defined? is it "client/src/components/Workspaces/WorkspaceForm.jsx"? What is a jsx file? --><!-- Is it important that "things" are "rendered" inside the return statement? -->

3. To add the `InputNumber` form field (a reusable component that receives numbers as input) above the "Height" field, you can use the following code snippet: <!-- Nico: "InputNumber" was not mentioned before, what is it? Where exactly should I paste the snippet? Does "after" in this context mean the same as "after" in step 1? -->

```js
<Grid item>
  {/* EXAMPLE*/}
  <InputNumber
    fullWidth
    id="example"
    name="example"
    color="secondary"
    label="Example"
    endAdornment={"units"}
    value={values.example}
    onChange={handleChange}
  />
</Grid> 
```
To position the new input field next to the "Height" field on the form, insert the code just after the closure of the Grid that contains the "Height" field. Refer to the below picture for a visual reference.

![Alt text](<Screen Shot 2023-08-01 at 11.23.56.png>)

4. Customize the form field properties according to your needs. Update the `id`, `name`, and `label` props with appropriate values for the new field. Adjust the `endAdornment` property to display the desired unit or symbol at the end of the input field.

5. To adjust the spacing between the fields, you might need to change the `xs` property of the `Grid` item and `Grid` container components. <!-- The other Grid items do not have the "xs" property, why? is there a default?--> The `Grid` component is part of Material-UI (Mui), and you can find a detailed explanation of the component and its properties, such as `xs`, here: <https://mui.com/material-ui/react-grid/>

6. You need to define the data type of the `example` field. If you're not familiar with Formik, which is a React library for creating forms, you can learn more about it here: <https://formik.org/docs/overview>. Additionally, you can use the Yup library for error control. You can find more information about Yup here: <https://www.npmjs.com/package/yup>.<!-- Nico: name disambiguation is needed here. It is a bit confusing. -->
To add the `example` field to the createWorkspaceSchema at the beginning of the WorkspaceForm.jsx, follow these steps:<!--  Nico: To which file should the code be added? Is it the same "WorkspaceForm.jsx" file? -->

<!-- The "WorkspaceForm.jsx" file has different values. Should those be changed or left be? are they relveant? Is it important to leave a final "," comma as in the original Jsx file? -->

```javascript
const createWorkspaceSchema = Yup.object().shape({
  name: Yup.string()
    .min(3, "Workspace name must have at least 3 characters")
    .max(24, "Workspace name can't have more than 24 characters")
    .required("Please enter name"),
  description: Yup.string().max(
    60,
    "Description can't have more than 60 characters"
  ),
  width: Yup.number(),
  length: Yup.number(),
  height: Yup.number(),
  example: Yup.number()
});
```

Replace `Yup.number()` with the appropriate validation method from Yup based on your specific data type and validation rules for the example field. In this example, we are assuming it's a number and marking it as required.

Make sure to customize the validation schema according to your actual needs for the example field.

This ensures that the example field is validated based on the defined schema when the form is submitted.

If you don't want to remove the other input fields, you should leave them as they are. If you are editing the last line as shown in the example you don't need to put a comma but if you want to add more fields you need to separate the lines with a comma. 

7. If you want to add an initial value for the new field which can be an integer or a decimal, make the necessary changes where the `createFormInitialValues` are declared at the same code above where the Formik form state object is declared. If you don't want to add an initial value just ignore this step.<!-- Nico: What and where is this? --> For example:<!-- Nico: should the value be left as "number" or changed to an integer? can decimals be used?-->

```javascript
const createFormInitialValues = {
  name: "",
  description: "",
  width: 200,
  length: 300,
  height: 150,
  newField: number
};
```

8. To store this information in the database, open the <!-- Nico: cuando se agreguen links a los archivos que hay que editar, estaría bueno hacerlo así: --> [api/src/db/models/workspaceModel.js](../api/src/db/models/workspaceModel.js) file and add the following line after "height":

```yaml
example: Number,
```

<!-- Nico: como resultado el "New Field" apareció en un segundo renglón. A eso refería con "after" en el step 1? Cómo podría hacer para que quede en otras posicones? Por ejemplo a la derecha en vez de "en el siguiente renglón"?-->

This example illustrates the steps to add a new number input to the `WorkspaceForm` component. Adapt these instructions to fit your specific requirements and component structure.

The forms in this app utilize the Yup library for error controls. For more information, refer to the following link:

- Yup: <https://www.npmjs.com/package/yup>
