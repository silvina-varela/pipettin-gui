# Backend

The backend is a RESTful API built with Express.js, which is a Node.js framework. It follows a basic folder structure:

```
  api/
  ├── src/
  │   ├── controllers/
  │   ├── db/
  │   ├── routes/
  │   ├── utils/
  ├── config.js
  ├── index.js
  └── package.json
```

- controllers: This folder holds the files that handle the logic of the application. Each controller corresponds to a specific route or functionality.
- db: This folder contains the database models or schemas used to define the structure and behavior of the MongoDB collections.
- routes: Files that define the API endpoints. For details about the endpoints see [this document](./Endpoints.md)
- utils: Helper functions used across different parts of the codebase.
- config.js: This file manages configuration settings and environment variables.
- index.js: This file serves as the entry point for the backend. It sets up the Express server, middleware, and connects to the MongoDB database.