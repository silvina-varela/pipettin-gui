# Troubleshooting Guide

## Reporting Errors

If you encounter any issues while using the Pipettin app, please follow these steps to report the problem effectively:

### 1. Capture Screenshots of Browser Console

When an error occurs, it's crucial to capture the error message displayed in the browser console. Here's how you can do it:

#### Open Developer Tools

For most browsers, you can open the Developer Tools by pressing F12 or Ctrl + Shift + I.
Alternatively, you can right-click on the page, select "Inspect", and navigate to the "Console" tab.

#### Take a Screenshot

Once the Developer Tools are open, navigate to the "Console" tab.
Capture a screenshot of the entire browser window, including the error message in the console.

### 2. Capture Screenshot of Terminal/Program

Since this is a local app, you need to capture a screenshot of the terminal or the program you used to start the application. This will help us understand the environment in which the app is running.

Capture a screenshot showing the terminal or the program window where you initiated the application.

### 3. Provide Context

It's essential to understand how the error occurred. Please include the following information in your report:

#### Description

Briefly describe what you were doing when the error occurred.

#### Steps to Reproduce

List the steps you took leading up to the error. This will help us recreate the issue on our end.

#### Environment

Mention the device, operating system, and browser you are using.

### 4. Send the Report

Once you've gathered the necessary information, please send it to our support team by [sending an email](mailto:contact-project+pipettin-bot-pipettin-gui-support@incoming.gitlab.com) to the GitLab service desk.

## Known troubles

### CORS errors

Other than incorrect configuration, some issues may depend on your OS or Node version.

In this case, the API is being served on an IPv6 address (`::1:3333`) when the local host is configured (i.e. `127.0.0.1`).

Notice that this does not happen in the case of the frontend, which correctly binds to the IPv4 local host address.

> The netstat command can be installed by running `sudo apt install net-tools`.

```txt
$ sudo netstat -ntlp
Active Internet connections (only servers)
tcp        0      0 127.0.0.1:3000          0.0.0.0:*               LISTEN      51156/node          
tcp        0      0 127.0.0.1:27017         0.0.0.0:*               LISTEN      511/mongod          
tcp6       0      0 ::1:3333                :::*                    LISTEN      51173/node
```

We patched this by using the `dns` module in the backend: [index.js](../api/index.js)

```js
// Added this to prevent resolving "127.0.0.1" to IPv6 first.
// See: https://github.com/nodejs/node/issues/40537
var dns = require('dns');
dns.setDefaultResultOrder('ipv4first');
```

See:

- <https://github.com/nodejs/node/issues/40537>
- <https://stackoverflow.com/a/14045163>
