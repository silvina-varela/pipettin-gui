(WIP)

# API Endpoints

This README provides a brief overview of the API endpoints.

## Sending Requests

To interact with the API endpoints, you need to send HTTP requests to the corresponding URLs. The structure of the URL depends on the type of request and the required parameters.

- Base URL: http://localhost:3333/api/ENDPOINT
- Replace ENDPOINT with the specific endpoint you want to access.

The API supports different ways to send data:

- Query Parameters: Query parameters are appended to the URL and are used to provide additional information to the endpoint. For example, http://localhost:3333/api/protocols?id=value.
- URL Parameters: URL parameters are used to specify dynamic parts of the URL path. For example, http://localhost:3333/api/workspaces/workspaceID.
- Request Body: The request body is used to send complex data to the API. It is typically used in POST or PUT requests and contains a JSON or object. The structure and content of the request body depend on the specific endpoint and the expected data format.

## Request Types

The API supports the following request types:

- GET: Retrieves data from the server. It is used to read or retrieve information from the API.
- POST: Submits data to be processed by the server. It is used to create new resources or send data to be processed.
- PUT: Updates existing resources on the server. It is used to modify or update existing data.
- DELETE: Removes resources from the server. It is used to delete or remove data from the API.

## Routers

### Platform Router

#### Get Platforms

- **GET** /platforms
- Query Parameters (optional): id, name
- Returns: 
  - If no query parameters are provided, returns all platforms.
  - If only the name query parameter is provided, returns the platform(s) matching the given name.
  - If the id query parameter is provided, returns the platform with the corresponding id.

#### Create Platform

- **POST** /platforms
- Request Body: platform object
- Returns: The created platform object.

#### Update Platform

- **PUT** /platforms
- Query Parameters: id
- Request Body: updated platform object
- Returns: The updated platform object.

#### Delete Platform

- **DELETE** /api/platforms
- Query Parameters: id
- Returns: The deleted platform object.

#### Check Platform Name Availability

- **GET** /platforms/check/:name
- Path Parameter: name
- Returns:
  - If the platform name exists, returns an object with `exists: true` and a message indicating that the name is not available.
  - If the platform name does not exist, returns an object with `exists: false` and a message indicating that the name is available.

### Workspace Router

#### Get Workspaces

- **GET** /workspaces
- Query Parameters (optional): id, name
- Returns: 
  - If no query parameters are provided, returns all workspaces.
  - If only the name query parameter is provided, returns the workspace(s) matching the given name.
  - If the id query parameter is provided, returns the workspace with the corresponding id.

#### Create Workspace

- **POST** /workspaces
- Request Body: workspace object
- Returns: The created workspace object.

#### Update Workspace

- **PUT** /workspaces
- Query Parameters: id
- Request Body: workspace object
- Returns: The updated workspace object.

#### Delete Workspace

- **DELETE** /workspaces
- Query Parameters: id
- Returns: The deleted workspace object.

### Protocols Router

#### Get Protocols

- **GET** /protocols
- Query Parameters (optional): id, name, workspaceID, workspaceName
- Returns:
  - If no query parameters are provided, returns all protocols.
  - If the id query parameter is provided, returns the protocol with the corresponding id.
  - If the name query parameter is provided, returns the protocols with the matching name.
  - If the workspaceID or workspaceName query parameter is provided, returns all protocols associated with that workspace.

#### Create Protocol

- **POST** /protocols
- Request Body: protocol object (name, description, workspaceID)
- Returns: The created protocol object.

#### Update Protocol

- **PUT** /protocols
- Query Parameters: id
- Request Body: protocol object
- Returns: The updated protocol object.

#### Delete Protocol

- **DELETE** /protocols
- Query Parameters: id
- Returns: The deleted protocol object.

#### Validate Protocol

- **POST** /protocols/validate
- Request Body: protocolID
- Returns: The validation result for the protocol.

### Robot controller

#### Run Protocol

- **POST** /robot/run
- Request Body: protocolID
- Description: Creates the protocol actions and sends request to the robot to run the specified protocol.
- Returns: The protocol object that was created.

#### Stop Process

- **POST** /robot/stop
- Description: Stops the current running process on the robot.

#### Go To Position

- **POST** /robot/goto
- Request Body: workspaceID, platformID, contentID
- Description: Sends a request to move the robot to a specific position in the workspace.
- Returns: The calibration result.

#### Get Robot Position
- **GET** /robot/position
- Description: Retrieves the current position of the robot.