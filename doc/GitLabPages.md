# Serving the Client over GitLab Pages

The client is _mostly_ a static page, it requires a backend API actually doing the work.

The client can be served through gitlab pages. For example, it is currently served at <https://pipettin-gui-481614.gitlab.io/> although it wont do much until you point it to a working API server.

Currently there are two caveats:

- The main page does not display a very informative error, it only says: "Server error: there was an error loading the workspaces".
- You can confidently use the app, but only access it through the main URL. Paths under it wont work (e.g. <https://pipettin-gui-481614.gitlab.io/settings>).
