# Default data

## Database contents

Files located at [defaults](../api/src/db/defaults) are seeded into the DB during setup.

## Initial values for forms

These are split depending on the object:

- Platforms: [PlatformAttributes.jsx](../client/src/components/Platforms/Forms/PlatformAttributes.jsx)
- TO-DO: add others.

# Manual DB Seed

## Requisites

MongoDB must be installed and running.

## Initial seed

When there is no data in the database, run these to import all collections:

```bash
# git clone --depth=1 https://gitlab.com/pipettin-bot/pipettin-gui
mongoimport --db pipettin --collection platforms --file api/src/db/defaults/platforms.json --jsonArray
mongoimport --db pipettin --collection protocols --file api/src/db/defaults/protocols.json --jsonArray
mongoimport --db pipettin --collection workspaces --file api/src/db/defaults/workspaces.json --jsonArray
mongoimport --db pipettin --collection containers --file api/src/db/defaults/containers.json --jsonArray
mongoimport --db pipettin --collection settings --file api/src/db/defaults/settings.json --jsonArray
mongoimport --db pipettin --collection hLprotocols --file api/src/db/defaults/hLprotocols.json --jsonArray
mongoimport --db pipettin --collection tools --file api/src/db/defaults/tools.json --jsonArray
```

## Update procedure

First, drop tables from mongo, from its command-line interface:

```txt
mongo
> use pipettin
> db.platforms.drop()
> db.protocols.drop()
> db.workspaces.drop()
> db.tools.drop()
# And so on...
```

Exit Mongo's CLI (Ctrl+C), get the latest objects from the remote git repo and import them using `mongoimport`:

```sh
git pull  # Update documents.
mongoimport --db pipettin --collection platforms --file latest/platforms_export.json --jsonArray
mongoimport --db pipettin --collection protocols --file latest/protocols_export.json --jsonArray
mongoimport --db pipettin --collection workspaces --file latest/workspaces_export.json --jsonArray
# And so on...
```

## Export procedure

General syntax:

```txt
mongoexport -d <database> -c <collection_name> -o <output_file.json> --jsonArray
```

Examples:

```sh
mongoexport -d pipettin -c platforms  -o platforms_export.json --jsonArray
mongoexport -d pipettin -c workspaces -o workspaces_export.json --jsonArray
mongoexport -d pipettin -c protocols  -o protocols_export.json --jsonArray
```

### Pretty JSON

Prettify the output using BASH and Python (<https://stackoverflow.com/a/1920585>):

```sh
cat platforms_export.json | python -m json.tool > platforms_export.pretty.json
cat workspaces_export.json | python -m json.tool > workspaces_export.pretty.json
cat protocols_export.json | python -m json.tool > protocols_export.pretty.json
```

Delete the old ones optionally:

```sh
rm platforms_export.json workspaces_export.json protocols_export.json
```

## Import procedure

A document holding a single platform can be imported with `mongoimport`:

```sh
mongoimport --db pipettin --collection platforms --file platform.json
```

Documents holding multiple platforms each, can be imported with `mongoimport --jsonArray`:

> Note: the document _must_ be a JSON array. The command will fail if the JSON document is not an array ([ref](https://stackoverflow.com/a/40524744).). Each definition must be enclosed in curly braces ( `{}`), separated with commas (`,`) and the whole file must be enclosed with square brackets ( in `[]`).

```bash
mongoimport --db pipettin --collection platforms --file platforms.json --jsonArray
mongoimport --db pipettin --collection platforms --file platforms_enanas.json --jsonArray
mongoimport --db pipettin --collection workspaces --file workspaces.json --jsonArray
mongoimport --db pipettin --collection protocols --file protocols.json --jsonArray
mongoimport --db pipettin --collection protocols --file protocols_for_tests.json --jsonArray
mongoimport --db pipettin --collection protocols --file protocol_test1.json --jsonArray

# Or, for short: https://stackoverflow.com/a/44195823
for filename in proto*s.json; do mongoimport --db pipettin --collection protocols --jsonArray $filename; done;
for filename in plat*s.json; do mongoimport --db pipettin --collection platforms --jsonArray $filename; done;
for filename in work*s.json; do mongoimport --db pipettin --collection workspaces --jsonArray $filename; done;
```

## Inspecting collections

The mongo shell can be used to check the imported contents:

```txt
$ mongo
> use pipettin
> db.workspaces.find().pretty()
> db.protocols.find().pretty()
```

## Troubleshooting

In the Raspberry Pi 4 I had to append `--jsonArray` to the `mongoimport` commands to fix the following error: `BSON representation of supplied JSON is too large`.

You may need to set "export LC_ALL=C" in bash if you get the following error:

```txt
BadValue Invalid or no user locale set. Please ensure LANG and/or LC_* environment variables are set correctly
```

See: <https://stackoverflow.com/questions/26337557/>

# Update defaults

See instructions in [scripts/README.md](../scripts/README.md#database-to-json)
