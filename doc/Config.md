# Configuration files

We talk about config files [over here](https://discord.com/channels/1042462799772782613/1218254644925567217).

There are several config files:

- [.env](../.env): environment variables.
  - [api/src/db/config.js](../api/src/db/config.js): contains default values for the `.env` file, and reads from it.
- [config.json](../config.json): main config file for backend's port and host URL (i.e. the binding IP and PORT for the backend API) and for the frontend's port. This file is used to overwrite the following config files:
  - [client/public/config.json](../client/public/config.json): updated `npm run dev` is executed, to update its values. Do not change manually.
  - [client/dist/config.json](../client/dist/config.json): updated when `npm start` is executed, to update its values. Do not change manually.
- [client/vite.config.js](../client/vite.config.js): preferences for the frontend server, including the port where it is served, and other settings.
  - The port is now loaded from the main `config.json` file.

Others:

- [api/config.js](../api/config.js): deprecated.
- [api/src/db/config.js](../api/src/db/config.js): not a config file.

Notes:

- The port used by "serve" (i.e. when running "npm start") is 3000 by default. It is not affected by the port set in [client/vite.config.js](../client/vite.config.js). It can only be set when the `serve` command is called (<https://github.com/vercel/serve/issues/82>). For example, change the `serve -s dist -l 3000` bit to `serve -s dist -l 3001` to run the server at port 3001.
  - The "serve" is defined in the "start" command in the [package.json](../client/package.json) file of the client app. This command calls a [serve-config](../client/serve-config.js) script, which uses the `UI_PORT` variable from the [config.json](../config.json) file.

## config.json

The [config.json](../config.json) file is read by both the [client](../client) and the api [api](../api). The backend needs to know on which IP to serve the application, and the UI needs to know to which IP it must connect. These values need to match, and are thus set by a single configuration file.

The following keys are read from this file:

- `PORT`: The API's port.
- `UI_PORT`: The UI's port.
- `HOST`: The API's address.
- `USE_CORS`: Flag indicating wether to enforce CORS or allow all.
- `USE_STORED_URL`: Flag indicating if the clien should prioritize the stored API URL over `HOST`.

The purpose of `USE_STORED_URL` is to allow the user to update the backend's IP with some persistence, whitout needing to update the `config.json` file directly. Although it sounds simple, it does require a certain set of basic skills, or it may be impossible if the client is served statically.

If the `USE_CORS` flag is set to `false`, the backend will allow all origins (i.e. disabling CORS). On the client side, `serve-config.js` will attempt to guess the IP of the backend (with the local IP of a network interface) and override the `HOST` key in `client/dist/config.json`. This is meant for machines with changing IPs (e.g. machines in LANs with DHCP) serving the dist (i.e. served with `npm start` and not `npm run dev`), although an app restart will be needed if this IP changes.

If the `USE_CORS` flag is set to `false`, the backend will only allow local IPs and the address in the `HOST` key, and the client will try to reach the backend at that address (with no overrides).

On the client side, it is crucial that the `npm start` and `npm run dev` commands copy the `config.json` file at the root directory, into the relevant directories in the client (e.g. `client/dist/` or `client/public/`, respectively). This is required for both static and development deployments of the frontend to work correctly. This behavior is implemented by the client's [package.json](../client/package.json) file.

Usage:

- Edit this file when installing the app for the first time.
- Edit this file when the server changes its accessible IP address or URL.

## `.env`

The [.env](../.env) file contains two important variables, the URI of your MongoDB instance and the database name. Values on this file are read and written by the application's backend.

Values in the `.env` are read by the app using [dotenv](https://www.npmjs.com/package/dotenv).

The file is used by the backend to handle changes in the connection to the MongoDB database. The file is updated when new values arrive from the UI, with the new values set by the user.

Usage:

- There is no need to edit them manually unless your MongoDB is not running locally on the default URL and port.
- You may need to edit this file if the DB's address has changed since the initial configuration.
- If an invalid address is set on the UI by a user, the value is not saved to the `.env` file, but the app will need to be restarted (this happens automatically only for a systemd-managed `npm start`).

## Troubleshooting

Find frequent issues [here](Troubleshooting.md#known-troubles).
