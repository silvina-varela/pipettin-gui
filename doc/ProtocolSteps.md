# Protocol Steps

## Transfer
Transfers content from a source to a target. 

### Source
![TransferStep01.png](../assets/TransferStep01.png)

- Platform: list of platforms in the workspace. The UI doesn't filter out incorrect platform types (such as buckets or anchors).
- Select by name or tag: defines the following selection.
- Content: list of contents from the selected platform filtered by name or tag.

### Target
![TransferStep02.png](../assets/TransferStep02.png)

Same behaviour as in Source. When validating the protocol, it will check that Source and Target are not exactly the same.

### Volume
Users have three ways to distribute volume:

#### To each target
This option transfers the specified volume to each selected target. If multiple targets are chosen, each one receives the designated volume, resulting in a total volume transferred equal to volume multiplied by the number of selected targets.

![TransferStep03.png](../assets/TransferStep03.png)

#### In total
Selecting this option evenly distributes the specified volume among all chosen targets. The operation is calculated as volume divided by the number of selected targets.

![TransferStep04.png](../assets/TransferStep04.png)

#### For each content with tag
Users can designate a tag to determine the volume transferred to each target. In this case, the volume is multiplied by the number of contents that utilized the specified tag.

![TransferStep05.png](../assets/TransferStep05.png)

### Tips
![TransferStep06.png](../assets/TransferStep06.png)

Section for choosing the desired tip rack, tool, and bucket. Users also have the option to select one of four possible behaviors for reusing tips.

![TransferStep07.png](../assets/TransferStep07.png)


## Wait
Remains inactive for a specified duration in seconds.

![WaitStep01.png](../assets/WaitStep01.png)

## Human Intervention
Triggers an alert in the UI with the message specified in the step. The user can choose to click "OK" to proceed with the protocol or "Cancel" to halt the process.

![HumanStep01.png](../assets/HumanStep01.png)
![HumanStep02.png](../assets/HumanStep02.png)

## Comment
This step serves as a comment for the user and will be skipped by the controller.

![CommentStep01.png](../assets/CommentStep01.png)

## Mix
This step mixes the specified percentage of the selected content for a specified duration.

![MixStep01.png](../assets/MixStep01.png)

![MixStep02.png](../assets/MixStep02.png)
