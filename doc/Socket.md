# Socket Events

Socket events are used to communicate between the backend and the robot. Messages can flow from the backend to the robot or vice versa.

## Events from the backend to the robot

Whenever an instruction needs to be sent to the robot, a socket event is used. Below are the possible events to send.

### Run Protocol Event

- Event name: "run_protocol"
- Parameters:

  ```
    {
      name: "A protocol name",
      settings : {},
    }
  ```
The "run_protocol" event triggers the execution of a specified protocol on the robot. The "protocol" parameter is mandatory, requiring an exact match with predefined protocols. Optional "settings" provide configurability.

 ### Kill Commander event

 - Event name: "kill_commander"
 - Parameters:

```
  {
    reason: "The reason",
  };
```
The "kill_commander" event halts the current process on the robot. The optional "reason" parameter adds contextual information.

- Returns: If the event is succesful returns a string that says "Request sent", if not a message wih the error.

### Goto Event

- Event name: "go_to"
- Parameters:

```
  {
        position : { x: number, y: number, z: number},
        tool : "",
  }
```

The "go_to" event instructs the robot to move to a specific position. The mandatory "position" parameter signifies coordinates, while the optional "tool" parameter adds flexibility.

- Returns: If the event is successful returns a string that says "Request sent", if not a message wih the error.

### Human Intervention Events

- Event name: "human_intervention_continue"
- Parameters: "text"

- Event name: "human_intervention_cancelled"
- Parameters: "text"

These human intervention events inform the robot about the state of the intervention.

## Events from the robot to the backend

In addition to specific requests, the backend listens to events sent by the robot. For this purpose, there are listeners in the backend that, upon receiving events, execute specific instructions for each event. Each event sends an object which includes the "data" property with the necessary information. Below are the possible events that the robot can send.

### Human Intervention Required Event

- Event name: "human_intervention_required"
- Receives: The "data" object with a "message" property which is a string informing the user about the required intervention.

### Alert Event

- Event name: "alert"
- Receives: The "data" object with a "message" property which is a string that has alert message.

### Tool Data Event

- Event name: "tool_data"
- Receives: The "data" object with a "position" property, revealing the spatial coordinates (x, y, z) of the robot's tool.

### Stop Event

- Event name: "stop"
- Receives: The "data" object with a "message" property which is a string.

### Controller Status Event

- Event name: "controller_status"
- Receives: The "data" object encompassing the "status" and "message" properties, unfolding the current state of the robot controller.