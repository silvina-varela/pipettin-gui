# Database

MongoDB is a NoSQL database that relies on two primary concepts: documents and collections. A document is a set of key-value pairs and is the basic unit of data in MongoDB. It is similar to a JSON object. A collection is a group of documents and is analogous to a table in relational databases. Essentially, documents represent individual records, whereas collections are groups of related documents.

## Configuration

All database-related configurations are stored in the `./api/src/db` directory, which follows this structure:

```
db/
  ├── defaults/
  ├── models/
  ├── config.js
  ├── seed.js
  └── seedDefaultSettings.js
```

Within this structure, `config.js` functions as the primary configuration file for MongoDB. It contains the necessary code to establish the database connection. The configuration function in this file is invoked in the `index.js` file located at the root of the API to initialize the database connection. Once the connection is established, the function defined in `seedDefaultSettings.js` is invoked, and the default settings are added to the corresponding collection if the database was empty. Additionally, there is a `seed.js` file that can be executed using the command `npm run seed`, which populates default data into all collections. All the default data is stored in the `/defaults` directory.

## Collections

The schemas of the collections used in this project are defined in the `/models` directory. Each collection represents a specific type of data and serves a distinct purpose. Here is an overview each collection:

- **hLprotocols**: Stores high-level protocols, which are abstract sequences of steps defined through the GUI. 
- **platforms**: The platforms represent lab objects. The documents in this collection describe the characteristics and properties of the various objects that can be used in the protocols.
- **protocols**: This collection represents sequences of pipetting actions that are generated by the GUI from the high-level protocols and can be read by the Pipettin Bot.
- **protocol_templates**: Stores reusable templates for common laboratory protocols.
- **settings**: The configuration settings and preferences relevant to the application are contained here. It includes database configurations, as well as GUI-related configurations. 
- **tools**: The data related to the tools and equipment used in the laboratory is stored in this collection. This includes details about instruments, their specifications, and usage guidelines.
- **workspaces**: Workspaces represent virtual environments where platforms are placed and protocols are conducted. This collection manages the arrangement and interaction of lab objects within these virtual spaces.








