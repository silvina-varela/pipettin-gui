# Main features

### Platform creation

Users can create tube racks, tip racks, petri dishes or buckets by filling out a form while simultaneously visualizing a preview.

![02_platform_form.png](../assets/02_platform_form.png)

### Workspace creation

Users can create and edit workspaces using the previously created platforms

![03_workspace.png](../assets/03_workspace.png)

### Platform panel

When editing the workspace, the platform panel serves as a place to easily edit and visualize all the platforms inserted in the workspace.

![04_panel.png](../assets/04_panel.png)

### Protocol panel

Each workspace can have multiple protocols associated with it. This panel provides a convenient way for visualizing protocols and setting up steps.

![05_Protocols.png](../assets/05_Protocols.png)

The app supports 5 types of steps: 
- Transfer
- Wait
- Human Intervention
- Comment
- Mix

For a breakdown of each step see this [this document](./ProtocolSteps.md).

### Protocol templates
Users have the ability to create and modify their own protocol templates using a JSON editor.

![09_ProtocolTemplatesEdit.png](../assets/09_ProtocolTemplatesEdit.png)

The templates can be employed for streamlined protocol creation, with the additional requirement of writing a script to interpret the template.

![10_ProtocolFromTemplate.png](../assets/10_ProtocolFromTemplate.png)

The UI includes a default PCR mix template with the script already integrated. 

### Interaction with the robot

The app enables users to interact with the robot through four primary commands: *run*, *stop*, *show position* and *go to*.

#### Protocol Execution Buttons and Log
Located within the protocol panel, the protocol execution buttons send commands, such as run, stop, pause (work in progress), and continue (work in progress) to the robot controller. Additionally, the app displays a log of all controller responses to the user, although the storage of this log is still a work in progress.

![07_robotInteraction.png](../assets/07_robotInteraction.png)

* The "Show robot" button gets the current position of the robot and shows it directly on the workspace.
* The "Go to" button sends a request to move the robot to a specific position.

![08_robotController.png](../assets/08_robotController.png)

### Settings
The Settings page empowers users to customize their experience and manage database and controller configurations effortlessly, requiring no coding experience.

![11_Settings.png](../assets/11_Settings.png)
![12_DBSettings.png](../assets/12_DBSettings.png)
![13_ControllerSettings.png](../assets/13_ControllerSettings.png)

