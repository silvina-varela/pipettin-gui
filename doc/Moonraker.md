# Updates from Moonraker

We can update `pipettin-gui` from Mainsail by [configuring Moonraker correctly](https://moonraker.readthedocs.io/en/latest/configuration/#update_manager).

Open Mainsail and add the following to `moonraker.conf` on your raspberry pi.

```ini
[update_manager nodegui]
type: git_repo
channel: dev
path: ~/pipettin-bot/code/pipettin-gui
origin: https://gitlab.com/pipettin-bot/pipettin-gui.git
primary_branch: develop
enable_node_updates: True
managed_services: nodegui
info_tags:
    desc=Protocol Writer
```

Then edit the `moonraker.asvc` file under printer data, adding `nodegui` to the end of the file, to [allow Moonraker to manage it](https://moonraker.readthedocs.io/en/latest/configuration/#allowed-services).
