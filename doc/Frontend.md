# Frontend

The frontend is built using React, Redux, and Material UI. Here's an overview of the folder structure:

```txt
  client/
  ├── public/
  ├── src/
  │   ├── components/
  │   ├── pages/
  │   ├── redux/
  │   ├── utils/
  │   ├── App.jsx
  │   ├── colors.js
  │   ├── main.css
  │   ├── main.jsx
  ├── index.html
  ├── package.json
  └── vite.config.js
```

- `public`: The public folder holds the static assets of the React application.
- `src`: The src folder is where the main source code resides. It includes:
  - `components`: Contains the reusable components. The components are organized based on their function, such as "Platforms," "Protocols," "Workspaces," "Buttons," and "NavBar." This organization helps to maintain a clear separation of concerns and allows for easier navigation and management of the components. For a more detailed explanation, refer to [this document](./ReactComponents.md).
  - `pages`: Holds the components that represent individual pages of the application.
  - `redux`: Contains the Redux configuration, as well as the actions and reducers.
  - `utils`: This folder contains helper or reusable functions used across the application.
  - `App.jsx`: This file sets the Routes for the frontend.
  - `main.jsx`: This file is the entry point for the React app.
- `index.html`: This file serves as the entry point for the application and is the main HTML file that is loaded by the web browser.
- `vite.config.js`: This is the configuration file for the Vite development server and build process.
