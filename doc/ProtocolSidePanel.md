(WIP)

## ProtocolSidePanel

### Inputs Folder

1. Toggle component:

   - This component represents a group of toggle buttons with two options.
   - It receives option 1 and option 2 as props for the button labels.
   - It also receives handleChange and toggleValue as props to handle changes and store the selected toggle value.

2. NumberInput component:

   - This component represents a numeric input field.
   - It receives value, handleSaveValue, handleChange, and maxNumber as props.

3. InputSelect component:

   - This component represents a selection input field.
   - It receives label, options, handleChange, and initialValue as props.
   - It uses the useState hook to manage the selected value.
   - The useEffect hook is used to set the initial state based on the initialValue prop.
   - The selected value is stored in the component's state and updated through the handleSelection function.

4. DefinedSelect component:
   - This component is similar to InputSelect but represents a selection input field with predefined options.
   - It receives label, options, handleChange, and initialValue as props.
   - It uses the useState hook to manage the selected value.
   - The useEffect hook is used to set the initial state based on the initialValue prop.
   - The selected value is stored in the component's state and updated through the handleSelection function.

> These components are used to create a form where users can select tips and targets, and the selected values are stored in the definition object. The definition object can be passed to other components or used to perform other actions within the application.

### Steps Folder

#### MixStep

1. MixStep component:

   - Description: It combines other related components to configure the mix parameters.
   - Related components: Target, MixVolume, TipsSection.
   - Main functions:
     - Handles the selection of the target platform.
     - Passes the definition and platforms to the related components for configuration.

2. MixVolume component:

   - Description: This component represents the mixing volume in a mixing operation. It allows the user to specify the percentage and mixing type.
   - Related components: NumberInput, DefinedSelect.
   - Main functions:
     - Controls and displays the mixing percentage and mixing type.
     - Updates and saves the percentage and mixing type values in the definition object.

3. Target component:

   - This component represents a form for selecting targets.
   - It receives definition and platforms as props.
   - It uses the useState hook to manage the selected platform, toggle value, content values, content selection, and selected item count.
   - The useEffect hooks are used to set the initial state based on the definition and platforms props.
   - The component contains input components such as InputSelect and Toggle to handle platform and content selection.
   - The selected values are stored in the component's state and updated through respective handleChange functions.
   - The useEffect hook is used to update the definition prop whenever the selected values change.

#### TransferStep

1. Volume component:

   - Description: This component represents the transfer volume in a transfer operation. It allows the user to specify the volume and transfer type.
   - Related components: NumberInput, DefinedSelect, InputSelect.
   - Main functions:
     - Controls and displays the transfer volume and transfer type.
     - Updates and saves the volume and transfer type values in the definition object.

2. TransferStep component:

   - Description: It combines other related components to configure the transfer parameters.
   - Related components: Source, Target, Volume, TipsSection.
   - Main functions:
     - Handles the selection of the source & target platform.
     - Passes the definition and platforms to the related components for configuration.

3. Source component:

   - Description: It allows the user to select the platform from which the transfer is performed and the corresponding content or tag.
   - Related components: InputSelect, Toggle, DefinedSelect.
   - Main functions:
     - Controls and displays the source platform, content selection type (name or tag), and selected content.
     - Updates and saves the selected values in the definition object.

4. Target component: It has the same functions as MixSteps's Target component.

5. TipsSection component:

   - This component generates a form for selecting tips.
   - It receives definition, items, and behavior as props.
   - It uses the useState hook to manage the selected tip, tool, bucket, and behavior.
   - The component contains several input components such as InputSelect and DefinedSelect to handle tip selection, tool selection, bucket selection, and behavior selection.
   - The selected values are stored in the component's state and updated through handleChange functions.
   - The useEffect hook is used to update the definition prop whenever the selected values change.
   - This component is used at the MixStep as well.

### Other Step Components

#### StepCard

- It is a functional component that receives props such as setProtocolSelected, step, platforms, and setSelectedSteps.
- It uses the useState hook to manage the title edition through "isStepTitleEditing", "newStepTitle" to save the title of the steps, and "checked" to know if the step is selected.
- The component includes an useEffect hook that runs whenever the step prop changes. Inside the effect, it sets the checked state variable to false.
- The component defines several event handling functions:
  - handleCheckbox: Handles the checkbox click event, updating the selectedSteps state based on whether the checkbox is checked or unchecked.
  - handleStepTitleDoubleClick: Handles the double-click event on the step title, allowing it to be edited.
  - handleStepTitleBlur: Handles the blur event on the step title input, saving the new step title and exiting the editing mode.
  - handleKeyDown: Handles the keydown event on the step title input, checking if the Enter key is pressed to save the step title.
  - handleStepTitleChange: Handles the change event on the step title input, updating the newStepTitle state with the new value.
  - handleSaveStepTitle: Saves the updated step title to the protocolSelected state.
- The component renders different step definition components based on the step.type value. The appropriate component is chosen from the stepDefinition object, which maps step types to their corresponding components.

#### CommentStep

- It displays a description and provides a text input field for the user to enter and edit the comment. 
- The component uses an useEffect hook that runs when the component mounts. It checks if the definition prop has a property text and sets the text state to the value of definition.text if it exists.

#### HumanStep

- Within the component, there is a state variable text defined using the useState hook. It represents the text value of the instructions for the human step and is initially set to an empty string.
- The component uses an useEffect hook that runs when the component mounts. It checks if the definition prop has a property text and sets the text state to the value of definition.text if it exists.

#### WaitStep

- Within the component, there is a state variable time defined using the useState hook. It represents the duration of the wait step in seconds and is initially set to 0.
- The component uses an useEffect hook that runs when the component mounts. It checks if the definition prop has a property seconds and sets the time state to the value of definition.seconds if it exists.

### Other Protocol Panel Components

1. The "AddStepMenu" component is a dropdown menu that shows different menu items for adding steps. When selecting a menu item, the "handleAddStep" function is executed, passing the ID of the selected item.

2. The "PanelButtons" component displays two buttons: "Validate" and "Run". Clicking on these buttons triggers the "handleValidate" and "handleRun" functions, respectively. These functions make HTTP requests using the axios library and then display a notification using the "snackAlert" function.

3. The "ProtocolDetail" component displays details of a protocol, including the name, description, and a button to save the protocol. Clicking the save button triggers the "handleSaveProtocol" function.

4. The "ProtocolForm" component is a form used to create or edit a protocol. When submitting the form, the "handleSubmit" function is executed, which makes an HTTP request using the "createProtocol" function from the Redux store.

5. The "ProtocolList" component displays a list of protocols in a table. The protocols are passed as the "protocols" prop. The component also has functionality for selecting multiple protocols, exporting protocols, and deleting selected protocols.

6. The "ProtocolPanel" component represents a panel for managing protocols within a workspace, allowing users to view, create, edit, and delete protocols. The component takes in several props: workspace, snackAlert, and handleCloseAlert. 
- The component defines several functions:
  - handleDeleteSelectedSteps: Updates the protocolSelected state by removing the selected steps from the protocol's steps array.
  - handleBackAlert: Handles the action when the back button is clicked, closing an alert and showing a confirmation dialog to save the protocol before closing it.
  - saveProtocolAndNavigate: Dispatches an action to update the protocol and navigate to a new state.
  - notsaveProtocolAndNavigate: Sets the protocolSelected state to null and closes the alert.
  - handleProtocolCreated: Handles the action when a protocol is created, closing a popup and setting the protocolSelected state to the newly created protocol.
  - handleSaveProtocol: Handles the saving of a protocol by dispatching an action to update the protocol and displaying snack alerts based on the success or failure of the save operation.
  - handleAddStep: Adds a new step to the protocolSelected state.
  - handleRenderProtocol: Renders the protocol details and a list of step cards based on the protocolSelected state.
- The component also contains an useEffect hook that runs when the workspace or alert.open variables change. Inside the effect, it dispatches an action to get protocols based on the workspace.name value.
