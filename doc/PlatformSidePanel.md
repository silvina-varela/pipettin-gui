(WIP)

## Platform Side Panel

### PlatformPanel

- This component represents a panel that displays a list of platforms. It includes a header with the text "Platforms" and a list of PlatformAccordion components.
- Inside the Box component, there is a conditional rendering logic. If workspace.items is truthy (exists and is not null or undefined), it maps over the workspace.items array. For each item in the array, it renders a div element with a key attribute set to the index value. Inside this div, it renders the PlatformAccordion component, passing the platform, index, and items props.

### PlatformAccordion

- It receives props such as platform, index, and items. The component is responsible for rendering an accordion-style panel for a specific platform.
- Within the component, there are several state variables defined using the useState hook. These include isPlatformTitleEditing, isExpanded, platformTitle, and dispatch. The component also uses the useEffect hook to set the initial value of platformTitle based on the platform.name prop.
- The component contains event handlers and functions to handle platform title editing, saving platform title changes, handling changes to platform content, selecting a platform, expanding the accordion panel, and deleting a platform.

### PlatformContentAccordion

- It is a reusable component that renders a content item within an accordion layout. It provides functionality for editing the content title, volume, and tags, as well as expanding and collapsing the accordion.
- The component defines several state variables using the useState hook, including isExpanded, isEditingContentTitle, platformContentVolume, contentTitle, and contentSelected. These variables are used to manage the component's state and control its behavior.
- The component utilizes useEffect hooks to handle side effects. It updates the contentTitle state variable when the platformContentName prop changes and updates the contentSelected state variable when the selected prop changes.
- The Accordion component represents the main accordion container. It manages the expanded state using the isExpanded state variable. It also applies custom styling based on the contentSelected state variable.
- The AccordionSummary component represents the header section of the accordion. It displays the content title, which can be edited if isEditingContentTitle is true. The ExpandMoreIcon component is used to indicate the expand/collapse functionality.
- The AccordionDetails component contains the content details section of the accordion. It displays information such as the content index, position, volume, and tags. The Tags component is used to handle tags related to the content item.

### Tags

- This component is used to render a list of tags with the ability to add and delete tags dynamically.
- The component defines two state variables using the useState hook, tags and inputValue. tags holds the current list of tags, and inputValue holds the value of the input field for adding new tags.
- The renderTags function is called to generate the JSX for each tag in the tags state variable. It maps over the tags array and renders a Chip component for each tag. The Chip component displays the tag label and provides a delete button to remove the tag. The background color and text color of the Chip component are determined based on the tag's color using the colorHash and getLuma utility functions.
- The Autocomplete component is used to provide an input field for adding new tags. The onChange event handler is used to trigger the handleSaveTags function when a new tag is added. The renderInput function is used to render the input field, and the renderTags function is overridden to hide the default rendering of selected tags.