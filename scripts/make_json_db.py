import json
import os
import sys

def main(database_name, json_dir='.'):
    # List of JSON files to combine
    json_files = [
        'containers.json', 
        'hLprotocols.json', 
        'platforms.json', 
        'protocols.json', 
        'protocolTemplates.json', 
        'settings.json', 
        'tools.json', 
        'workspaces.json'
    ]

    # Dictionary to hold the new data
    new_data = {}

    # Loop through each JSON file
    for json_file in json_files:
        print("Processing " + json_file)
        # Extract the basename without the .json extension
        key = os.path.splitext(os.path.basename(json_file))[0]
        
        # Full path to the JSON file
        file_path = os.path.join(json_dir, json_file)

        # Read the content of the JSON file
        with open(file_path, 'r') as f:
            data = json.load(f)

        # Store the data in the dictionary with the appropriate key
        new_data[key] = data

    # Read the existing databases.json if it exists
    if os.path.exists('databases.json'):
        with open('databases.json', 'r') as f:
            combined_data = json.load(f)
    else:
        combined_data = {}

    # Update the database key with the new data
    combined_data[database_name] = new_data
    
    # Full path to the JSON file
    db_path = os.path.join(json_dir, 'databases.json')

    # Write the combined data to databases.json
    with open(db_path, 'w') as f:
        json.dump(combined_data, f, indent=4)

    print(f"Updated databases.json with the database '{database_name}' successfully.")

if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print("Usage: python scripts/make_json_db.py <database_name> [json_directory]")
        sys.exit(1)

    database_name = sys.argv[1]
    json_dir = sys.argv[2] if len(sys.argv) == 3 else '.'
    main(database_name, json_dir)
