from copy import deepcopy
from pprint import pprint
import json

# See:
# - https://www.slas.org/education/ansi-slas-microplate-standards/
# - https://gaudi.ch/PocketPCR/

# Standard 96-well plate.
OFFSET_X = 14.38
OFFSET_Y = 11.24

slots = []
slot = {
        "slotPosition": {
            "slotX": 14.38,
            "slotY": 11.24,
            "slotZ": 2
        },
        "slotName": "Slot1",
        "slotSize": 9,
        "slotHeight": 10,
        "containers": [
            {
                "containerID": "659e0111baff2512cee4ce98",
                "containerOffsetZ": 0,
            }
        ]
    }

# Left half of the hybrid plate
n_cols = int(12/2)
n_rows = 8
for row in range(n_rows):
    for col in range(n_cols):
        d = deepcopy(slot)
        d["slotName"] = f"Well{1+col+row*n_cols}/48"
        d["slotPosition"]["slotX"] = OFFSET_X+col*9
        d["slotPosition"]["slotY"] = OFFSET_Y+row*9
        slots.append(d)

# Right half of the hybrid plate
# OFFSET_X = 14.38 + 4.5 + (9.0*n_cols)
# OFFSET_Y = 11.24 + 4.5
# n_cols = int(6/2)
# n_rows = 4
# for row in range(n_rows):
#     for col in range(n_cols):
#         d = deepcopy(slot)
#         d["slotName"] = f"Well{1+col+row*n_cols}/24"
#         d["slotSize"] = 18
#         d["slotPosition"]["slotX"] = OFFSET_X+col*18
#         d["slotPosition"]["slotY"] = OFFSET_Y+row*18
#         slots.append(d)

with open('slot-coords-48.json', 'w', encoding ='utf8') as json_file:
    json.dump(slots, json_file, ensure_ascii = False, indent=2)

# From R:
# angle = 2*pi/5
# radius = 10
# offset_x = 20
# offset_y = 20

# d = data.frame(
#   x=offset_x+radius*sin(0:4 * angle),
#   y=offset_y+radius*cos(0:4 * angle)
# )
pentagon = [
    [20.00000, 30.00000],
    [29.51057, 23.09017],
    [25.87785, 11.90983],
    [14.12215, 11.90983],
    [10.48943, 23.09017]
]
slots = []

for vertex in pentagon:
    d = deepcopy(slot)
    d["slotName"] = f"Tube{1+len(slots)}"
    d["slotPosition"]["slotX"] = vertex[0]
    d["slotPosition"]["slotY"] = vertex[1]
    slots.append(d)

with open('slot-coords-pentagon.json', 'w', encoding ='utf8') as json_file:
    json.dump(slots, json_file, ensure_ascii = False, indent=2)
