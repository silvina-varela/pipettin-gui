"""Extract and save a thumbnail to an SVG file.

How to use the tool:

- Save the script to a file, e.g., save_svg.py.
- Run it from the command line, passing the workspace name as an argument:

```bash
python extract_workspace_thumbnail.py "MK3 Baseplate" api/src/db/defaults/workspaces.json
```

What the script does:

- Step 1: Opens and reads the workspaces.json file.
- Step 2: Searches for the workspace with the given name.
- Step 3: Extracts the thumbnail (serialized SVG string) from the workspace.
- Step 4: Saves the SVG string to a file named <workspace_name>.svg.

Example workspaces.json structure:
```json
[
  {
    "name": "MK3 Baseplate",
    "thumbnail": "<svg>...</svg>"
  },
  {
    "name": "Experiment",
    "thumbnail": "<svg>...</svg>"
  }
]
```

ChatGPT has helped: https://chatgpt.com/share/66edc7f0-12f0-800f-a8d6-fcf34886a609
"""

import json
import argparse
import sys

def save_svg_from_workspace(workspace_name: str, workspace_file: str = "workspaces.json"):
    """Extract and save a thumbnail to an SVG file

    Args:
        workspace_name (str): Name of the workspace in the 'workspaces.json' file.
        workspace_file (str): Path to the 'workspaces.json' file.
    """
    # Step 1: Load the serialized SVG from the specified JSON file
    try:
        with open(workspace_file, 'r') as json_file:
            data = json.load(json_file)
    except FileNotFoundError:
        print(f"Error: {workspace_file} file not found.")
        sys.exit(1)

    # Step 2: Find the workspace by name
    svg_string = None
    for workspace in data:
        if workspace.get("name") == workspace_name:
            svg_string = workspace.get("thumbnail")
            break

    if not svg_string:
        print(f"Error: Workspace '{workspace_name}' not found or no thumbnail available.")
        sys.exit(1)

    # Step 3: Write the SVG string to a .svg file
    output_filename = f"{workspace_name}.svg"
    with open(output_filename, 'w') as svg_file:
        svg_file.write(svg_string)

    print(f"SVG file saved successfully as '{output_filename}'.")


if __name__ == "__main__":
    # Set up argument parsing
    parser = argparse.ArgumentParser(description="Save SVG thumbnail from a workspace.")
    parser.add_argument('workspace_name', help="The name of the workspace to extract the SVG thumbnail from.")
    parser.add_argument(
        'workspace_file',
        default='workspaces.json',
        help="Path to the JSON file containing the workspace data (default: 'workspaces.json')."
    )

    args = parser.parse_args()

    # Call the function with the provided workspace name and file path
    save_svg_from_workspace(args.workspace_name, args.workspace_file)
