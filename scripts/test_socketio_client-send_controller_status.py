# A python script that:
# 
# 1) defines a socket.io client
# 2) connects to a remote socket.io server
# 3) posts "controller status" event, one without an ack function, the second with an ack function.
#
# Issue: https://gitlab.com/pipettin-bot/pipettin-gui/-/issues/39
# 
# Usage example: python test_socketio_client-send_controller_status.py
#
# The dependencies must be installed first.

import sys
import socketio
import threading
import json
import time
import traceback

# Define socket.io client and event handlers
sio = socketio.Client()

# Define threading events
connected = threading.Event()
response_received = threading.Event()

# Define "connect" event handler
@sio.event
def connect():
    """Handles the socketio "connect" event when the client successfully connects to the server."""
    print("Connected to server")
    connected.set()

# Define "disconnect" event handler
@sio.event
def disconnect():
    """Handles the socketio "disconnect" event when the client is disconnected from the server."""
    print("Disconnected from server")

# Define "event_ack_function" event handler
@sio.event
def event_ack_function(data=None):
    """Handles the "event_ack_function" event, which is emitted by the server to acknowledge receipt of
    the event sent by the client. The "data" parameter contains any data sent by the server
    as part of the acknowledgement."""
    print(f"Received acknowledgement from server with data: {data}")
    response_received.set()

# Connect to remote socket.io server
sio.connect("http://localhost:3333")

# Wait for connection to be established
connected.wait()

# Post event with command line argument data and set up callback
def on_event_ack(event_name: str, event_data: dict):
    """Function to be called in the background to emit the event and set up the acknowledgement
    callback function."""
    sio.emit(event_name, event_data, callback=event_ack_function)

# Possible status messages
status = ["OK", "WARN", "ERROR"]
i = 0
data = {
    "id": 123456,
    "method": "status",
    "data": {
        "controller_id": "piper_asdf1234",
        "status": status[i], # También puede ser: WARN ERROR
        "message": "description of what is going on"
    }
}

try:
    print(f"Sending controller_status event with data: {data}")
    sio.emit("controller_status", data)
    print("Event sent, waiting for response.")

    if response_received.wait(2):
        print("Server responded to message")
    else:
        print("Server did not respond to message")

    response_received.clear()
    i += 1

    print(f"Sending controller_status event with ack function and data: {data}")
    sio.emit("controller_status", data, callback=event_ack_function)
    print("Event sent, waiting for response.")

    if response_received.wait(2):
        print("Server responded to message")
    else:
        print("Server did not respond to message")
    response_received.clear()

except Exception as e:
    print(f"Error: {e}" + traceback.format_exc())
    # Disconnect from server and exit
    sio.disconnect()

sys.exit(0)
