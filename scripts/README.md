# Helper scripts

Miscelaneous scripts to interact with the frontend or backend.

You first need to:

- Install `python` version 3: <https://www.python.org/downloads/>
- Install `git`: <https://git-scm.com/downloads>
- Download and extract the project's repo: <https://gitlab.com/pipettin-bot/pipettin-gui>

Run all commands from the "scripts" directory of this project.

First create and activate a Python3 virtual environment.

> Note: the command to activate the environment may vary depending on your OS or shell, find yours [over here](https://docs.python.org/3/library/venv.html#how-venvs-work).

## Setup

### Create a new virtual environment

Create a new virtual environment (named ".venv") in the current directory.

```bash
python3 -m venv .venv # Linux
python -m venv .venv # Windows
```

### Activate the virtual environment

```bash
# This command may vary depending on your OS/shell.
source .venv/bin/activate # Linux
source .venv/Scripts/activate # Windows with Git Bash
source .venv/Scripts/Activate # Windows with PowerShell
```

> Note: if you face permissions issues with Windows' PowerShell see [this answer](https://superuser.com/a/106363) at SE.

### Install the python dependencies

#### Linux

```bash
# New GUI
pip3 install python-socketio python-engineio requests websocket-client eventlet requests # json ¿not available in mac?
pip3 install "python-socketio[client]"  # https://github.com/miguelgrinberg/python-socketio/issues/356

# # Old GUI's version of socketio
# pip3 install python-socketio==4.6.1 requests
```

#### Windows

```bash
# New GUI
pip install python-socketio python-engineio requests websocket-client eventlet requests
pip install "python-socketio[client]"
```

Reading material:

- [test_socketio_server-forward_all.py](./test_socketio_server-forward_all.py)
- <https://python-socketio.readthedocs.io/en/latest/server.html#creating-a-server-instance>
- <https://python-socketio.readthedocs.io/en/latest/server.html#catch-all-event-handlers>
- <https://python-socketio.readthedocs.io/en/latest/api.html#socketio.Server>

> **IMPORTANT:** Prior to executing any of the following scripts, ensure that the virtual environment is activated.

## Protocol handler

Script: [test_socketio_client-catch_all-piper.py](./test_socketio_client-catch_all-piper.py)

This script also catches all events, and:

- Implements prototype reports of protocol progress: <https://gitlab.com/pipettin-bot/pipettin-gui/-/issues/231#note_1699791648>
- Handles `protocol_template` event appropriately.

First, install the `piper`, `newt`, and `mix` packages from the main `pipettin-bot` repository.

Linux:

```bash
pip3 install git+https://gitlab.com/pipettin-bot/pipettin-mix.git # Mix (step sequence generator)
pip3 install git+https://gitlab.com/pipettin-bot/pipettin-piper.git # Piper (the controller)
pip3 install git+https://gitlab.com/pipettin-bot/pipettin-newt.git # Newt (object schemas and "API")
```

Windows:

```bash
pip install git+https://gitlab.com/pipettin-bot/pipettin-mix.git # Mix (step sequence generator)
pip install git+https://gitlab.com/pipettin-bot/pipettin-piper.git # Piper (the controller)
pip install git+https://gitlab.com/pipettin-bot/pipettin-newt.git # Newt (object schemas and "API")
```

Run the script (from the virtual environment):

Linux:

```bash
python3 test_socketio_client-catch_all-piper.py
```

Windows:

```bash
python test_socketio_client-catch_all-piper.py
```

## Event forwarding

Script: [test_socketio_server-forward_all.py](./test_socketio_server-forward_all.py)

Setup a Socket.io events server using `eventlet` that forwards all events it receives:

Linux:

```bash
python3 test_socketio_server-forward_all.py
```

Windows:

```bash
python test_socketio_server-forward_all.py
```

Useful to test the controller without the GUI.

## Event catcher

Script: [test_socketio_client-catch_all.py](./test_socketio_client-catch_all.py)

The script then runs until the server disconnects.

Run the script specifying the port number of the socket.io server:

Linux:

```bash
python3 test_socketio_client-catch_all.py 3333
```

Windows:

```bash
python test_socketio_client-catch_all.py 3333
```

The following alternative script catches all events and prints out the data it received. It also sends `tool_data` events every second, forever.

Linux:

```bash
python3 test_socketio_client-catch_all-spam_data.py 3333
```

Windows:

```bash
python test_socketio_client-catch_all-spam_data.py 3333
```

## Send events

In the example below, a "tool_data" event is sent to the application's socket.io server at `http://localhost:3333`.

Script:  [test_socketio_client-send_event.py](./test_socketio_client-send_event.py)

Usage:

Linux:

```bash
python3 test_socketio_client-send_event.py 'tool_data' '{"position": {"x": 25, "y": 100, "z": 501}}'
```

Windows:

```bash
python test_socketio_client-send_event.py 'tool_data' '{"position": {"x": 25, "y": 100, "z": 501}}'
```

Note that for this to work you will need the following python packages:

- `python-socketio` and `python-engineio`
  - Must be compatible with the version of socketio running on your server. Version compatibility table here: <https://pypi.org/project/python-socketio/>
- `requests`
- `json`

For the "old" GUI you'll need `python-socketio==4.6.1` (see install instructions above)

## Send status events

Status events are sent to the application's socket.io server at `http://localhost:3333`. Relevant issue: <https://gitlab.com/pipettin-bot/pipettin-gui/-/issues/39>

Script:  [test_socketio_client-send_status.py](./test_socketio_client-send_status.py)

Linux:

```bash
python3 test_socketio_client-send_status.py
```

Windows:

```bash
python test_socketio_client-send_status.py
```

## Database to JSON

This script exports all databases to JSON files, and produces a combined JSON file too.

It removes several fields, internal to MongoDB (`_id`, `__v`, `createdAt`, and `updatedAt`).

To use it, you'll need `mongoexport` and the `jq` utility (<https://jqlang.github.io/jq/>) installed in your system.

To update the defaults, run:

```sh
# First argument is the path to the "defaults", and the second is the "database name".
# The third argument is the DB name used in the combined the JSON file, defaulting to "pipettin".
./scripts/defaults_pretty_update.sh api/src/db/defaults/ pipettin
```

### Combiner script

This Python script combines multiple JSON files into a single JSON file called `databases.json`, organizing each file's content under a specified database name key.

### Usage

Runing the script:

```txt
$ python scripts/make_json_db.py <database_name> [json_directory]
```

- `<database_name>`: The name of the database key under which JSON files will be stored in `databases.json`.
- `[json_directory]` (optional): Directory containing the JSON files (default is the current directory).

### Example

Combine JSON files into a database named `pipettin` located in the defaults directory, saving the combination into a new 'databases.json' file within:

```sh
python scripts/make_json_db.py 'pipettin' api/src/db/defaults/
```

### Output

The script will generate or update `databases.json` in the directory of the JSON files, with the combined JSON data organized under the specified database name key.
