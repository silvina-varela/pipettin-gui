#!/bin/bash
# Usage:
# ./scripts/defaults_pretty_update.sh api/src/db/defaults/ pipettin pipettin

# Default values
DIRECTORY=${1:-.}
DATABASE=${2:-'pipettin'}
JSON_DATABASE_NAME=${3:-'pipettin'}

# Create directory if it doesn't exist
mkdir -p "$DIRECTORY"

# Function to export data from MongoDB
export_data() {
    local collection=$1 # Collection name in MongoDB
    local file_name=$2  # Base name for the JSON file.
    local output_file="$DIRECTORY/$file_name.json"
    mongoexport -d "$DATABASE" -c "$collection" --jsonArray | \
    jq '[ walk(if type == "object" then with_entries(select(.key | test("^(__v|_id|createdAt|updatedAt)$") | not)) else . end) ][0]' > "$output_file"
}

# Export data for each collection
export_data containers containers
export_data hLprotocols hLprotocols
export_data platforms platforms
export_data protocols protocols
export_data protocol_templates protocolTemplates
export_data settings settings
export_data tools tools
export_data workspaces workspaces

# Combine objects.
python3 scripts/make_json_db.py "$JSON_DATABASE_NAME" "$DIRECTORY"
