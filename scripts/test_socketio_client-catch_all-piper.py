import socketio
from pprint import pformat, pprint
import sys, traceback, time, tempfile, os, json

# Get command line arguments
if len(sys.argv) < 2:
    print("Usage:\n    python test_socketio_client-send_event.py [port_number]")
    sys.exit(1)

# Define socket.io client and event handlers
sio = socketio.Client()


# Third-party imports
import logging, json, traceback, re
from pprint import pformat, pprint

# Extra Pipettin modules
from piper.plugins.pcr_template import TemplateRouter
from piper.datatools.mongo import MongoObjects
import newt, mix  # Not used explicitly, just here to check.

@sio.event
def connect():
    print("Connected to server")

@sio.event
def disconnect():
    print("Disconnected from server")

@sio.on("message")
def handle_message(data):
    print("Received message:", pformat(data))

@sio.on("kill_commander")
def handle_message(data):
    print("Received kill event:", pformat(data))
    sio.emit("stop", "Received kill event")

pause_requested = False
@sio.on("pause_protocol")
def handle_message(data):
    pause_requested = True
    print("Received pause event:", pformat(data))

@sio.on("continue_protocol")
def handle_message(data):
    pause_requested = False
    print("Received continue event:", pformat(data))

@sio.on("run_protocol")
def handle_custom_event(data):
    print("Received 'run_protocol' event:", pformat(data))
    protocol_name = data.get("name")
    db_settings = data.get("settings").get("database")
    
    database_tools = MongoObjects(mongo_url='mongodb://' + db_settings.get("uri") + "/", 
                                  database_name=db_settings.get("name"))
    
    protocol, workspace, platforms_in_workspace = database_tools.getProtocolObjects(protocol_name)
    actions = protocol.get("actions")
  
    for i, action in enumerate(actions):
        # Prevents "Object of type ObjectId is not JSON serializable".
        # Caused by the id object being something weird: 'action': {'_id': ObjectId('6580f423d6b1737c89b61d32'), 'cmd': 'HOME'}
        del action['_id']  
        
        # Prepare data.
        data = {
            "protocol": protocol_name,
            "action_index": i, 
            "action": action,  # Excluded action content, it does not seem relevant.
            "status": ""
        }
        
        if pause_requested:
            data["status"] = "paused"
            print("Sending:")
            pprint(data)
            sio.emit("execution", data)
        while pause_requested:
            time.sleep(0.2)
        
        data["status"] = "running"
        print("Sending:")
        pprint(data)
        sio.emit("execution", data)
        
        time.sleep(2)
        
        data["status"] = "done"
        print("Sending:")
        pprint(data)
        sio.emit("execution", data)
        
        time.sleep(2)
    
    data["status"] = "error"
    print("Sending:")
    pprint(data)
    sio.emit("execution", data)

@sio.on("protocol_template")
def handle_custom_event(data):
    print("Received template event with input:\n", pformat(data))
    
    try:
        router = TemplateRouter(verbose=True)
        result = router.process_template(data)

        print("\nProcessed template event with output:\n" + pformat(result))
        sio.emit("alert", "Template event processed successfully.")
        
        file_handle, file_path = tempfile.mkstemp(prefix= "protocol_template" + "-", suffix=".json", dir=None)
        file = os.path.join(tempfile.gettempdir(), file_path)
        with open(file, 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)
        print(f"Wrote event data to: {file}")
    except Exception as e:
        msg = "Error processing template event, returning original data. Error message: " + traceback.format_exc()
        print(msg)
        sio.emit("alert", msg)
        result = data

    return result

@sio.on("custom_event")
def handle_custom_event(data):
    print("Received custom event:", pformat(data))

# https://python-socketio.readthedocs.io/en/latest/client.html#catch-all-event-handlers
@sio.on('*')
def catch_all(event, data):
    # A catch-all event handler receives the event name as a first argument.
    # The remaining arguments are the same as for a regular event handler.
    print(f"Received unhandled '{event}' event:", pformat(data))

# Connect to the Socket.IO server
sio.connect(f"http://localhost:{int(sys.argv[1])}")

# Wait for events to be handled
sio.wait()
