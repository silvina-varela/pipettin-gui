import socketio
from pprint import pformat
import sys, time, threading, tempfile, os, json

# Get command line arguments
if len(sys.argv) < 2:
    print("Usage:\n    python test_socketio_client-send_event.py [port_number]")
    sys.exit(1)

# Define socket.io client and event handlers
sio = socketio.Client()

# Define threading events
connected = threading.Event()

# Define "connect" event handler
@sio.event
def connect():
    """Handles the socketio "connect" event when the client successfully connects to the server."""
    print("Connected to server")
    connected.set()

@sio.event
def disconnect():
    print("Disconnected from server")

# https://python-socketio.readthedocs.io/en/latest/client.html#catch-all-event-handlers
@sio.on('*')
def catch_all(event, data):
    # A catch-all event handler receives the event name as a first argument.
    # The remaining arguments are the same as for a regular event handler.
    print(f"Received event '{event}' with data:\n", pformat(data))
    file_handle, file_path = tempfile.mkstemp(prefix= event + "-", suffix=".json", dir=None)
    file = os.path.join(tempfile.gettempdir(), file_path)
    with open(file, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
    print(f"Wrote event data to: {file}")
    return data

# Connect to the Socket.IO server
sio.connect(f"http://localhost:{int(sys.argv[1])}")

# Wait for connection to be established
connected.wait()

# Wait for events to be handled
print("Waiting until server disconnects")
sio.wait()
