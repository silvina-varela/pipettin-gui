import socketio
from pprint import pformat
import sys, time, threading

# Get command line arguments
if len(sys.argv) < 2:
    print("Usage:\n    python test_socketio_client-send_event.py [port_number]")
    sys.exit(1)

# Define socket.io client and event handlers
sio = socketio.Client()

# Define threading events
connected = threading.Event()

# Define "tool_position_ack" event handler
@sio.event
def tool_position_ack(data=None):
    """Handles the "tool_position_ack" event, which is emitted by the server to acknowledge receipt of
    the "tool_position" event sent by the client. The "data" parameter contains any data sent by the server
    as part of the acknowledgement."""
    print(f"Received acknowledgement from server with data: {data}")

# Send a tool data position every second, forever. Start this as a sio background task.
def tool_position_send():
    i = 1
    while True:
        tool_position_data = {"data": {"x": 5*i, "y": 2*i, "z": 3*i}}
        print("Sending tool data: " + str(tool_position_data))
        sio.emit("tool_data", tool_position_data, callback=tool_position_ack)
        i = (i + 1) % 20
        time.sleep(1)

# Define "connect" event handler
@sio.event
def connect():
    """Handles the socketio "connect" event when the client successfully connects to the server."""
    print("Connected to server")
    connected.set()

@sio.event
def disconnect():
    print("Disconnected from server")

@sio.on("message")
def handle_message(data):
    print("Received message:", pformat(data))

@sio.on("kill_commander")
def handle_message(data):
    print("Received kill event:", pformat(data))
    sio.emit("stop", "Received kill event")

@sio.on("p2g_command")
def handle_custom_event(data):
    print("Received run event:", pformat(data))

@sio.on("protocol_template")
def handle_custom_event(data):
    print("Received template event:", pformat(data))
    return data

@sio.on("custom_event")
def handle_custom_event(data):
    print("Received custom event:", pformat(data))

@sio.on("protocol_template")
def handle_custom_event(data):
    print("Received template event:", pformat(data))
    return data

# https://python-socketio.readthedocs.io/en/latest/client.html#catch-all-event-handlers
@sio.on('*')
def catch_all(event, data):
    # A catch-all event handler receives the event name as a first argument.
    # The remaining arguments are the same as for a regular event handler.
    print(f"Received unhandled '{event}' event:", pformat(data))

# Connect to the Socket.IO server
sio.connect(f"http://localhost:{int(sys.argv[1])}")

# Wait for connection to be established
connected.wait()

# Send a tool data position every second.
sio.start_background_task(tool_position_send)

# Wait for events to be handled
print("Waiting until server disconnects")
sio.wait()
