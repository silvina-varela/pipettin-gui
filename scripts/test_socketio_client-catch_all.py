import socketio
from pprint import pformat
import sys, time, threading

# Get command line arguments
if len(sys.argv) < 2:
    print("Usage:\n    python test_socketio_client-send_event.py [port_number]")
    sys.exit(1)

# Define socket.io client and event handlers
sio = socketio.Client()

# Define threading events
connected = threading.Event()

# Define "connect" event handler
@sio.event
def connect():
    """Handles the socketio "connect" event when the client successfully connects to the server."""
    print("Connected to server")
    connected.set()

@sio.event
def disconnect():
    print("Disconnected from server")

@sio.on("message")
def handle_message(data):
    print("Received message:", pformat(data))

@sio.on("kill_commander")
def handle_message(data):
    print("Received kill event:", pformat(data))
    sio.emit("stop", "Received kill event")

@sio.on("p2g_command")
def handle_custom_event(data):
    print("Received run event:", pformat(data))

@sio.on("protocol_template")
def handle_custom_event(data):
    print("Received template event:", pformat(data))
    return data

@sio.on("custom_event")
def handle_custom_event(data):
    print("Received custom event:", pformat(data))

@sio.on("protocol_template")
def handle_custom_event(data):
    print("Received template event:", pformat(data))
    return data

# https://python-socketio.readthedocs.io/en/latest/client.html#catch-all-event-handlers
@sio.on('*')
def catch_all(event, data):
    # A catch-all event handler receives the event name as a first argument.
    # The remaining arguments are the same as for a regular event handler.
    print(f"Received unhandled '{event}' event:", pformat(data))

# Connect to the Socket.IO server
sio.connect(f"http://localhost:{int(sys.argv[1])}")

# Wait for connection to be established
connected.wait()

# Wait for events to be handled
print("Waiting until server disconnects")
sio.wait()
