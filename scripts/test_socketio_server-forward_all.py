import socketio, eventlet

# Create a Socket.IO server instance
sio = socketio.Server(cors_allowed_origins="*")

@sio.on('*')
# Define a function to handle incoming events
def catch_all(event, sid, data):
    print("event: " + str(event))
    print("sid: " + str(sid))
    print("data: " + str(data))
    # Broadcast the received event to all connected clients
    sio.emit(event, data)

# Create a WSGI application to wrap the Socket.IO server
app = socketio.WSGIApp(sio)

if __name__ == '__main__':
    # Run the server on port 3333
    eventlet.wsgi.server(eventlet.listen(('127.0.0.1', 3333)), app)
