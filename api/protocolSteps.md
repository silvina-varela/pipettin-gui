The property "steps" of the collection hLProtocols is an array that contains objects. Each object represents a step.
Every step must have the following properties:

- order: a number that represents the position of the step in the sequence
- name: a string with the name of the step. Initially it might be a generic name, like "step1"
- type: it's the type of step and it accepts only the following strings: "TRANSFER", "WAIT", "HUMAN", "COMMENT", "MIX".
- definition: an object that contains the definition of the step. The data sent for this property depends on which step type was selected. 

#### TRANSFER STEP

The "definition" property of "TRANSFER" step should have the following properties:

```
{
    source: {
        item: STRING --> name of the platform
        by: STRING ('name', 'tag') --> defines how the platform content is going to be selected
        value: STRING --> name of the content or tag
        treatAs: STRING ('same', 'for_each')
        } 
    target: {
        item: STRING --> name of the platform
        by: STRING ('name', 'tag') --> defines how the platform content is going to be selected
        value: STRING --> name of the content or tag
        } 
    volume: {
        type: STRING ('fixed_each', 'fixed_total', 'for_each_target_tag')
        value: NUMBER
        tag: STRING --> name of the tag (only if 'for_each_target_tag' is chosen)
        }
    tip: {
        item: STRING --> name of tip rack
        discardItem: STRING --> name of bucket
        mode: STRING ('reuse', 'isolated_source_only', 'isolated_targets', 'reuse_same_source')
    }
    tool: STRING --> name of tool
}
```

#### WAIT STEP

The "definition" property of the "WAIT" step should have the following properties:

```
{
    seconds: NUMBER
}
```

#### HUMAN STEP and COMMENT STEP

The "definition" property of the "HUMAN" and "COMMENT" steps should have the following properties:

```
{
    text: STRING
}
```

#### MIX STEP

The "definition" property of the "MIX" step should have the following properties:

```
{
    target: {
        item: STRING --> platform name
        by: STRING ('tag' or 'name')
        value: STRING --> name of content or tag
        }
    mix: {
        type: STRING ('content' or 'tip')
        percentage: NUMBER (from 1 to 100)
        count: NUMBER
    }
    tip: {
        item: STRING --> name of tip rack
        discardItem: STRING --> name of bucket
        mode: STRING ('reuse', 'isolated_targets')
    }
    tool: STRING --> name of tool
}
```


