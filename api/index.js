const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const connectDB = require("./src/db/config.js");
const initializeSocketIO = require("./src/services/socketIOService");
const socketHandlerMiddleware = require("./src/middlewares/socketHandlerMiddleware");
const router = require("./src/routes/index.js");
const cors = require("cors");
const morgan = require("morgan");
const fs = require("fs");
const os = require("os");
const path = require("path");

// Added this to prevent resolving "127.0.0.1" to IPv6 first.
// See: https://github.com/nodejs/node/issues/40537
// import { setDefaultResultOrder } from "dns";
// setDefaultResultOrder("ipv4first");
var dns = require('dns');
dns.setDefaultResultOrder('ipv4first');

const app = express();

const data = JSON.parse(fs.readFileSync("../config.json", "utf8"));
const { PORT, HOST, UI_PORT, USE_CORS } = data;
const port = PORT || 3333;
const host = HOST || "localhost";
const ui_port = UI_PORT || 3000;

// Allow all by default.
let cors_set = true;

// Restrict if requested.
if(USE_CORS){
  // Get LAN IP addresses dynamically
  const networkInterfaces = os.networkInterfaces();
  const localIPs = Object.values(networkInterfaces)
    .flat()
    .filter((iface) => iface.family === 'IPv4' && !iface.internal) // Filter out internal (127.0.0.1) addresses
    .map((iface) => `http://${iface.address}:${ui_port}`);

  // Restricted CORS setup.
  // IMPORTANT: It is critical that the URLs do NOT have an "/" at the end, and start with 'http'. Otherwise they won't match anything.
  // const cors_hosts = ["*"]  // Unrestricted origin. This always works.
  const cors_hosts = [
    `http://localhost:${ui_port}`,
    `http://127.0.0.1:${ui_port}`,
    `http://[::1]:${ui_port}`,
    `http://${host}:${ui_port}`,
    ...localIPs, // Include all LAN IPs
  ]
  // Unique set of origins.
  cors_set = [...new Set(cors_hosts)];
  console.log("Allowing CORS: ", cors_set)
} else {
  // Skip override of "cors_set", defaulting to "true".
  console.log("CORS is disabled, allowing all origins.")
}

// Configure CORS with dynamic origins.
app.use(cors({
  origin: cors_set
}));

// Alternative unrestricted CORS setup.
//app.use(cors());

app.use(morgan("dev"));

app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));
app.use(bodyParser.json({ limit: "50mb" }));

// Create a new HTTP server
const server = http.createServer(app);

// Create a new Socket.IO server
const ioServer = initializeSocketIO(server);

app.use(socketHandlerMiddleware(ioServer));


/*
Wisdom from: <https://stackoverflow.com/a/53594774>
> The hostname argument is used in situations where a server has more than one network interface, 
> and you only want the server to listen on one of those interfaces (as opposed to the default, 
> which is to listen to all interfaces).
> For instance, if you want the server to be only accessible by clients running on the server itself,
> you make it listen on the loopback network interface, which has IP-address "127.0.0.1" or hostname "localhost":
>     server.listen(7000, "localhost")
>     server.listen(7000, "127.0.0.1")

Also, the reason why some CORS errors were prevented by listening in more interfaces,
was that at least one of the URLs of the backend matched the "origin" URL of the client.

Previously the server was configured to listen at "localhost" and the HOST address, if provided.
If the client was loaded from a URL other than these two, CORS errors would arise.
Due to changes in Node, sometimes this meant that IPv6 was used silently, generating CORS errors
even though the server was set to listen to "localhost" (i.e. it resolved to ::1 instead of 127.0.0.1).

server.listen(port, host, () => {
  console.log(`Running server on http://${host}:${port}`);
});

if (host !== "localhost") {
  // Also listen on a custom host if it was specified.
  server.listen(port, "localhost", () => {
    console.log(`Also running server on http://localhost:${port}`);
  });
}
*/

server.listen(port, () => {
    /*
    Bind the server to both IPv4 and IPv6 interfaces, by omitting the "host" parameter.
    
    If host is omitted, the server will accept connections on the unspecified IPv6 address (::) when
    IPv6 is available, or the unspecified IPv4 address (0.0.0.0) otherwise.
    
    In most operating systems, listening to the unspecified IPv6 address (::) may cause the net.Server
    to also listen on the unspecified IPv4 address (0.0.0.0).

    From: <https://nodejs.org/api/net.html#serverlistenport-host-backlog-callback>
    */
  console.log(`Backend server listening on all interfaces (IPv4 and IPv6) at port ${port}`);
});

connectDB();

// Serve the favicon
app.get("/favicon.ico", (req, res) => {
  res.sendFile(path.join(__dirname, "public/favicon.ico"));
});

app.use("/api", router);

// Serve static files for the HTML page
app.use(express.static("public"));

// Add a route to reset the backend
app.post("/reset", (req, res) => {
  console.log("Resetting backend...");
  res.json({ message: "The backend is restarting." });

  // Perform reset logic here
  setTimeout(() => {
    console.log("Backend reset complete.");
    process.exit(0); // Optionally restart the server (handled by systemd if configured)
  }, 1000);
});

app.on("error", (error) => {
  console.log(`[ERROR]: ${error}`);
});
