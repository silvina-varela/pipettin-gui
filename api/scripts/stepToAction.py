from actionBuilder import get_mix_action, get_pick_tip_action, get_wait_action, get_human_action, get_load_liquid_action, get_drop_liquid_action, get_discard_tip_action, get_comment_action, get_json_action

import re
from typing import Dict, Any

def compute_re_tag(definition: Dict[str, Any], source: Dict[str, Any]) -> str:
    """
    Computes the reTag for a given definition and source, replacing placeholders with actual values.

    Args:
        definition (Dict[str, Any]): The step definition containing the 'target' information.
        source (Dict[str, Any]): The source information including 'content' with 'name' and 'position'.

    Returns:
        str: The computed reTag value or an empty string if no reTag is defined.
    """
    # Check if the target has an 'addTag' property and its 'value' is defined.
    if 'addTag' in definition.get('target', {}) and definition['target']['addTag'].get('value'):
        new_tag = definition['target']['addTag']['value']
        
        # Replace placeholders in the tag value with actual source information.
        new_tag = re.sub(r'\{\{source\.name\}\}', source['content']['name'], new_tag)
        new_tag = re.sub(
            r'\{\{source\.position\}\}',
            f"{source['content']['position']['col']}:{source['content']['position']['row']}",
            new_tag
        )
        
        return new_tag
    
    # Return an empty string if no reTag is defined in the target.
    return ""

from typing import Dict, Any

def update_target_volume(target: Dict[str, Any], volume: float, current_workspace: Dict[str, Any]) -> None:
    """
    Updates the target volume in the current workspace.

    Args:
        target (Dict[str, Any]): The target object containing the item and content.
        volume (float): The volume to be updated.
        current_workspace (Dict[str, Any]): The current workspace containing items and their contents.
    """
    content = get_contents_in_workspace(
        current_workspace,
        target['item'],
        "name",
        target['content']['name']
    )
    if content and len(content) > 0:
        content[0]['content']['volume'] += volume

from typing import List, Dict, Any

def get_contents_in_workspace(
    workspace: Dict[str, Any],
    item_name: str,
    by: str,
    content_value: str
) -> List[Dict[str, Any]]:
    """
    Retrieves contents in the workspace based on item name, search criteria, and content value.

    Args:
        workspace (Dict[str, Any]): The workspace object containing items and their contents.
        item_name (str): The item name to search for.
        by (str): The search criteria (e.g., "name" or "tag").
        content_value (str): The content value to match.

    Returns:
        List[Dict[str, Any]]: A list of contents in the workspace matching the search criteria.
    """
    contents = []

    if workspace and 'items' in workspace and len(workspace['items']) > 0:
        for item in workspace['items']:
            if item['name'] == item_name or item_name == "":
                if item.get('content') and len(item['content']) > 0:
                    for content in item['content']:
                        if by == "name":
                            if content['name'] == content_value:
                                contents.append({
                                    'item': item['name'],
                                    'content': content
                                })
                        elif by == "tag":
                            if content_value in (content.get('tags') or []):
                                contents.append({
                                    'item': item['name'],
                                    'content': content
                                })

    return contents

from typing import List, Dict, Union

def get_available_source_and_update_it(
    sources: List[Dict[str, Any]],
    volume: float,
    multiplexor_or_nothing: Union[float, None]
) -> Union[Dict[str, Any], bool]:
    """
    Gets an available source and updates its volume in the workspace.

    Args:
        sources (List[Dict[str, Any]]): A list of source dictionaries containing 'content' with 'volume'.
        volume (float): The required volume to obtain.
        multiplexor_or_nothing (Union[float, None]): The multiplexor value or None if not used.

    Returns:
        Union[Dict[str, Any], bool]: A dictionary with 's' (source) and 'v' (volume) if a source is found,
                                      or False if no source is available.
    """
    i = 0
    while i < len(sources):
        source = sources[i]
        source_volume = source['content']['volume']
        
        if source_volume >= volume:
            source['content']['volume'] -= volume
            return {'s': source, 'v': volume}
        elif source_volume > 0:
            if multiplexor_or_nothing is not None:
                how_many = source_volume / multiplexor_or_nothing
                round_how_many = int(how_many)
                if round_how_many == 0:
                    sources.pop(i)
                else:
                    current_vol = multiplexor_or_nothing * round_how_many
                    source['content']['volume'] -= current_vol
                    return {'s': source, 'v': current_vol}
            else:
                current_vol = source_volume
                source['content']['volume'] = 0
                return {'s': source, 'v': current_vol}
        else:
            sources.pop(i)
    
    return False

from typing import Dict, Any, Optional

def get_item(name: str, workspace: Dict[str, Any]) -> Optional[Dict[str, Any]]:
    """
    Gets an item from the workspace based on its name.

    Args:
        name (str): The name of the item to search for.
        workspace (Dict[str, Any]): The workspace object containing items.

    Returns:
        Optional[Dict[str, Any]]: The item dictionary if found, or None if not found.
    """
    if workspace and 'items' in workspace and len(workspace['items']) > 0:
        item = next((el for el in workspace['items'] if el['name'] == name), None)
        return item
    return None

from typing import Dict, Any

def get_tip_volume(workspace: Dict[str, Any], item: str) -> float:
    """
    Gets the tip volume based on the workspace and item.

    Args:
        workspace (Dict[str, Any]): The workspace object containing items and their contents.
        item (str): The item name to search for.

    Returns:
        float: The tip volume, or 0 if not found or not a tip.
    """
    item_data = get_item(item, workspace)
    if item_data and 'content' in item_data and len(item_data['content']) > 0:
        content = item_data['content'][0]
        container_data = content.get('containerData', {})
        if container_data.get('type') == "tip" and container_data.get('maxVolume'):
            return min([container_data['maxVolume']])
    return 0.0

from typing import Dict, Any, List, Union

def build_simple_pipettin_step(
    step: Dict[str, Any],
    current_workspace: Dict[str, Any]
) -> List[Dict[str, Any]]:
    """
    Builds a simple transfer step based on the definition.

    Args:
        step (Dict[str, Any]): The step object containing the transfer definition.
        current_workspace (Dict[str, Any]): The current workspace object.

    Returns:
        List[Dict[str, Any]]: An array of actions for the step.
    """
    if (
        'treatAs' in step['definition'].get('source', {}) and
        step['definition']['source']['treatAs'] == "for_each"
    ):
        return build_simple_pipettin_step_treat_each(step, current_workspace)
    else:
        return build_simple_pipettin_step_treat_same(step, current_workspace)

def build_simple_pipettin_step_treat_same(
    step: Dict[str, Any],
    current_workspace: Dict[str, Any]
) -> List[Dict[str, Any]]:
    """
    Builds a simple pipetting step where the source is treated the same for each target.

    Args:
        step (Dict[str, Any]): The step object containing the transfer definition.
        current_workspace (Dict[str, Any]): The current workspace object.

    Returns:
        List[Dict[str, Any]]: An array of actions for the step.
    """
    actions = []

    # Retrieve the list of sources based on the step definition
    sources = get_contents_in_workspace(
        current_workspace,
        step['definition']['source']['item'],
        step['definition']['source']['by'],
        step['definition']['source']['value']
    )
    if not sources:
        raise ValueError(
            f"On step {step['order']}: Invalid source. Please select a valid source."
        )

    # Retrieve the list of targets based on the step definition
    targets = get_contents_in_workspace(
        current_workspace,
        step['definition']['target']['item'],
        step['definition']['target']['by'],
        step['definition']['target']['value']
    )
    if not targets:
        raise ValueError(
            f"On step {step['order']}: Invalid target. Please select a valid target."
        )

    # Determine the volume to transfer for each target
    volume_for_each_target = float(step['definition']['volume']['value'])  # fixed_each

    if step['definition']['volume']['type'] == "for_each_target_tag":
        # Calculate volume for each target based on the number of contents with the tags
        volume_multiplier = len(
            get_contents_in_workspace(
                current_workspace,
                "",
                "tag",
                step['definition']['volume']['tag']
            )
        )
        volume_for_each_target = volume_multiplier * step['definition']['volume']['value']  # for_each_target_tag
    elif step['definition']['volume']['type'] == "fixed_total":
        # Calculate volume per target when total volume is fixed
        volume_for_each_target = step['definition']['volume']['value'] / len(targets)  # fixed_total

    # Get the maximum volume that the tip can handle
    tip_volume = get_tip_volume(current_workspace, step['definition']['tip']['item'])

    if not tip_volume:
        raise ValueError(f"The tip rack used in step {step['order']} has no tips")

    # Calculate the number of travels needed for each target based on tip volume.
    travels_for_each_target = -(-volume_for_each_target // tip_volume)  # ceiling division

    if volume_for_each_target == 0:
        raise ValueError(f"On step {step['order']}: volume to transfer can't be 0.")

    # Handle pipetting based on tip mode and volume constraints
    if (
        step['definition']['tip']['mode'] in ["isolated_source_only", "reuse"] and
        volume_for_each_target < tip_volume
    ):
        # Handle the case when using "solated source only" or "always reuse" mode, 
        # and volume is less than the tip's max volume.
        
        # Determine how many targets can be fed with the available tip volume
        can_feed = int(tip_volume // volume_for_each_target)

        need_tip = True
        while targets:
            if need_tip:
                # Pick a new tip if needed
                actions.append(
                    get_pick_tip_action(step['definition']['tip']['item'], step['definition']['tool'])
                )
            if can_feed > len(targets):
                can_feed = len(targets)
            # Draw volume once for multiple dispenses.
            pick_volume = can_feed * volume_for_each_target
            using_source = get_available_source_and_update_it(
                sources, pick_volume, volume_for_each_target
            )
            if not using_source:
                print(step)
                raise ValueError(
                    f"There is no source that meets the required volume to pick from it in step {step['order']}"
                )
            actions.append(
                get_load_liquid_action(
                    using_source['s']['item'],
                    "name",
                    using_source['s']['content']['name'],
                    using_source['v']
                )
            )
            # Number of times a pipette will need to move or "travel" to
            # transfer the required volume from the source to the targets.
            travels = min(using_source['v'] / volume_for_each_target, len(targets))
            for _ in range(int(travels)):
                target = targets.pop()
                actions.append(
                    get_drop_liquid_action(
                        target['item'],
                        "name",
                        target['content']['name'],
                        volume_for_each_target
                    )
                )
                update_target_volume(target, volume_for_each_target, current_workspace)
            if step['definition']['tip']['mode'] == "isolated_source_only":
                # Discard tip before pipetting again from the source.
                actions.append(get_discard_tip_action(step['definition']['tip']['discardItem']))
                need_tip = True
            else:
                # Reuse the tip to draw from the source.
                need_tip = False
        if not need_tip:
            actions.append(get_discard_tip_action(step['definition']['tip']['discardItem']))

    else:
        # Handle cases when not using the "isolated source only" or "reuse" modes,
        # or when volume is greater than the tip's volume.
        need_tip = True
        for target in targets:
            transfered = 0
            for _ in range(int(travels_for_each_target)):
                if need_tip:
                    # Pick a new tip if needed
                    actions.append(
                        get_pick_tip_action(step['definition']['tip']['item'], step['definition']['tool'])
                    )
                volume_left = volume_for_each_target - transfered
                pick_volume = min(tip_volume, volume_left)
                using_source = get_available_source_and_update_it(sources, pick_volume)
                if not using_source:
                    raise ValueError(
                        f"On step {step['order']}: There is no source that meets the required volume to pick from it."
                    )
                actions.append(
                    get_load_liquid_action(
                        using_source['s']['item'],
                        "name",
                        using_source['s']['content']['name'],
                        using_source['v']
                    )
                )
                transfered += using_source['v']
                actions.append(
                    get_drop_liquid_action(
                        target['item'],
                        "name",
                        target['content']['name'],
                        using_source['v']
                    )
                )
                update_target_volume(target, using_source['v'], current_workspace)
                if step['definition']['tip']['mode'] in ["isolated_source_only", "isolated_targets"]:
                    # Discard tip after each target if isolated mode
                    actions.append(get_discard_tip_action(step['definition']['tip']['discardItem']))
                    need_tip = True
                else:
                    need_tip = False
        if step['definition']['tip']['mode'] == "reuse":
            # Drop the final tip if reusing
            actions.append(get_discard_tip_action(step['definition']['tip']['discardItem']))

    # Append a comment action to indicate the end of the step
    actions.append(get_comment_action(f"End step: {step['name']}"))

    # Add step ID to each action for tracking
    for action in actions:
        action['stepID'] = step['_id']

    return actions


def build_simple_pipettin_step_treat_each(
    step: Dict[str, Any],
    current_workspace: Dict[str, Any]
) -> List[Dict[str, Any]]:
    """
    Builds a pipetting step where each source is treated individually for each target.

    Args:
        step (Dict[str, Any]): The step object containing the transfer definition.
        current_workspace (Dict[str, Any]): The current workspace object.

    Returns:
        List[Dict[str, Any]]: An array of actions for the step.
    """
    actions = []
    print("for_each")

    sources = get_contents_in_workspace(
        current_workspace,
        step['definition']['source']['item'],
        step['definition']['source']['by'],
        step['definition']['source']['value']
    )

    if not sources:
        raise ValueError(
            f"On step {step['order']}: Invalid source. Please select a valid source."
        )

    targets = get_contents_in_workspace(
        current_workspace,
        step['definition']['target']['item'],
        step['definition']['target']['by'],
        step['definition']['target']['value']
    )

    if not targets:
        raise ValueError(
            f"On step {step['order']}: Invalid target. Please select a valid target (at least one target)."
        )

    if len(targets) != len(sources):
        raise ValueError(
            f"On step {step['order']}: Invalid target vs. source count. Detected sources must be equal to detected targets."
        )

    volume_for_each_target = step['definition']['volume']['value']  # fixed_each

    if step['definition']['volume']['type'] == "for_each_target_tag":
        volume_multiplier = len(
            get_contents_in_workspace(
                current_workspace,
                "",
                "tag",
                step['definition']['volume']['tag']
            )
        )
        volume_for_each_target = volume_multiplier * step['definition']['volume']['value']
    elif step['definition']['volume']['type'] == "fixed_total":
        volume_for_each_target = step['definition']['volume']['value'] / len(targets)

    tip_volume = get_tip_volume(current_workspace, step['definition']['tip']['item'])

    if not tip_volume:
        raise ValueError(
            f"On step {step['order']}: No max volume defined in tip rack. Please set a maxVolume value in the tip container."
        )

    travels_for_each_target = -(-volume_for_each_target // tip_volume)  # ceiling division
    print("==volumeForEachTarget==", volume_for_each_target)
    print("==travelsForEachTarget==", travels_for_each_target)

    if volume_for_each_target == 0:
        raise ValueError(f"On step {step['order']}: Volume to transfer can't be 0.")

    need_tip = True
    for i, source in enumerate(sources):
        transfered = 0
        for _ in range(int(travels_for_each_target)):
            if need_tip:
                actions.append(
                    get_pick_tip_action(step['definition']['tip']['item'], step['definition']['tool'])
                )
            volume_left = volume_for_each_target - transfered
            pick_volume = min(tip_volume, volume_left)
            print("volumeLeft", volume_left)
            print("pickVolume", pick_volume)
            using_source = get_available_source_and_update_it([source], pick_volume)
            if not using_source:
                raise ValueError(
                    f"On step {step['order']}: There is no source that meets the required volume to pick from it."
                )
            actions.append(
                get_load_liquid_action(
                    using_source['s']['item'],
                    "name",
                    using_source['s']['content']['name'],
                    using_source['v']
                )
            )
            transfered += using_source['v']
            re_tag = compute_re_tag(step['definition'], using_source['s'])
            actions.append(
                get_drop_liquid_action(
                    item_name=targets[i]['item'],
                    by="name",
                    value=targets[i]['content']['name'],
                    volume=using_source['v'],
                    re_tag=re_tag
                )
            )
            update_target_volume(targets[i], using_source['v'], current_workspace)
            if (
                step['definition']['tip']['mode'] in ["reuse_same_source", "reuse"]
            ):
                need_tip = False
            else:
                actions.append(get_discard_tip_action(step['definition']['tip']['discardItem']))
                need_tip = True
        if step['definition']['tip']['mode'] == "reuse_same_source":
            actions.append(get_discard_tip_action(step['definition']['tip']['discardItem']))
            need_tip = True

    if step['definition']['tip']['mode'] == "reuse":
        actions.append(get_discard_tip_action(step['definition']['tip']['discardItem']))

    actions.append(get_comment_action(f"End step: {step['name']}"))

    for action in actions:
        action['stepID'] = step['_id']

    return actions

def build_mix_step(
    step: Dict[str, Any],
    current_workspace: Dict[str, Any]
) -> List[Dict[str, Any]]:
    """
    Builds a mix step where each target is processed with mixing actions.

    Args:
        step (Dict[str, Any]): The step object containing the mix definition.
        current_workspace (Dict[str, Any]): The current workspace object.

    Returns:
        List[Dict[str, Any]]: An array of actions for the step.
    """
    actions = []

    targets = get_contents_in_workspace(
        current_workspace,
        step['definition']['target']['item'],
        step['definition']['target']['by'],
        step['definition']['target']['value']
    )
    if not targets:
        raise ValueError(
            f"On step {step['order']}: No matching targets. Please specify a valid target."
        )

    tip_volume = get_tip_volume(current_workspace, step['definition']['tip']['item'])
    if not tip_volume:
        raise ValueError(
            f"On step {step['order']}: No max volume defined in tip rack. Please set a maxVolume value in the tip container."
        )

    need_tip = True
    for target in targets:
        if need_tip:
            actions.append(
                get_pick_tip_action(step['definition']['tip']['item'], step['definition']['tool'])
            )
        actions.append(
            get_mix_action(
                target['item'],
                "name",
                target['content']['name'],
                step['definition']['mix']['percentage'],
                step['definition']['mix']['type'],
                step['definition']['mix']['count']
            )
        )
        if step['definition']['tip']['mode'] == "reuse":
            need_tip = False
        else:
            # Assumes isolated_targets
            need_tip = True
            actions.append(get_discard_tip_action(step['definition']['tip']['discardItem']))

    if step['definition']['tip']['mode'] == "reuse":
        actions.append(get_discard_tip_action(step['definition']['tip']['discardItem']))

    actions.append(get_comment_action(f"End step: {step['name']}"))

    for action in actions:
        action['stepID'] = step['_id']

    return actions

def build_wait_step(step: Dict[str, Any]) -> List[Dict[str, Any]]:
    """
    Builds a wait step that pauses for a specified number of seconds.

    Args:
        step (Dict[str, Any]): The step object containing the wait definition.

    Returns:
        List[Dict[str, Any]]: An array of actions for the step, including a wait action.
    """
    seconds = int(step['definition']['seconds'])
    wait_action = get_wait_action(seconds)
    wait_action['stepID'] = step['_id']
    return [wait_action]

def build_human_step(step: Dict[str, Any]) -> List[Dict[str, Any]]:
    """
    Builds a human step that triggers a human action with the specified text.

    Args:
        step (Dict[str, Any]): The step object containing the human step definition.

    Returns:
        List[Dict[str, Any]]: An array of actions for the step, including a human action.
    """
    human_action = get_human_action(step['definition']['text'])
    human_action['stepID'] = step['_id']
    return [human_action]

def build_comment_step(step: Dict[str, Any]) -> List[Dict[str, Any]]:
    """
    Builds a comment step that triggers a comment action with the specified text.

    Args:
        step (Dict[str, Any]): The step object containing the comment step definition.

    Returns:
        List[Dict[str, Any]]: An array of actions for the step, including a comment action.
    """
    comment_action = get_comment_action(step['definition']['text'])
    comment_action['stepID'] = step['_id']
    return [comment_action]

def build_json_step(step: Dict[str, Any]) -> List[Dict[str, Any]]:
    """
    Builds a JSON step that triggers a JSON action with the specified definition.

    Args:
        step (Dict[str, Any]): The step object containing the JSON step definition.

    Returns:
        List[Dict[str, Any]]: An array of actions for the step, including JSON actions.
    """
    if isinstance(step['definition'], list):
        actions = [
            {**get_json_action(element), 'stepID': step['_id']} 
            for element in step['definition']
        ]
    elif isinstance(step['definition'], dict):
        actions = [{**get_json_action(step['definition']), 'stepID': step['_id']}]
    else:
        actions = []
    
    return actions

# Define the stepToAction dictionary
step_to_action = {
    "TRANSFER": build_simple_pipettin_step,
    "WAIT": build_wait_step,
    "HUMAN": build_human_step,
    "COMMENT": build_comment_step,
    "MIX": build_mix_step,
    "JSON": build_json_step
}
