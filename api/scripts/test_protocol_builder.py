import json
import pytest
from protocolBuilder import protocol_builder
from pprint import pprint
pprint
@pytest.fixture
def load_json_data():
    def _load_json(file_path):
        with open(file_path) as file:
            return json.load(file)
    return _load_json

def test_protocol_builder(load_json_data):
    # Load test data from JSON files
    workspaces = load_json_data("../src/db/defaults/workspaces.json")
    protocols = load_json_data("../src/db/defaults/hLprotocols.json")

    workspace = next(w for w in workspaces if w["name"] == "MK3 Baseplate")
    protocol = next(p for p in protocols if p["workspace"] == "MK3 Baseplate")

    # Build the actions using the protocol_builder function
    protocol = protocol_builder(protocol, workspace)
    
    # Assertions (if any)
    assert isinstance(protocol, dict)
    assert "actions" in protocol
    
    assert isinstance(protocol["actions"], list), "Actions should be a list"
    assert len(protocol["actions"]) > 0, "Actions list should not be empty"
    
    # Save the result to a JSON file in pretty format
    with open('actions_result.json', 'w') as f:
        json.dump(protocol["actions"], f, indent=4)
