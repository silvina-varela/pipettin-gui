from typing import List, Dict, Any

def validate_protocol_step(steps: List[Dict[str, Any]]) -> None:
    """
    Validates a list of protocol steps to ensure each step has the correct properties.

    Args:
        steps (List[Dict[str, Any]]): A list of step dictionaries to be validated.

    Raises:
        ValueError: If any step is missing required properties or has invalid values.
    """
    for step in steps:
        if not isinstance(step.get('definition'), dict):
            raise ValueError(f"On step {step['order']}: Please define the step.")

        step_type = step['type']
        definition = step['definition']

        if step_type == 'TRANSFER':
            transfer_keys = ["source", "target", "volume", "tip"]
            source_and_target_keys = ["item", "by", "value"]
            volume_keys = ["type", "value"]
            tip_keys = ["item", "discardItem", "mode"]

            for key in transfer_keys:
                if key not in definition:
                    raise ValueError(f"On step {step['order']}: step is missing '{key}' definition")
                
                if key == "source":
                    for source_key in source_and_target_keys:
                        if source_key not in definition['source']:
                            props = {
                                "item": "platform",
                                "by": "name/tag",
                                "value": "name or tag"
                            }
                            raise ValueError(f"Step {step['order']} is missing '{props[source_key]}' selection in 'source'")
                
                if key == "target":
                    for target_key in source_and_target_keys:
                        if target_key not in definition['target']:
                            props = {
                                "item": "platform",
                                "by": "name/tag",
                                "value": "name or tag"
                            }
                            raise ValueError(f"Step {step['order']} is missing '{props[target_key]}' selection in 'target'")

                if key == "volume":
                    for volume_key in volume_keys:
                        if volume_key not in definition['volume']:
                            props = {
                                "type": "type",
                                "value": "value"
                            }
                            raise ValueError(f"Step {step['order']} is missing '{props[volume_key]}' selection in 'volume'")
                        if volume_key == "value" and definition['volume']['value'] == 0:
                            raise ValueError(f"On step {step['order']}: volume value can't be 0.")
                        if definition['volume']['type'] == "for_each_target_tag" and "tag" not in definition['volume']:
                            raise ValueError(f"Step {step['order']} is missing 'tag' selection in 'volume'")

                if key == "tip":
                    for tip_key in tip_keys:
                        props = {
                            "item": "tip rack",
                            "discardItem": "discard item",
                            "mode": "behavior"
                        }
                        if tip_key not in definition['tip'] or not definition['tip'][tip_key]:
                            raise ValueError(f"Step {step['order']} is missing '{props[tip_key]}' selection")

            if (definition['source'].get('item') == definition['target'].get('item') and
                definition['source'].get('value') == definition['target'].get('value')):
                raise ValueError(f"On step {step['order']}: source and target can't be the same")

        elif step_type == 'WAIT':
            if not definition.get('seconds') or definition['seconds'] == 0:
                raise ValueError(f"On step {step['order']}: Please define the step.")

        elif step_type in {'COMMENT', 'HUMAN'}:
            if not definition.get('text'):
                raise ValueError(f"On step {step['order']}: Please define the step.")

        elif step_type == 'JSON':
            if not definition:
                raise ValueError(f"On step {step['order']}: Please define the step.")

        elif step_type == 'MIX':
            mix_keys = ["target", "mix", "tip"]
            target_keys = ["item", "by", "value"]
            mixin_mix_keys = ["type", "percentage", "count"]
            tipin_mix_key = ["item", "discardItem", "mode"]

            for key in mix_keys:
                if key not in definition:
                    raise ValueError(f"On step {step['order']}: step is missing '{key}' definition")

                if key == "target":
                    for target_key in target_keys:
                        props = {
                            "item": "platform",
                            "by": "name/tag",
                            "value": "name or tag"
                        }
                        if target_key not in definition['target']:
                            raise ValueError(f"Step {step['order']} is missing '{props[target_key]}' selection in 'target'")

                if key == "mix":
                    for mix_key in mixin_mix_keys:
                        if mix_key not in definition['mix']:
                            raise ValueError(f"Step {step['order']} is missing '{mix_key}' definition in 'mix'")
                        if mix_key == "percentage" and definition['mix']['percentage'] == 0:
                            raise ValueError(f"On step {step['order']}: mix percentage can't be 0")
                        if mix_key == "count" and definition['mix']['count'] == 0:
                            raise ValueError(f"On step {step['order']}: the number of times the content is mixed can't be 0")

                if key == "tip":
                    for tip_key in tipin_mix_key:
                        props = {
                            "item": "tip rack",
                            "discardItem": "discard item",
                            "mode": "behavior"
                        }
                        if tip_key not in definition['tip'] or not definition['tip'][tip_key]:
                            raise ValueError(f"Step {step['order']} is missing '{props[tip_key]}' selection")

        else:
            # Handle unsupported step types if needed
            pass
