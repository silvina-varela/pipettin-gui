from typing import Dict, Optional, Any


def get_mix_action(item_name: str, by: str, value: str, percentage: int, of: str, count: int) -> Dict[str, Any]:
    """
    Generates a "MIX" action object for a protocol.

    Args:
        item_name (str): The name of the item to be mixed.
        by (str): The method used for selecting the item (e.g., "name" or "tag").
        value (str): The value used for selecting the item.
        percentage (int): The percentage for the mix action.
        of (str): A description of the mix action.
        count (int): The count for the mix action.

    Returns:
        Dict[str, Any]: The "MIX" action object.
    """
    return {
        'cmd': 'MIX',
        'args': {
            'item': item_name,
            'selector': {
                'by': by,
                'value': value,
            },
            'percentage': percentage,
            'of': of,
            'count': count,
        }
    }


def get_pick_tip_action(item_name: str, tool: str) -> Dict[str, Any]:
    """
    Generates a "PICK_TIP" action object for a protocol.

    Args:
        item_name (str): The name of the item associated with the tip.
        tool (str): The tool to be used for picking the tip.

    Returns:
        Dict[str, Any]: The "PICK_TIP" action object.
    """
    return {
        'cmd': 'PICK_TIP',
        'args': {
            'item': item_name,
            'tool': tool,
        }
    }


def get_wait_action(seconds: int) -> Dict[str, Any]:
    """
    Generates a "WAIT" action object for a protocol.

    Args:
        seconds (int): The number of seconds to wait.

    Returns:
        Dict[str, Any]: The "WAIT" action object.
    """
    return {
        'cmd': 'WAIT',
        'args': {
            'seconds': seconds,
        }
    }


def get_human_action(text: str) -> Dict[str, Any]:
    """
    Generates a "HUMAN" action object for a protocol.

    Args:
        text (str): The text to be used in the human action.

    Returns:
        Dict[str, Any]: The "HUMAN" action object.
    """
    return {
        'cmd': 'HUMAN',
        'args': {
            'text': text,
        }
    }


def get_load_liquid_action(item_name: str, by: str, value: str, volume: float) -> Dict[str, Any]:
    """
    Generates a "LOAD_LIQUID" action object for a protocol.

    Args:
        item_name (str): The name of the item associated with the liquid.
        by (str): The method used for selecting the item (e.g., "name" or "tag").
        value (str): The value used for selecting the item.
        volume (float): The volume of the liquid to be loaded.

    Returns:
        Dict[str, Any]: The "LOAD_LIQUID" action object.
    """
    return {
        'cmd': 'LOAD_LIQUID',
        'args': {
            'item': item_name,
            'selector': {
                'by': by,
                'value': value,
            },
            'volume': volume,
        }
    }


def get_drop_liquid_action(item_name: str, by: str, value: str, volume: float, re_tag: Optional[str] = None) -> Dict[str, Any]:
    """
    Generates a "DROP_LIQUID" action object for a protocol, with an optional reTag parameter.

    Args:
        item_name (str): The name of the item associated with the liquid.
        by (str): The method used for selecting the item (e.g., "name" or "tag").
        value (str): The value used for selecting the item.
        volume (float): The volume of the liquid to be dropped.
        re_tag (Optional[str]): An optional reTag value to be applied to the dropped liquid.

    Returns:
        Dict[str, Any]: The "DROP_LIQUID" action object, possibly including an 'addTag' property.
    """
    action = {
        'cmd': 'DROP_LIQUID',
        'args': {
            'item': item_name,
            'selector': {
                'by': by,
                'value': value,
            },
            'volume': volume,
        }
    }
    if re_tag:
        action['args']['addTag'] = re_tag
    return action


def get_discard_tip_action(item_name: str) -> Dict[str, Any]:
    """
    Generates a "DISCARD_TIP" action object for a protocol.

    Args:
        item_name (str): The name of the item associated with the tip.

    Returns:
        Dict[str, Any]: The "DISCARD_TIP" action object.
    """
    return {
        'cmd': 'DISCARD_TIP',
        'args': {
            'item': item_name,
        }
    }


def get_comment_action(text: str) -> Dict[str, Any]:
    """
    Generates a "COMMENT" action object for a protocol.

    Args:
        text (str): The text to be used in the comment action.

    Returns:
        Dict[str, Any]: The "COMMENT" action object.
    """
    return {
        'cmd': 'COMMENT',
        'args': {
            'text': text,
        }
    }


def get_json_action(json: Dict[str, Any]) -> Dict[str, Any]:
    """
    Generates a "JSON" action object for a protocol.

    Args:
        json (Dict[str, Any]): The JSON data to be included in the action.

    Returns:
        Dict[str, Any]: The "JSON" action object.
    """
    return {
        'cmd': json.get('cmd', 'JSON'),
        'args': json,
    }


def get_home_action() -> Dict[str, Any]:
    """
    Generates a "HOME" action object for a protocol.

    Returns:
        Dict[str, Any]: The "HOME" action object.
    """
    return {
        'cmd': 'HOME',
    }


action_builder = {
    'get_mix_action': get_mix_action,
    'get_pick_tip_action': get_pick_tip_action,
    'get_wait_action': get_wait_action,
    'get_human_action': get_human_action,
    'get_load_liquid_action': get_load_liquid_action,
    'get_drop_liquid_action': get_drop_liquid_action,
    'get_discard_tip_action': get_discard_tip_action,
    'get_comment_action': get_comment_action,
    'get_home_action': get_home_action,
    'get_json_action': get_json_action,
}
