from typing import List, Dict

def get_unique_name(collection: List[Dict[str, str]], file_name: str, index: int = 0) -> str:
    """
    Generates a unique name by appending an index to the base file name if necessary.

    Args:
        collection (List[Dict[str, str]]): A list of dictionaries, each containing a 'name' key.
        file_name (str): The base name to be used.
        index (int): The starting index to append if the base name is not unique. Defaults to 0.

    Returns:
        str: A unique name that does not conflict with existing names in the collection.
    """
    check_name = file_name
    ext = ""
    if index > 0:

        # Generate new name with index
        check_name = f"{file_name} ({index}){ext}"

    name_exists = any(item['name'] == check_name for item in collection)
    if name_exists:
        return get_unique_name(collection, file_name, index + 1)
    return check_name
