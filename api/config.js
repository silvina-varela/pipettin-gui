const config = {
  protocol2gcodePath: "../protocol2gcode/commander.py",
  communicationMode: "ws",
  tools: [
    {
      "name": "P200",
      "defaultMaxVolume": 200
    },
    {
      "name": "P20",
      "defaultMaxVolume": 20
    }
  ]
}

module.exports = { config }
