# Default document objects

Use `npm run seed` from the root directory to insert them into the database.

Use `mongodump -d <pipettin2> -o <my_backup_folder>` to backup a database named "pipettin2" to a backup folder called `my_backup_folder` (adjust names to your need).

Read the repo's [docs](../../../../doc) for more info, especially [Defaults.md](../../../../doc/Defaults.md).

Utility commands to pretty-format exports:

```sh
python -m json.tool Workspaces.json > workspaces.json && rm Workspaces.json
python -m json.tool Platforms.json > platforms.json && rm Platforms.json
python -m json.tool Containers.json > containers.json && rm Containers.json
python -m json.tool Protocols.json > hLprotocols.json && rm Protocols.json
python -m json.tool Tools.json > tools.json && rm Tools.json
python -m json.tool "Protocol Templates.json" > protocolTemplates.json && rm "Protocol Templates.json"

python3 scripts/make_json_db.py 'pipettin' api/src/db/defaults/
```

Using `jq` will also give nice results, and cleans up some keys:

```bash
cat Tools.json | jq '[ walk(if type == "object" then with_entries(select(.key | test("^(__v|_id|createdAt|updatedAt)$") | not)) else . end) ][0]' > tools.json
```
