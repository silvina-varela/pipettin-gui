const mongoose = require('mongoose');
const seedDefaultSettings = require('./seedDefaultSettings.js');
const setEnvValueService = require('../services/setEnvValueService.js');
require("dotenv").config({ path: "../.env" });

const { DATABASE_NAME, DATABASE_URI } = process.env;

const default_db = "pipettin"
const default_uri = "127.0.0.1"

const connectDB = async () => {
  
  // Create ".env" file if either parameter was not provided.
  if(!DATABASE_NAME || !DATABASE_URI ){
    console.log("Create .env")
    await setEnvValueService("DATABASE_NAME", default_db);
    await setEnvValueService("DATABASE_URI", default_uri);
  }

  // Set default values.
  const name = DATABASE_NAME || default_db
  const uri = DATABASE_URI || default_uri

  // Error handler.
  mongoose.connection.on('error', (err) => {
    console.error.bind(console, 'Connection error:')
  });
  
  // ???
  mongoose.set("strictQuery", false);

  // Connect to MongoDB.
  mongoose.connect('mongodb://' + uri + '/' + name, { useNewUrlParser: true, useUnifiedTopology: true });    
  
  // Open connection handler.
  mongoose.connection.once('open', async () => {
    console.log(`MongoDB connected on port ${mongoose.connection.port} and database ${mongoose.connection.name} (connectDB)`);
    // Seed defaults.
    await seedDefaultSettings();        
  });
}

module.exports = connectDB