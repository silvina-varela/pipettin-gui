const fs = require("fs");

// Models
const Settings = require("./models/settingsModel");

// Path where files are stored
const jsonPath = `${__dirname}/defaults`;

// Connection between each file and model
const collections = [
  { name: "settings", file: "settings.json", model: Settings },
];

async function seedDefaultSettings(database) {
  try {
    // Populate each collection.
    for (const collectionInfo of collections) {
      const { name, file, model } = collectionInfo;

      const count = await model.countDocuments();
      if (count > 0) {
        console.log(`Collection '${name}' is not empty. Skipping seed...`);
        continue;
      }

      // Process each json file
      const filePath = `${jsonPath}/${file}`;
      const data = JSON.parse(fs.readFileSync(filePath, "utf8"));

      // Seed data
      await model.insertMany(data);
      console.log(`Seeded data from '${file}' into collection '${name}'`);
    }

    // Update database name in settings if provided.
    if (database) {
      await Settings.findOneAndUpdate({}, { database: database });
      console.log("Updated database settings");
    }
    
    console.log("Seed default data operation completed successfully.");
  } catch (err) {
    console.error("Error seeding default settings:", err);
    throw new Error(err);
  }
}

module.exports = seedDefaultSettings;
