const mongoose = require("mongoose");
const Protocol = require("./protocolModel");

const hLprotocolsSchema = mongoose.Schema(
  {
    name: {
      type: String,
      set: function(name) {
        this._previousName = this.name;
        return name;
      },
      required: [true, "Please enter a name for the protocol"],
    },
    description: {
      type: String,
    },
    workspace: {
      // workspace name
      type: String,
    },
    template: {
      type: String,
    },
    templateDefinition: { // template definition for the controller
      type: {},
    },
    templateFields: {
      type: {}
    },
    steps: [
      {
        order: Number,
        name: String,
        type: {
          type: String,
          enum: ["TRANSFER", "WAIT", "HUMAN", "COMMENT", "MIX", "JSON"],
        },
        definition: {
          // objeto con diferentes propiedades según el step type
          //TODO: add schemas for each step type
          type: {},
        },
      },
    ],
  },
  {
    timestamps: true,
    strictQuery: true,
  }
);

// Middleware to update hLprotocol name in linked midlevel protocols
hLprotocolsSchema.pre('save', async function (next) {
  if (this.isModified('name')) {
    let previousName = this._previousName;
    try{
      await Protocol.updateMany({ hLprotocol: previousName, workspace: this.workspace }, { hLprotocol: this.name });
    } catch(error){
      console.log(error)
    }
    return next();
  }
  next();
});

const HlProtocol = mongoose.model(
  "hLprotocols",
  hLprotocolsSchema,
  "hLprotocols"
);

module.exports = HlProtocol;
