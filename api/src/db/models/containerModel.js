const mongoose = require("mongoose");

const containerSchema = mongoose.Schema(
  {
    name: {
      type: String,
      set: function (name) {
        this._previousName = this.name;
        return name;
      },
      required: [true, "Please enter a name for the container"],
      unique: [true, "Duplicate names are not allowed"],
    },
    description: {
      type: String,
    },
    type: {
      type: String,
      enum: ["tube", "tip", "well"],
      required: [true, "Please insert a container type"],
    },
    length: {
      type: Number,
    },
    maxVolume: {
      type: Number,      
    }, 
    activeHeight: {
      type: Number,
    },
    // These values will be calculated by the controller
    // topPosition: {
    //     type: Number,
    //   },
    // bottomPosition: {
    //     type: Number,
    //   },
  },
  {
    timestamps: true,
    strictQuery: true,
  }
);

// Middleware to update container name in linked collections (platforms)
containerSchema.pre("save", async function (next) {
  const Platform = require("./platformModel");
  const Workspace = require("./workspaceModel");
  if (this.isModified("name") && !this.isNew) {
    let previousName = this._previousName;
    try {
      await Platform.updateMany(
        {
          $or: [
            { "containers.container": previousName },
            { "slots.containers.container": previousName },
          ],
        },
        {
          $set: {
            "containers.$[selectedContainer].container": this.name,
            "slots.$[].containers.$[selectedSlotContainer].container":
              this.name,
          },
        },
        {
          arrayFilters: [
            { "selectedContainer.container": previousName },
            { "selectedSlotContainer.container": previousName }, // Filtering containers within slots
          ],
        }
      );
      await Workspace.updateMany(
        { "items.content.container": previousName },
        {
          $set: {
            "items.$[].content.$[contentToUpdate].container": this.name,
          },
        },
        {
          arrayFilters: [
            { "contentToUpdate.container": previousName },
          ],
        }
      );
    } catch (error) {
      console.log(error);
    }
    return next();
  } 
  next();
});

const Container = mongoose.model("container", containerSchema);

module.exports = Container;
