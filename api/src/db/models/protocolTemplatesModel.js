const mongoose = require("mongoose");

const protocolTemplateSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Please enter a name for the template"],
    },
    description: {
      type: String,
    },
    fields: {
      type: Array,
      required: [true, "You need to specify at least one field"]
    },
  },
  {
    timestamps: true,
    strictQuery: true,
  }
);

const Template = mongoose.model("protocol_templates", protocolTemplateSchema);

module.exports = Template;
