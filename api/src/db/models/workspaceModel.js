const mongoose = require('mongoose')

const workspaceSchema = mongoose.Schema(
  {
    name: {
      type: String,
      set: function(name) {
        this._previousName = this.name;
        return name;
      },
      required: [true, "Workspace name is required"],
      unique: [true, "Duplicate names are not allowed"],
    },
    description: String,
    width: {
      type: Number,
      default: 0
    },
    length: {
      type: Number,
      default: 0
    },
    height: {
      type: Number,
      default: 0
    },
    padding: {
      top: {
        type: Number,
        default: 0
      },
      bottom: {
        type: Number,
        default: 0
      },
      left: {
        type: Number,
        default: 0
      },
      right: {
        type: Number,
        default: 0
      },
    },
    items: [
      {
        platform: String, // platform name from Platform collection
        name: String, // platform name given inside workspace
        snappedAnchor: {
          type: String,
          default: null
        },
        position: {
          x: {
            type: Number,
            default: 0
          },
          y: {
            type: Number,
            default: 0
          },
          z: {
            type: Number,
            default: 0
          },
        },
        content: [
          {
            container: String,
            // type: {type: String}, //borrar
            index: Number,
            name: String,
            tags: [String],
            position: { col: Number, row: Number, x: Number, y: Number, z: Number },
            // maxVolume: Number, //borrar
            volume: Number,
          },
        ],
        locked: Boolean,
      },
    ],
    thumbnail: String,
  },
  {
    timestamps: true,
  }
);

// Middleware to update workspace name in linked midlevel protocols and high level protocols
workspaceSchema.pre('save', async function (next) {
  if (this.isModified('name') && !this.isNew) {
    const HlProtocol = require("./hLprotocolModel");
    const Protocol = require("./protocolModel.js");
    // Update related documents in other collections
    let previousName = this._previousName;
    try{
      await HlProtocol.updateMany({ workspace: previousName }, { workspace: this.name });
      await Protocol.updateMany({ workspace: previousName }, { workspace: this.name });
    } catch(error){
      console.log(error)
    }
    // Continue with the save operation
    return next();
  }
  next();
});

const Workspace = mongoose.model("workspaces", workspaceSchema);

module.exports = Workspace;
