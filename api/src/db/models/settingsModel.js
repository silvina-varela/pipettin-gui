const mongoose = require("mongoose");

const settingsSchema = mongoose.Schema(
    {
        workspace: {
            defaultWidth: Number,
            defaultLength: Number,
            defaultHeight: Number,
            zoom: Number,
            roundUp: Number,
            incrementPosition: Number,
            panelWidth: Number,
            defaultPadding: {
                top: Number,
                bottom: Number,
                left: Number,
                right: Number,
            }
        },
        database: {
            uri: String,
            name: String,
        },
        controller: {
            joystickUrl: String,
            helpLink: String,
            configuration: Object
            
        },
    },
    {
        timestamps: true,
        strictQuery: true,
    }
);

const Settings = mongoose.model("settings", settingsSchema);

module.exports = Settings;
