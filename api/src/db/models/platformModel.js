const mongoose = require("mongoose");
const anchorSchema = require("./platformSchemas/anchorSchema");
const bucketSchema = require("./platformSchemas/bucketSchema");
const customPlatformSchema = require("./platformSchemas/customPlatformSchema");
const petriDishSchema = require("./platformSchemas/petriDishSchema");
const rackSchema = require("./platformSchemas/rackSchema");

const platformSchema = mongoose.Schema(
  {
    name: {
      type: String,
      set: function (name) {
        this._previousName = this.name;
        return name;
      },
      required: [true, "Please enter a name for the platform"],
      unique: [true, "Duplicate names are not allowed"],
    },
    description: {
      type: String,
    },
    type: {
      type: String,
      enum: [
        "TUBE_RACK",
        "TIP_RACK",
        "BUCKET",
        "PETRI_DISH",
        "CUSTOM",
        "ANCHOR",
      ],
      required: [true, "Please select a platform type"],
    },
    color: {
      type: String,
    },
  },
  {
    timestamps: true,
    strictQuery: true,
    discriminatorKey: "type",
  }
);

// Middleware to update platform name in linked workspaces
platformSchema.pre("save", async function (next) {
  const Workspace = require("./workspaceModel");

  if (this.isModified("name") && !this.isNew) {
    let previousName = this._previousName;
    try {
      await Workspace.updateMany(
        { "items.platform": previousName },
        {
          $set: {
            "items.$[item].platform": this.name,
          },
        },
        {
          arrayFilters: [{ "item.platform": previousName }],
        }
      );
    } catch (error) {
      console.log(error);
    }
    return next();
  }
  next();
});

const Platform = mongoose.model("platforms", platformSchema);

Platform.discriminator("ANCHOR", anchorSchema);
Platform.discriminator("BUCKET", bucketSchema);
Platform.discriminator("TUBE_RACK", rackSchema);
Platform.discriminator("TIP_RACK", rackSchema);
Platform.discriminator("PETRI_DISH", petriDishSchema);
Platform.discriminator("CUSTOM", customPlatformSchema);

module.exports = Platform;
