const mongoose = require("mongoose");

const toolSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Please enter a name"],
      unique: [true, "Duplicate names are not allowed"],
    },
    enabled: {
      type: Boolean,
      required: [true, "Please set the enabled flag"],
      default: true,
    },  
   description: {
     type: String,
   },
   type: {
     type: String,
     required: [true, "Please insert a tool type"],
   },
   parameters: {  
     type: Object,
     required: [true, "Please define properties"],
     description: "Parameters needed to use the tool as intented."
   },
   platforms: { // platform IDs
     type: Array,
     description: "Platforms that this tool can interact with."
   },
   platformTypes: {
    type: Array, // platform type
    description: "Platform types that this tool can interact with."
   },
   stepTypes: { // step types 
     type: Array,
     description: "Step types that can use this tool"
   }
  },
  {
    timestamps: true,
    strictQuery: true,
  }
);

const Tool = mongoose.model("tools", toolSchema);

module.exports = Tool;
