const mongoose = require("mongoose");

const containerItemSchema = new mongoose.Schema({
  container: { type: String, required: true }, // Container name
  containerOffsetZ: { type: Number, default: 0 },
});

module.exports = containerItemSchema;
