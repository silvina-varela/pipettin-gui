const mongoose = require("mongoose");

const bucketSchema = new mongoose.Schema({
  width: {
    type: Number,
    default: 0,
  },
  length: {
    type: Number,
    default: 0,
  },
  height: {
    type: Number,
    default: 0,
  },
  activeHeight: {
    type: Number,
    default: 0,
  },
  rotation: {
    type: Number,
    default: 0,
  },
});

module.exports = bucketSchema;
