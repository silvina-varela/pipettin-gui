const mongoose = require("mongoose");
const containerItemSchema = require("./containerItemSchema");

const customPlatformSchema = new mongoose.Schema({
  height: {
    type: Number,
    required: true,
    default: 0,
  },
  activeHeight: {
    type: Number,
    default: 0,
  },
  rotation: {
    type: Number,
    default: 0,
  },
  slots: [
    {
      slotName: String, /// replace keys
      slotIndex: Number,
      slotDescription: String,
      slotPosition: {
        slotX: {
          type: Number,
          default: 0,
        },
        slotY: {
          type: Number,
          default: 0,
        },
      },
      slotActiveHeight: {
        type: Number,
        default: 0,
      },
      slotSize: {
        type: Number,
        default: 0,
      },
      slotHeight: {
        type: Number,
        default: 0,
      },
      containers: {
        type: [
          {
            type: containerItemSchema,
            required: true,
          },
        ],
      },
    },
  ],

  // Rectancular
  width: {
    type: Number,
    default: 0,
  },
  length: {
    type: Number,
    default: 0,
  },

  // Circular
  diameter: {
    type: Number,
    default: 0,
  },
});

module.exports = customPlatformSchema;
