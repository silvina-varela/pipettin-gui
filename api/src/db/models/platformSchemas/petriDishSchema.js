const mongoose = require("mongoose");

const petriDishSchema = new mongoose.Schema({
  diameter: {
    type: Number,
    default: 0,
  },
  height: {
    type: Number,
    default: 0,
  },
  maxVolume: {
    type: Number,
    default: 0,
  },
  activeHeight: {
    type: Number,
    default: 0,
  },
  rotation: {
    type: Number,
    default: 0,
  },
});

module.exports = petriDishSchema;
