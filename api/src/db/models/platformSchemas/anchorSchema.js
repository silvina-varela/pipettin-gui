const mongoose = require("mongoose");

const anchorSchema = new mongoose.Schema({
  width: {
    type: Number,
    default: 0,
  },
  length: {
    type: Number,
    default: 0,
  },
  height: {
    type: Number,
    default: 0,
  },
  anchorOffsetX: {
    type: Number,
    default: -10,
  },
  anchorOffsetY: {
    type: Number,
    default: -10,
  },
  anchorOffsetZ: {
    type: Number,
    default: 0,
  },
  activeHeight: {
    type: Number,
    default: 0,
  },
  rotation: {
    type: Number,
    default: 0,
  },
});

module.exports = anchorSchema;
