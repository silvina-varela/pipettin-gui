const mongoose = require("mongoose");
const containerItemSchema = require("./containerItemSchema");

const rackSchema = new mongoose.Schema({
  width: {
    type: Number,
    default: 0,
  },
  length: {
    type: Number,
    default: 0,
  },
  height: {
    type: Number,
    default: 0,
  },
  activeHeight: {
    type: Number,
    default: 0,
  },
  rotation: {
    type: Number,
    default: 0,
  },
  firstWellCenterX: {
    type: Number,
    default: 0,
  },
  firstWellCenterY: {
    type: Number,
    default: 0,
  },
  wellDiameter: {
    type: Number,
    default: 0,
  },
  wellSeparationX: {
    type: Number,
    default: 0,
  },
  wellSeparationY: {
    type: Number,
    default: 0,
  },
  wellsColumns: {
    type: Number,
    default: 0,
  },
  wellsRows: {
    type: Number,
    default: 0,
  },
  containers: {
    type: [
      {
        type: containerItemSchema,
        required: true,
      },
    ],
  },
});

module.exports = rackSchema;
