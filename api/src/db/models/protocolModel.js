const mongoose = require("mongoose");

const protocolSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Please enter a name for the protocol"],
    },
    description: {
      type: String,
    },
    meta: {
      type: String,
    },
    hLprotocol: {
      type: String, // protocol name
    },
    workspace: {
      type: String,  // workspace name
      required: [true, "You need to link a workspace"]
    },
    date: String,
    actions: [
      {
        cmd: {
          type: String,
          // enum: [
          //   "HOME",
          //   "PICK_TIP",
          //   "LOAD_LIQUID",
          //   "DROP_LIQUID",
          //   "DISCARD_TIP",
          //   "COMMENT",
          //   "HUMAN",
          //   "WAIT",
          //   "MIX",
          //   "JSON"
          // ],
        },
        args: {
          type: {},
        },
        index: Number,
        step: Number,
        stepID: String,
      },
    ],
  },
  {
    timestamps: true,
    strictQuery: true,
  }
);

const Protocol = mongoose.model("protocols", protocolSchema);

module.exports = Protocol;
