const mongoose = require("mongoose");
const fs = require("fs");
const { getUniqueName } = require("../utils/getUniqueName");
require("dotenv").config({ path: "../.env" });

// Models
const HlProtocol = require("./models/hLprotocolModel");
const Platform = require("./models/platformModel");
const Workspace = require("./models/workspaceModel");
const Protocol = require("./models/protocolModel");
const Tool = require("./models/toolModel");
const Template = require("./models/protocolTemplatesModel");
const Container = require("./models/containerModel");

// Path where files are stored
const jsonPath = `${__dirname}/defaults`;

// Connection configuration
const { DATABASE_NAME, DATABASE_URI } = process.env;
const name = DATABASE_NAME || "pipettin";
const uri = DATABASE_URI || "127.0.0.1:27017";

// Collections configuration
/** 
 * !! IMPORTANT
 * Don't change the order. The order in which the collections are created is important for proper handling of linked documents
 */
const collections = [
  { name: "containers", file: "containers.json", model: Container },
  { name: "platforms", file: "platforms.json", model: Platform },
  { name: "workspaces", file: "workspaces.json", model: Workspace },
  { name: "hLprotocols", file: "hLprotocols.json", model: HlProtocol },
  { name: "protocols", file: "protocols.json", model: Protocol },
  { name: "tools", file: "tools.json", model: Tool },
  {
    name: "protocol_templates",
    file: "protocolTemplates.json",
    model: Template,
  },
];

async function connectToDatabase() {
  try {
    mongoose.set("strictQuery", false);
    await mongoose.connect(`mongodb://${uri}/${name}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log("Connected to the database");
  } catch (error) {
    console.error("Error connecting to the database:", error);
    throw new Error(error);
  }
}

async function closeDatabaseConnection() {
  try {
    await mongoose.disconnect();
    console.log("Disconnected from the database");
  } catch (error) {
    console.error("Error disconnecting from the database:", error);
  }
}

/**
 * Search in nested objects for names and update
 * @param {Object} obj 
 * @param {String} originalName 
 * @param {String} newName 
 */

/**
 * Updates names in linked collections
 * @param {String} originalName
 * @param {String} newName
 */
async function updateContainerName(originalName, newName) {
  for (const collection of collections) {
    if (collection.name === "platforms") {
      for (const platform of collection.data || []) {
        if (platform.slots?.length) {
          for (const slot of platform.slots) {
            if (slot.containers?.length) {
              for (const slotContainer of slot.containers) {
                if (slotContainer.container === originalName) {
                  slotContainer.container = newName;
                }
              }
            }
          }
        } else if (platform.containers?.length) {
          for (const platformContainer of platform.containers) {
            if (platformContainer.container === originalName) {
              platformContainer.container = newName;
            }
          }
        }
      }
    }
    if (collection.name === "workspaces") {
      for (const workspace of collection.data || []) {
        for (const item of workspace.items || []) {
          for (const content of item.content || []) {
            if (content.container === originalName) {
              content.container = newName;
            }
          }
        }
      }
    }
  }
}

async function updatePlatformName(originalName, newName) {
  for (const collection of collections) {
    if (collection.name === "workspaces") {
      for (const workspace of collection.data || []) {
        for (const item of workspace.items || []) {
          if (item.platform === originalName) {
            item.platform = newName;
          }
        }
      }
    }
  }
}

async function updateWorkspaceName(originalName, newName) {
  for (const collection of collections) {
    if (collection.name === "hLprotocols" || collection.name === "protocols") {
      for (const protocol of collection.data || []) {
        if (protocol.workspace === originalName) {
          protocol.workspace = newName;
        }
      }
    }
  }
}

async function updatehLprotocolName(originalName, newName) {
  for (const collection of collections) {
    if (collection.name === "protocols") {
      for (const protocol of collection.data || []) {
        if (protocol.hLprotocol === originalName) {
          protocol.hLprotocol = newName;
        }
      }
    }
  }
}

async function seedDatabase() {
  try {
    // Process data to seed
    for (const collectionInfo of collections) {
      const { file } = collectionInfo;

      const filePath = `${jsonPath}/${file}`;
      const data = await JSON.parse(fs.readFileSync(filePath, "utf8"));

      collectionInfo.data = data;
    }

    // Process names and linking
    for (const collection of collections) {
      const { name, data, file, model } = collection;

      // Make sure name is unique
      for (const document of data) {
        const allDocumentsFromModel = await model.find({});
        const uniqueName = getUniqueName(allDocumentsFromModel, document.name);

        // If name was not unique, change it and updated linked documents
        if (document.name !== uniqueName) {
          const originalName = document.name;
          document.name = uniqueName;

          // Update other collections
          switch (name) {
            case "containers":
              await updateContainerName(originalName, uniqueName);
              break;
            case "platforms":
              await updatePlatformName(originalName, uniqueName);
              break;
            case "workspaces":
              await updateWorkspaceName(originalName, uniqueName);
              break;
            case "hLprotocols":
              await updatehLprotocolName(originalName, uniqueName);
              break;
            default:
              break;
          }
        }
      }

      try {
        // Seed db and save response
        await model.create(data);

        console.log(`Seeded data from '${file}' into collection '${name}'`);
      } catch (error) {
        // Handle the error
        console.error(`Error while creating ${name}: ${error}`);
      }
    }

    console.log("Database seeding completed.");
  } catch (error) {
    console.error("Error during seeding:", error);
  }
}

async function runSeedScript() {
  try {
    await connectToDatabase();
    await seedDatabase();
  } catch (error) {
    console.error("Error during seeding:", error);
  } finally {
    await closeDatabaseConnection();
  }
}

// Execute the seedDatabase function only if the script is run directly
if (require.main === module) {
  runSeedScript();
}

module.exports = seedDatabase;
