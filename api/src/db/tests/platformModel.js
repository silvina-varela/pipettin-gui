const assert = require('assert');
// const assert = require('chai').assert;
const fs = require('fs');
const Model = require('../models/platformModel.js');

// To run:
//  mocha src/db/tests/*.js
// To install mocha executable locally (no root):
//  npm -g install packageName --prefix ~/.local

// Specify the path to your JSON file
const filePath = 'src/db/defaults/platforms.json';

describe("Default platforms validation", function(){

  let data = null;
  try {
    // Read the content of the JSON file synchronously
    data = fs.readFileSync(filePath, 'utf8');
  } catch(err) {
    console.error('Error reading platforms JSON file:', err);
  }

  describe('Load data from JSON file', () => {
    // Test if the JSON file could be loaded.
    it("The JSON file of the default platforms can be loaded", function (done) {
      assert(data != null, 'The default JSON file of the platforms could not be loaded');
      done();
    });
  });

  describe('Validate data in JSON file', () => {
    // Parse the JSON data
    const jsonData = JSON.parse(data);
    
    // Iterate over the protocols.
    jsonData.forEach(function(item, i){
      it("Default platform " + i + " matches the data model", function (done) {
        const modelInstance = new Model(item);
        
        // Hay que pasar el "done" callback a validate.
        // Ver: https://stackoverflow.com/a/25379775
        modelInstance.validate(done);

      }); // End it.
    });  // End foreach.
  }); // End describe.
}); // End describe.
