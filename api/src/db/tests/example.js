// test.js
const assert = require('assert');

function areAllPositiveNumbers(arr) {
  return arr.every(num => typeof num === 'number' && num > 0);
}

// Describe the test suite for the 'areAllPositiveNumbers' function in 'ArrayUtils'.
describe('Example test for array validation', () => {
  // Describe the specific function being tested within the 'ArrayUtils' suite.
  describe('#areAllPositive', () => {
    // In this code, describe is a function provided by Mocha to group and organize tests. 
    // The #areAllPositiveNumbers() inside the describe block signifies that you are describing 
    // the behavior of the areAllPositiveNumbers method or function. The # is not a special 
    // character in Mocha itself; it's just a convention to indicate that you are dealing with 
    // a method or function.
    
    // Define an array of test cases, each with input values and expected results.
    const testCases = [
      { input: [1, 2, 3, 4], expected: true },
      { input: [1, 2, -3, 4], expected: false },
      { input: [1, 2, 'three', 4], expected: false },
    ];

    // Iterate over each test case and dynamically generate an 'it' clause for it.
    testCases.forEach(({ input, expected }, index) => {
      // Define an individual test case.
      it(`Test Case ${index + 1}: ${JSON.stringify(input)}`, () => {
        // Execute the function with the test input and check the result against the expected value.
        const result = areAllPositiveNumbers(input);
        assert.strictEqual(result, expected);
      });
    });
  });
});
