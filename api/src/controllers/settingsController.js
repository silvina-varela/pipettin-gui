const Settings = require("../db/models/settingsModel.js");
const fs = require("fs");
const os = require("os");

// Models
const HlProtocol = require("../db/models/hLprotocolModel.js");
const Platform = require("../db/models/platformModel");
const Workspace = require("../db/models/workspaceModel");
const Protocol = require("../db/models/protocolModel");
const Tool = require("../db/models/toolModel");
const Template = require("../db/models/protocolTemplatesModel");
const Container = require("../db/models/containerModel.js");

const seedDefaults = require("../db/seed.js");
const mongoose = require("mongoose");
const seedDefaultSettings = require("../db/seedDefaultSettings.js");
const setEnvValueService = require("../services/setEnvValueService.js");
/**
 * @returns settings
 */
const getSettings = async () => {
  const settings = await Settings.findOne({});

  if (settings) return settings;
  else throw new Error("Couldn't access DB");
};

/**
 * @returns settings data
 */
const getSettingsData = async () => {
  const settingsData = await Settings.findOne({}).lean();

  if (settingsData) return settingsData;
  else throw new Error("Couldn't access DB");
};

// Define a list of databases to exclude (internal system databases).
const excludedDatabases = ["admin", "config", "local"];

/**
 * Change database settings
 * @param {object} settings
 * @returns settings
 */
const changeDatabase = async (newDatabase) => {
  try {
    if (excludedDatabases.includes(newDatabase.name)) {
      throw new Error(`Cannot switch to the administrative database: ${newDatabase.name}`);
    }

    console.log(`Disconnecting from current database (changeDatabase): ${newDatabase}`);
    await mongoose.disconnect();

    console.log("Connecting to database '" + newDatabase.name + "' (changeDatabase).");
    mongoose.connect("mongodb://" + newDatabase.uri + "/" + newDatabase.name, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    const settingsPromise = new Promise(async (resolve, reject) => {
      mongoose.connection.once("open", async () => {
        console.log(
          `MongoDB connected on port ${mongoose.connection.port} and database ${mongoose.connection.name} (changeDatabase).`
        );
        await seedDefaultSettings(newDatabase);

        await setEnvValueService("DATABASE_NAME", newDatabase.name);
        await setEnvValueService("DATABASE_URI", newDatabase.uri);

        const settings = await getSettings();
        resolve(settings);
      });
    });
    return await settingsPromise;
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};


const updateSettings = async (updatedSettings) => {
  if (updatedSettings) {
    const settings = await Settings.findOneAndUpdate({}, updatedSettings, {
      new: true,
    });

    return settings;
  } else {
    throw new Error("updateSettings: You need to provide new information to update");
  }
};

const resetDatabase = async () => {
  try {
    const currentDatabase = mongoose.connection.name;
    if (excludedDatabases.includes(currentDatabase)) {
      throw new Error(`Cannot reset the administrative database: ${currentDatabase}`);
    }

    const collections = [
      { name: "Platforms", model: Platform },
      { name: "Workspaces", model: Workspace },
      { name: "HLprotocols", model: HlProtocol },
      { name: "Protocols", model: Protocol },
      { name: "Tools", model: Tool },
      { name: "Protocol templates", model: Template },
      { name: "Containers", model: Container },
    ];

    let countDocuments = 0;
    for (const collectionInfo of collections) {
      const { name, model } = collectionInfo;
      const result = await model.deleteMany({});

      console.log(
        `${result.deletedCount} documents deleted from the collection ${name}`
      );
      countDocuments += result.deletedCount;
    }
    return `The database was reset successfully. ${countDocuments} documents were deleted`;
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};


const seedDatabase = async () => {
  try {
    const currentDatabase = mongoose.connection.name;
    if (excludedDatabases.includes(currentDatabase)) {
      throw new Error(`Cannot seed the administrative database: ${currentDatabase}`);
    }

    await seedDefaults();
    return "The default data was seeded successfully";
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};


// Endpoint to list databases
const listDatabases = async () => {
  try {
    // Get the native MongoDB client from Mongoose
    const admin = mongoose.connection.db.admin();

    // Run the listDatabases command
    const { databases } = await admin.listDatabases();

    // Return only databases not in the excluded list
    return databases
      .map(db => db.name) // Extract database names
      .filter(dbName => !excludedDatabases.includes(dbName)); // Filter out internal databases
  } catch (error) {
    console.error('Error listing databases:', error);
    throw new Error(error);
  }
};

module.exports = {
  getSettings,
  getSettingsData,
  changeDatabase,
  updateSettings,
  resetDatabase,
  seedDatabase,
  listDatabases,
};
