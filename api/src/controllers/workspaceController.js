const Workspace = require("../db/models/workspaceModel.js");
const Container = require("../db/models/containerModel.js");
const HlProtocol = require("../db/models/hLprotocolModel.js");
const Platform = require("../db/models/platformModel.js");
const { getUniqueName } = require("../utils/getUniqueName.js");
const { getProtocolsByWorkspace } = require("./protocolController.js");
const mongoose = require("mongoose");
const { createPlatform } = require("./platformController.js");
const { createContainer } = require("./containerController.js");
const { colorHash } = require("../utils/colorHash.js");
const { roundNumbersInObject } = require("../utils/roundFloat.js");

/**
 * Get platform data for each workspace item
 * @param {object} foundWorkspace
 * @returns
 */
const matchPlatformsAndContainers = async (foundWorkspace) => {
  const updatedItems = await Promise.all(
    foundWorkspace.items.map(async (item) => {
      const platform = await Platform.findOne({ name: item.platform });

      if (platform) {
        // Map each content and fetch container data
        const updatedContent = await Promise.all(
          item.content.map(async (content) => {
            if (content.container) {
              const container = await Container.findOne({
                name: content.container,
              });
              return {
                ...content,
                containerData: container ? container.toObject() : null,
              };
            } else {
              return {
                ...content,
                containerData: null,
              };
            }
          })
        );

        return {
          ...item,
          platformData: platform.toObject(),
          content: updatedContent,
        };
      } else {
        return {
          ...item,
          platformData: null,
        };
      }
    })
  );

  const updatedWorkspace = {
    ...foundWorkspace,
    items: updatedItems,
  };

  return updatedWorkspace;
};

/**
 * @param {string} name
 * @returns all workspaces or workspace by name
 */
const getWorkspaces = async (name) => {
  try {
    if (name) {
      const foundWorkspace = await getWorkspaceByName(name);
      if (foundWorkspace) return foundWorkspace;
      else throw new Error("Workspace not found");
    } else {
      const workspaces = await Workspace.find({});

      if (workspaces) return workspaces;
      else throw new Error("Couldn't access DB");
    }
  } catch (error) {
    throw new Error(error);
  }
};

/**
 * @param {string} id
 * @returns workspace by ID
 */
const getWorkspaceByID = async (id) => {
  try {
    const foundWorkspace = await Workspace.findById(id);

    if (foundWorkspace) {
      const workspace = foundWorkspace.toObject();
      if (workspace.items) {
        const modifiedWorkspace = await matchPlatformsAndContainers(workspace);
        return modifiedWorkspace;
      } else return foundWorkspace;
    } else throw new Error("Couldn't find workspace");
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};

/**
 * @param {string} name
 * @returns workspace by name
 */
const getWorkspaceByName = async (name) => {
  try {
    const foundWorkspace = await Workspace.findOne({ name });

    if (foundWorkspace) {
      const workspace = foundWorkspace.toObject();
      if (workspace.items) {
        const modifiedWorkspace = await matchPlatformsAndContainers(workspace);
        return modifiedWorkspace;
      } else return foundWorkspace;
    } else throw new Error("Couldn't find workspace");
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};

/**
 * @param {*} id
 * @returns success message
 */
const deleteWorkspace = async (id) => {
  if (id) {
    const deleteWorkspace = await Workspace.findByIdAndDelete(id);
    if (deleteWorkspace) {
      const updatedWorkspaces = await Workspace.find({});
      return updatedWorkspaces;
    } else throw new Error(`Workspace with id ${id} doesn't exist`);
  } else {
    throw new Error("You need to specify the id");
  }
};

/**
 * @param {object} workspace
 * @returns created workspace
 */

const createWorkspace = async (workspace, inheritData) => {
  try {
    const { name } = workspace;

    const allWorkspaces = await Workspace.find({});

    // Check for unique name. If no name is provided it uses 'Untitled'
    const uniqueName = await getUniqueName(allWorkspaces, name || "Untitled");
    workspace.name = uniqueName;

    // Create new workspace
    const newWorkspace = await Workspace.create(workspace);

    // Inherit data from another workspace
    if (newWorkspace && inheritData && inheritData.workspaceID) {
      const workspaceToImportFrom = await Workspace.findById(
        inheritData.workspaceID
      );

      if (workspaceToImportFrom) {
        if (inheritData.data.anchors || inheritData.data.platforms) {
          // Get platforms from workspace
          const matchedItems = await matchPlatformsAndContainers(
            workspaceToImportFrom.toObject()
          );

          // Import anchors and add to new workspace
          if (inheritData.data.anchors) {
            const anchors = matchedItems?.items?.filter((item, index) => {
              return item.platformData?.type === "ANCHOR";
            });

            await Promise.all(
              anchors.map(async (anchor) => {
                anchor.locked = true;
                await Workspace.findOneAndUpdate(
                  { _id: newWorkspace._id.toString() },
                  { $push: { items: anchor } },
                  { new: true }
                );
              })
            );
          }

          // Import platforms and add to new workspace
          if (inheritData.data.platforms) {
            const platforms = matchedItems?.items?.filter(
              (item) => item.platformData?.type !== "ANCHOR"
            );
            await Promise.all(
              platforms.map(async (platform) => {
                await Workspace.findOneAndUpdate(
                  { _id: newWorkspace._id.toString() },
                  { $push: { items: platform } },
                  { new: true }
                );
              })
            );
          }
        }
        if (inheritData.data.protocols) {
          const protocolsToImport = await getProtocolsByWorkspace(
            inheritData.workspaceID
          );

          const newProtocols = protocolsToImport.map((protocol) => ({
            name: protocol.name,
            description: protocol.description,
            workspace: newWorkspace.name,
            steps: protocol.steps,
          }));

          await HlProtocol.insertMany(newProtocols);
        }

        const updatedWorkspace = await Workspace.findById(
          newWorkspace._id.toString()
        );

        return updatedWorkspace;
      } else {
        console.log("No matching workspace");
      }
    } else return newWorkspace;
  } catch (error) {
    console.log(error.stack);
    throw new Error(error);
  }
};

const updateWorkspace = async (id, newWorkspace) => {
  if (newWorkspace && id) {
    try {
      const { name } = newWorkspace;

      const workspaceToUpdate = await Workspace.findById(id);

      if (!workspaceToUpdate) {
        throw new Error("Workspace not found");
      }
      // Checks if name hasn't changed
      if (workspaceToUpdate.name !== name) {
        const allWorkspaces = await Workspace.find({});
        const uniqueName = await getUniqueName(allWorkspaces, name);
        newWorkspace.name = uniqueName;
      }

      Object.assign(workspaceToUpdate, newWorkspace);

      const updatedWorkspace = await workspaceToUpdate.save();
      const modifiedWorkspace = await matchPlatformsAndContainers(
        updatedWorkspace.toObject()
      );
      return modifiedWorkspace;
    } catch (error) {
      console.log(error);
    }
  } else {
    throw new Error("updateWorkspace: You need to provide new information to update");
  }
};

/**
 * Adds new items to the workspace or updates existing one
 * @param {string} workspaceID
 * @param {object} item
 * @returns updated workspace
 */
const updateWorkspaceItems = async (workspaceID, newItem) => {
  if (workspaceID && newItem) {
    try {
      // Check if the item already exists in the workspace
      const currentWorkspace = await Workspace.findOne({
        _id: workspaceID,
        "items._id": newItem._id,
      });

      if (currentWorkspace) {
        // Item exists, check if the name has changed
        const existingItemName = currentWorkspace.items.find(
          (item) => item._id.toString() === newItem._id
        )?.name;

        if (existingItemName !== newItem.name) {
          // Name has changed
          // Check for name uniqueness.
          const checkName = getUniqueName(currentWorkspace.items, newItem.name);
          newItem.name = checkName;
        }

        // If item exists, modify it
        await Workspace.updateOne(
          {
            _id: workspaceID,
            "items._id": newItem._id,
          },
          {
            $set: {
              "items.$": newItem,
            },
          }
        );
      }
      // If item doesn't exists, add new item
      else {
        // If no existing item was found, add the new item
        await Workspace.updateOne(
          { _id: workspaceID },
          { $push: { items: newItem } }
        );
      }

      const updatedWorkspace = await getWorkspaceByID(workspaceID);
      return updatedWorkspace;
    } catch (error) {
      console.error(error);
      throw new Error(error);
    }
  }
};

/**
 * Delete specific items from a workspace
 * @param {string} workspaceID
 * @param {string} itemID
 * @returns updated workspace
 */
const deleteWorkspaceItem = async (workspaceID, itemID) => {
  if (workspaceID && itemID) {
    try {
      const update = await Workspace.updateOne(
        { _id: workspaceID },
        { $pull: { items: { _id: itemID } } }
      );

      if (update.matchedCount > 0) {
        // If at least one document was modified, return the updated workspace
        const updatedWorkspace = await getWorkspaceByID(workspaceID);

        return updatedWorkspace;
      } else {
        // If no documents were modified, the item might not exist
        throw new Error(
          `Item with ID ${itemID} not found in workspace ${workspaceID}`
        );
      }
    } catch (error) {
      console.error(error);
      throw new Error(error);
    }
  }
};

/**
 * Add new contents to an item or update existing ones
 * @param {string} workspaceID
 * @param {string} itemID
 * @param {array} newContents
 * @returns
 */
const updateItemContents = async (workspaceID, itemID, newContents) => {
  if (workspaceID && itemID) {
    try {
      const updates = await Promise.all(
        newContents.map(async (newContent) => {
          const update = await Workspace.updateOne(
            {
              _id: workspaceID,
              "items._id": itemID,
              "items.content._id": newContent._id,
            },
            {
              $set: {
                "items.$[item].content.$[content]": newContent,
              },
            },
            {
              arrayFilters: [
                { "item._id": itemID },
                { "content._id": newContent._id },
              ],
            }
          );

          if (update.matchedCount === 0) {
            // If no existing content was found, add the new content to the item
            const insertContent = await Workspace.updateOne(
              {
                _id: workspaceID,
                "items._id": itemID,
              },
              {
                $push: {
                  "items.$.content": newContent,
                },
              }
            );

            return insertContent;
          }

          return update;
        })
      );

      // Check if any update was successful
      const isAnyUpdateSuccessful = updates.some(
        (update) => update.matchedCount > 0
      );

      if (!isAnyUpdateSuccessful) {
        console.log("No content was updated.");
      }

      const updatedWorkspace = await getWorkspaceByID(workspaceID);
      return updatedWorkspace;
    } catch (error) {
      console.error(error);
      throw new Error(error);
    }
  } else {
    console.error("Invalid workspaceID or itemID");
    throw new Error("Invalid workspaceID or itemID");
  }
};

/**
 * Delete specific contents from an item
 * @param {string} workspaceID
 * @param {string} itemID
 * @param {array} contentsToDelete
 * @returns
 */
const deleteItemContents = async (workspaceID, itemID, contentsToDelete) => {
  if (
    workspaceID &&
    itemID &&
    contentsToDelete &&
    contentsToDelete.length > 0
  ) {
    try {
      const updates = await Promise.all(
        contentsToDelete.map(async (contentToDelete) => {
          const update = await Workspace.updateOne(
            {
              _id: workspaceID,
              "items._id": itemID,
            },
            {
              $pull: {
                "items.$.content": { _id: contentToDelete._id },
              },
            }
          );

          return update;
        })
      );

      // Check if any update was successful
      const isAnyUpdateSuccessful = updates.some(
        (update) => update.matchedCount > 0
      );

      if (!isAnyUpdateSuccessful) {
        console.log("No content was deleted.");
      }

      const updatedWorkspace = await getWorkspaceByID(workspaceID);
      return updatedWorkspace;
    } catch (error) {
      console.error(error);
      throw new Error(error);
    }
  } else {
    console.error("Invalid workspaceID, itemID, or contentsToDelete");
    throw new Error("Invalid workspaceID, itemID, or contentsToDelete");
  }
};

// TODO>
const duplicateWorkspace = async (workspace, params) => {
  if (workspace) {
    const oldWorkspaceId = workspace._id;
    workspace.name = `Copy of ${workspace.name}`;
    delete workspace._id;
    delete workspace.createdAt;
    delete workspace.updatedAt;
    delete workspace.__v;

    const newWorkspace = await createWorkspace(workspace);
    const includeProtocol = params.hasOwnProperty("withProtocol")
      ? params.withProtocol == "true"
      : false;
    if (newWorkspace) {
      if (!includeProtocol) return newWorkspace;
      else {
        try {
          const protocols = await getProtocolsByWorkspace(oldWorkspaceId);

          const newProtocols = protocols.map((protocol) => ({
            name: protocol.name,
            description: protocol.description,
            workspace: newWorkspace.name,
            steps: protocol.steps,
          }));

          await HlProtocol.insertMany(newProtocols);
          return newWorkspace;
        } catch (error) {
          throw new Error(error);
        }
      }
    } else throw new Error("Couldn't create workspace");
  }
};

/**
 *
 * @param {array} workspaces
 */
const exportWorkspaces = async (workspaces) => {
  const arrayToExport = await Promise.all(
    workspaces.map(async (workspace) => {
      let modifiedWorkspace = { ...workspace };
      delete modifiedWorkspace._id;
      delete modifiedWorkspace.thumbnail;

      if (modifiedWorkspace.items) {
        modifiedWorkspace = await matchPlatformsAndContainers(
          modifiedWorkspace
        );

        await modifiedWorkspace.items.forEach((item) => {
          delete item._id;
          if(item.platformData) delete item.platformData._id;

          item.content.forEach((content) => {
            delete content._id;
            if (content.containerData) {
              delete content.containerData._id;
              if (content.containerData.platforms) {
                content.containerData.platforms.forEach((platform) => {
                  delete platform._id;
                });
              }
            }
          });
        });
      }
      return modifiedWorkspace;
    })
  );
  return arrayToExport;
};

/**
 *
 * @param {array} workspaces
 */
const importWorkspaces = async (files) => {
  if (!Array.isArray(files))
    throw new Error("Invalid type. Data must be contained within an array");
  else {
    const successfulImports = [];

    for (const file of files) {
      const session = await mongoose.startSession();
      try {
        session.startTransaction();

        for (const workspace of file.content) {
          // Create workspace
          const newWorkspace = {};
          Object.keys(workspace).forEach((key) => {
            if (
              key !== "_id" &&
              key !== "items" &&
              key !== "createdAt" &&
              key !== "updatedAt"
            ) {
              newWorkspace[key] = workspace[key];
            }
          });

          const importedWorkspace = await createWorkspace(newWorkspace);

          if (!importedWorkspace) {
            throw new Error(
              `Unable to import workspace with name ${workspace.name}`
            );
          } else {
            if (workspace.items && workspace.items.length) {
              // Workspace was created. Now add the items
              for (const item of workspace.items) {

                // Insert container data in the DB if required.
                if (item?.containerData) {
                  // Handle the items' "containerData".
                  // Iterate over each container definition.
                  for(const container_data of item.containerData){
                    // Query object to find existing containers.
                    const container_query = {};
                    // Build the query.
                    Object.keys(container_data).forEach((key) => {
                      if (
                        // Exclude Mongo keys.
                        key !== "_id" &&
                        key !== "createdAt" &&
                        key !== "updatedAt" &&
                        key !== "__v" &&
                        // Non-hardware keys.
                        key !== "description"
                        // TODO: also remove the "name" key, to compare only
                        // the physical properties. Then rename the container
                        // here and in the item's content.
                      ) {
                        container_query[key] = container_data[key];
                      }
                    });

                    // Round the numbers in the parsed data to 2 significant digits.
                    const final_query = roundNumbersInObject(container_query, 2);
                    // Needed to handle false mismatches between float numbers
                    // like 13.2 and 13.200000000000003.

                    // Look for an existing platform in the DB.
                    const existingContainer = await Container.findOne(final_query);

                    // Create a new container if no matching container was found.
                    if (!existingContainer){
                      // Get all containers.
                      const allContainers = await Container.find({});
                      // Generate a unique name.
                      const uniqueName = await getUniqueName(
                        allContainers,
                        container_data.name || "Untitled"
                      );
                      // TODO: If the name was updated,
                      // update all containers used in contents and platform data.
                      container_data.name = uniqueName;

                      // Create the container.
                      await createContainer(container_data);
                    }
                  }
                }

                // Query to find existing platforms.
                const query = {};
                // Handle items with and without "platformData".
                if (!item.platformData) {
                  // Look for an existing platform in the DB, using only the platform name.
                  query["name"] = item.platform;
                } else {
                  // Look for an existing platform in the DB, matching all parameters
                  // that are important for the platform definition (dimensions, sizes,
                  // etc.), discarding all the "superficial" parameters from the query.
                  const data = item.platformData;
                  Object.keys(data).forEach((key) => {
                    if (
                      // Mongo ID keys.
                      key !== "_id" &&
                      key !== "createdAt" &&
                      key !== "updatedAt" &&
                      key !== "__v" &&
                      // Non-hardware keys.
                      key !== "description" &&
                      key !== "color" &&
                      key !== "name" &&
                      key !== "containers"
                    ) {
                      query[key] = data[key];
                    }
                  });

                  // Round the numbers in the parsed data to 2 significant digits.
                  // Needed to handle false mismatches between float numbers
                  // like 13.2 and 13.200000000000003.
                  const final_query = roundNumbersInObject(query, 2);

                  // Look for an existing platform in the DB.
                  const existingPlatform = await Platform.findOne(final_query);

                  // If no platform was matched by name when no
                  // platform data was provided, then error-out here.
                  if (!existingPlatform && !item.platformData){
                    throw new Error(
                      `On file: ${file.fileName}, workspace with name ${workspace.name} has missing platform data on item with name ${item.name}, its platform ${item["platform"]} was not found.`
                    );
                  }

                  // If platform exists, link to new item
                  if (existingPlatform) {
                    // NOTE: This should no have an effect. Is it just in case?
                    item.platform = existingPlatform.name;
                  } else {
                    // No matching platform was found
                    // console.log("No existing platform was found during import:", item.platform.name)

                    // Create new platform
                    const allPlatforms = await Platform.find({});

                    const uniqueName = await getUniqueName(
                      allPlatforms,
                      item.platformData.name || "Untitled"
                    );

                    item.platformData.name = uniqueName;

                    item.platformData.color = item.platformData.color || colorHash(uniqueName).hex; // Nice default color.

                    const newPlatform = await createPlatform(item.platformData);

                    item.platform = newPlatform.name;

                  }
                  await updateWorkspaceItems(
                    importedWorkspace._id.toString(),
                    item
                  );
                }
              }
            }
            const uploadedWorkspace = await getWorkspaceByID(
              importedWorkspace._id.toString()
            );
            successfulImports.push(uploadedWorkspace);
          }
        }

        session.commitTransaction();
      } catch (error) {
        await session.abortTransaction();
        console.log(error);
        throw new Error(`Error on file ${file.fileName}: ${error.message}`);
      } finally {
        session.endSession();
      }
    }

    return successfulImports;
  }
};

const duplicateWorkspaceItem = async (workspace, duplicateItem) => {
  const contentsToDuplicate = duplicateItem.content;
  duplicateItem.content = [];
  delete duplicateItem._id;
  duplicateItem.position = { x: 0, y: 0, z: duplicateItem.position.z };
  // updateWorkspaceItems(workspace._id, duplicateItem)

  const checkName = getUniqueName(workspace.items, duplicateItem.name);
  duplicateItem.name = checkName;

  const updatedWorkspace = await Workspace.findOneAndUpdate(
    { _id: workspace._id },
    { $push: { items: duplicateItem } },
    { new: true }
  );

  // Now 'updatedWorkspace' contains the modified document, including the newly added item
  const newItemId = updatedWorkspace.items.slice(-1)[0]._id;

  contentsToDuplicate.forEach((content) => {
    delete content._id;
  });

  const finalUpdatedWorkspace = await updateItemContents(
    workspace._id,
    newItemId,
    contentsToDuplicate
  );

  return finalUpdatedWorkspace;
};

module.exports = {
  getWorkspaces,
  getWorkspaceByID,
  getWorkspaceByName,
  deleteWorkspace,
  createWorkspace,
  updateWorkspace,
  updateWorkspaceItems,
  deleteWorkspaceItem,
  updateItemContents,
  deleteItemContents,
  duplicateWorkspace,
  exportWorkspaces,
  importWorkspaces,
  duplicateWorkspaceItem,
};
