const Template = require("../db/models/protocolTemplatesModel.js");
const { getUniqueName } = require("../utils/getUniqueName.js");

/**
 * @param {*} name
 * @returns all templates or template by name
 */
const getTemplates = async (name) => {
  if (name) {
    const foundTemplate = await Template.findOne({ name });

    if (foundTemplate) {
      return foundTemplate;
    } else {
      throw new Error("Template not found");
    }
  } else {
    const templates = await Template.find({});
    if (templates) return templates;
    else throw new Error("Couldn't access DB");
  }
};

/**
 * @param {*} id
 * @returns template by ID
 */
const getTemplateByID = async (id) => {
  try {
    const foundTemplate = await Template.findById(id);

    if (foundTemplate) return foundTemplate;
    else throw new Error("Template not found");
  } catch (error) {
    throw new Error(`${error.message}`);
  }
};

/**
 * @param {*} id
 * @returns success message
 */
const deleteTemplate = async (id) => {
  if (id) {
    const deletedTemplate = await Template.findByIdAndDelete(id);

    if (deletedTemplate) {
      const templates = await getTemplates();
      return templates;
    } else throw new Error(`Template with id ${id} doesn't exist`);
  } else {
    throw new Error("You need to specify the id");
  }
};

/**
 * @param {*} template
 * @returns created platfrom
 */
const createTemplate = async (template) => {
  const { name } = template;

  const allTemplates = await Template.find({});

  // Check for unique name. If no name is provided it uses 'Untitled'
  const uniqueName = await getUniqueName(allTemplates, name || "Untitled");
  template.name = uniqueName;

  const newTemplate = await Template.create(template);

  if (newTemplate) return newTemplate;
  else throw new Error("Couldn't create template");
};

/**
 * @param {*} template
 * @returns updated template
 */
const updateTemplate = async (updatedTemplate) => {
  if (updatedTemplate) {
    const { name } = updatedTemplate;
    const allTemplates = await Template.find({});

    // Checks if name hasn't changed
    const oldTemplate = await Template.findById(updatedTemplate._id);
    const checkName = oldTemplate.name === name;

    // If true, name hasn't changed. If false, it checks for repeated name.
    if (!checkName) {
      const uniqueName = await getUniqueName(allTemplates, name);
      updatedTemplate.name = uniqueName;
    }

    // Updates template
    await Template.findByIdAndUpdate(updatedTemplate._id, updatedTemplate);

    // Finds and returns the updated Workspace with all the updated info
    const modifiedTemplate = await Template.findById(updatedTemplate._id);

    return modifiedTemplate;
  } else {
    throw new Error("updateTemplate: You need to provide new information to update");
  }
};

module.exports = {
  getTemplates,
  getTemplateByID,
  deleteTemplate,
  createTemplate,
  updateTemplate,
};
