const Platform = require("../db/models/platformModel.js");
const { getUniqueName } = require("../utils/getUniqueName.js");

/**
 * @param {*} name
 * @returns all platforms or platform by name
 */
const getPlatforms = async (name) => {
  if (name) {
    const foundPlatform = await Platform.findOne({ name });

    if (foundPlatform) {
      return foundPlatform;
    } else {
      throw new Error("Platform not found");
    }
  } else {
    const platforms = await Platform.find({});
    if (platforms) return platforms;
    else throw new Error("Couldn't access DB");
  }
};

/**
 * @param {*} id
 * @returns platform by ID
 */
const getPlatformByID = async (id) => {
  try {
    const foundPlatform = await Platform.findById(id);

    if (foundPlatform) return foundPlatform;
    else throw new Error("Platform not found");
  } catch (error) {
    throw new Error(`${error.message}`);
  }
};

/**
 * @param {*} id
 * @returns success message
 */
const deletePlatform = async (id) => {
  if (id) {
    const deletePlatform = await Platform.findByIdAndDelete(id);

    if (deletePlatform) {
      const platforms = await getPlatforms();
      return platforms;
    } else throw new Error(`Platform with id ${id} doesn't exist`);
  } else {
    throw new Error("You need to specify the id");
  }
};

/**
 * @param {*} platform
 * @returns created platfrom // TODO: WARNING THIS IS WRONG - BUG ALERT!
 */
const createPlatform = async (platform) => {
  const { name } = platform;

  if (name) {
    const foundRepeated = await Platform.findOne({ name });

    if (foundRepeated) {
      throw new Error("A platform with that same name already exists");
    } else {
      const newPlatform = await Platform.create(platform);

      // Disabled this as it is no longer needed by createPlatformAsync in the frontend.
      // This restores the expected output of this function during a "POST /api/platforms",
      // in "importWorkspaces", and elsewhere.
      // if (newPlatform) {
      //   const platforms = await getPlatforms();
      //   // BUG: This function returns all the platforms, or nothing at all,
      //   //      in contrast with what the docstring says.
      //   return platforms;
      // }
      return newPlatform; // New return value.
    }
  } else {
    throw new Error("You need to provide a name for the platform");
  }
};

/**
 * @returns created platfroms
 */
const createPlatformsInBulk = async (platformList) => {
  const errors = [];
  const success = [];
  try {
    for (const file of platformList) {
      for (let i = 0; i < file.content.length; i++) {
        const platform = file.content[i];

        try {
          const allPlatforms = await Platform.find({});
          const uniqueName = await getUniqueName(
            allPlatforms,
            platform.name || "Untitled"
          );
          platform.name = uniqueName;

          delete platform.updatedAt;
          delete platform.createdAt;
          delete platform.__v;
          delete platform.createdAt;

          const created = await Platform.create(platform);
          success.push(`Platform '${created.name}' from file ${file.fileName}`);
        } catch (error) {
          console.log(error);
          const apiError = error.message.replace(
            /platforms validation failed:/i,
            " "
          );
          const errorMessage = `At the ${file.fileName} file, in element ${
            i + 1
          }:
              ${apiError}`;
          errors.push(errorMessage);
        }
      }
    }

    return { errors, success };
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};

/**
 * @param {*} id
 * @param {*} platform
 * @returns updated platform
 */
const updatePlatform = async (id, platform) => {
  if (!id || !platform)
    throw new Error("You need to provide id and new platform data to update");

  // Checks if name is the same
  const { name } = platform;
  const oldPlatform = await Platform.findById(id);
  const checkName = oldPlatform.name === name;

  // If name has changed, it checks for repeated names.
  if (!checkName) {
    const foundRepeated = await Platform.findOne({ name });

    if (foundRepeated)
      throw new Error("A platform with that same name already exists");
  }

  try {
    Object.assign(oldPlatform, platform);

    const updatedPlatform = await oldPlatform.save();
    if (updatedPlatform) {
      const platforms = await getPlatforms();
      return platforms;
    }
  } catch (error) {
    console.log(error);
    throw new Error(error.message);
  }
};

module.exports = {
  getPlatforms,
  getPlatformByID,
  deletePlatform,
  createPlatform,
  updatePlatform,
  createPlatformsInBulk,
};
