/**
 * Sends request to move robot to specific position
 */
const runCalibration = async (req, options) => {
  try {
    if (!options) {
      throw new Error("Options are not provided.");
    }
    const ioServer = req.ioServer;

    console.log(options);
    await ioServer.emit("go_to", options);
    return "Request sent";
    //TODO: errores del controller
  } catch (error) {
    throw new Error(error);
  }
};

/**
 * Sends request for parking a specific tool
 */
const runPark = async (req, options) => {
  try {
    if (!options) {
      throw new Error("Options are not provided.");
    }
    const ioServer = req.ioServer;

    console.log(options);
    await ioServer.emit("park_tool", options);
    return "Request sent";
    //TODO: errores del controller
  } catch (error) {
    throw new Error(error);
  }
};

/**
 * Stops the process
 *
 */
const killProcess = async (req) => {
  const ioServer = req.ioServer;

  const killOptions = {
    reason: "none",
  };

  try {
    await ioServer.emit("kill_commander", killOptions);
    return "Request sent";
  } catch (error) {
    throw new Error(error);
  }
};

/**
 * Stops the protocol
 *
 */
const stopProtocol = async (req) => {
  
  const { protocolID, protocol } = req.body;

  const ioServer = req.ioServer;

  const options = {
    // Not checking if "protocolID" is undefined/null for now.
    // It might be interpreted by the controller specially.
    protocolId: protocolID,
    protocol: protocol,
  };

  try {
    await ioServer.emit("stop_protocol", options);
    return "Request sent";
  } catch (error) {
    throw new Error(error);
  }
};

/**
 * Pauses the protocol
 *
 */
const pauseProtocol = async (req) => {
  
  const { protocolID, protocol } = req.body;

  const ioServer = req.ioServer;

  const options = {
    // Not checking if "protocolID" is undefined/null for now.
    // It might be interpreted by the controller specially.
    protocolId: protocolID,
    protocol: protocol,
  };

  try {
    await ioServer.emit("pause_protocol", options);
    return "Request sent";
  } catch (error) {
    throw new Error(error);
  }
};

/**
 * Unpauses the protocol
 *
 */
const continueProtocol = async (req) => {
  
  const { protocolID, protocol } = req.body;

  const ioServer = req.ioServer;

  const options = {
    // Not checking if "protocolID" is undefined/null for now.
    // It might be interpreted by the controller specially.
    protocolId: protocolID,
    protocol: protocol,
  };

  try {
    await ioServer.emit("continue_protocol", options);
    return "Request sent";
  } catch (error) {
    throw new Error(error);
  }
};

/**
 * Runs the protocol
 */
const runCommand = async (req, args) => {
  try {
    if (!args) {
      throw new Error("Arguments are not provided.");
    }

    const ioServer = req.ioServer;

    console.log("Sending message run_protocol:", args.name);

    ioServer.emit("run_protocol", args);
  } catch (error) {
    console.error("An error occurred in runCommand:", error.message);
  }
};

module.exports = { runCalibration, killProcess, stopProtocol, pauseProtocol, continueProtocol, runCommand, runPark };
