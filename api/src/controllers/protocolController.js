const Protocol = require("../db/models/protocolModel.js");
const HlProtocol = require("../db/models/hLprotocolModel");
const Workspace = require("../db/models/workspaceModel");
const { getUniqueName } = require("../utils/getUniqueName.js");
const { validateProtocolStep } = require("../utils/protocolValidations.js");
const protocolBuilder = require("../utils/protocolBuilder.js");

/**
 * @param {*} name
 * @returns all protocols or protocols by name if name is provided
 */
const getAllProtocols = async (name) => {
  if (name) {
    const foundProtocol = await HlProtocol.findOne({ name: name });

    if (foundProtocol) return foundProtocol;
    else throw new Error("Protocol not found");
  } else {
    const protocols = await HlProtocol.find({});

    if (protocols) return protocols;
    else throw new Error("Couldn't access DB");
  }
};

/**
 * @param {*} id
 * @returns protocols by ID
 */
const getProtocolByID = async (id) => {
  try {
    const foundProtocol = await HlProtocol.findById(id);

    if (foundProtocol) return foundProtocol;
    else throw new Error("Protocol not found");
  } catch (error) {
    throw new Error(`${error.message}`);
  }
};

/**
 * @param {String} workspaceID
 * @returns protocols associated to workspace
 */
const getProtocolsByWorkspace = async (workspaceID) => {
  if (workspaceID) {
    try {
      const workspace = await Workspace.findById(workspaceID);
      const foundProtocols = await HlProtocol.find({
        workspace: workspace.name,
      });

      if (foundProtocols) return foundProtocols;
      else throw new Error(`No protocols found`);
    } catch (error) {
      throw new Error(`${error.message}`);
    }
  } else {
    throw new Error("Invalid workspace ids");
  }
};

/**
 * @param {*} id
 * @returns success message
 */
const deleteProtocol = async (id) => {
  if (id) {
    const deleteProtocol = await HlProtocol.findByIdAndDelete(id);

    if (deleteProtocol) {
      const protocols = await HlProtocol.find({
        workspace: deleteProtocol.workspace,
      });
      return protocols;
    } else throw new Error(`Protocol with id ${id} doesn't exist`);
  } else {
    throw new Error("You need to specify the id");
  }
};

/**
 * Creates a protocol with name ("Untitled" if no name is provided), description (not required) and workspaceID (required)
 * @param {*} protocol
 * @returns created protocol
 */
const createProtocol = async (protocol) => {
  const { name, workspace } = protocol;

  if (!workspace) throw new Error("You need to link a workspace");

  // Finds all protocols associated with workspace and gives them unique names (appends number after names that aren't unique). If no name is provided, it names the protocol "Untitled"
  const allProtocols = await HlProtocol.find({});
  const protocolsAssociatedToWorkspace = allProtocols.filter(
    (protocol) => protocol.workspace === workspace
  );

  const uniqueName = await getUniqueName(
    protocolsAssociatedToWorkspace,
    name || "Untitled"
  );
  protocol.name = uniqueName;

  const newProtocol = await HlProtocol.create(protocol);

  if (newProtocol) {
    return newProtocol;
  } else throw new Error("Couldn't create protocol");
};

/**
 * Updates protocol
 * @param {*} id, protocol
 * @returns updated protocol
 */
const updateProtocol = async (id, updatedProtocol) => {
  if (updatedProtocol && id) {
    const { name, workspaceID } = updatedProtocol;

    const oldProtocol = await HlProtocol.findById(id);
    if (name) {
      // Checks if name hasn't changed
      const nameChanged = !(oldProtocol.name === name);

      // If name has changed, it checks for repeated name within the workspace
      if (nameChanged) {
        const allProtocols = await HlProtocol.find({});
        const protocolsAssociatedToWorkspace = allProtocols.filter(
          (protocol) => (protocol.workspaceID = workspaceID)
        );
        const uniqueName = await getUniqueName(
          protocolsAssociatedToWorkspace,
          name || "Untitled"
        );
        updatedProtocol.name = uniqueName;
      }
    }

    try {
      Object.assign(oldProtocol, updatedProtocol);

      const updated = await oldProtocol.save();

      return updated;
    } catch (error) {
      console.log(error);
      throw new Error(error.message);
    }
  } else {
    throw new Error("updateProtocol: You need to provide new information to update");
  }
};

/**
 * Validates protocol steps
 * @param {*} protocolID
 * @returns error message to show in the frontend
 */
const validateProtocol = async (protocolID) => {
  const protocol = await HlProtocol.findById(protocolID);
  if (!protocol) throw new Error("You need to save the protocol first");

  const { steps } = protocol;

  // Checks if the protocol has steps
  if (!steps || !steps.length)
    throw new Error("There are no steps defined in this protocol");

  // Runs basic validations on the protocol
  validateProtocolStep(steps);

  const { getWorkspaceByName } = require("./workspaceController.js");

  const workspace = await getWorkspaceByName(protocol.workspace);

  const protocolValidation = protocolBuilder(protocol, workspace);

  // Save the "middleLevelProtocol" to the database.
  const midLevelProtocol = await Protocol.create(protocolValidation);

  // If nothing returns error, protocol is valid
  return midLevelProtocol;
};

/** Mid level protocols */
/**
 * @param {*} name
 * @returns all protocols or protocols by name if name is provided
 */
const getAllMidLevelProtocols = async (name) => {
  if (name) {
    const foundProtocol = await Protocol.findOne({ name: name });

    if (foundProtocol) return foundProtocol;
    else throw new Error("Protocol not found");
  } else {
    const protocols = await Protocol.find({});

    if (protocols) return protocols;
    else throw new Error("Couldn't access DB");
  }
};

/**
 * @param {*} id
 * @returns protocols by ID
 */
const getMidLevelProtocolByID = async (id) => {
  try {
    const foundProtocol = await Protocol.findById(id);

    if (foundProtocol) return foundProtocol;
    else throw new Error("Protocol not found");
  } catch (error) {
    throw new Error(`${error.message}`);
  }
};

/**
 * @param {String} hLprotocol name
 * @param {String} workspaceName
 * @returns array
 */
const getMidLevelProtocolByProtocol = async (hLprotocol, workspaceName) => {
  if (hLprotocol) {
    try {
      const foundProtocols = await Protocol.find({
        hLprotocol: hLprotocol,
        workspace: workspaceName,
      });

      if (foundProtocols) return foundProtocols;
      else
        throw new Error(`No protocols found for hL protocol '${hLprotocol}'`);
    } catch (error) {
      throw new Error(`${error.message}`);
    }
  }
};

const updateWorkspaceFromTemplate = async (workspaceValues, templateValues) => {
  const { currentWorkspace, allPlatforms, platformsToInsert } = workspaceValues;

  let updatedProtocol = { ...templateValues };

  const removeEmptyValues = (obj) => {
    for (const key in obj) {
      if (obj.hasOwnProperty(key) && obj[key] === "") {
        delete obj[key];
      }
    }
  };

  const insertPlatformsToWorkspace = async (platforms) => {
    const newNames = [];
    const insert = platforms.map((platform) => {
      const newItem = {
        platform: platform.name,
        platformData: { ...platform },
        name: getUniqueName(
          [...currentWorkspace.items, ...newNames],
          platform.name
        ),
        color: platform.color,
        type: platform.type,
        position: {
          x: 0,
          y: 0,
        },
        content: [],
      };
      newNames.push({ name: newItem.name });
      return newItem;
    });

    const updatedWorkspace = {
      ...currentWorkspace,
      items: [...currentWorkspace.items, ...insert],
    };
    return { insert, updatedWorkspace };
  };

  removeEmptyValues(platformsToInsert);
  const platformData = [];

  for (const key in platformsToInsert) {
    const platform = allPlatforms.find(
      (platform) => platform.name === platformsToInsert[key]
    );
    platformData.push(platform);
  }

  const response = await insertPlatformsToWorkspace(platformData);
  const { insert, updatedWorkspace } = response;

  const templateDefinition = { ...updatedProtocol.templateDefinition };
  const templateFieldsArray = [...updatedProtocol.templateFields.fields];

  let index = 0;
  for (const key in platformsToInsert) {
    templateDefinition[key] = insert[index].name;

    const indexField = templateFieldsArray.findIndex(
      (field) => field.field_id === key
    );

    const currentField = templateFieldsArray.find(
      (field) => field.field_id === key
    );
    currentField.field_value = insert[index].name;
    templateFieldsArray[indexField] = currentField;

    index++;
  }

  updatedProtocol = {
    ...updatedProtocol,
    templateDefinition: templateDefinition,
    templateFields: {
      ...updatedProtocol.templateFields,
      fields: [...templateFieldsArray],
    },
  };
  return { updatedWorkspace, updatedProtocol };
};

const createProtocolFromTemplate = async (
  req,
  protocolTemplate,
  workspaceValues,
  settings
) => {
  try {
    const ioServer = req.ioServer;

    // Clone the protocolTemplate object to avoid mutation
    let newProtocol = { ...protocolTemplate };

    // Find file upload fields and make buffers with base64-encoded data.
    newProtocol.templateFields.fields.forEach((field) => {
      if (field.field_type === "fileUpload") {
        const fieldName = field.field_id;
        console.log(newProtocol.templateDefinition)

        // Check if the corresponding file data exists in templateDefinition
        if (newProtocol.templateDefinition?.[fieldName]) {
          const fileField = newProtocol.templateDefinition[fieldName];
          console.log("Parsing field: " + fieldName)
          
          // Convert the Base64 data to a Buffer for storage
          const fileBuffer = Buffer.from(fileField.data, "base64");

          // Replace the entry in templateDefinition with the file's metadata and Buffer data
          newProtocol.templateDefinition[fieldName] = {
            name: fileField.name,
            type: fileField.type,
            size: fileField.size,
            data: fileBuffer, // Store the binary data as a Buffer
          };

          // Optionally, remove the field_value in templateFields if you want to clear it out
          field.field_value = {};
        }
      }
    });

    let newWorkspace;
    if (Object.values(workspaceValues.platformsToInsert).length) {
      const { updatedWorkspace, updatedProtocol } =
        await updateWorkspaceFromTemplate(workspaceValues, protocolTemplate);
      newWorkspace = updatedWorkspace;
      newProtocol = updatedProtocol;
    } else {
      newWorkspace = workspaceValues.currentWorkspace;
    }

    const response = await ioServer
      .timeout(10000)
      .emitWithAck("protocol_template", {
        protocol: { ...newProtocol, workspaceID: newWorkspace._id },
        workspace: newWorkspace,
        settings,
      });
    console.log({ response });
    if (response) {
      if (response.length && typeof response[0] !== "undefined") {
        const { protocol, workspace } = response[0];

        // if (
        //   response.length &&
        //   typeof response === "object" &&
        //   response !== null
        // ) {
        //   const { protocol, workspace } = response;

        if (protocol && workspace) {
          const newProtocol = await createProtocol(protocol);
          const updatedWorkspace = await Workspace.findByIdAndUpdate(
            workspace._id,
            workspace
          );

          if (newProtocol && updatedWorkspace) {
            return newProtocol;
          }
        }
      } else throw new Error("Error with the controller response");
    } else throw new Error("Couldn't connect to the controller");
  } catch (error) {
    // the server did not acknowledge the event in the given delay
    console.log(error);
    throw new Error(error);
  }
};

/**
 *
 * @param {array} protocol
 * @returns
 */
const importProtocol = async (protocols, workspaceID) => {
  if (workspaceID && protocols?.length) {
    try {
      const workspace = await Workspace.findById(workspaceID);

      const newProtocols = protocols.flatMap((item) => item.content);

      const createdProtocols = await Promise.all(
        newProtocols
          .filter((protocol) => protocol?.steps?.length)
          .map(async (protocol) => {
            delete protocol.createdAt;
            delete protocol.updatedAt;
            delete protocol.__v;
            delete protocol._id;
            protocol.workspaceID = workspaceID;
            protocol.workspace = workspace.name;
            const created = await createProtocol(protocol);
            return created.name;
          })
      );

      if (createdProtocols.length) return createdProtocols;
      else throw new Error("No protocols were created");
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  }
};

const updateProtocolFromTemplate = async (
  req,
  protocolTemplate,
  workspaceValues,
  settings
) => {
  try {
    const ioServer = req.ioServer;

    // Clone the protocolTemplate object to avoid mutation
    let newProtocol = { ...protocolTemplate };

    // Find file upload fields and make buffers with base64-encoded data.
    newProtocol.templateFields.fields.forEach((field) => {
      if (field.field_type === "fileUpload") {
        const fieldName = field.field_id;

        // Check if the corresponding file data exists in templateDefinition
        if (newProtocol.templateDefinition?.[fieldName]) {
          const fileField = newProtocol.templateDefinition[fieldName];
          
          // Convert the Base64 data to a Buffer for storage
          const fileBuffer = Buffer.from(fileField.data, "base64");

          // Replace the entry in templateDefinition with the file's metadata and Buffer data
          newProtocol.templateDefinition[fieldName] = {
            name: fileField.name,
            type: fileField.type,
            size: fileField.size,
            data: fileBuffer, // Store the binary data as a Buffer
          };

          // Optionally, remove the field_value in templateFields if you want to clear it out
          field.field_value = {};
        }
      }
    });

    let newWorkspace;
    if (Object.values(workspaceValues.platformsToInsert).length) {
      const { updatedWorkspace, updatedProtocol } =
        await updateWorkspaceFromTemplate(workspaceValues, protocolTemplate);
      newWorkspace = updatedWorkspace;
      newProtocol = updatedProtocol;
    } else {
      newWorkspace = workspaceValues.currentWorkspace;
    }

    const response = await ioServer
      .timeout(10000)
      .emitWithAck("protocol_template", {
        protocol: { ...newProtocol, workspaceID: newWorkspace._id },
        workspace: newWorkspace,
        settings,
      });
    if (response) {
      if (response.length && typeof response[0] !== "undefined") {
        const { protocol, workspace } = response[0];
        // if (
        //   response.length &&
        //   typeof response === "object" &&
        //   response !== null
        // ) {
        //   const { protocol, workspace } = response;
        if (protocol && workspace) {
          const updatedProtocol = await updateProtocol(protocol._id, protocol);
          const updatedWorkspace = await Workspace.findByIdAndUpdate(
            workspace._id,
            workspace
          );

          if (updatedProtocol && updatedWorkspace) {
            return updatedProtocol;
          }
        }
      } else throw new Error("Error with the controller response");
    } else throw new Error("Couldn't connect to the controller");
  } catch (error) {
    // the server did not acknowledge the event in the given delay
    console.log(error);
    throw new Error(error);
  }
};

const updateProtocolSteps = async (protocolID, newSteps) => {
  if (protocolID && newSteps && Array.isArray(newSteps)) {
    try {
      const updatedSteps = [];

      for (const newStep of newSteps) {
        // Check if the step already exists in the protocol
        const currentProtocol = await HlProtocol.findOne({
          _id: protocolID,
          "steps._id": newStep._id,
        });

        if (currentProtocol) {
          // Step exists, check if the name has changed
          const existingStepName = currentProtocol.steps.find(
            (step) => step._id == newStep._id
          )?.name;

          if (existingStepName !== newStep.name) {
            // Name has changed
            // Check for name uniqueness.
            const checkName = getUniqueName(
              currentProtocol.steps,
              newStep.name
            );
            newStep.name = checkName;
          }

          // If step exists, modify it
          await HlProtocol.updateOne(
            {
              _id: protocolID,
              "steps._id": newStep._id,
            },
            {
              $set: {
                "steps.$": newStep,
              },
            }
          );

          updatedSteps.push(newStep);
        } else {
          // If step doesn't exist, add new step
          await HlProtocol.updateOne(
            { _id: protocolID },
            { $push: { steps: newStep } }
          );

          updatedSteps.push(newStep);
        }
      }

      const updatedProtocol = await getProtocolByID(protocolID);
      return updatedProtocol;
    } catch (error) {
      console.error(error);
      throw new Error(error);
    }
  }
};

const deleteProtocolStep = async (protocolID, stepID) => {
  if (protocolID && stepID) {
    try {
      const protocol = await getProtocolByID(protocolID);

      if (protocol) {
        // Check if step exists
        const deletedStep = protocol.steps.find((step) => step._id == stepID);

        if (!deletedStep) {
          throw new Error(
            `Step with ID ${stepID} not found in protocol ${protocolID}`
          );
        }

        const orderOfDeletedStep = deletedStep.order;

        const update = await HlProtocol.updateOne(
          { _id: protocolID },
          { $pull: { steps: { _id: stepID } } }
        );

        if (update.matchedCount > 0) {
          // If the document was modified, update the order of remaining steps
          await HlProtocol.updateOne(
            { _id: protocolID },
            { $inc: { "steps.$[elem].order": -1 } },
            { arrayFilters: [{ "elem.order": { $gt: orderOfDeletedStep } }] }
          );
          // If the document was modified, return the updated protocol
          const updatedProtocol = await getProtocolByID(protocolID);

          return updatedProtocol;
        } else {
          // If the document wasn't modified, the step might not exist
          throw new Error(
            `It was not possible to delete step with ID ${stepID} from protocol ${protocolID}`
          );
        }
      }
    } catch (error) {
      console.error(error);
      throw new Error(error);
    }
  }
};

module.exports = {
  getAllProtocols,
  getProtocolByID,
  getProtocolsByWorkspace,
  deleteProtocol,
  createProtocol,
  updateProtocol,
  validateProtocol,
  getAllMidLevelProtocols,
  getMidLevelProtocolByID,
  getMidLevelProtocolByProtocol,
  createProtocolFromTemplate,
  importProtocol,
  updateProtocolFromTemplate,
  updateProtocolSteps,
  deleteProtocolStep,
};
