const Container = require("../db/models/containerModel.js");
const { getUniqueName } = require("../utils/getUniqueName.js");

/**
 * @param {*} name
 * @returns all conainers or container by name
 */
const getContainers = async (name) => {
  if (name) {
    const foundContainer = await Container.findOne({ name });

    if (foundContainer) {
      return foundContainer;
    } else {
      throw new Error("Container not found");
    }
  } else {
    const containers = await Container.find({});
    if (containers) return containers;
    else throw new Error("Couldn't access DB");
  }
};

/**
 * @param {*} id
 * @returns container by ID
 */
const getContainerByID = async (id) => {
  try {
    const foundContainer = await Container.findById(id);

    if (foundContainer) return foundContainer;
    else throw new Error("Container not found");
  } catch (error) {
    throw new Error(`${error.message}`);
  }
};

/**
 * @param {*} id
 * @returns success message
 */
const deleteContainer = async (id) => {
  if (id) {
    const deleteContainer = await Container.findByIdAndDelete(id);

    if (deleteContainer) {
      const containers = await getContainers();
      return containers;
    } else throw new Error(`Container with id ${id} doesn't exist`);
  } else {
    throw new Error("You need to specify the id");
  }
};

/**
 * @param {*} container
 * @returns created platfrom
 */
const createContainer = async (container) => {
  const { name } = container;

  if (name) {
    const foundRepeated = await Container.findOne({ name });

    if (foundRepeated) {
      throw new Error("A container with that same name already exists");
    } else {
      const newContainer = await Container.create(container);

      if (newContainer) {
        const containers = await getContainers();
        return containers;
      } else {
        throw new Error("There was an error creating the container");
      }
    }
  } else {
    throw new Error("You need to provide a name for the container");
  }
};

/**
 * @returns created containers
 */
const importContainers = async (containerList) => {
  const errors = [];
  const success = [];

  try {
    for (const file of containerList) {
      for (let i = 0; i < file.content.length; i++) {
        const container = file.content[i];

        try {
          const allContainers = await Container.find({});
          const uniqueName = await getUniqueName(
            allContainers,
            container.name || "Untitled"
          );
          container.name = uniqueName;

          delete container.updatedAt;
          delete container.createdAt;
          delete container.__v;

          const created = await Container.create(container);

          success.push(
            `Container '${created.name}' from file ${file.fileName}`
          );
        } catch (error) {
          const apiError = error.message.replace(
            /containers validation failed:/i,
            " "
          );
          const errorMessage = `An error occurred at the ${container.fileName} file:
        ${apiError}`;
          errors.push(errorMessage);
        }
      }
    }

    return { errors, success };
  } catch (error) {
    throw new Error("Couldn't save the data");
  }
};

/**
 * @param {*} id
 * @param {*} container
 * @returns updated container
 */
const updateContainer = async (id, container) => {
  if (!id || !container)
    throw new Error("You need to provide id and new container data to update");

  // Checks if name is the same
  const { name } = container;
  const oldContainer = await Container.findById(id);
  const checkName = oldContainer.name === name;

  // If name has changed, it checks for repeated names.
  if (!checkName) {
    const foundRepeated = await Container.findOne({ name });

    if (foundRepeated)
      throw new Error("A container with that same name already exists");
  }

  try {
    Object.assign(oldContainer, container);

    await oldContainer.save();
    const containers = await getContainers();
    return containers;
  } catch (error) {
    console.log(error);
    throw new Error(error.message);
  }
};

module.exports = {
  getContainers,
  getContainerByID,
  deleteContainer,
  createContainer,
  updateContainer,
  importContainers,
};
