const Tool = require("../db/models/toolModel.js");
const { getUniqueName } = require("../utils/getUniqueName.js");

/**
 * @returns all Tools or Tools by name if name is provided
 */
const getAllTools = async () => {
  const tool = await Tool.find({});
  if (tool) return tool;
  else throw new Error("Couldn't access DB");
};

/**
 * @param {*} id
 * @returns Tool by ID
 */
const getToolById = async (id) => {
  try {
    const foundTool = await Tool.findById(id);

    if (foundTool) return foundTool;
    else {
      throw new Error("Tool not found");
    }
  } catch (error) {
    throw new Error(`${error.message}`);
  }
};

/**
 * @param {*} name
 * @returns Tool by ID
 */
const getToolByName = async (name) => {
  try {
    const foundTool = await Tool.find({ name: name });

    if (foundTool) return foundTool;
    else {
      throw new Error("Tool not found");
    }
  } catch (error) {
    throw new Error(`${error.message}`);
  }
};

/**
 * @param {*} id
 * @returns success message
 */
const deleteTool = async (id) => {
  if (id) {
    const deleteTool = await Tool.findByIdAndDelete(id);

    if (deleteTool) {
      const tools = await getAllTools();
      return tools;
    } else throw new Error(`Tool with id ${id} doesn't exist`);
  } else {
    throw new Error("You need to specify the id");
  }
};

/**
 * Creates a tool with name ("Untitled" if no name is provided), type and parameters. This are required
 * @param {*} tool
 * @returns created Tool
 */
const createTool = async (tool) => {
  const { name, type } = tool;

  if (!name || !type) throw new Error("The name and type are required");

  const allTools = await Tool.find({});

  const uniqueName = await getUniqueName(allTools, name || "Untitled");

  tool.name = uniqueName;

  const newTool = await Tool.create(tool);

  if (newTool) {
    const tools = await getAllTools();
    return tools;
  } else throw new Error("Couldn't create tool");
};

/**
 * Updates tool basic info.
 * @param {*} id, tool
 * @returns updated tool
 */
const updateTool = async (id, updateTool) => {
  if (updateTool && id) {
    const { name } = updateTool;

    if (name) {
      // Checks if name hasn't changed
      const oldTool = await Tool.findById(id);
      const nameChanged = !(oldTool.name === name);

      // If name has changed, it checks for repeated name within the workspace
      if (nameChanged) {
        const allTools = await Tool.find({});

        const uniqueName = await getUniqueName(allTools, name || "Untitled");
        updateTool.name = uniqueName;
      }
    }

    // Updates Tool
    const tool = await Tool.findByIdAndUpdate(id, updateTool);

    if (tool) {
      const tools = await getAllTools();
      return tools;
    } else {
      throw new Error("There was an error updating the tool");
    }
  } else {
    throw new Error("updateTool: You need to provide new information to update");
  }
};

/**
 * @returns imported tools
 */
const importTools = async (toolList) => {
  const errors = [];
  const success = [];

  try {
    for (const file of toolList) {
      for (let i = 0; i < file.content.length; i++) {
        const tool = file.content[i];

        try {
          const alltools = await Tool.find({});
          const uniqueName = await getUniqueName(
            alltools,
            tool.name || "Untitled"
          );
          tool.name = uniqueName;

          delete tool.updatedAt;
          delete tool.createdAt;
          delete tool.__v;

          const created = await Tool.create(tool);

          success.push(`Tool '${created.name}' from file ${file.fileName}`);
        } catch (error) {
          const apiError = error.message.replace(
            /tools validation failed:/i,
            " "
          );
          const errorMessage = `An error occurred at the ${tool.fileName} file:
        ${apiError}`;
          errors.push(errorMessage);
        }
      }
    }

    return { errors, success };
  } catch (error) {
    throw new Error("Couldn't save the data");
  }
};

module.exports = {
  getAllTools,
  getToolById,
  getToolByName,
  deleteTool,
  createTool,
  updateTool,
  importTools,
};
