const { Router } = require("express");
const {
  createProtocol,
  updateProtocol,
  getAllProtocols,
  getProtocolByID,
  getProtocolsByWorkspace,
  deleteProtocol,
  validateProtocol,
  getAllMidLevelProtocols,
  getMidLevelProtocolByID,
  getMidLevelProtocolByProtocol,
  createProtocolFromTemplate,
  importProtocol,
  updateProtocolFromTemplate,
  updateProtocolSteps,
  deleteProtocolStep,
} = require("../controllers/protocolController.js");
const { getSettings, getSettingsData } = require("../controllers/settingsController.js");

let protocolRouter = Router();

protocolRouter
  .route("/")
  /**
   * @route  GET /api/protocols
   * @query id, name, workspaceID, workspaceName
   * @returns if no query provided: returns all protocols
   * @returns if id is provided by query: returns protocol by id
   * @returns if name is provided by query: returns protocols with that name
   * @returns if workspaceID or workspaceName is provided: returns all protocols associated with that workspace
   */
  .get(async (req, res) => {
    const workspaceID = req.query.workspaceID;
    if (workspaceID) {
      try {
        res.status(200).send(await getProtocolsByWorkspace(workspaceID));
      } catch (error) {
        res.status(404).send(error.message);
      }
    } else {
      const id = req.query.id;
      if (id) {
        try {
          res.status(200).send(await getProtocolByID(id));
        } catch (error) {
          res.status(404).send(error.message);
        }
      } else {
        const name = req.query.name;

        try {
          res.status(200).send(await getAllProtocols(name));
        } catch (error) {
          res.status(404).send(error.message);
        }
      }
    }
  })
  /**
   * Creates the protocol with basic info (name, description and workspace)
   * @route  POST /api/protocols
   * @returns created protocol
   */
  .post(async (req, res) => {
    const { protocol, workspaceValues } = req.body;

    if (protocol?.template?.length && workspaceValues) {
      console.log("Getting new templated protocol from the controller.")
      try {
        const settings = await getSettingsData();
        const socketResponse = await createProtocolFromTemplate(
          req,
          protocol,
          workspaceValues,
          settings
        );
        res.status(200).send(socketResponse);
      } catch (error) {
        res.status(400).json({ error: error.message });
      }
    } else {
      console.log("Creating protocol.")
      try {
        const createdProtocol = await createProtocol(protocol);
        res.status(200).send(createdProtocol);
      } catch (error) {
        res.status(400).json({ error: error.message });
      }
    }
  })
  /**
   * Edits protocol
   * @route  PUT /api/protocols
   * @query id
   * @returns updated protocol
   */
  .put(async (req, res) => {
    const { protocol, workspaceValues } = req.body;
    const id = req.query.id;

    if (protocol?.template?.length && workspaceValues) {
      console.log("Getting updated templated protocol from the controller.")
      try {
        const settings = await getSettingsData();
        const socketResponse = await updateProtocolFromTemplate(
          req,
          protocol,
          workspaceValues,
          settings
        );
        res.status(200).send(socketResponse);
      } catch (error) {
        res.status(400).json({ error: error.message });
      }
    } else {
      console.log("Updating protocol.")
      try {
        const updatedProtocol = await updateProtocol(id, protocol);
        res.status(200).send(updatedProtocol);
      } catch (error) {
        res.status(400).send(error.message);
      }
    }
  })
  /**
   * @route  DELETE /api/protocols
   * @query id
   * @returns deleted protocol
   */
  .delete(async (req, res) => {
    const id = req.query.id;
    try {
      res.status(200).send(await deleteProtocol(id));
    } catch (error) {
      res.status(400).send(error.message);
    }
  });

protocolRouter
  .route("/steps")
  .put(async (req, res) => {
    const { protocolID, steps } = req.body;

    try {
      const updatedProtocol = await updateProtocolSteps(protocolID, steps);

      res.status(200).send(updatedProtocol);
    } catch (error) {
      res.status(400).send(error.message);
    }
  })
  .delete(async (req, res) => {
    const protocolID = req.query.protocolID;
    const stepID = req.query.stepID;

    try {
      res.status(200).send(await deleteProtocolStep(protocolID, stepID));
    } catch (error) {
      res.status(400).send(error.message);
    }
  });

/** Protocol validation */
protocolRouter.route("/validate").post(async (req, res) => {
  const { protocolID } = req.body;

  if (!protocolID) {
    return res.status(400).send("You need to send a protocol");
  }
  try {
    const validation = await validateProtocol(protocolID);

    res.status(200).send(validation);
  } catch (error) {
    res.status(400).send(error.message);
  }
});

/** Mid level protocols */
protocolRouter.route("/midLevel").get(async (req, res) => {
  const hLprotocol = req.query.hLprotocol;
  const workspace = req.query.workspace;

  if (hLprotocol && workspace) {
    try {
      res
        .status(200)
        .send(await getMidLevelProtocolByProtocol(hLprotocol, workspace));
    } catch (error) {
      res.status(404).send(error.message);
    }
  } else {
    const id = req.query.id;
    if (id) {
      try {
        res.status(200).send(await getMidLevelProtocolByID(id));
      } catch (error) {
        res.status(404).send(error.message);
      }
    } else {
      const name = req.query.name;

      try {
        res.status(200).send(await getAllMidLevelProtocols(name));
      } catch (error) {
        res.status(404).send(error.message);
      }
    }
  }
});

/**
 * @route  post /api/protocols/import
 * @returns protocols created
 */
protocolRouter.route("/import").post(async (req, res) => {
  const { protocols, workspaceID } = req.body;
  try {
    const protocolCreated = await importProtocol(protocols, workspaceID);
    res.status(200).send(protocolCreated);
  } catch (error) {
    res.status(400).send(error.message);
  }
});

module.exports = protocolRouter;
