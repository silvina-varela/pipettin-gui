const { Router } = require("express");
const {
  getWorkspaces,
  getWorkspaceByID,
  createWorkspace,
  deleteWorkspace,
  updateWorkspace,
  duplicateWorkspace,
  updateWorkspaceItems,
  updateItemContents,
  deleteWorkspaceItem,
  deleteItemContents,
  exportWorkspaces,
  importWorkspaces,
  duplicateWorkspaceItem,
} = require("../controllers/workspaceController.js");

let workspaceRouter = Router();

workspaceRouter
  .route("/")
  /**
   * @route  GET /api/workspaces
   * @query id & name
   * @returns if no query, returns all workspaces. If name and no id, returns workspaces by name. If id, returns workspaces by id
   */
  .get(async (req, res) => {
    const id = req.query.id;

    if (id) {
      try {
        res.status(200).send(await getWorkspaceByID(id));
      } catch (error) {
        res.status(404).send(error.message);
      }
    } else {
      const name = req.query.name;

      try {
        res.status(200).send(await getWorkspaces(name));
      } catch (error) {
        res.status(404).send(error.message);
      }
    }
  })
  /**
   * @route  POST /api/workspaces
   * @returns created workspace
   */
  .post(async (req, res) => {
    const newWorkspace = req.body.newWorkspace;
    const inheritData = req.body.inheritData;

    try {
      const createdWorkspace = await createWorkspace(newWorkspace, inheritData);
      res.status(200).send(createdWorkspace);
    } catch (error) {
      res.status(400).send(error.message);
    }
  })
  /**
   * @route  PUT /api/workspaces
   * @query id
   * @returns updated workspace
   */
  .put(async (req, res) => {
    const workspace = req.body;
    const id = req.query.id;

    try {
      const updatedWorkspace = await updateWorkspace(id, workspace);
      res.status(200).send(updatedWorkspace);
    } catch (error) {
      res.status(400).send(error.message);
    }
  })
  /**
   * @route  DELETE /api/workspaces
   * @query id
   * @returns deleted workspace
   */

  .delete(async (req, res) => {
    const id = req.query.id;
    try {
      res.status(200).send(await deleteWorkspace(id));
    } catch (error) {
      res.status(400).send(error.message);
    }
  });

workspaceRouter
  .route("/items")
  /**
   * @route  PUT /api/workspaces/items
   * @returns updated workspace
   */
  .put(async (req, res) => {
    const item = req.body.item; // items to add or update existing one
    const workspaceID = req.body.workspaceID;

    try {
      const updatedWorkspace = await updateWorkspaceItems(workspaceID, item);
      res.status(200).send(updatedWorkspace);
    } catch (error) {
      res.status(400).send(error.message);
    }
  })

  /**
   * @route  DELETE /api/workspaces/items
   * @returns updated workspace
   */
  .delete(async (req, res) => {
    const workspaceID = req.query.workspaceID;
    const itemID = req.query.itemID;

    console.log("Delete tool with ID:", { workspaceID });
    try {
      res.status(200).send(await deleteWorkspaceItem(workspaceID, itemID));
    } catch (error) {
      res.status(400).send(error.message);
    }
  });

workspaceRouter
  .route("/contents")
  /**
   * @route  PUT /api/workspaces/contents
   * @returns updated workspace
   */
  .put(async (req, res) => {
    const contents = req.body.contents; // contents to add or update existing ones
    const workspaceID = req.body.workspaceID;
    const itemID = req.body.itemID;
    const deleteContents = req.body.deleteContents;
    try {
      if (deleteContents) {
        const updatedWorkspace = await deleteItemContents(
          workspaceID,
          itemID,
          contents
        );
        res.status(200).send(updatedWorkspace);
      } else {
        const updatedWorkspace = await updateItemContents(
          workspaceID,
          itemID,
          contents
        );
        res.status(200).send(updatedWorkspace);
      }
    } catch (error) {
      res.status(400).send(error.message);
    }
  });

workspaceRouter.route("/duplicate").post(async (req, res) => {
  const workspace = req.body;
  const params = req.query;

  try {
    const duplicatedWorkspace = await duplicateWorkspace(workspace, params);
    res.status(200).send(duplicatedWorkspace);
  } catch (error) {
    res.status(400).send(error.message);
  }
});

workspaceRouter.route("/export").post(async (req, res) => {
  const workspaces = req.body;
  try {
    const exportedWorkspaces = await exportWorkspaces(workspaces);
    res.status(200).send(exportedWorkspaces);
  } catch (error) {
    console.log(error);
    res.status(400).send(error.message);
  }
});

workspaceRouter.route("/import").post(async (req, res) => {
  const workspaces = req.body;
  try {
    const importedWorkspaces = await importWorkspaces(workspaces);
    res.status(200).send(importedWorkspaces);
  } catch (error) {
    console.log(error);
    res.status(400).send(error.message);
  }
});

workspaceRouter.route("/duplicateItem").post(async (req, res) => {
  const { workspace, item } = req.body;
  try {
    const updatedWorkspace = await duplicateWorkspaceItem(workspace, item);
    res.status(200).send(updatedWorkspace);
  } catch (error) {
    console.log(error);
    res.status(400).send(error.message);
  }
});

module.exports = workspaceRouter;
