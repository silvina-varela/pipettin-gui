const { Router } = require("express");
const {
  getTemplates,
  getTemplateByID,
  deleteTemplate,
  createTemplate,
  updateTemplate
} = require("../controllers/protocolTemplatesController.js");

let protocolTemplatesRouter = Router();

protocolTemplatesRouter
  .route("/")
  /**
   * @route  GET /api/templates
   * @query id & name
   * @returns if no query, returns all templates. If name and no id, returns templates by name. If id, returns template by id
   */
  .get(async (req, res) => {
    const id = req.query.id;

    if (id) {
      try {
        res.status(200).send(await getTemplateByID(id));
      } catch (error) {
        res.status(404).send(error.message);
      }
    } else {
      const name = req.query.name;

      try {
        res.status(200).send(await getTemplates(name));
      } catch (error) {
        res.status(404).send(error.message);
      }
    }
  })
  /**
   * @route  POST /api/templates
   * @returns created template
   */
  .post(async (req, res) => {
    const template = req.body;

    try {
      const createdTemplate = await createTemplate(template);
      res.status(200).send(createdTemplate);
    } catch (error) {
      res.status(400).send(error.message);
    }
  })
  /**
   * @route  PUT /api/templates
   * @query id
   * @returns updated template
   */
  .put(async (req, res) => {
    const template = req.body;

    try {
      const updatedTemplate = await updateTemplate(template);
      res.status(200).send(updatedTemplate);
    } catch (error) {
      res.status(400).send(error.message);
    }
  })
  /**
   * @route  DELETE /api/templates
   * @query id
   * @returns deleted template
   */
  .delete(async (req, res) => {
    const id = req.query.id;
    try {
      res.status(200).send(await deleteTemplate(id));
    } catch (error) {
      res.status(400).send(error.message);
    }
  });

module.exports = protocolTemplatesRouter;
