const { Router } = require("express");
const {
  getPlatforms,
  getPlatformByID,
  createPlatform,
  deletePlatform,
  updatePlatform,
  createPlatformsInBulk
} = require("../controllers/platformController.js");

let platformRouter = Router();

platformRouter
  .route("/")
  /**
   * @route  GET /api/platforms
   * @query id & name
   * @returns if no query, returns all platforms. If name and no id, returns platform by name. If id, returns platform by id
   */
  .get(async (req, res) => {
    const id = req.query.id;

    if (id) {
      try {
        res.status(200).send(await getPlatformByID(id));
      } catch (error) {
        res.status(404).send(error.message);
      }
    } else {
      const name = req.query.name;

      try {
        res.status(200).send(await getPlatforms(name));
      } catch (error) {
        res.status(404).send(error.message);
      }
    }
  })
  /**
   * @route  POST /api/platforms
   * @returns created platform
   */
  .post(async (req, res) => {
    const platform = req.body;
    // NOTE: This endpoint is used by "createPlatformAsync", which
    // expects "data" and passes it on as its payload.
    try {
      const createdPlatform = await createPlatform(platform);
      res.status(200).send(createdPlatform);
    } catch (error) {
      console.log(error)
      res.status(400).send(error.message);
    }
  })
  /**
   * @route  PUT /api/platforms
   * @query id
   * @returns updated platform
   */
  .put(async (req, res) => {
    const platform = req.body;
    const id = req.query.id;

    try {
      const updatedPlatform = await updatePlatform(id, platform);
      res.status(200).send(updatedPlatform);
    } catch (error) {
      res.status(400).send(error.message);
    }
  })
  /**
   * @route  DELETE /api/platforms
   * @query id
   * @returns deleted platform
   */
  .delete(async (req, res) => {
    const id = req.query.id;
    try {
      res.status(200).send(await deletePlatform(id));
    } catch (error) {
      res.status(400).send(error.message);
    }
  });

    /**
   * @route  post /api/platforms/createPlatformInBulk
   * @returns returns true if platform name exists
   */
    platformRouter.route("/createPlatformsInBulk")
    .post(async (req, res) => {
      const listPlatforms = req.body;
      try {
        const createdPlatforms = await createPlatformsInBulk(listPlatforms);
        if (createdPlatforms.length) res.status(400).send(createdPlatforms);
        else res.status(200).send(createdPlatforms);
      } catch (error) {
        res.status(400).send(error.message);
      }
    });

      /**
   * @route  GET /api/platforms/check/name
   * @param name
   * @returns returns true if platform name exists
   */
platformRouter.route("/check/:name")
  .get(async (req, res) => {
    const name = req.params.name;

    try {
      const nameExists = await getPlatforms(name);

      return res
        .status(200)
        .send({
          exists: true,
          message: `Platform name ${name} isn't available`,
        });
    } catch (error) {
      return res
        .status(200)
        .send({ exists: false, message: `Platform name ${name} is available` });
    }
  });

module.exports = platformRouter;
