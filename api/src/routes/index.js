const { Router } = require("express");
const platformRouter = require("./platforms.js");
const workspaceRouter = require("./workspaces.js");
const protocolRouter = require("./protocols.js");
const robotRouter = require("./robot.js");
const settingsRouter = require("./settings.js");
const toolRouter = require("./tools.js");
const protocolTemplatesRouter = require("./protocolTemplates.js");
const containerRouter = require("./containers.js");

let router = Router();

router.use("/platforms", platformRouter);
router.use("/workspaces", workspaceRouter);
router.use("/protocols", protocolRouter);
router.use("/robot", robotRouter);
router.use("/settings", settingsRouter);
router.use("/tools", toolRouter);
router.use("/templates", protocolTemplatesRouter);
router.use("/containers", containerRouter);


module.exports = router;
