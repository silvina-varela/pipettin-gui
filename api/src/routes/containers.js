const { Router } = require("express");
const {
  getContainers,
  getContainerByID,
  createContainer,
  deleteContainer,
  updateContainer,
  importContainers
} = require("../controllers/containerController.js");

let containerRouter = Router();

containerRouter
  .route("/")
  /**
   * @route  GET /api/containers
   * @query id & name
   * @returns if no query, returns all containers. If name and no id, returns container by name. If id, returns container by id
   */
  .get(async (req, res) => {
    const id = req.query.id;

    if (id) {
      try {
        res.status(200).send(await getContainerByID(id));
      } catch (error) {
        res.status(404).send(error.message);
      }
    } else {
      const name = req.query.name;

      try {
        res.status(200).send(await getContainers(name));
      } catch (error) {
        res.status(404).send(error.message);
      }
    }
  })
  /**
   * @route  POST /api/containers
   * @returns created container
   */
  .post(async (req, res) => {
    const container = req.body;

    try {
      const createdContainer = await createContainer(container);
      res.status(200).send(createdContainer);
    } catch (error) {
      res.status(400).send(error.message);
    }
  })
  /**
   * @route  PUT /api/containers
   * @query id
   * @returns updated container
   */
  .put(async (req, res) => {
    const container = req.body;
    const id = req.query.id;

    try {
      const updatedContainer = await updateContainer(id, container);
      res.status(200).send(updatedContainer);
    } catch (error) {
      res.status(400).send(error.message);
    }
  })
  /**
   * @route  DELETE /api/containers
   * @query id
   * @returns deleted container
   */
  .delete(async (req, res) => {
    const id = req.query.id;
    try {
      res.status(200).send(await deleteContainer(id));
    } catch (error) {
      res.status(400).send(error.message);
    }
  });

    /**
   * @route  post /api/containers/import
   * @returns returns true if container name exists
   */
    containerRouter.route("/import")
    .post(async (req, res) => {
      const listContainers = req.body;
      try {
        const createdContainers = await importContainers(listContainers);
        if (createdContainers.length) res.status(400).send(createdContainers);
        else res.status(200).send(createdContainers);
      } catch (error) {
        res.status(400).send(error.message);
      }
    });

      /**
   * @route  GET /api/containers/check/name
   * @param name
   * @returns returns true if container name exists
   */
containerRouter.route("/check/:name")
  .get(async (req, res) => {
    const name = req.params.name;

    try {
      const nameExists = await getContainers(name);

      return res
        .status(200)
        .send({
          exists: true,
          message: `Container name ${name} isn't available`,
        });
    } catch (error) {
      return res
        .status(200)
        .send({ exists: false, message: `Container name ${name} is available` });
    }
  });

module.exports = containerRouter;
