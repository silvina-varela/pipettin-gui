const { Router } = require("express");
const {
  getSettings,
  updateSettings,
  resetDatabase,
  seedDatabase,
  changeDatabase,
  listDatabases,
} = require("../controllers/settingsController.js");

let settingsRouter = Router();

settingsRouter
  .route("/")
  /**
   * @route  GET /api/settings
   * @returns settings
   */
  .get(async (req, res) => {
    try {
      res.status(200).send(await getSettings());
    } catch (error) {
      res.status(404).send(error.message);
    }
  })
  /**
   * @route  POST /api/settings
   * @returns created settings
   */
  .post(async (req, res) => {
    const newDatabase = req.body;

    try {
      const response = await changeDatabase(newDatabase);
      console.log("Change database response:", {response})
      res.status(200).send(response);
    } catch (error) {
      res.status(400).send(error.message);
    }
  })
  /**
   * @route  PUT /api/settings
   * @returns updated settings
   */
  .put(async (req, res) => {
    const settings = req.body;

    try {
      const updatedSettings = await updateSettings(settings);
      res.status(200).send(updatedSettings);
    } catch (error) {
      console.error(error);
      res.status(400).send(error.message);
    }
  });

settingsRouter.route("/resetDatabase").post(async (req, res) => {
  try {
    const response = await resetDatabase();
    res.status(200).send(response);
  } catch (error) {
    console.error(error);
    res.status(400).send(error.message);
  }
});

settingsRouter.route("/seedDatabase").post(async (req, res) => {
  try {
    const response = await seedDatabase();
    res.status(200).send(response);
  } catch (error) {
    console.error(error);
    res.status(400).send(error.message);
  }
});

// New route to list all databases
settingsRouter.route("/listDatabases").get(async (req, res) => {
  try {
    const databases = await listDatabases(); // Call the function to list databases
    res.status(200).send(databases); // Send back the list of databases
  } catch (error) {
    console.error('Failed to list databases:', error);
    res.status(500).send(error.message);
  }
});

module.exports = settingsRouter;
