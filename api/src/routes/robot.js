const { Router } = require("express");
const {
  killProcess,
  stopProtocol, pauseProtocol, continueProtocol,
  runPark,
  runCalibration,
  runCommand,
} = require("../controllers/robotController.js");
const { validateProtocol } = require("../controllers/protocolController.js");
const { getSettings, getSettingsData } = require("../controllers/settingsController.js");
const { exec } = require("child_process");

let robotRouter = Router();

/** Protocol Run */
robotRouter.route("/run").post(async (req, res) => {
  const { protocolID } = req.body;

  if (!req.connected) {
    console.log("Controller not connected");
    return res.status(400).send("The controller is not connected");
  }

  if (!protocolID) {
    return res.status(400).send("You need to send a protocol");
  }
  try {
    const middleLevelProtocol = await validateProtocol(protocolID);
    const settings = await getSettingsData();

    const args = {
      name: middleLevelProtocol.name,
      protocolID: middleLevelProtocol._id.toString(),
      settings,
    };

    await runCommand(req, args);

    res.status(200).send(middleLevelProtocol);
  } catch (error) {
    console.log(error);
    res.status(400).send(error.message);
  }
});
/**
 * Stops the process
 */
robotRouter.route("/stop").post(async function (req, res) {
  if (!req.connected) {
    console.log("Controller not connected");
    return res.status(400).send("The controller is not connected");
  }

  try {
    let response = await killProcess(req);
    res.status(200).json(response);
  } catch (error) {
    res.status(400).send(error.message);
  }
});

/**
 * Restart the controller process
 */
robotRouter.route("/restart-controller").post(async function (req, res) {
  console.log("Restarting the controller...");

  // Requires polkit setup when API run with user permissions only:
  // https://gitlab.com/pipettin-bot/pipettin-bot/-/issues/349
  const command = `systemctl restart piper.service`;

  exec(command, (error, stdout, stderr) => {
    if (error) {
      console.error(`Error restarting service: ${stderr}`);
      return res.status(500).json({ error: "Failed to restart the service.", details: stderr });
    }

    console.log(`Controller restarted successfully: ${stdout}`);
    res.status(200).json({ message: `Controller service restarted successfully.` });
  });
});

/**
 * Stops the protocol
 */
robotRouter.route("/stop_protocol").post(async function (req, res) {
  if (!req.connected) {
    console.log("Controller not connected");
    return res.status(400).send("The controller is not connected");
  }

  try {
    let response = await stopProtocol(req);
    res.status(200).json(response);
  } catch (error) {
    res.status(400).send(error.message);
  }
});

/**
 * Pauses the protocol
 */
robotRouter.route("/pause_protocol").post(async function (req, res) {
  if (!req.connected) {
    console.log("Controller not connected");
    return res.status(400).send("The controller is not connected");
  }

  try {
    let response = await pauseProtocol(req);
    res.status(200).json(response);
  } catch (error) {
    res.status(400).send(error.message);
  }
});

/**
 * Pauses the protocol
 */
robotRouter.route("/continue_protocol").post(async function (req, res) {
  if (!req.connected) {
    console.log("Controller not connected");
    return res.status(400).send("The controller is not connected");
  }

  try {
    let response = await continueProtocol(req);
    res.status(200).json(response);
  } catch (error) {
    res.status(400).send(error.message);
  }
});

/**
 * Sends request for moving the robot to a specific position
 */
robotRouter.route("/goto").post(async function (req, res) {
  if (!req.connected) {
    console.log("Controller not connected");
    return res.status(400).send("The controller is not connected");
  }

  const { position, tool, workspace } = req.body;

  // Set options for command
  if (position) {
    try {
      const options = {
        position,
        tool,
        workspace,
      };
      console.log(options);
      const calibration = await runCalibration(req, options);
      res.send(calibration);
    } catch (e) {
      res.status(400).send(e.message);
    }
  }
});

/**
 * Sends request for parking a specific tool
 */
robotRouter.route("/park").post(async function (req, res) {
  if (!req.connected) {
    console.log("Controller not connected");
    return res.status(400).send("The controller is not connected");
  }

  const { tool, workspace } = req.body;

  // Set options for command
  try {
    // NOTE: An empty "tool" is valid in this case,
    // indicating that the controller shoudl know
    // that its toolhead is empty (i.e. has no tool).
    const options = {
      tool,
      workspace,
    };
    console.log(options);
    const park = await runPark(req, options);
    res.send(park);
  } catch (e) {
    res.status(400).send(e.message);
  }
});


/**
 * Shows robot position
 */
robotRouter.route("/position").get(async function (req, res) {
  if (!req.connected) {
    console.log("Controller not connected");
    return res.status(400).send("The controller is not connected");
  }

  const robot_position = req.robot_position;
  console.log(robot_position);

  if (robot_position) return res.status(200).send(robot_position);
  else return res.status(400).send("Cannot access robot position");
});
module.exports = robotRouter;
