const { Router } = require("express");
const {
  getAllTools,
  getToolById,
  getToolByName,
  deleteTool,
  createTool,
  updateTool,
  importTools,
} = require("../controllers/toolController.js");

let toolRouter = Router();

toolRouter
  .route("/")
  /**
   * @route  GET /api/tools
   * @query id, name
   * @returns if no query provided: returns all tools
   * @returns if id is provided by query: returns tool by id
   * @returns if name is provided by query: returns tool with that name
   * @returns if id or name is provided: returns all tools.
   */
  .get(async (req, res) => {
    const id = req.query.id;

    if (id) {
      try {
        res.status(200).send(await getToolById(id));
      } catch (error) {
        res.status(404).send(error.message);
      }
    } else {
      const name = req.query.name;

      try {
        res.status(200).send(await getAllTools(name));
      } catch (error) {
        res.status(404).send(error.message);
      }
    }
  })
  /**
   * @route  POST /api/tools
   * @returns created workspace
   */
  .post(async (req, res) => {
    const tool = req.body;

    try {
      const _createTool = await createTool(tool);
      res.status(200).send(_createTool);
    } catch (error) {
      res.status(400).send(error.message);
    }
  })
  /**
   * This endpoint edits the tool.
   * @route  PUT /api/tools
   * @query id
   * @returns updated tool
   */
  .put(async (req, res) => {
    const tool = req.body;
    const id = req.query.id;

    try {
      const updatedTool = await updateTool(id, tool);
      res.status(200).send(updatedTool);
    } catch (error) {
      res.status(400).send(error.message);
    }
  })

  /**
   * @route  DELETE /api/tools
   * @query id
   * @returns deleted tool
   */
  .delete(async (req, res) => {
    const id = req.query.id;
    console.log("Delete tool with ID:", id);
    try {
      res.status(200).send(await deleteTool(id));
    } catch (error) {
      res.status(400).send(error.message);
    }
  });

/**
 * @route  post /api/tools/import
 * @returns returns true if tool name exists
 */
toolRouter.route("/import").post(async (req, res) => {
  const listTools = req.body;
  try {
    const createdTools = await importTools(listTools);
    if (createdTools.length) res.status(400).send(createdTools);
    else res.status(200).send(createdTools);
  } catch (error) {
    res.status(400).send(error.message);
  }
});

module.exports = toolRouter;
