const { actionBuilder } = require("./actionBuilder.js");

const {
  getMixAction,
  getPickTipAction,
  getWaitAction,
  getHumanAction,
  getLoadLiquidAction,
  getDropLiquidAction,
  getDiscardTipAction,
  getCommentAction,
  getJsonAction,
} = actionBuilder;

//TODO: check col, row
/**
 * Computes the reTag for a given definition and source, replacing placeholders with actual values.
 *
 * @param {Object} definition - The step definition.
 * @param {Object} source - The source information.
 * @returns {string} - The computed reTag value.
 */
function computeReTag(definition, source) {
  // Check if the target has an 'addTag' property and its 'value' is defined.
  if (definition.target.addTag && definition.target.addTag.value) {
    // Replace placeholders in the tag value with actual source information.
    let newTag = definition.target.addTag.value.replace(
      /\{\{source\.name\}\}/g,
      source.content.name
    );
    // Replace '{{source.position}}' with the formatted column-row position value (e.g. "2:4").
    newTag = newTag.replace(
      /\{\{source\.position\}\}/g,
      `${source.content.position.col}:${source.content.position.row}`
    );
    // Return the computed reTag value.
    return newTag;
  }
  // Return an empty string if no reTag is defined in the target.
  return "";
}

//TODO: check
/**
 * Updates the target volume in the current workspace.
 *
 * @param {Object} target - The target object.
 * @param {number} volume - The volume to be updated.
 * @param {Object} currentWorkspace - The current workspace.
 */
function updateTargetVolume(target, volume, currentWorkspace) {
  const content = getContentsInWorkspace(
    currentWorkspace,
    target.item,
    "name",
    target.content.name
  );
  if (content && content.length) {
    content[0].content.volume += volume;
  }
}

//TODO: throw errors for the front
/**
 * Retrieves contents in the workspace based on item name, search criteria, and content value.
 *
 * @param {Object} workspace - The workspace object.
 * @param {string} itemName - The item name.
 * @param {string} by - The search criteria (e.g., "name" or "tag").
 * @param {string} contentValue - The content value to match.
 * @returns {Array} - An array of contents in the workspace.
 */
function getContentsInWorkspace(workspace, itemName, by, contentValue) {
  const contents = [];

  if (workspace && workspace.items && workspace.items.length) {
    for (let i = 0; i < workspace.items.length; i++) {
      if (workspace.items[i].name === itemName || itemName === "") {
        if (
          workspace.items[i] &&
          workspace.items[i].content &&
          workspace.items[i].content.length
        ) {
          for (let j = 0; j < workspace.items[i].content.length; j++) {
            if (by === "name") {
              if (workspace.items[i].content[j].name === contentValue) {
                contents.push({
                  item: workspace.items[i].name,
                  content: workspace.items[i].content[j],
                });
              }
            } else if (by === "tag") {
              if (
                (workspace.items[i].content[j].tags || []).indexOf(
                  contentValue
                ) > -1
              ) {
                contents.push({
                  item: workspace.items[i].name,
                  content: workspace.items[i].content[j],
                });
              }
            }
          }
        }
      }
    }
  }
  return contents;
}

/**
 * Gets an available source and updates its volume in the workspace.
 *
 * @param {Array} sources - An array of sources.
 * @param {number} volume - The required volume.
 * @param {number} multiplexorOrNothing - The multiplexor or nothing value.
 * @returns {Object|boolean} - An object representing the selected source and volume, or false if no source is available.
 */
function getAvailableSourceAndUpdateIt(sources, volume, multiplexorOrNothing) {
  for (let i = 0; i < sources.length; i++) {
    if (sources[i].content.volume >= volume) {
      // pick from here
      sources[i].content.volume = sources[i].content.volume - volume;
      return { s: sources[i], v: volume };
    } else if (sources[i].content.volume > 0) {
      if (multiplexorOrNothing) {
        const howMany = sources[i].content.volume / multiplexorOrNothing;
        const roundHowMany = parseInt(howMany, 10);
        if (roundHowMany === 0) {
          // chau, no me sirve
          sources.splice(i, 1);
        } else {
          const currectVol = multiplexorOrNothing * roundHowMany;
          sources[i].content.volume = sources[i].content.volume - currectVol;
          return { s: sources[i], v: currectVol };
        }
      } else {
        const currectVol = sources[i].content.volume;
        sources[i].content.volume = 0;
        return { s: sources[i], v: currectVol };
      }
    } else {
      sources.splice(i, 1);
    }
  }
  return false;
}

/**
 * Gets an item from the workspace based on its name.
 *
 * @param {string} name - The name of the item.
 * @param {Object} workspace - The workspace object.
 * @returns {Object|null} - The item object or null if not found.
 */
function getItem(name, workspace) {
  if (workspace && workspace.items?.length) {
   const item =  workspace.items.find((el) => el.name === name)
    if(item) return item
    else return null;
  } else return null;
}

/**
 * Gets the tip volume based on the workspace, item, and tool.
 *
 * @param {Object} workspace - The workspace object.
 * @param {string} item - The item name.
 * @param {string} tool - The tool name.
 * @returns {number} - The tip volume.
 */
function getTipVolume(workspace, item) {
  const itemData = getItem(item, workspace);
  const content = itemData.content[0];
  if (content?.containerData?.type === "tip" && content?.containerData?.maxVolume) {
    const min = Math.min.apply(Math, [content.containerData.maxVolume]);
    return min;
  }
  return 0;
}

// TRANSFER STEP
/**
 * Builds a simple transfer step.
 *
 * @param {Object} step - The step object.
 * @param {Object} currentWorkspace - The current workspace object.
 * @returns {Array} - An array of actions for the step.
 */
function buildSimplePipettinStep(step, currentWorkspace) {
  if (
    step.definition.source.treatAs &&
    step.definition.source.treatAs === "for_each"
  ) {
    return buildSimplePippetinStepTreatEach(step, currentWorkspace);
  } else return buildSimplePipettinStepTreatSame(step, currentWorkspace);
}

// TRANSFER with source treat same
/**
 * Builds a mix step with source treated the same.
 *
 * @param {Object} step - The step object.
 * @param {Object} currentWorkspace - The current workspace object.
 * @returns {Array} - An array of actions for the step.
 */
function buildSimplePipettinStepTreatSame(step, currentWorkspace) {
  const actions = [];

  const sources = getContentsInWorkspace(
    currentWorkspace,
    step.definition.source.item,
    step.definition.source.by,
    step.definition.source.value
  );
  console.log("===sources", sources);
  if (!sources.length) {
    throw new Error(
      `On step ${step.order}: Invalid source. Please select a valid source.`
    );
  }

  const targets = getContentsInWorkspace(
    currentWorkspace,
    step.definition.target.item,
    step.definition.target.by,
    step.definition.target.value
  );
  console.log("===targets", targets);
  if (!targets.length) {
    throw new Error(
      `On step ${step.order}: Invalid target. Please select a valid target.`
    );
  }

  let volumeForEachTarget = parseFloat(step.definition.volume.value); // fixed_each

  if (step.definition.volume.type === "for_each_target_tag") {
    const volumeMultipler = getContentsInWorkspace(
      currentWorkspace,
      "",
      "tag",
      step.definition.volume.tag
    ).length;

    volumeForEachTarget = volumeMultipler * step.definition.volume.value; // for_each_target_tag
  } else if (step.definition.volume.type === "fixed_total") {
    volumeForEachTarget = step.definition.volume.value / targets.length; // fixed_total
  }

  const tipVolume = getTipVolume(currentWorkspace, step.definition.tip.item);

  if (!tipVolume) {
    throw new Error(`The tip rack used in step ${step.order} has no tips`);
  }

  const travelsForEachTarget = Math.ceil(volumeForEachTarget / tipVolume);

  console.log("==volumeForEachTarget==", volumeForEachTarget);
  console.log("==travelsForEachTarget==", travelsForEachTarget);

  if (volumeForEachTarget === 0) {
    throw new Error(`On step ${step.order}: volume to transfer can't be 0.`);
  }

  if (
    (step.definition.tip.mode === "isolated_source_only" ||
      step.definition.tip.mode === "reuse") &&
    volumeForEachTarget < tipVolume
  ) {
    // how many targets can I feed?
    let canFeed = parseInt(tipVolume / volumeForEachTarget, 10);
    let needTip = true;
    while (targets.length) {
      if (needTip) {
        actions.push(
          getPickTipAction(step.definition.tip.item, step.definition.tool)
        );
      }
      if (canFeed > targets.length) {
        canFeed = targets.length;
      }
      const pickVolume = canFeed * volumeForEachTarget;
      const usingSource = getAvailableSourceAndUpdateIt(
        sources,
        pickVolume,
        volumeForEachTarget
      );
      console.log("usingSource", usingSource);
      if (!usingSource) {
        throw new Error(
          `There is no source that meets the required volume to pick from it in step ${step.order}`
        );
      }
      actions.push(
        getLoadLiquidAction(
          usingSource.s.item,
          "name",
          usingSource.s.content.name,
          usingSource.v
        )
      );
      let travels = usingSource.v / volumeForEachTarget;
      if (travels > targets.length) {
        travels = targets.length;
      }
      for (let t = 0; t < travels; t++) {
        const target = targets.pop();
        actions.push(
          getDropLiquidAction(
            target.item,
            "name",
            target.content.name,
            volumeForEachTarget
          )
        );
        updateTargetVolume(target, volumeForEachTarget, currentWorkspace);
      }
      if (step.definition.tip.mode === "isolated_source_only") {
        actions.push(getDiscardTipAction(step.definition.tip.discardItem));
        needTip = true;
      } else {
        needTip = false;
      }
    }
    if (needTip === false) {
      actions.push(getDiscardTipAction(step.definition.tip.discardItem));
    }

    // isolated_targets (necesita un viaje por cada target) o no alcanza un solo viaje para completar todos.
  } else {
    let needTip = true; // TODO may re-use tip from previous step?
    for (let i = 0; i < targets.length; i++) {
      const target = targets[i];
      // travels
      let transfered = 0;
      for (let t = 0; t < travelsForEachTarget; t++) {
        if (needTip) {
          actions.push(
            getPickTipAction(step.definition.tip.item, step.definition.tool)
          );
        }
        const volumeLeft = volumeForEachTarget - transfered;
        let pickVolume = tipVolume;
        if (volumeLeft >= tipVolume) {
          pickVolume = tipVolume;
        } else {
          pickVolume = volumeLeft;
        }
        console.log("volumeLeft", volumeLeft);
        console.log("pickVolume", pickVolume);
        const usingSource = getAvailableSourceAndUpdateIt(sources, pickVolume);
        console.log("usingSource", usingSource);
        if (!usingSource) {
          throw new Error(
            `On step ${step.order}: There is no source that meets the required volume to pick from it.`
          );
        }
        actions.push(
          getLoadLiquidAction(
            usingSource.s.item,
            "name",
            usingSource.s.content.name,
            usingSource.v
          )
        );

        transfered = transfered + usingSource.v;
        actions.push(
          getDropLiquidAction(
            target.item,
            "name",
            target.content.name,
            usingSource.v
          )
        );
        updateTargetVolume(target, usingSource.v, currentWorkspace);

        // drop tip?
        if (
          step.definition.tip.mode === "isolated_source_only" ||
          step.definition.tip.mode === "isolated_targets"
        ) {
          actions.push(getDiscardTipAction(step.definition.tip.discardItem));
          needTip = true;
        } else {
          needTip = false;
        }
      } // end travels
    } // end target
    if (step.definition.tip.mode === "reuse") {
      // drop the final tip
      actions.push(getDiscardTipAction(step.definition.tip.discardItem));
    }
  } // end pipeting normal

  actions.push(getCommentAction("End step: " + step.name));
  
  actions.forEach((action) => {
    action.stepID = step._id
  })

  return actions;
}

// TRANSFER with source treat each
/**
 * Builds a mix step with source treated each.
 *
 * @param {Object} step - The step object.
 * @param {Object} currentWorkspace - The current workspace object.
 * @returns {Array} - An array of actions for the step.
 */
function buildSimplePippetinStepTreatEach(step, currentWorkspace) {
  const actions = [];
  console.log("for_each");

  const sources = getContentsInWorkspace(
    currentWorkspace,
    step.definition.source.item,
    step.definition.source.by,
    step.definition.source.value
  );

  if (!sources.length) {
    throw new Error(
      `On step ${step.order}: Invalid source. Please select a valid source.`
    );
  }

  const targets = getContentsInWorkspace(
    currentWorkspace,
    step.definition.target.item,
    step.definition.target.by,
    step.definition.target.value
  );

  if (!targets.length) {
    throw new Error(
      `On step ${step.order}: Invalid target. Please select a valid target (at least one target).`
    );
  }

  if (targets.length !== sources.length) {
    throw new Error(
      `On step ${step.order}: Invalid target vs. source count. Detected sources must be equal to detected targets.`
    );
  }

  let volumeForEachTarget = step.definition.volume.value; // fixed_each

  if (step.definition.volume.type === "for_each_target_tag") {
    const volumeMultipler = getContentsInWorkspace(
      currentWorkspace,
      "",
      "tag",
      step.definition.volume.tag
    ).length;

    volumeForEachTarget = volumeMultipler * step.definition.volume.value;
  } else if (step.definition.volume.type === "fixed_total") {
    volumeForEachTarget = step.definition.volume.value / targets.length;
  }

  const tipVolume = getTipVolume(currentWorkspace, step.definition.tip.item);

  if (!tipVolume) {
    throw new Error(
      `On step ${step.order}: No max volume defined in tip rack. Please set a maxVolume value in the tip container.`
    );
  }

  const travelsForEachTarget = Math.ceil(volumeForEachTarget / tipVolume);
  console.log("==volumeForEachTarget==", volumeForEachTarget);
  console.log("==travelsForEachTarget==", travelsForEachTarget);

  if (volumeForEachTarget === 0) {
    throw new Error(`On step ${step.order}: Volume to transfer can't be 0.`);
  }

  let needTip = true;
  for (let i = 0; i < sources.length; i++) {
    const source = sources[i];
    let transfered = 0;
    for (let t = 0; t < travelsForEachTarget; t++) {
      if (needTip) {
        actions.push(
          getPickTipAction(step.definition.tip.item, step.definition.tool)
        );
      }
      const volumeLeft = volumeForEachTarget - transfered;
      let pickVolume = tipVolume;
      if (volumeLeft >= tipVolume) {
        pickVolume = tipVolume;
      } else {
        pickVolume = volumeLeft;
      }
      console.log("volumeLeft", volumeLeft);
      console.log("pickVolume", pickVolume);
      const usingSource = getAvailableSourceAndUpdateIt([source], pickVolume);
      console.log("usingSource", usingSource);

      if (!usingSource) {
        throw new Error(
          `On step ${step.order}: There is no source that meets the required volume to pick from it.`
        );
      }
      actions.push(
        getLoadLiquidAction(
          usingSource.s.item,
          "name",
          usingSource.s.content.name,
          usingSource.v
        )
      );

      transfered = transfered + usingSource.v;

      // NOTE: Omitted passing "targets[i]" as it is not an argument to computeReTag since commit:
      // https://gitlab.com/pipettin-bot/pipettin-grbl-devel/-/commit/12905241930b6ddc69c45b419eee15419ebd7dc7
      // NOTE: this never raised an error because JS allows passing more arguments than declared:
      // https://stackoverflow.com/a/2525592/11524079
      const reTag = computeReTag(step.definition, usingSource.s);

      actions.push(
        getDropLiquidAction(
          (itemName = targets[i].item),
          (by = "name"),
          (value = targets[i].content.name),
          (volume = usingSource.v),
          // TODO: reTag is used to set addTag if not null. Check if this is used at all anywhere.
          (reTag = reTag)
        )
      );
      updateTargetVolume(targets[i], usingSource.v, currentWorkspace);
      // drop tip?
      if (
        step.definition.tip.mode === "reuse_same_source" ||
        step.definition.tip.mode === "reuse"
      ) {
        needTip = false;
      } else {
        actions.push(getDiscardTipAction(step.definition.tip.discardItem));
        needTip = true;
      }
    } // end travels
    // change tip for next source?
    if (step.definition.tip.mode === "reuse_same_source") {
      actions.push(getDiscardTipAction(step.definition.tip.discardItem));
      needTip = true;
    }
  } // end of loop sources
  if (step.definition.tip.mode === "reuse") {
    // drop the final tip
    actions.push(getDiscardTipAction(step.definition.tip.discardItem));
  }
  actions.push(getCommentAction("End step: " + step.name));

  actions.forEach((action) => {
    action.stepID = step._id
  })

  return actions;
}

// MIX step
/**
 * Builds a mix step.
 *
 * @param {Object} step - The step object.
 * @param {Object} currentWorkspace - The current workspace object.
 * @returns {Array} - An array of actions for the step.
 */
function buildMixStep(step, currentWorkspace) {
  const actions = [];

  const targets = getContentsInWorkspace(
    currentWorkspace,
    step.definition.target.item,
    step.definition.target.by,
    step.definition.target.value
  );
  if (!targets.length) {
    throw new Error(
      `On step ${step.order}: No matching targets. Please specify a valid target. `
    );
  }

  const tipVolume = getTipVolume(currentWorkspace, step.definition.tip.item);
  if (!tipVolume) {
    throw new Error(
      `On step ${step.order}: No max volume defined in tip rack. Please set a maxVolume value in the tip container.`
    );
  }

  let needTip = true;
  for (let i = 0; i < targets.length; i++) {
    const target = targets[i];
    if (needTip) {
      actions.push(
        getPickTipAction(step.definition.tip.item, step.definition.tool)
      );
    }
    actions.push(
      getMixAction(
        target.item,
        "name",
        target.content.name,
        step.definition.mix.percentage,
        step.definition.mix.type,
        step.definition.mix.count
      )
    );
    if (step.definition.tip.mode === "reuse") {
      needTip = false;
    } else {
      // asume isolated_targets
      needTip = true;
      actions.push(getDiscardTipAction(step.definition.tip.discardItem));
    }
  }
  if (step.definition.tip.mode === "reuse") {
    actions.push(getDiscardTipAction(step.definition.tip.discardItem));
  }
  actions.push(getCommentAction("End step: " + step.name));

  actions.forEach((action) => {
    action.stepID = step._id
  })

  return actions;
}

// WAIT step
/**
 * Builds a wait step.
 *
 * @param {Object} step - The step object.
 * @returns {Array} - An array of actions for the step.
 */
function buildWaitStep(step) {
   const seconds = parseInt(step.definition.seconds);
   return [{...getWaitAction(seconds), stepID: step._id}];
}

// HUMAN step
/**
 * Builds a human step.
 *
 * @param {Object} step - The step object.
 * @returns {Array} - An array of actions for the step.
 */
function buildHumanStep(step) {
      return [{...getHumanAction(step.definition.text), stepID: step._id}];
}

// COMMENT step
/**
 * Builds a comment step.
 *
 * @param {Object} step - The step object.
 * @returns {Array} - An array of actions for the step.
 */
function buildCommentStep(step) {
  return [{...getCommentAction(step.definition.text), stepID: step._id}];
}
// JSON step
/**
 * Builds a json step.
 *
 * @param {Object} step - The step object.
 * @returns {Array} - An array of actions for the step.
 */
function buildJsonStep(step) {
  // If the step is a list, it will map each element and return it as a separate action.
  if (Array.isArray(step.definition)) {
    let actions = [];
    step.definition.map((element) => actions.push({...getJsonAction(element), stepID: step._id}));
    return actions;
  } else if (typeof step === "object" && step !== null) {
    return [{...getJsonAction(step.definition), stepID: step._id}];
  }
}
// Mapping of step types to corresponding action-building functions.
const stepToAction = {
  TRANSFER: buildSimplePipettinStep,
  WAIT: buildWaitStep,
  HUMAN: buildHumanStep,
  COMMENT: buildCommentStep,
  MIX: buildMixStep,
  JSON: buildJsonStep,
};

module.exports = { stepToAction };
