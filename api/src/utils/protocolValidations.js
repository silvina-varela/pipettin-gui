// Protocol validations
const validateProtocolStep = (steps) => {

  // Checks if step has property "definition"
  steps.forEach((step) => {
    if (!(typeof step.definition === "object")) {
      throw new Error(
        `On step ${step.order}: Please define the step.`
      );
    }

  // Validates that each step has the correct properties
  switch (step.type) {
        case "TRANSFER":
        const transferKeys = ["source", "target", "volume", "tip", "tool"];
        const sourceAndTargetKeys = ["item", "by", "value"];
        const volumeKeys = ["type", "value"];
        const tipKeys = ["item", "discardItem", "mode"];

        transferKeys.forEach((key) => {
          // Checks that all properties exist
          if (!(key in step.definition)) {
            throw new Error(
              `On step ${step.order}: step is missing "${key}" definition`
            );
          }

          // Checks if source is defined.
          // TODO: check for "treatAs".
          if(key === "source") sourceAndTargetKeys.forEach((sourceKey) => {
            const props = {
              item: "platform",
              by: "name/tag",
              value: "name or tag",
            }
            if (!(sourceKey in step.definition.source)) {
              throw new Error(
                `Step ${step.order} is missing "${props[sourceKey]}" selection in "source"`
              );
            }
          });

          // Checks if target is defined.
          if(key === "target") sourceAndTargetKeys.forEach((targetKey) => {
            const props = {
              item: "platform",
              by: "name/tag",
              value: "name or tag",
            }
            if (!(targetKey in step.definition.target)) {
              throw new Error(
                `Step ${step.order} is missing "${props[targetKey]}" selection in "target"`
              );
            }
          });

          // Checks if volume is defined.
          if(key === "volume") volumeKeys.forEach((volumeKey) => {
            const props = {
              type: "type",
              value: "value",
            }
            if (!(volumeKey in step.definition.volume)) {
              throw new Error(
                `Step ${step.order} is missing "${props[volumeKey]}" selection in "volume"`
              );
            }
            if(volumeKey === "value" && step.definition.volume.value == 0){
              throw new Error(
                `On step ${step.order}: volume value can't be 0.`
              );
            }
            if(step.definition.volume.type === "for_each_target_tag"){
              if(!("tag" in step.definition.volume)) {
                throw new Error(
                  `Step ${step.order} is missing "tag" selection in "volume"`
                );
              }
            }
          });

          // Check the tip keys.
          if(key === "tip") tipKeys.forEach((tipKey) => {
            const props = {
              item: "tip rack",
              discardItem: "discard item",
              mode: "behavior",
            }
            if (!(tipKey in step.definition.tip) || !step.definition.tip[tipKey].length) {
              throw new Error(
                `Step ${step.order} is missing "${props[tipKey]}" selection`
              );
            }
          });

          // Check the tool key.
          if(key === "tool"  && !step.definition.tool.length){
            throw new Error(
              `Step ${step.order} is missing "${key}" selection.`
            );
          }
        });

        // Checks that source.item is different than target.item
        if(step.definition.source.item.length && step.definition.target.item.length && step.definition.source.item === step.definition.target.item && step.definition.source.value === step.definition.target.value){
            throw new Error(
              `On step ${step.order}: source and target can't be the same`
            );
        }
        break;
        case "WAIT":
        if (!step.definition.seconds || step.definition.seconds == 0) {
          throw new Error(
            `On step ${step.order}: Please define the step.`
          );
        }
        break;
        case "COMMENT":
        if (!step.definition.text) {
          throw new Error(
            `On step ${step.order}: Please define the step.`
          );
        }
        break;
        case "HUMAN":
          if (!step.definition.text) {
            throw new Error(
              `On step ${step.order}: Please define the step.`
            );
          }
        break;
        case "JSON":
          if (Object.keys(step.definition).length === 0) {
            throw new Error(
              `On step ${step.order}: Please define the step.`
            );
          }
          break;
        case "MIX":
        const mixKeys = ["target", "mix", "tip", "tool"];
        const targetKeys = ["item", "by", "value"];
        const mixinMixKeys = ["type", "percentage", "count"];
        const tipinMixKey = ["item", "discardItem", "mode"];

        mixKeys.forEach((key) => {
          // Checks that all properties exist
          if (!(key in step.definition)) {
            throw new Error(
              `On step ${step.order}: step is missing "${key}" definition`
            );
          }
          // Checks if target is defined
          if(key === "target") targetKeys.forEach((targetKey) => {
            const props = {
              item: "platform",
              by: "name/tag",
              value: "name or tag",
            }
            if (!(targetKey in step.definition.target)) {
              throw new Error(
                `Step ${step.order} is missing "${props[targetKey]}" selection in "target"`
              );
            }
          });

          // Checks if mix is defined
          if(key === "mix") mixinMixKeys.forEach((mixKey) => {
            if (!(mixKey in step.definition.mix)) {
              throw new Error(
                `Step ${step.order} is missing "${mixKey}" definition in "mix"`
              );
            }
            if(mixKey === "type" && !step.definition.mix.type.length) {
              throw new Error(
                `On step ${step.order}: mix type must be set.`
              );
            }
            if(mixKey === "percentage" && step.definition.mix.percentage == 0) {
              throw new Error(
                `On step ${step.order}: mix percentage can't be 0`
              );
            }
            if(mixKey === "count" && step.definition.mix.count == 0) {
              throw new Error(
                `On step ${step.order}: the number of times the content is mixed can't be 0`
              );
            }
          });

          // Checks if tip is defined
          if(key === "tip") tipinMixKey.forEach((tipKey) => {
            const props = {
              item: "tip rack",
              discardItem: "discard item",
              mode: "behavior",
            }
            if (!(tipKey in step.definition.tip) || !step.definition.tip[tipKey].length) {
              throw new Error(
                `Step ${step.order} is missing "${props[tipKey]}" selection`
              );
            }
          });

          // Check the tool key.
          if(key === "tool"  && !step.definition.tool.length){
            throw new Error(
              `Step ${step.order} is missing "${key}" selection.`
            );
          }
        });
        break;
      default:
        break;
    }
  });
};

module.exports = { validateProtocolStep };
