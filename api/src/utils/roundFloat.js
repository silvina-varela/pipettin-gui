function roundNumbersInObject(obj, precision = 2) {
    if (Array.isArray(obj)) {
        return obj.map(item => roundNumbersInObject(item, precision));
    } else if (typeof obj === 'object' && obj !== null) {
        return Object.fromEntries(
            Object.entries(obj).map(([key, value]) => [key, roundNumbersInObject(value, precision)])
        );
    } else if (typeof obj === 'number') {
        // Round to the specified precision
        return parseFloat(obj.toFixed(precision));
    }
    return obj; // Return non-numeric values unchanged
}

module.exports = { roundNumbersInObject };

