/**
 * Get a unique name
 * @param {*} collection array of objects with property 'name'
 * @param {*} fileName string
 * @param {*} index number
 * @returns 
 */
const getUniqueName = (collection, fileName, index = 0) => {
  let checkName = fileName,
  ext = "";
  if (index) {
    // if (checkName.indexOf(".") > -1) {
    //   let tokens = checkName.split(".");
    //   ext = "." + tokens.pop();
    //   checkName = tokens.join(".");
    // }

    // Pattern
    checkName = `${checkName} (${index})${ext}`;
  }

  const nameExists = collection.filter((f) => f.name === checkName).length > 0;

  // Return the original name if it does not exist,
  // or call this function recursively, adding a suffix
  // until it becomes unique.
  return nameExists
    ? getUniqueName(collection, fileName, index + 1)
    : checkName;
};

module.exports = { getUniqueName };
