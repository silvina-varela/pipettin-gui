function getMixAction(itemName, by, value, percentage, of, count) {
  return {
    cmd: "MIX",
    args: {
      item: itemName,
      selector: {
        by,
        value,
      },
      percentage: parseInt(percentage, 10),
      of,
      count: parseInt(count, 10),
    },
  };
}

function getPickTipAction(itemName, tool) {
  return {
    cmd: "PICK_TIP",
    args: {
      item: itemName,
      tool: tool,
    },
  };
}

function getWaitAction(seconds) {
  return {
    cmd: "WAIT",
    args: {
      seconds,
    },
  };
}

function getHumanAction(text) {
  return {
    cmd: "HUMAN",
    args: {
      text,
    },
  };
}

function getLoadLiquidAction(itemName, by, value, volume) {
  return {
    cmd: "LOAD_LIQUID",
    args: {
      item: itemName,
      selector: {
        by,
        value,
      },
      volume,
    },
  };
}

/**
 * Generates a "DROP_LIQUID" action object for a pipetting protocol, specifying the action to drop a liquid.
 *
 * @param {string} itemName - The name of the item associated with the liquid.
 * @param {string} by - The method used for selecting the item (e.g., "name" or "tag").
 * @param {string} value - The value used for selecting the item.
 * @param {number} volume - The volume of the liquid to be dropped.
 * @param {string} [reTag] - An optional parameter representing a reTag value to be applied to the dropped liquid.
 *                           If provided, an 'addTag' property is added to the action, with the value in reTag.
 * @returns {Object} - The "DROP_LIQUID" action object with specified parameters.
 * @throws {Error} - Throws an error if required parameters are missing or invalid.
 *
 * @example
 * const dropAction = getDropLiquidAction("Container_A", "name", "Liquid_A", 50, "NewTag");
 * // Returns: { cmd: "DROP_LIQUID", args: { item: "Container_A", selector: { by: "name", value: "Liquid_A" }, volume: 50, addTag: "NewTag" } }
 */
function getDropLiquidAction(itemName, by, value, volume, reTag) {
  const cmd = {
    cmd: "DROP_LIQUID",
    args: {
      item: itemName,
      selector: {
        by,
        value,
      },
      volume,
    },
  };
  if (reTag) {
    cmd.args.addTag = reTag;
  }
  return cmd;
}

function getDiscardTipAction(itemName) {
  return {
    cmd: "DISCARD_TIP",
    args: {
      item: itemName,
    },
  };
}

function getCommentAction(text) {
  return {
    cmd: "COMMENT",
    args: {
      text,
    },
  };
}

function getJsonAction(json) {
  return {
    cmd: json.cmd || "JSON",
    args: json,
  };
}

function getHomeAction() {
  return {
    cmd: "HOME",
  };
}

const actionBuilder = {
  getMixAction,
  getPickTipAction,
  getWaitAction,
  getHumanAction,
  getLoadLiquidAction,
  getDropLiquidAction,
  getDiscardTipAction,
  getCommentAction,
  getHomeAction,
  getJsonAction,
};

module.exports = { actionBuilder };
