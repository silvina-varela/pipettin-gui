/**
 * Config for the backend-robot socket communication
 * @param {*} ioServer
 * @param {*} frontendNamespace
 * @returns
 */
const controllerService = (ioServer, frontendNamespace) => {
  let robot_position;
  let connected = false;

  ioServer.on("connection", function (client) {
    try {
      console.log("Websocket connected:", client.id, client.rooms);
      console.log("Connected clients:", ioServer.engine.clientsCount);
      connected = true;

      frontendNamespace.emit("controller", `Controller connected (ID: ${client.id})`);
      frontendNamespace.emit("robotStatus", "UNK");

      client.on("human_intervention_required", function (response) {
        if (typeof response === "object" && response !== null) {
          if (response.hasOwnProperty("data")) {
            console.log("Response to human intervention:", response);
            frontendNamespace.emit(
              "human_intervention_required",
              response.data
            );
          } else {
            console.error("Response to 'human_intervention_required' does not have a 'data' property.");
          }
        } else {
          console.error("Response to 'human_intervention_required' is not an object or is null.");
        }
      });

      client.on("alert", function (response) {
        if (typeof response === "object" && response !== null) {
          console.log("Incoming 'alert' event with data: ", response);
          if (response.hasOwnProperty("text")) {
            frontendNamespace.emit("alert", response.text);
          } else {
            console.error("Data from 'alert' event does not have a 'text' property.");
          }
        } else {
          console.error("Data from 'alert' event is not an object or is null.");
        }
      });

      client.on("tool_data", function (response, ack) {
        if (typeof response === "object" && response !== null) {
          if (response.hasOwnProperty("data")) {
            // console.log("tool data received");
            robot_position = response.data;
          } else {
            console.error("Response to 'tool_data' does not have a 'data' property.");
          }
        } else {
          console.error("Response to 'tool_data' is not an object or is null.");
        }
        if (typeof ack === "function") ack("Position received!");
      });

      client.on("stop", function (response) {
        if (typeof response === "object" && response !== null) {
          console.log("Response to stop:", response);
          frontendNamespace.emit("stopped", "Stopped robot");
        } else {
          console.error("Response to 'stop' is not an object or is null.");
        }
      });

      client.on("controller_status", function (status, ack) {
        if (typeof status === "object" && status !== null) {
          if (status.hasOwnProperty("data")) {
            //console.log(status);
            frontendNamespace.emit("robotStatus", status.data);
          } else {
            console.error("Status does not have a 'data' property.");
          }
        } else {
          console.error("Status is not an object or is null");
        }
        if (typeof ack === "function") ack("receiving robot status");
      });

      client.on("disconnect", function () {
        const disconnectedStatus = "OFF";
        frontendNamespace.emit("robotStatus", disconnectedStatus);

        // Send an alert to the UI with a custom title.
        frontendNamespace.emit("alert", {
          title: "Controller disconnected",
          text: `Client with ID '${client.id}' disconnected from the backend.`
        });
        
        // Check if there are no clients connected after the current one disconnects.
        // TODO: Check if this always happens when the client count is "1" (not "0").
        //       Who is the other client?
        if (ioServer.engine.clientsCount === 1) {
          connected = false;
          console.log("No clients connected. Setting connected to false.");
        } else {
          console.log("Some clients remain connected:", ioServer.engine.clientsCount);
        }
      });

      client.on("execution", (response) => {
        if (typeof response === "object" && response !== null) {
          console.log("Response to execution:", response);
          frontendNamespace.emit("execution", response);
        } else {
          console.error("Response to 'execution' is not an object or is null");
        }
      });
    } catch (error) {
      console.error("An error occurred during socket communication:", error);
    }
  });

  return {
    getRobotPosition: () => robot_position,
    isConnected: () => connected,
  };
};

module.exports = controllerService;
