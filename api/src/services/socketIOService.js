const socketIO = require('socket.io');

const initializeSocketIO = (server) => {
  const ioServer = socketIO(server, {
    cors: {
      origin: '*',
      methods: ['GET', 'POST'],
      allowedHeaders: ['Content-Type'],
      credentials: true,
    },
  });

  ioServer.use((client, next) => {
    client.join('unauthenticated');
    client.join('global');
    return next();
  });

  return ioServer;
};

module.exports = initializeSocketIO;
