/**
 * Config for the frontend-backend socket communication
 * @param {*} ioServer 
 * @returns 
 */

const frontendService = (ioServer) => {
    // Namespace for frontend communication
    const frontendNamespace = ioServer.of("/frontend");

    frontendNamespace.on("connection", (socket) => {
        try {
            console.log("Front connected", socket.id);
            socket.emit("welcome", "Client connected to server via socket.io");

            /** Human intervention responses from frontend */
            socket.on("human_intervention_continue", (data) => {
                try {
                    console.log("Received 'continue' response for a human intervention event");
                    ioServer.emit("human_intervention_continue", {"text": data});
                } catch (error) {
                    console.error("Error in handling 'human_intervention_continue' event:", error);
                }
            });

            socket.on("human_intervention_cancelled", (data) => {
                try {
                    console.log("Received 'cancelled' response for a human intervention event");
                    ioServer.emit("human_intervention_cancelled", {"text": data});
                } catch (error) {
                    console.error("Error in handling 'human_intervention_cancelled' event:", error);
                }
            });
        } catch (error) {
            console.error("Error in connection event:", error);
        }
    });

    return frontendNamespace;
};

  
  module.exports = frontendService;
  