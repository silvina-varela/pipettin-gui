const fs = require("fs");
const os = require("os");

const setEnvValueService = async (key, value) => {
  const filePath = "../.env"
  // read file from hdd & split if from a linebreak to a array
  try{
    const exists = fs.existsSync(filePath)

    if(!exists){
      fs.writeFileSync(filePath, '', 'utf8');
      console.log('Empty file created successfully');
    }

    const ENV_VARS = fs.readFileSync(filePath, "utf8").split(os.EOL);
  
    // find the env we want based on the key
    const target = ENV_VARS.indexOf(
      ENV_VARS.find((line) => {
        return line.match(new RegExp(key));
      })
    );
  
    if (target === -1) {
      ENV_VARS.push(`${key}=${value}`);
    } else {
      // replace the key/value with the new value
      ENV_VARS.splice(target, 1, `${key}=${value}`);
    }
  
    // write everything back to the file system
    fs.writeFileSync(filePath, ENV_VARS.join(os.EOL));

  } catch(error){
    console.log(error)
  }
};

module.exports = setEnvValueService;
