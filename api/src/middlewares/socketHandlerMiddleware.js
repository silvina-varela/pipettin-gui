const controllerService = require("../services/controllerSocketService");
const frontendService = require("../services/frontendSocketService");

const socketHandlerMiddleware = (ioServer) => {
  const socketForFrontend = frontendService(ioServer);
  const socketForController = controllerService(ioServer, socketForFrontend);

  return (req, res, next) => {
    req.ioServer = ioServer;
    req.robot_position = socketForController.getRobotPosition();
    req.connected = socketForController.isConnected();
    next();
  };
};

module.exports = socketHandlerMiddleware;
