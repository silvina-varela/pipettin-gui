# Pipettin Writer

This is the web app for creating protocols and operating the [Pippetin bot](https://gitlab.com/pipettin-bot/pipettin-bot).
It provides an easy setup process of workspaces and protocols, and it can be used to send commands directly to the robot. Running the project is simple and requires minimal to no coding knowledge.  

![01_workspacePage.png](./assets/01_workspacePage.png)

## Features

For a detailed overview of the app's main features, please refer to [this document](./doc/Features.md).

## Project Status

This is an open-source project that is currently under development. Although it is not yet available for contributions, it will be in the near future. Once we are ready to accept contributions, we will update this README with detailed information on how to contribute.

## Documentation

Comprehensive documentation for the entire project is available on our [Wiki](https://docs.openlabautomata.xyz/).

Additional technical documentation can be found in the [doc](./doc) directory.

### Getting started

Pipettin Writer is compatible with all major operating systems.

Follow the [installation guide](https://docs.openlabautomata.xyz/Software-Setup) to get pipettin'.

## Development team

- Silvina Varela: FullStack Developer, Project Manager, and Team Leader.  
    - Backend development: Responsible for the implementation of server-side functionalities, API design, and database setup. Integrated the socket.io library for seamless interact with the robot controller.
    - Frontend development: Responsible for setting up the project structure, and managing the overall frontend architecture. Worked on many of the UI features, with a focus on leveraging the d3 library for a visual representation of the workspace and platform manipulation. Additionally, implemented dynamic forms for building protocol templates.
    - Prototype design: Utilized Figma to design the project's prototype.
    - Project Management and Team Leadership: Assumed the roles of Project Manager and Team Leader, overseeing the project's progress, coordinating tasks and deadlines, and providing support to the development team.
- Zeynep Tarkan: FullStack Developer.
    - Frontend development: Key contributor to various UI components, including the side panels and overall workspace page structure. Focused on enhancing platform customization, introducing custom platforms and containers.
    - Backend development: Worked on integrating new schemas and adapting existing ones to facilitate the implementation of customizable platforms and containers.
- Lucas Fernández: Frontend Developer.
    - Frontend development: Focused on building reusable components and creating the structural foundation for the Dashboard and Settings pages.
- Nicolás Méndez: Project owner.
    - Executed Python scripts to test the interaction of the robot with the app through socket.io.
- Facundo Méndez: FullStack developer.
    - Legacy Pipettin GUI Design: Designer of the first Pipettin GUI ([legacy branch](https://gitlab.com/pipettin-bot/pipettin-bot/-/tree/master-backup-v1-archive/gui)).
    - Advisory during the migration to ReactJS.

## Acknowledgements

The development team that worked on this project was recruited by Fiqus Coop. We are grateful for their commitment to fostering growth and collaboration by offering opportunities for learning and professional development.

Special thanks to Nicolás Dimarco for assembling the team and providing ongoing guidance throughout the entire development process, as well as for ensuring it progress and managing the business aspects.

We would also like to express our gratitude to Lucas Ferraro and Martín Vallone for their valuable tech support and guidance during the initial phase of development. Their expertise and willingness to assist significantly contributed to overcoming technical challenges and achieving the project's goals.

We appreciate the support and contributions of the entire Fiqus team, whose commitment to nurturing talent and providing opportunities for growth has been vital in bridging the gap between academia and industry.

Open source projects have made this work possible. This project stands on the shoulders of the open source community giant.

We thank the Gathering for Open Science Hardware (<http://openhardware.science>) and the Alfred P. Sloan Foundation (<https://sloan.org>) for their support.

![logo-gosh.png](./assets/logo-gosh.png)

We thank the [reGOSH](https://regosh.libres.cc/en/home-en/) free tech, latin-american network, for planting the first seeds and making the project possible.

![logo-regosh.png](./assets/logo-regosh.png)

## License

This project is distributed under the terms of the GNU Affero General Public License v3.0 only (AGPL-3.0-only). See [LICENSE.txt](./LICENSE.txt).
